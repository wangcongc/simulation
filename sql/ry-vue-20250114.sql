/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2025-01-14 20:05:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for plat_class
-- ----------------------------
DROP TABLE IF EXISTS `plat_class`;
CREATE TABLE `plat_class` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT NULL COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) NOT NULL COMMENT '所属学院id',
  `major_id` bigint(32) NOT NULL COMMENT '所属专业id',
  `teacher` text COMMENT '授课教师',
  `serial_number` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_class
-- ----------------------------
INSERT INTO `plat_class` VALUES ('14', '三班', null, null, '2024-07-09 21:25:53', '2024-09-13 22:32:12', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('20', '二班', null, null, '2024-07-11 20:22:01', '2024-09-13 22:32:08', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('25', '一班', null, null, '2024-07-11 20:42:34', '2024-09-13 22:32:03', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('26', '五班', null, null, '2024-12-10 21:40:46', null, '21', '2', '@109@106@', null);

-- ----------------------------
-- Table structure for plat_college
-- ----------------------------
DROP TABLE IF EXISTS `plat_college`;
CREATE TABLE `plat_college` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '学院id',
  `name` varchar(20) NOT NULL COMMENT '学院名称',
  `manager` varchar(20) NOT NULL COMMENT '负责人',
  `manager_mobile` varchar(20) NOT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_college
-- ----------------------------
INSERT INTO `plat_college` VALUES ('21', '计算机学院', '李老师', '18888888888', '2024-07-05 20:34:49', '2024-12-13 12:41:33');
INSERT INTO `plat_college` VALUES ('22', '教育学院', '李老师', '18888888888', '2024-12-13 12:41:04', '2024-12-13 12:41:18');
INSERT INTO `plat_college` VALUES ('23', '工业学院', '', '', '2024-12-13 12:42:00', null);

-- ----------------------------
-- Table structure for plat_interfere_health
-- ----------------------------
DROP TABLE IF EXISTS `plat_interfere_health`;
CREATE TABLE `plat_interfere_health` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `assistant_text` varchar(200) DEFAULT NULL,
  `assistant_audio_url` varchar(200) DEFAULT NULL,
  `patient_information_info` varchar(2000) DEFAULT NULL,
  `assistant_health_text` varchar(200) DEFAULT NULL,
  `assistant_health_audio_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_interfere_health
-- ----------------------------
INSERT INTO `plat_interfere_health` VALUES ('288', '李白是一名高血压患者，针对于李白这个对象，我们需要对其进行高血压健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下李白的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/2be121ee-5911-498e-9092-b4739d67xx.mp3', '李白，男，58岁，上海本地居民，IT领域高级工程师，企业高管，身高175cm，体重88kg，腰围88cm。高血压病1年，未来服药时收缩压142mmHg，舒张压95mmHg。李某按时服用降压药，平时在外应酬频繁，未在社区接受定期慢性病随访，缺少运动，常头痛，头晕，经常焦虑，睡眠质量不佳。最近的一次体检指标显示，收缩压130mmHg，舒张压83mmHg，空腹血糖控制在6.0 mmol/L, 餐后2小时血糖7.7 mmol/L；总胆固醇（TC）7.2mmol/L, 甘油三酯（TG）1.6mmol/L,高密度脂蛋白胆固醇（HDL-C）0.9mmol/L ，低密度脂蛋白胆固醇LDL5.0mmol/L，Hb1Ac5.6%，母亲有冠心病，烟龄20年，目前每天1包烟，经常饮酒，每日52度白酒至少200ml，饮食荤食为主，口味偏咸，喜欢吃腌制食品。心房纤颤，左心室肥厚。', '你作为健康管理师，针对于李白的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/2be121ee-5911-498e-9092-b4739d679300_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('289', '刘英是一名冠心病患者，针对于刘英这个对象，我们需要对其进行冠心病健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下刘英的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/e62e0e91-7819-4402-84e0-77264d42ded7_FlowPanel_node_3.mp3', '刘英，女，67岁，杭州本地居民，某邮政储蓄银行退休职工，身高165cm，体重60kg，腰围76cm。37岁被诊断为冠心病，日常服用丹参片，收缩压120mmHg，舒张压75mmHg。刘某爱吃咸菜，水果蔬菜摄入较少，不吸烟，不饮酒，平时早晨起早走步1h。最近的一次体检指标显示，空腹血糖控制在5.8 mmol/L, 餐后2小时血糖7.3 mmol/L, 总胆固醇（TC）6.2mmol/L, 甘油三酯2.2 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.8mmol/L ，低密度脂蛋白胆固醇LDL5.4mmol/L，经常会出现焦虑，睡眠质量不好，晚上多次醒来，难以入睡，有心房纤颤，无左心室肥厚。', '你作为健康管理师，针对于刘英的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/e62e0e91-7819-4402-84e0-77264d42ded7_FlowPanel_node_9_F918F85622161828.mp3');
INSERT INTO `plat_interfere_health` VALUES ('290', '陈峰是一名脑卒中患者，针对于陈峰这个对象，我们需要对其进行脑卒中健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下陈峰的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/807858bb-ffe0-4668-8376-a18b4d93474e_FlowPanel_node_3.mp3', '陈某，男，75岁，广州本地居民，某卫健委退休职工，身高173cm，体重65kg，腰围86cm。半年前曾出现过小中风，日常血压测量，收缩压145mmHg，舒张压95mmHg。陈某爱吃甜食，水果蔬菜摄入较少，吸烟年限20年，平均每天5支，每周饮用白酒2次，每次约20ml，平时基本不运动。最近的一次体检指标显示，空腹血糖6.8 mmol/L, 餐后2小时血糖8.7mmol/L, 总胆固醇（TC）5.5mmol/L, 甘油三酯2.4 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.7mmol/L ，低密度脂蛋白胆固醇LDL4.4mmol/L，睡眠质量不好，无心房纤颤，左心室肥厚。', '你作为健康管理师，针对于陈峰的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/807858bb-ffe0-4668-8376-a18b4d93474e_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('291', '张玲是一名糖尿病患者，针对于张玲这个对象，我们需要对其进行糖尿病健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下张玲的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/97fb8fc0-36ea-4e93-b6d2-2afd867b6891_FlowPanel_node_3.mp3', '张玲，女，某国企退休人员，60岁，杭州本地居民，身高160cm，体重72kg，腰围90cm，确诊糖尿病近2年，按时服用降糖药物，本次因为腿部静息痛来健康管理中心接受检查，静脉抽血测量李女士空腹血糖7.5mmol/L，收缩压132mmHg，舒张压85mmHg，总胆固醇（TC）5.2mmol/L, 甘油三酯（TG）2.2mmol/L, 高密度脂蛋白胆固醇（HDL-C）1.5mmol/L，低密度脂蛋白胆固醇LDL2.9mmol/L，血尿酸400 umol/L，HbA1c=7.1%；无心房纤颤，无左心室肥厚。父亲患有糖尿病，不吸烟，不饮酒，平时主食基本不吃，以素食为主，嗜糖，因为家中琐事繁多，心理压力很大，乏力，常无精打采，对很多事情都没有兴趣，睡眠不踏实，每天健步走4000步，请根据李女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于张玲的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/97fb8fc0-36ea-4e93-b6d2-2afd867b6891_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('292', '张军是一名肥胖患者，针对于张军这个对象，我们需要对其进行肥胖健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下张军的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/cf0f0cd9-61b1-468a-98c4-7cf9c28d55ee_FlowPanel_node_3.mp3', '张军，男性，30岁，武汉市本地人，现为某IT公司的程序员，身高175cm，因为疫情，最近1年体重增加到了100kg，腰围100cm。平时缺少运动，经常熬夜加班，喜食肥肉，油炸食品，应酬多等，嗜酒如命，喜食甜食。每周吸烟1-2次，每次1支。无慢性病家族史，性格外向，擅言谈，睡眠质量不太好，最近因为有些头晕，头痛不适，到医院进行体检主要指标如下：空腹血糖8.5mmol/L，收缩压139mmHg，舒张压89mmHg，总胆固醇（TC）8.2mmol/L, 甘油三酯（TG）1.3mmol/L,高密度脂蛋白胆固醇（HDL-C）0.7mmol/L，低密度脂蛋白胆固醇（LDL-C）3.8mmol/L，血尿酸450 umol/L，HbA1c=6.4%；无心房纤颤，左心室肥厚。请根据张先生的情况为其提供健康管理服务。  ', '你作为健康管理师，针对于张军的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/aa.mp3');
INSERT INTO `plat_interfere_health` VALUES ('293', '赵云是一名COPD患者，针对于赵云这个对象，我们需要对其进行COPD健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下赵云的个人健康信息情况。。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1b86d4ed-3902-4974-b511-10a9341671e1_FlowPanel_node_3.mp3', '赵云，北京户籍，北京市某外企行政办公室主任，男性，55岁，身高175cm，体重55kg，腰围82cm，20岁开始吸烟，每天平均吸烟10支，不喝酒，性格开朗，乐观积极，近半年白天和夜间不感冒时经常出现咳嗽、胸闷，走路时气短等症状，睡眠质量不佳，父亲有哮喘和慢性支气管炎，怀疑自己肺部有健康问题。请根据赵先生的情况为其提供健康管理服务。', '你作为健康管理师，针对于张军的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/cf0f0cd9-61b1-468a-98c4-7cf9c28d55ee_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('294', '吴霞是一名骨质疏松患者，针对于吴霞这个对象，我们需要对其进行骨质疏松健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下吴霞的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1bce064b-1f2c-490b-b8fc-429471b2f7f1_FlowPanel_node_3.mp3', '吴霞，女性，杭州人，某高校党政办公室职员，年龄49岁，身高170cm，体重50kg，腰围73，爱喝咖啡，爱喝酒，不吸烟，不爱运动，喜欢宅在办公室或家里，性格外向，睡眠质量良好，有母亲髋关节脆性骨折家族史，有皮肤病，常服用糖皮质激素类药物。最近一次下楼不小心摔倒，导致小腿骨折，她怀疑自己为骨质疏松，请根据赵女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于吴霞的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1bce064b-1f2c-490b-b8fc-429471b2f7f1_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('295', '秦芳是一名盆底肌功能患者，针对于秦芳这个对象，我们需要对其进行盆底肌功能健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下秦芳的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/c0de8716-0789-41fc-8a64-70f2466484ac_FlowPanel_node_3.mp3', '秦芳，女性，北京人，家庭主妇，年龄50岁，身高160cm，体重65kg，腰围92cm，有3个孩子，其中两个孩子出生时体重超过4kg，45岁时开始抽烟，每天约1支烟，烟龄5年，不饮酒，目前有慢性支气管炎，慢性盆腔炎，平时体力活动少，性格开朗，睡眠良好，当她开怀大笑、打喷嚏等时会出现尿失禁，请根据秦女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于秦芳的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/c0de8716-0789-41fc-8a64-70f2466484ac_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('401', '上面是案例人何某的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/b2c526ab-2aac-4594-9272-68df40d075ca_FlowPanel_node_2_BFFB38DB1A203F5C.mp3', '何某，28岁，身高180，体重100kg，上班族，朝九晚五，一坐就是一天，爱喝奶茶，平时很少运动，晚上常常凌晨1-2点入睡，睡前爱吃宵夜。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('402', '上面是案例人张叔叔的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/019780fd-dbe9-47f5-9746-a62b8860f12c_FlowPanel_node_2.mp3', '56岁张叔叔，身高173cm，体重70kg，患有高血压，踝关节曾经扭伤过，现在基本可以正常行走但是跳跃等运动时会伴有疼痛感，在体检时进行了徒手的反应性测试，测试的结果是33cm。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('403', '上面是案例人张爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/d8de4211-f615-4b97-b1f8-cb4da647bbd9_FlowPanel_node_2_2162DAE75B6D6ACB.mp3', '张爷爷是一名68岁的退休军人，体重离开军队之后一直在从事军训教官直到退休，退休之后因为摔倒过一次，导致腰痛，他在一次体检的时候进行了肺功能的测试，相关结果如下（包含测试和访谈的结果）：憋气实验测得时间是20s；平常活动不会出现气短的情况；每周会有几天咳嗽，在运动或进行重体力劳动时会出现气促，爬山或在平地快速行走时，不得不减慢速度或停下来，且经常容易筋疲力尽；个人比较爱抽烟，饮酒。\r\n\r\n       张爷爷进行了COPD的问卷调查，他说自己大部分时间很正常，但是想去跑步就不行了，通常每周有几天咳嗽，一旦运动或提重物行走时就会气促，爬山或在平地快速行走时，不得不减慢速度或停下来，且经常容易筋疲力尽。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('404', '上面是案例人小何的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/95188c8c-57a6-42db-858e-311c56d0f2bd_FlowPanel_node_2_269F6229B297004F.mp3', '23岁的小何是一名大学生，日常运动主要以晚间在操场慢跑10min为主，最近学习进行了体质监测考试，他其中一项俯卧撑完成了27个，测试结束之后小何问了一个问题：如果身体未降至肩与肘处于同一水平面是否会得分？', null, null);
INSERT INTO `plat_interfere_health` VALUES ('405', '上面是案例人小张的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/97974728-57e6-4c82-ad59-40dc049ead56_FlowPanel_node_2_6A8EC6D5F266286C.mp3', '28岁的小张是一名律师，身高180cm,体重75kg,日常主要的运动是晚餐过后的步行51min,无其他运动病史，在体质检测中分别进行了背肌耐力检测，握力计测试，纵跳测试。其中测试的结果如下：背肌耐力检测是130kg；握力计测试36kg；纵跳30cm。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('406', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/e9ccebd9-db0c-4104-b8d1-395a20e20d4f_FlowPanel_node_2.mp3', ' 60岁的何爷爷，身高167cm, 体重60kg, 在一次体检上进行了睁眼双足前后脚 一字型站立，他的结果是28s', null, null);
INSERT INTO `plat_interfere_health` VALUES ('407', null, null, '张叔叔，50岁，体重65kg,身高175cm,血压血糖的指标属于正常标准的范围内，在某体检机构进行了相关的柔韧性检测。平常有运动习惯，平常下班之后会选择约30分钟的慢跑，运动结束会进行2分钟的放松练习。\r\n\r\n相关的结果如下：\r\n\r\n       徒手的坐位体前屈结果为：8cm,；背后交接的检测，发现双手距离15cm ,伴有受限的情况，主诉曾经受伤过；转身测试结果是65°；屈伸功能测试，伸展测试顺利完成动作，没有卡顿的情况，主诉也没有相关的疼痛。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('408', '请按照文字指导和图片示例进行自己或他人的协调能力测试，并记录自己的测试结果，年龄需要在45岁以上，系统才能提供评分结果。', 'content/assets/audio/plat-questionnaire/risk_obstructive/32f67d93-2b27-4559-b1b6-b87833cec4dc_FlowPanel_node_3_E067A487DDD2FA77.mp3', null, null, null);
INSERT INTO `plat_interfere_health` VALUES ('409', '上面是案例人游叔叔的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/106ddd04-9931-4bda-b0e1-885a79b8b209_FlowPanel_node_2_4B40FFC2D4AB8DC1.mp3', '游某某，男，39岁，身高173cm，体重64kg。按照“国民体质测定标准”进行测试，结果为： 台阶(30cm)试验：蹬台阶持续时间200s，运动后3次心率分别为51次/30s，39次/30s、37次/30s。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('410', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('411', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('412', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('413', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('414', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('415', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('416', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);

-- ----------------------------
-- Table structure for plat_lesson
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson`;
CREATE TABLE `plat_lesson` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) DEFAULT NULL COMMENT '所属学院id',
  `majors` varchar(2000) DEFAULT '' COMMENT '适用专业',
  `teachers` varchar(2000) DEFAULT NULL COMMENT '授课教师',
  `picture` longtext COMMENT '缩略图',
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL COMMENT '描述',
  `type_id` bigint(32) DEFAULT NULL COMMENT '类型',
  `college_name` varchar(2000) DEFAULT NULL COMMENT '所属学院名称',
  `serial_number` int(4) DEFAULT NULL COMMENT '排序',
  `student_title` varchar(200) DEFAULT NULL COMMENT '学习页面标题',
  `student_img_url` varchar(200) DEFAULT NULL COMMENT '学习页面文件路径',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson
-- ----------------------------
INSERT INTO `plat_lesson` VALUES ('1', '中医九种体质评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('2', '设计健康体检套餐--糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:22', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('3', '设计健康体检套餐--脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:29', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('4', '健康体检报告解读--乳腺癌和甲状腺', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:33', '21', '@2@4@', '@106@', null, null, '', '1', '', '4', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('5', '健康体检报告解读--脑卒中和外周血管疾病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:40', '21', '@2@4@', '@106@', null, null, '', '1', '', '5', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('6', '社区健康评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '6', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('7', '企业人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '7', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('8', '社区人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '8', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('9', '养老院人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '9', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('10', '健康促进生活方式评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '201', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('36', '慢性病饮食干预-高血压', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('37', '营养配餐课程', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('38', '慢性病饮食干预-冠心病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('39', '饮食评价报告模块', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('40', '慢性病饮食干预-脑卒中', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('41', '慢性病饮食干预-糖尿病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('42', '慢性病饮食干预-肥胖症', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('43', '慢性病饮食干预-慢性阻塞性肺病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('44', '慢性病饮食干预-骨质疏松', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('45', '慢性病饮食干预-盆底功能障碍', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('46', '正常人群食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('47', '高血压患者食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('48', '糖尿病患者食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('288', '慢性病健康干预-高血压', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '288', '高血压的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/gaoxueya.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('289', '慢性病健康干预-冠心病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '289', '冠状动脉粥样硬化性心脏病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/guanxibing.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('290', '慢性病健康干预-脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '290', '脑卒中的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/naocuzhong.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('291', '慢性病健康干预-糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '291', '\r\n2型糖尿病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/bf514d96-0d8d-49f5-bd4a-7ec21a684485.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('292', '慢性病健康干预-肥胖症', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '292', '肥胖症的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/b5a5dd91-b9b1-42a9-9d1c-676d0477bca9.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('293', '慢性病健康干预-慢性阻塞性肺病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', '慢性阻塞性肺疾病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/5798c351-9526-49a9-a44e-d8c8d8405d7f.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('294', '慢性病健康干预-骨质疏松', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', null, null, 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('295', '慢性病健康干预-盆底功能障碍', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', '\r\n盆底功能障碍的评估与干预', 'content/assets/img/plat-questionnaire/risk_dysfunction/a4c603af-32ec-4f52-8240-596ce4c475f5.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('296', '心血管病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('297', '脑卒中风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('298', '糖尿病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('299', '慢性阻塞性肺疾病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '299', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('305', '睡眠质量评估', '王老师', '无', '2024-12-15 16:25:18', null, null, '@2@4@', '@109@106@3@', '', null, '', '2', '', '203', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('401', 'BMI训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('402', '反应力能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('403', '肺功能能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('404', '核心力能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('405', '局部力量训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('406', '平衡能力评估', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('407', '柔软度能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('408', '协调能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('409', '心功能能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('410', '本体感觉训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('411', '床上操', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('412', '养生操', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('413', '家务运动训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('414', '徒手保健养生操（75岁以下）', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('415', '徒手保健养生操（75岁以上）', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('416', '懒人训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');

-- ----------------------------
-- Table structure for plat_lesson_chapter_type
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_chapter_type`;
CREATE TABLE `plat_lesson_chapter_type` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '章节类型id',
  `name` varchar(200) NOT NULL COMMENT '章节类型名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_chapter_type
-- ----------------------------
INSERT INTO `plat_lesson_chapter_type` VALUES ('1', '122222222222', null, '2024-07-17 14:43:26', '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('33', '333', '2024-07-17 14:43:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('34', '11', '2024-07-17 15:09:30', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('35', '66666', '2024-07-17 15:17:16', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('36', 'ff', '2024-07-17 15:18:16', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('37', '1', '2024-07-17 15:20:50', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('94', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('95', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('96', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('97', '2', '2024-07-17 15:24:46', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('98', '1', '2024-07-17 15:24:50', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('102', '3', '2024-07-17 15:35:41', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('103', '5', '2024-07-17 15:37:31', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('105', 'www', '2024-07-17 15:38:14', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('107', '2222t', '2024-07-17 15:40:54', '2024-07-17 15:41:04', '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('108', '1', '2024-07-17 16:31:21', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('110', '1', '2024-07-17 16:34:34', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('111', '666', '2024-07-17 16:34:46', null, '32');

-- ----------------------------
-- Table structure for plat_lesson_copy
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_copy`;
CREATE TABLE `plat_lesson_copy` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) DEFAULT NULL COMMENT '所属学院id',
  `majors` varchar(2000) DEFAULT '' COMMENT '适用专业',
  `teachers` varchar(2000) DEFAULT NULL COMMENT '授课教师',
  `picture` longtext COMMENT '缩略图',
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL COMMENT '描述',
  `type_id` bigint(32) DEFAULT NULL COMMENT '类型',
  `college_name` varchar(2000) DEFAULT NULL COMMENT '所属学院名称',
  `serial_number` int(4) DEFAULT NULL COMMENT '排序',
  `student_title` varchar(200) DEFAULT NULL COMMENT '学习页面标题',
  `student_img_url` varchar(200) DEFAULT NULL COMMENT '学习页面文件路径',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_copy
-- ----------------------------
INSERT INTO `plat_lesson_copy` VALUES ('1', '中医九种体质评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '202', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('2', '设计健康体检套餐--糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:22', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('3', '设计健康体检套餐--脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:29', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('4', '健康体检报告解读--乳腺癌和甲状腺', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:33', '21', '@2@4@', '@106@', null, null, '', '1', '', '4', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('5', '健康体检报告解读--脑卒中和外周血管疾病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:40', '21', '@2@4@', '@106@', null, null, '', '1', '', '5', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('6', '社区健康评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '6', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('7', '企业人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '7', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('8', '社区人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '8', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('9', '养老院人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '9', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('10', '健康促进生活方式评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '201', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('288', '慢性病健康干预-高血压', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '288', '高血压的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/gaoxueya.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('289', '慢性病健康干预-冠心病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '289', '冠状动脉粥样硬化性心脏病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/guanxibing.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('290', '慢性病健康干预-脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '290', '脑卒中的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/naocuzhong.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('291', '慢性病健康干预-糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '291', '\r\n2型糖尿病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/bf514d96-0d8d-49f5-bd4a-7ec21a684485.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('292', '慢性病健康干预-肥胖症', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '292', '肥胖症的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/b5a5dd91-b9b1-42a9-9d1c-676d0477bca9.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('293', '慢性病健康干预-慢性阻塞性肺病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', '慢性阻塞性肺疾病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/5798c351-9526-49a9-a44e-d8c8d8405d7f.png', null, null);
INSERT INTO `plat_lesson_copy` VALUES ('294', '慢性病健康干预-骨质疏松', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('295', '慢性病健康干预-盆底功能障碍', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', '\r\n盆底功能障碍的评估与干预', 'content/assets/img/plat-questionnaire/risk_dysfunction/a4c603af-32ec-4f52-8240-596ce4c475f5.png', null, null);
INSERT INTO `plat_lesson_copy` VALUES ('296', '心血管病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('297', '脑卒中风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('298', '糖尿病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('299', '慢性阻塞性肺疾病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '299', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('305', '睡眠质量评估', '王老师', '无', '2024-12-15 16:25:18', null, null, '@2@4@', '@109@106@3@', '', null, '', '2', '', '203', null, null, null, null);

-- ----------------------------
-- Table structure for plat_lesson_scale
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale`;
CREATE TABLE `plat_lesson_scale` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表id主键',
  `name` varchar(200) DEFAULT NULL COMMENT '量表名称',
  `lesson_son_id` bigint(32) DEFAULT NULL COMMENT '所属子课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale
-- ----------------------------
INSERT INTO `plat_lesson_scale` VALUES ('1', '十种慢病风险评估', '1', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '1');
INSERT INTO `plat_lesson_scale` VALUES ('2', '其它健康风险评估', '1', '2024-08-28 12:59:46', '2024-08-28 12:59:53', '2');

-- ----------------------------
-- Table structure for plat_lesson_scale_question
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_question`;
CREATE TABLE `plat_lesson_scale_question` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表主键id',
  `parent_id` bigint(32) DEFAULT NULL COMMENT '合并单元格问题，上级id',
  `name` varchar(200) DEFAULT NULL COMMENT '问题名称',
  `lesson_scale_son_id` bigint(32) DEFAULT NULL COMMENT '所属量表id',
  `lesson_scale_id` bigint(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `type` int(5) DEFAULT NULL COMMENT '类型（1打对勾，2填空，3标题）',
  `score` int(255) DEFAULT NULL COMMENT '分数',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_question
-- ----------------------------
INSERT INTO `plat_lesson_scale_question` VALUES ('1', null, '年龄(≥50岁)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '1', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('2', null, '使用电脑、驾驶员工作', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '2', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('3', null, '睡眠时间短(<6小时/天)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '3', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('4', null, '不合理用枕头(枕头与自己拳头比较高或低)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '4', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('5', null, '头颈部外伤史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '5', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('6', null, '缺少体育锻炼(偶尔或1-2次/周)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '6', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('7', null, '直系或旁系亲属颈椎疾病史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '7', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('8', null, '吸烟史', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '8', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('9', null, '超重/肥胖 BMI>24', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '9', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('10', null, '缺少体力锻炼(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '10', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('11', null, '血压升高 收缩压≥140或舒张压≥90', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '11', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('12', null, '脂肪摄入多(很油腻、较油腻)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '12', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('13', null, '肉类摄入多(喜欢、较喜欢)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '13', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('14', null, '蔬菜摄入少(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '14', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('15', null, '水果摄入少>(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '15', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('16', null, '父亲或母亲痛风家族史', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '16', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('17', null, '超重/肥胖(BMI>24)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '17', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('18', null, '饮酒史(3-4次/周;5次/周)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '18', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('19', null, '高嘌呤饮食(喜欢;较喜欢)例如:内脏、海鲜、老火汤等食物', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '19', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('20', null, '高血压(收缩压≥140或舒张压>90)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '20', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('21', null, '脂代谢紊乱(胆固醇>5.2或甘油三酯>2.3)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '21', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('22', null, '糖代谢紊乱(空腹血糖>6.0或餐后血糖>7.8)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '22', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('23', null, '您的性别:', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '23', '2', '2', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('24', null, '您的体重(KG):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '24', '2', '2', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('25', null, '您的身高(M):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '25', '2', '2', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('26', null, '您的腰围(CM):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '26', '2', '2', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('27', null, '肥胖症史', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '27', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('28', '27', '是', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '28', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('29', null, '上周运动次数', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '29', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('30', '29', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '30', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('31', '29', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '31', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('32', null, '肉类', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '32', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('33', '32', '喜欢\n', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '33', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('34', '32', '较喜欢', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '34', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('35', null, '口味', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '35', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('36', '35', '油腻', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '36', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('37', '35', '\n较油腻', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '37', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('38', null, '上周膳食纤维摄入量（水果）', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '38', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('39', null, '上周膳食纤维摄入量（蔬菜）', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '39', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('41', '38', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '41', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('42', '38', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '42', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('43', '39', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '43', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('44', '39', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '44', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('45', null, '男性', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '45', '3', '3', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('46', null, '年龄(岁)', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '46', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('47', '46', '54-56', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '47', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('48', '46', '57-59', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '48', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('49', '46', '60-62', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '49', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('50', '46', '63-65', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '50', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('51', '46', '66-68', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '51', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('52', '46', '69-72', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '52', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('53', '46', '73-75', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '53', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('54', '46', '76-78', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '54', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('55', '46', '79-81', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '55', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('56', '46', '82-84', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '56', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('57', '46', '85', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '57', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('58', null, '收缩压(未服药)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '58', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('62', '58', '97-105', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '62', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('63', '58', '106-115', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '63', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('64', '58', '116-125', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '64', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('65', '58', '126-135', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '65', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('66', '58', '136-145', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '66', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('67', '58', '146-155', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '67', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('68', '58', '156-165', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '68', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('69', '58', '166-175', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '69', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('70', '58', '176-185', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '70', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('71', '58', '186-195', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '71', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('72', '58', '196-205', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '72', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('73', null, '收缩压(服药后)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '73', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('74', '73', '97-105', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '74', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('75', '73', '106-112', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '75', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('76', '73', '113-117', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '76', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('77', '73', '118-123', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '77', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('78', '73', '124-129', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '78', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('79', '73', '130-135', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '79', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('80', '73', '136-142', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '80', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('81', '73', '143-150', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '81', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('82', '73', '151-161', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '82', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('83', '73', '162-176', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '83', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('84', '73', '177-205', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '84', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('85', null, '糖尿病家族史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '85', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('86', null, '吸烟史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '86', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('87', null, '心血管疾病史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '87', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('88', null, '房颤', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '88', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('89', null, '左心室肥厚', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '89', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('90', null, '女性', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '90', '3', '3', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('91', null, '年龄(岁)', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '91', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('92', '91', '54-56', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '92', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('93', '91', '57-59', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '93', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('94', '91', '60-62', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '94', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('95', '91', '63-64', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '95', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('96', '91', '65-67', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '96', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('97', '91', '68-70', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '97', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('98', '91', '71-73', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '98', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('99', '91', '74-76', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '99', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('100', '91', '77-78', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '100', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('101', '91', '79-81', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '101', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('102', '91', '82-84', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '102', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('103', null, '收缩压(未服药)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '103', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('104', '103', '95-106', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '104', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('105', '103', '107-118', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '105', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('106', '103', '119-130', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '106', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('107', '103', '131-143', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '107', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('108', '103', '144-155', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '108', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('109', '103', '156-167', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '109', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('110', '103', '168-180', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '110', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('111', '103', '181-192', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '111', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('112', '103', '193-204', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '112', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('113', '103', '205-216', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '113', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('114', null, '收缩压(服药后)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '114', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('115', '114', '95-106', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '115', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('116', '114', '107-113', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '116', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('117', '114', '114-119', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '117', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('118', '114', '120-125', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '118', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('119', '114', '126-131', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '119', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('120', '114', '132-139', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '120', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('121', '114', '140-148', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '121', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('122', '114', '149-160', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '122', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('123', '114', '161-204', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '123', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('124', '114', '205-216', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '124', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('125', null, '糖尿病家族史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '125', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('126', null, '吸烟史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '126', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('127', null, '心血管疾病史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '127', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('128', null, '房颤', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '128', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('129', null, '左心室肥厚', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '129', '1', '1', null);

-- ----------------------------
-- Table structure for plat_lesson_scale_son
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_son`;
CREATE TABLE `plat_lesson_scale_son` (
  `name` varchar(200) NOT NULL COMMENT '量表名称',
  `lesson_scale_id` bigint(32) DEFAULT NULL COMMENT '所属子课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `result_formula` varchar(2000) DEFAULT NULL,
  `risk_score` int(11) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_son
-- ----------------------------
INSERT INTO `plat_lesson_scale_son` VALUES ('ADL量表（Barthel指数）', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '19', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('Berg 平衡量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '15', null, '15', '平衡功能差，患者需要乘坐轮椅 提示有跌倒的危险', '15');
INSERT INTO `plat_lesson_scale_son` VALUES ('PHQ-9抑郁症筛查量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '26', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('SF-36健康状况问卷', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '24', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('TDL生命质量测定表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '19', null, '15', '较差', '19');
INSERT INTO `plat_lesson_scale_son` VALUES ('中医体质分类与评价', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '13', '', '18', null, '13');
INSERT INTO `plat_lesson_scale_son` VALUES ('健康促进生活方式量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '20', '建议合理膳食、适宜运动、戒烟限酒、保持心理平衡、定期体检。', '20', ' 生活方式不健康', '20');
INSERT INTO `plat_lesson_scale_son` VALUES ('匹茨堡睡眠质量指数量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '16', '①多倾听自己的声音，给身体放个假，出去走走，在旅行中，既可感受大自然的美，视野也会不断开阔，见识会不断增加，思维会不断通达，人的境界也会不断提升，对于生活中的不如意也便不会那么斤斤计较。心胸会开阔，人会快乐起来，快乐的情绪有助于睡眠。<br>\r\n②多参加团体活动，如：茶友会、摄影协会等，既可广交朋友放松心情，又可增广才识锻炼能力。<br>\r\n③多听音乐，音乐可以陶冶情操，缓解压力、放松心情，可以让我们暂时忘记不好的事情，更好的面对生活。对于大脑疲劳、气血上冲、头胀脑热、醒后难以入眠者可以多听音乐。用于体育运动，可抑制兴奋，对于赛后休整、消除疲劳、减少耗能、恢复体力有积极作用。可以听舒缓、轻快、欢乐的音乐，如《蓝色多瑙河》、《致爱丽丝》等，在优美的旋律中憧憬美好的生活。<br>\r\n④白天锻炼，锻炼会减少压力感从而提高睡眠质量。不过睡前3小时最好不要锻炼，因增加的肾上腺素会让机体保持清醒状态，难以进入睡眠。<br>\r\n⑤改善生活方式。限制酒水饮料，不要过多饮用易让人兴奋的酒精饮料和咖啡、茶等。晚饭不宜太饱，睡前2个小时尽量不要吃东西，睡前最好用热水泡泡脚，也可快速冲个热水澡及喝一杯热牛奶有利睡眠。<br>\r\n⑥健康饮食，晚上不要吃辛辣的富含油脂的食物，多吃蔬菜和水果，可选择香蕉、苹果可抗肌肉劳；若把桔橙一类的水果放在枕边，其香味也能促进睡眠。<br>\r\n⑦药膳疗法。柏子仁炖猪心、灵芝煲莲藕、桂圆芡实粥、酸枣仁粥，可以起到养心、安神、敛汗的作用，有助睡眠。<br>\r\n⑧改善卧室环境，卧室墙壁颜色选择温馨浅色系，卧室环境温度以18℃一22℃为宜，采光通风条件好，选择隔音、遮光性能好的窗帘，有助于睡眠。卧室内应尽量避免摆放过多的家用电器，关闭手机和电脑，整理好床铺，尽量床头朝北床脚朝南。午睡时间过久醒来会很不舒服，还会搅乱生物钟，影响到晚上的睡觉，故午休时间不宜过长，最好控制在半小时内。<br>\r\n⑨按摩疗法。按摩内关、神门、后溪等穴位有助睡眠。<br>\r\n⑩寻求心理评估师及睡眠治疗师的帮助，获得专业评估，采用正确的药物和物理干预疗法。', '12', '睡眠质量差', '16');
INSERT INTO `plat_lesson_scale_son` VALUES ('十年内心血管疾病危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '7', '戒烟限酒、保持心理健康、多吃青菜水果等，以及适当的食用含有优质蛋白的食物。', '4', '高危人群', '7');
INSERT INTO `plat_lesson_scale_son` VALUES ('安全感量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '18', null, '15', '安全感较高', '18');
INSERT INTO `plat_lesson_scale_son` VALUES ('工具性日常生活活動能力量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '11', '一、预约健康管理师，进行生活能力全面评估，根据评估结果在社区进行康复治疗。<br>\r\n二、情感支持，经常交流、沟通。应经常用抚摸动作和亲切的话语，给老年人与关心和爱护。谈话时语调要降低，态度要和蔼，吐词清晰、缓慢。<br>\r\n三、加强防护，防止意外走失。不要让老年人单独外出，以免迷路走失。最好给老年人口袋里放一个有自己名字、年龄、家庭地址、联系电话以及所患疾病的安全卡。另外家中要有安全措施，比如老年人穿防滑软底鞋，浴室、卫生间安装扶手，床边加床档等。<br>\r\n四、制定合理的康复训练。要经常督促老年人独自搞好个人卫生。让他们做一些泡茶、洗碗、扫地、买东西等简单家务，在头脑中建立新的条件反射。充分利用看电视、听音乐、看报纸、读杂志，给之以视听方而的外界刺激：经常有意识地让他们记忆、判断以锻炼大脑思维活动。也可进行针对性的认知功能锻炼，内容包括跳棋、五子棋、拼图、抄写诗歌、算数、简单图画、英语对话等。<br>\r\n五、健康饮食。老年人的饮食应丰富多样，定时定量，以高蛋白、低脂肪、高纤维素、易消化软食为主。必要时可家用辅助食品，如针对缺牙齿较多老年人的介护食品，以保证每日营养所需。<br>\r\n六、合理的身体锻炼。科学合理的身体锻炼对疾病的预防、治疗和康复都能发挥积极的作用，也是促进老年人长寿的重要手段之一。可选择家居慢运动中的关式养生操、床上操等。', '6', '失能', '11');
INSERT INTO `plat_lesson_scale_son` VALUES ('平和质', null, null, null, null, '(1)精神调养：保持乐观、开朗的情绪，积极进取，节制偏激的情感，及时消除生活中不利事件对情绪负面的影响。<br>\r\n(2)生活起居：起居应有规律，不要过度劳累。饭后宜缓行百步，不宜食后即睡。作息应有规律，应劳逸结合，保持充足的睡眠时间。<br>\r\n(3)体育锻炼：根据年龄和性别，参加适度的运动。如年轻人可适当跑步、打球，老年人可适当散步、打太极拳等。<br>\r\n(4)饮食调养：饮食应有节制，不要过饥过饱，不要常吃过冷过热和不干净的食物。粗细饮食要合理搭配，多吃五谷杂粮、蔬菜、瓜果，少食用过于油腻及辛辣之品。不要吸烟酗酒。<br>\r\n(5)药物调理：一般不提倡使用药物。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('慢性病患者生命质量测定量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '14', null, null, null, '14');
INSERT INTO `plat_lesson_scale_son` VALUES ('慢性病病人健康素养量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '17', null, '15', '健康素养差', '17');
INSERT INTO `plat_lesson_scale_son` VALUES ('日常生活活动能力量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '12', '', '8', '生活完全需要帮助', '12');
INSERT INTO `plat_lesson_scale_son` VALUES ('欧洲生存质量测定量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '11', '', null, '', null);
INSERT INTO `plat_lesson_scale_son` VALUES ('气虚质', null, null, null, null, '(1)精神调养：多参加有益的社会活动，多与人交谈、沟通。以积极进取的态度面对生活。<br>\r\n(2)生活起居：起居应有规律，夏季应适当午睡，保持充足的睡眠。平时要注意保暖，避免运动或剧烈运动时出汗受风。不要过于劳作，以免损伤正气。<br>\r\n(3)体育锻炼：可做一些柔缓的运动，如在公园、广场、庭院、湖畔、河边、山坡等空气清新之处散步、打太极拳、做操等，并持之以恒。平时可自行按摩足三里穴。不宜做大负荷和出大汗的运动，忌用猛力和做长久憋气的动作。<br>\r\n(4)饮食调养：应多食具有益气健脾作用的食物，如黄豆、白扁豆、鸡肉、鹤鹑肉、泥鳅、香菇、大枣、桂圆、蜂蜜等。少食具有耗气作用的食物，如槟榔、空心菜、生萝卜等。<br>\r\n(5)药物调理：常有自汗、感冒者、可服用玉屏风散预防。血瘀质<br>\r\n(1)精神调养：及时消除不良情绪，保持心情愉快，防止郁闷不乐而致气机不畅。可多听一些抒情柔缓的音乐来调节情绪。<br>\r\n(2)生活起居：作息时间宜有规律，可早睡早起，保持足够的睡眠；但不可过于安逸，以免气机郁滞而致血行不畅。<br>\r\n(3)体育锻炼：可进行一些有助于气血运行的运动项目，如太极拳、太极剑、各种舞蹈、步行健身法、徒手健身操等。保健按摩可使经络畅通。血瘀质的人在运动时如出现胸闷、呼吸困难、脉搏显著加快等不适症状，应停止运动，去医院进一步检查。<br>\r\n(4)饮食调养：可常食黑豆、海藻、紫菜、海带、萝卜、胡萝卜、金橘、柚、桃李、山楂、醋、玫瑰花、绿茶等具有活血、散结、行气、疏肝解郁作用的食物。少吃肥猪肉等滋腻之品。<br>\r\n(5)药物调理：可酌情服用桂枝茯苓丸、大黄?虫丸。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('气郁质', null, null, null, null, '(1)精神调养：保持乐观、开朗的情绪，积极进取，节制偏激的情感，及时消除生活中不利事件对情绪负面的影响。<br>\r\n(2)生活起居：居住环境宜干燥而不宜潮湿。平时多进行户外活动。衣着应透气、经常晒太阳或进行日光浴。在潮湿的气候条件下，应减少户外活动，避免受寒淋雨。不要过于安逸，贪恋床榻。<br>\r\n(3)体育锻炼：应尽量参加户外活动，可坚持较大量的运动锻炼，如跑步、登山、游泳、武术等。多参加群体性的体育运动项目，如打球、跳舞、下棋等，以便更多地融入社会，解除自我封闭的状态。<br>\r\n(4)饮食调养：多食小麦、芫荽、葱、蒜、黄花菜、海带、海藻、萝卜、金橘、山楂、槟榔、玫瑰花等具有行气、解郁、消食、醒神作用的食物。<br>\r\n(5)药物调理：可酌情服用逍遥散、舒肝和胃丸、开胸顺气丸、柴胡疏肝散、越鞠丸等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('湿热质', null, null, null, null, '(1)精神调养：克制过激的情绪。合理安排自己的工作、学习，培养广泛的兴趣爱好。<br>\r\n(2)生活起居：避免居住在低洼潮湿的地方，居住环境宜干燥、通风。不要熬夜、过于劳累。盛夏暑湿较重的季节，减少户外活动的时间。保持充足而有规律的睡眠。<br>\r\n(3)体育锻炼：适合做大强度、大运动量的锻炼，如中长跑、游泳、爬山、各种球类、武术等。夏天由于气温高、湿度大，最好选择在清晨或者傍晚较凉爽时锻炼。<br>\r\n(4)饮食调养：饮食以清淡为原则，可多食赤小豆、绿豆、空心菜、苋菜、芹菜、黄瓜、丝瓜、葫芦、冬瓜、藕、西瓜、荸荠等甘寒、甘平的食物。少食羊肉、狗肉、韭菜、生姜、芫荽辣椒、酒、饴糖、花椒、胡椒、蜂蜜等甘酸滋腻之品及火锅、烹炸烧烤等辛温助热的食物。应戒烟限酒。<br>\r\n(5)药物调理：可酌情服用六一散、清胃散、甘露消毒丹等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('特禀质', null, null, null, null, '(1)精神调养：合理安排作息时间，正确处理工作、生活和学习的关系，避免情绪紧张。<br>\r\n(2)生活起居：居室应通风良好。保持室内清洁，被褥、床单要经常洗晒，以防止螨过敏。室内装修后不宜立即搬进居住，应打开窗户，让油漆、甲醛等化学物质气味挥发干净后再搬进新居。夏季室外花粉较多时，要减少室外活动时间，以防止过敏。不宜养宠物，以免对动物皮毛过敏。起居应有规律，保持充足的睡眠时间。<br>\r\n(3)体育锻炼：积极参加各种体育锻炼，增强体质，天气寒冷时锻炼要注意防寒保暖，防止感冒。<br>\r\n(4)饮食调养：饮食宜清淡、均衡，粗细搭配适当，荤素配伍合理。少食荞麦(含致敏物质麦荧光素）、蚕豆、白扁豆、牛肉、鹅肉、鲤鱼、虾、蟹、茄子、酒、辣椒、浓茶咖啡等辛辣之品、腥膻发物及含致敏物质的食物。<br>\r\n(5)药物调理：可酌情服用玉屏风散、清风散、过敏煎等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('生命质量测定量表QLQ', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '22', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('痰湿质', null, null, null, null, '(1)精神调养：及时消除不良情绪，保持心情愉快，防止郁闷不乐而致气机不畅。可多听一些抒情柔缓的音乐来调节情绪。<br>\r\n(2)生活起居：居住环境宜于燥而不宜潮湿。平时多进行户外活动。衣着应透气、经常晒太阳或进行日光浴。在潮湿的气候条件下，应减少户外活动，避免受寒淋雨。不要过于安逸，贪恋床榻。<br>\r\n(3)体育锻炼：因形体肥胖，易于困倦，故应根据自己的具体情况循序渐进，长期坚持运动锻炼，如散步、慢跑、打乒乓球、羽毛球、网球、游泳、练武术，以及适合自己的各种舞蹈。<br>\r\n(4)饮食调养：饮食应以清淡为原则，少食肥肉及甜、黏、油腻的食物。可多食葱、蒜、海藻、海带、冬瓜、萝卜、金橘、芥末等食物。<br>\r\n(5)药物调理：痰湿之生与肺、脾、肾三脏关系最为密切，故重点在于调补肺、脾、肾三脏。若因肺失宣降，津液输布，聚湿生痰，当宣肺化痰，选用二陈汤；若因脾失健运，聚湿成痰者，当健脾化痰，方选六君子汤，或香砂六君子汤；若肾虚不能制水，水泛为痰液者，当温阳化痰，方选金匮肾气丸。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('社会支持评定量表SSQ', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '21', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('纪立农中国人群糖尿病风险', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '6', '需做OGTT试验，每3个月或半年一次空腹血糖、OGTT试验，每半年或1年一次血压、血脂、血常规、腹部B超等检查。减少脂肪、肉类摄入，增加蔬菜、水果摄入，控制进食总热量，减轻体重，控制腰围，加强体育锻炼。', '3', '高危人群', '6');
INSERT INTO `plat_lesson_scale_son` VALUES ('肥胖及肥胖风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '4', '规律进餐、控制食量、增加运动、减少久坐、规律作息等，如身体不适，建议及时就医', null, null, '4');
INSERT INTO `plat_lesson_scale_son` VALUES ('脂肪肝风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '10', '减少脂肪、肉类摄入，增加蔬菜、水果摄入，减轻体重，加强体育锻炼，戒酒。每3-6个月一次体重、血压、腰围测量，每半年或1年一次肝功能、血脂、血糖、腹部B超等检查。', '3', '高危人群', '10');
INSERT INTO `plat_lesson_scale_son` VALUES ('脑卒中风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '5', '每年一次常规体检（血压、血糖、血脂等），每2-3年一次头颅CT、经颅多普勒（TCD）、颈动脉彩超、心脏彩超、胸片、心电图、腹部B超等检查。减少脂肪、肉类摄入，增加蔬菜、水果摄入，减少食盐摄入，减轻体重，适当加强体育锻炼，戒烟。积极治疗原发病。积极治疗原发病。', '4', '高危人群', '5');
INSERT INTO `plat_lesson_scale_son` VALUES ('腰椎病风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '8', '睡硬板床，禁忌久坐、久站、剧烈运动，多休息，每天坚持适当腰部锻炼。每年一次常规体检、腰椎正侧位片、腰椎CT/MR。', '3', '高危人群', '8');
INSERT INTO `plat_lesson_scale_son` VALUES ('自测健康评定量表（不确定）', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '20', null, null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('血瘀质', null, null, null, null, '1.情志调养：血瘀体质者需注意情志调养，要培养积极乐观的情绪，积极调整自己的精神状态，避免长期抑郁、愤怒等不良情志因素的长时间刺激。<br>\r\n2.运动疗法：血瘀体质的人，经络气血运行不畅，运动是调养血瘀体质最为直接的方法，坚持经常性锻炼，常见的如太极拳、太极剑、五禽戏、步行健身法、徒手健身操等，长时间坚持均可起到改善患者体质的目的。<br>\r\n3.药物治疗：血瘀体质者可选取血府逐瘀汤进行调理，常见的药物有当归、生地黄、川芎、桔梗等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('阳虚质', null, null, null, null, '(1)精神调养：阳气不足之人常出现情绪不佳，如肝阳虚善恐、心阳虚善悲。因此平时多与别人交谈、沟通。对待生活中不顺心的事情，要从正反面分析、及时消除情绪中的消极因素。平时可多听一些激扬、高亢、豪迈的音乐以调动情绪，防止悲忧伤和惊恐。<br>\r\n(2)生活起居：居住环境应空气流通，秋冬注意保暖。夏季避免长时间在空调房间中，可在自然环境下纳凉，但不要睡在穿风的过道上及露天空旷之处。平时注意足下、背部及下腹部丹田部位的防寒保暖。防止出汗过多，在阳光下适当进行户外活动。保持足够的睡眠。<br>\r\n(3)体育锻炼：因“动则生阳”，故阳虚体质之人，要加强体育锻炼，春夏秋冬，坚持不懈，每天进行1~2次，具体项目因个体体力强弱而定。可做一些舒缓的运动，如慢跑、散步、五禽戏、广播操。夏天不宜做过分激烈的运动，冬天避免在大风、大寒、大雾、大雪及空气污染的环境中锻炼。自行按摩气海、足三里、涌泉等穴位，或经常灸足三里、关元，可适当洗桑拿、温泉浴，亦可常做日光浴、空气浴以强壮卫阳。<br>\r\n(4)饮食调养：应多食壮阳作用的食品，如羊肉、狗肉、鹿肉、鸡肉、鳝鱼、韭菜、生姜、辣椒、芫荽、葱、蒜、芥末、花椒、胡椒等甘温益气之品。少食黄瓜、柿子、冬瓜、藕、莴苣、梨、西瓜、荸荠等生冷寒凉食物，少饮寒凉食物，少饮绿茶。<br>\r\n(5)药物调理：可选用补阳驱寒、温养肝肾之品，常用药物有鹿茸、海狗肾、蛤蚧、冬虫夏草、巴戟天、淫羊藿、仙茅、肉苁蓉、补骨脂、胡桃、杜仲、续断、菟丝子等。可酌情服用金匮肾气丸等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('阴虚质', null, null, null, null, '(1)精神调养：阴虚质之人平素性情急躁、常常心烦易怒，是阴虚火旺、火扰神明之故，尤应遵循《内经》“恬淡虚无”“精神内守”之养神大法。平时宜克制情绪，遇事要冷静，正确对待顺境和逆境。平素加强自我涵养，常读自我修养的书籍，可以用练书法、下棋来怡情悦性，用旅游来寄情山水、陶冶情操。平时可多听一些舒缓、轻柔、抒情的音乐，防止恼怒。此外，节制性生活也很重要。<b>\r\n(2)生活起居：起居应有规律，居住环境宜安静，睡前不要饮茶、锻炼和玩游戏。应早睡早起，中午保持一定的午休时间。避免熬夜、剧烈运动和高温酷暑下工作。戒烟酒。<b>\r\n(3)体育锻炼：不宜过激活动，只适合做中小强度、间歇性的身体锻炼，可选择太极拳、太极剑、气功等动静结合的传统健身项目。锻炼时要控制出汗量，及时补充水分。皮肤干燥甚者，可多游泳。不宜桑拿。<b>\r\n(4)饮食调养：饮食调理的原则是保阴潜阳，宜芝麻、糯米、蜂蜜、乳品、甘蔗、蔬菜、水果、豆腐、鱼类等清淡食物，可多食瘦猪肉、鸭肉、龟、鳖、绿豆、冬瓜、赤小豆、海蜇、荸荠、百合等甘凉滋润之品。少吃羊肉、狗肉、韭菜、辣椒、葱、蒜、葵花籽等性温燥烈之品。<b>\r\n(5)药物调理：可选用滋阴清热、滋养肝肾之品，如女贞子、山茱萸、五味子、旱莲草、麦冬、天冬、黄精、玉竹、玄参、枸杞子、桑椹、龟甲诸药，均有滋阴清热的作用，可因证情选用。可酌情服用六味地黄丸、杞菊地黄丸等。', null, null, null);
INSERT INTO `plat_lesson_scale_son` VALUES ('颈椎病风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '1', '每年一次颈椎正侧位片、胸片、颈动脉彩超、经颅多普勒等检查。改变不良的姿势，纠正不合理用枕，增加颈椎休息放松时间，加强体育锻炼，坚持每天空闲时间做颈椎操。', '3', '高危人群', '1');
INSERT INTO `plat_lesson_scale_son` VALUES ('高尿酸血症风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '3', '限制高嘌呤食物（内脏、海鲜、老火汤等），增加蔬菜、水果摄入，控制或减轻体重，戒酒，多饮水、碱化尿液。高危人群每半年一次血尿酸、尿尿酸、肾功、血糖、血脂、血压、腹部B超等检查。', '3', '高危人群', '3');
INSERT INTO `plat_lesson_scale_son` VALUES ('高脂血症危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '2', '减少脂肪、肉类摄入，增加蔬菜、水果摄入，加强体育锻炼，减轻体重，戒烟，每月测血压，每半年体检一次，检查血生化指标（血糖、血脂等）、腹部B超等。', '3', '高危人群', '2');
INSERT INTO `plat_lesson_scale_son` VALUES ('高血压危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '9', '每1-2年测一次血压。减少脂肪、肉类摄入，增加蔬菜、水果摄入，加强体育锻炼，减轻体重，戒烟；', '3', '高危人群', '9');

-- ----------------------------
-- Table structure for plat_lesson_schedule
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_schedule`;
CREATE TABLE `plat_lesson_schedule` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `teacher_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `class_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `lesson_time` varchar(32) DEFAULT NULL COMMENT '200',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_schedule
-- ----------------------------
INSERT INTO `plat_lesson_schedule` VALUES ('74', '2024-12-18 19:13:28', '2024-12-18 19:14:28', '305', '106', '14', '2024-12-01 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('75', '2024-12-18 19:13:55', '2024-12-18 19:16:35', '305', '106', '25', '2024-12-16 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('76', '2024-12-18 19:17:08', null, '8', '106', '25', '2024-12-23 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('77', '2024-12-18 19:18:21', '2024-12-18 19:24:41', '6', '1', '25', '2024-12-23 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('78', '2024-12-18 19:35:31', null, '6', '106', '25', '2024-12-24 00:00:00');

-- ----------------------------
-- Table structure for plat_lesson_son
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_son`;
CREATE TABLE `plat_lesson_son` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '子课程id主键',
  `name` varchar(200) NOT NULL COMMENT '子课程名称',
  `lesson_id` bigint(32) NOT NULL COMMENT '所属课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `experimental_method` varchar(200) DEFAULT NULL COMMENT '实验方式',
  `suggested_duration` varchar(200) DEFAULT NULL COMMENT '分值权重占比',
  `serial_number` varchar(200) DEFAULT NULL COMMENT '分值权重占比',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  `lesson_son_folder_name` varchar(100) DEFAULT NULL COMMENT '子课程文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30344 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_son
-- ----------------------------
INSERT INTO `plat_lesson_son` VALUES ('1', '自我评估', '1', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2', '中医九种体质分型', '1', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '60', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('3', '理论学习', '35', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '45', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('4', '实训流程', '35', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('5', '理论知识', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('6', '健康信息收集', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('7', '健康干预', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('8', '理论知识', '37', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('9', '健康干预', '37', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('10', '理论知识', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('11', '健康信息收集', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('12', '健康干预', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('13', '学习及咨询模块', '39', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('14', '饮食评价报告', '39', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '30', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('15', '理论知识', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('16', '健康信息收集', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('17', '健康干预', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('18', '理论知识', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('19', '健康信息收集', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('20', '健康干预', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('21', '理论知识', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('22', '健康信息收集', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('23', '健康干预', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('24', '理论知识', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('25', '健康信息收集', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('26', '健康干预', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('27', '健康信息收集', '44', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('28', '健康干预', '44', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('29', '理论知识', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('30', '健康信息收集', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('31', '健康干预', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('32', '理论学习', '46', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('33', '营养标签', '46', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('34', '理论学习', '47', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('35', '实训环节', '47', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('36', '理论学习', '48', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('37', '实训环节', '48', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2991', '理论知识', '299', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2992', '慢性阻塞性肺疾病风险评估', '299', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2993', '理论知识', '298', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2994', '糖尿病风险评估', '298', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2995', '理论知识', '297', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2996', '脑卒中风险评估', '297', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2997', '理论知识', '296', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2998', '心血管病风险评估', '296', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2999', '理论知识', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3000', '健康信息收集', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3001', '健康干预', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3002', '健康信息收集', '294', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3003', '健康干预', '294', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3004', '理论知识', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3005', '健康信息收集', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3006', '健康干预', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3007', '理论知识', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3008', '健康信息收集', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3009', '健康干预', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3010', '理论知识', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3011', '健康信息收集', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3012', '健康干预', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3013', '理论知识', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3014', '健康信息收集', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3015', '健康干预', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3016', '理论知识', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3017', '健康信息收集', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3018', '健康干预', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3019', '理论知识', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3020', '健康信息收集', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3021', '健康干预', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3022', '理论学习', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3023', '理论习题', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '12%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3024', '测试方法考核', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '5%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3025', '自我检测', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3026', '训练方法考核', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '27%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3027', '案例评估', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '56%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3028', '理论学习', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3029', '理论习题', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '12%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3030', '测试方法考核', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '28%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3031', '自我检测', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3032', '训练方法考核', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3033', '案例评估', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3034', '理论学习', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3036', '测试方法考核', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '24%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3037', '自我检测', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3038', '训练方法考核', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '8%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3039', '案例评估', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '68%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3040', '理论学习', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3041', '测试方法考核', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '30%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3042', '自我检测', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3043', '训练方法考核', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3044', '案例评估', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '60%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3045', '理论学习', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3046', '测试方法考核', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '15%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3047', '自我检测', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3048', '训练方法考核', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '15%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3049', '案例评估', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3050', '理论学习', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3051', '测试方法考核', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '30%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3052', '自我检测', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3053', '训练方法考核', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3054', '案例评估', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3055', '理论学习', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3056', '测试方法考核', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '35%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3057', '自我检测', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3058', '训练方法考核', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3059', '案例评估', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '45%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3060', '理论学习', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3061', '测试方法考核', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3062', '自我检测', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3063', '训练方法考核', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '50%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3064', '案例评估', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '40%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3065', '理论学习', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3066', '理论习题', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3067', '测试方法考核', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3068', '自我检测', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3069', '训练方法考核', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3070', '案例评估', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3071', '理论学习', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3072', '理论习题', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3073', '测试方法考核', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3074', '自我检测', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3075', '训练方法考核', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3076', '案例评估', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '60%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3077', '理论学习', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3078', '测试方法考核', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3079', '自我检测', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3080', '训练方法考核', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3081', '案例评估', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3082', '理论学习', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3083', '测试方法考核', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3084', '自我检测', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3085', '训练方法考核', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3086', '案例评估', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3087', '理论学习', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3088', '测试方法考核', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3089', '自我检测', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3090', '训练方法考核', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3091', '案例评估', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3092', '理论学习', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3093', '测试方法考核', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3094', '自我检测', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3095', '训练方法考核', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3096', '案例评估', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3097', '理论学习', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3098', '测试方法考核', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3099', '自我检测', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3100', '训练方法考核', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3101', '案例评估', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3102', '理论学习', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3103', '测试方法考核', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3104', '自我检测', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3105', '训练方法考核', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3106', '案例评估', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');

-- ----------------------------
-- Table structure for plat_lesson_student
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_student`;
CREATE TABLE `plat_lesson_student` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '学生课程id',
  `user_id` bigint(32) NOT NULL COMMENT '学生id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) NOT NULL COMMENT '课程id',
  `state` int(1) NOT NULL COMMENT '0未开始，1已结束',
  `schedule_id` bigint(32) NOT NULL COMMENT '课程id',
  `score` varchar(200) DEFAULT '0' COMMENT '分数',
  `complete_time` varchar(200) DEFAULT NULL COMMENT '完成时间',
  `class_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_student
-- ----------------------------
INSERT INTO `plat_lesson_student` VALUES ('161', '111', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('162', '113', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('163', '117', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('164', '119', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('165', '120', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');

-- ----------------------------
-- Table structure for plat_lesson_type
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_type`;
CREATE TABLE `plat_lesson_type` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '课程类型',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_type
-- ----------------------------
INSERT INTO `plat_lesson_type` VALUES ('1', '基于大数据技术的区域性健康风险评估');
INSERT INTO `plat_lesson_type` VALUES ('2', '慢性病人生活质量评估');
INSERT INTO `plat_lesson_type` VALUES ('3', '饮食综合评估及干预虚拟仿真实验软件');
INSERT INTO `plat_lesson_type` VALUES ('4', '运动功能检测与评估');

-- ----------------------------
-- Table structure for plat_major
-- ----------------------------
DROP TABLE IF EXISTS `plat_major`;
CREATE TABLE `plat_major` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(20) DEFAULT '' COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) NOT NULL COMMENT '所属学院id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_major
-- ----------------------------
INSERT INTO `plat_major` VALUES ('1', '信息管理与信息系统', '', '', '2024-07-07 12:21:14', '2024-12-13 12:47:21', '21');
INSERT INTO `plat_major` VALUES ('2', '计算机科学与技术', '', '', '2024-07-07 12:53:29', '2024-12-13 12:47:18', '21');

-- ----------------------------
-- Table structure for plat_menu
-- ----------------------------
DROP TABLE IF EXISTS `plat_menu`;
CREATE TABLE `plat_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（1正常 0停用）',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `roles` varchar(500) DEFAULT '' COMMENT '可查看角色',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of plat_menu
-- ----------------------------
INSERT INTO `plat_menu` VALUES ('1', '客户管理', '0', '2', 'plat-manager', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('2', '教职工管理', '1', '2', 'teacher', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@');
INSERT INTO `plat_menu` VALUES ('3', '学生管理', '1', '2', 'student', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('4', '组织管理', '0', '1', 'plat-organization', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('5', '学院管理', '4', '1', 'college', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('6', '专业管理', '4', '1', 'major', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('7', '班级管理', '4', '1', 'class', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('8', '课程资源', '0', '3', 'plat-lesson', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@3@4@');
INSERT INTO `plat_menu` VALUES ('9', '课程列表', '8', '3', 'lesson', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('10', '章节类型列表', '8', '4', 'chapterType', '0', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('11', '章节列表', '8', '5', 'chapter', '0', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('12', '课程管理', '8', '6', 'plat-teacher-lesson-manager', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@3@');
INSERT INTO `plat_menu` VALUES ('13', '课程安排', '8', '6', 'plat-teacher-lesson-schedule', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@3@');
INSERT INTO `plat_menu` VALUES ('14', '课程列表', '8', '6', 'plat-student-lesson-manager', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');
INSERT INTO `plat_menu` VALUES ('15', '成绩管理', '0', '6', 'plat-achievement', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');
INSERT INTO `plat_menu` VALUES ('16', '成绩查询', '15', '6', 'student', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');

-- ----------------------------
-- Table structure for plat_questionnaire
-- ----------------------------
DROP TABLE IF EXISTS `plat_questionnaire`;
CREATE TABLE `plat_questionnaire` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `number` varchar(200) DEFAULT NULL,
  `score` int(5) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2203 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_questionnaire
-- ----------------------------
INSERT INTO `plat_questionnaire` VALUES ('1', '颈椎病风险评估', '1', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('2', '颈椎病风险评估', '2', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('3', '颈椎病风险评估', '3', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('4', '颈椎病风险评估', '4', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('5', '颈椎病风险评估', '5', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('6', '颈椎病风险评估', '6', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('7', '颈椎病风险评估', '7', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('8', '高脂血症危险评估', '1', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('9', '高脂血症危险评估', '2', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('10', '高脂血症危险评估', '3', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('11', '高脂血症危险评估', '4', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('12', '高脂血症危险评估', '5', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('13', '高脂血症危险评估', '6', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('14', '高脂血症危险评估', '7', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('15', '高脂血症危险评估', '8', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('16', '高尿酸血症风险评估', '1', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('17', '高尿酸血症风险评估', '2', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('18', '高尿酸血症风险评估', '3', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('19', '高尿酸血症风险评估', '4', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('20', '高尿酸血症风险评估', '5', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('21', '高尿酸血症风险评估', '6', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('22', '高尿酸血症风险评估', '7', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('23', '肥胖及肥胖风险评估', '1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('24', '肥胖及肥胖风险评估', '2-1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('25', '肥胖及肥胖风险评估', '2-2', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('26', '肥胖及肥胖风险评估', '3-1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('27', '肥胖及肥胖风险评估', '3-2', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('28', '肥胖及肥胖风险评估', '4-1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('29', '肥胖及肥胖风险评估', '4-2', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('30', '肥胖及肥胖风险评估', '5-1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('31', '肥胖及肥胖风险评估', '5-2', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('32', '肥胖及肥胖风险评估', '6-1', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('33', '肥胖及肥胖风险评估', '6-2', '1', null);
INSERT INTO `plat_questionnaire` VALUES ('34', '脑卒中风险评估', '男-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('35', '脑卒中风险评估', '男-1-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('36', '脑卒中风险评估', '男-1-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('37', '脑卒中风险评估', '男-1-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('38', '脑卒中风险评估', '男-1-5', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('39', '脑卒中风险评估', '男-1-6', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('40', '脑卒中风险评估', '男-1-7', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('41', '脑卒中风险评估', '男-1-8', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('42', '脑卒中风险评估', '男-1-9', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('43', '脑卒中风险评估', '男-1-10', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('44', '脑卒中风险评估', '男-1-11', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('45', '脑卒中风险评估', '男-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('46', '脑卒中风险评估', '男-2-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('47', '脑卒中风险评估', '男-2-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('48', '脑卒中风险评估', '男-2-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('49', '脑卒中风险评估', '男-2-5', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('50', '脑卒中风险评估', '男-2-6', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('51', '脑卒中风险评估', '男-2-7', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('52', '脑卒中风险评估', '男-2-8', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('53', '脑卒中风险评估', '男-2-9', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('54', '脑卒中风险评估', '男-2-10', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('55', '脑卒中风险评估', '男-2-11', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('56', '脑卒中风险评估', '男-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('57', '脑卒中风险评估', '男-3-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('58', '脑卒中风险评估', '男-3-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('59', '脑卒中风险评估', '男-3-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('60', '脑卒中风险评估', '男-3-5', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('61', '脑卒中风险评估', '男-3-6', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('62', '脑卒中风险评估', '男-3-7', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('63', '脑卒中风险评估', '男-3-8', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('64', '脑卒中风险评估', '男-3-9', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('65', '脑卒中风险评估', '男-3-10', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('66', '脑卒中风险评估', '男-3-11', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('67', '脑卒中风险评估', '男-4-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('68', '脑卒中风险评估', '男-4-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('69', '脑卒中风险评估', '男-5-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('70', '脑卒中风险评估', '男-5-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('71', '脑卒中风险评估', '男-6-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('72', '脑卒中风险评估', '男-6-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('73', '脑卒中风险评估', '男-7-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('74', '脑卒中风险评估', '男-7-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('75', '脑卒中风险评估', '男-8-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('76', '脑卒中风险评估', '男-8-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('77', '脑卒中风险评估', '女-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('78', '脑卒中风险评估', '女-1-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('79', '脑卒中风险评估', '女-1-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('80', '脑卒中风险评估', '女-1-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('81', '脑卒中风险评估', '女-1-5', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('82', '脑卒中风险评估', '女-1-6', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('83', '脑卒中风险评估', '女-1-7', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('84', '脑卒中风险评估', '女-1-8', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('85', '脑卒中风险评估', '女-1-9', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('86', '脑卒中风险评估', '女-1-10', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('87', '脑卒中风险评估', '女-1-11', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('88', '脑卒中风险评估', '女-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('89', '脑卒中风险评估', '女-2-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('90', '脑卒中风险评估', '女-2-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('91', '脑卒中风险评估', '女-2-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('92', '脑卒中风险评估', '女-2-5', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('93', '脑卒中风险评估', '女-2-6', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('94', '脑卒中风险评估', '女-2-7', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('95', '脑卒中风险评估', '女-2-8', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('96', '脑卒中风险评估', '女-2-9', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('97', '脑卒中风险评估', '女-2-10', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('99', '脑卒中风险评估', '女-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('100', '脑卒中风险评估', '女-3-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('101', '脑卒中风险评估', '女-3-3', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('102', '脑卒中风险评估', '女-3-4', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('103', '脑卒中风险评估', '女-3-5', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('104', '脑卒中风险评估', '女-3-6', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('105', '脑卒中风险评估', '女-3-7', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('106', '脑卒中风险评估', '女-3-8', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('107', '脑卒中风险评估', '女-3-9', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('108', '脑卒中风险评估', '女-3-10', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('110', '脑卒中风险评估', '女-4-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('111', '脑卒中风险评估', '女-4-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('112', '脑卒中风险评估', '女-5-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('113', '脑卒中风险评估', '女-5-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('114', '脑卒中风险评估', '女-6-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('115', '脑卒中风险评估', '女-6-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('116', '脑卒中风险评估', '女-7-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('117', '脑卒中风险评估', '女-7-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('118', '脑卒中风险评估', '女-8-1', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('119', '脑卒中风险评估', '女-8-2', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('120', '纪立农中国人群糖尿病风险', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('121', '纪立农中国人群糖尿病风险', '1-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('122', '纪立农中国人群糖尿病风险', '1-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('123', '纪立农中国人群糖尿病风险', '1-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('124', '纪立农中国人群糖尿病风险', '1-5', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('125', '纪立农中国人群糖尿病风险', '1-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('126', '纪立农中国人群糖尿病风险', '1-7', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('127', '纪立农中国人群糖尿病风险', '1-8', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('128', '纪立农中国人群糖尿病风险', '1-9', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('129', '纪立农中国人群糖尿病风险', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('130', '纪立农中国人群糖尿病风险', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('131', '纪立农中国人群糖尿病风险', '2-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('132', '纪立农中国人群糖尿病风险', '2-4', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('133', '纪立农中国人群糖尿病风险', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('134', '纪立农中国人群糖尿病风险', '3-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('135', '纪立农中国人群糖尿病风险', '3-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('136', '纪立农中国人群糖尿病风险', '3-4', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('137', '纪立农中国人群糖尿病风险', '3-5', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('138', '纪立农中国人群糖尿病风险', '3-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('139', '纪立农中国人群糖尿病风险', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('140', '纪立农中国人群糖尿病风险', '4-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('141', '纪立农中国人群糖尿病风险', '4-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('142', '纪立农中国人群糖尿病风险', '4-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('143', '纪立农中国人群糖尿病风险', '4-5', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('144', '纪立农中国人群糖尿病风险', '4-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('145', '纪立农中国人群糖尿病风险', '4-7', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('146', '纪立农中国人群糖尿病风险', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('147', '纪立农中国人群糖尿病风险', '5-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('148', '纪立农中国人群糖尿病风险', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('149', '纪立农中国人群糖尿病风险', '6-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('150', '十年内心血管疾病危险评估', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('151', '十年内心血管疾病危险评估', '1-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('152', '十年内心血管疾病危险评估', '1-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('153', '十年内心血管疾病危险评估', '1-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('154', '十年内心血管疾病危险评估', '1-5', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('155', '十年内心血管疾病危险评估', '1-6', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('156', '十年内心血管疾病危险评估', '1-7', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('157', '十年内心血管疾病危险评估', '1-8', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('158', '十年内心血管疾病危险评估', '1-9', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('159', '十年内心血管疾病危险评估', '1-10', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('160', '十年内心血管疾病危险评估', '1-11', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('161', '十年内心血管疾病危险评估', '1-12', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('162', '十年内心血管疾病危险评估', '1-13', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('163', '十年内心血管疾病危险评估', '1-14', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('164', '十年内心血管疾病危险评估', '1-15', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('165', '十年内心血管疾病危险评估', '1-16', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('166', '十年内心血管疾病危险评估', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('167', '十年内心血管疾病危险评估', '2-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('168', '十年内心血管疾病危险评估', '2-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('169', '十年内心血管疾病危险评估', '2-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('170', '十年内心血管疾病危险评估', '2-5', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('171', '十年内心血管疾病危险评估', '2-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('172', '十年内心血管疾病危险评估', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('173', '十年内心血管疾病危险评估', '3-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('174', '十年内心血管疾病危险评估', '3-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('175', '十年内心血管疾病危险评估', '3-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('176', '十年内心血管疾病危险评估', '3-5', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('177', '十年内心血管疾病危险评估', '3-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('178', '十年内心血管疾病危险评估', '3-7', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('179', '十年内心血管疾病危险评估', '3-8', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('180', '十年内心血管疾病危险评估', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('181', '十年内心血管疾病危险评估', '4-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('182', '十年内心血管疾病危险评估', '4-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('183', '十年内心血管疾病危险评估', '4-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('184', '十年内心血管疾病危险评估', '4-5', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('185', '十年内心血管疾病危险评估', '4-6', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('186', '十年内心血管疾病危险评估', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('187', '十年内心血管疾病危险评估', '5-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('188', '十年内心血管疾病危险评估', '5-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('189', '十年内心血管疾病危险评估', '5-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('190', '十年内心血管疾病危险评估', '5-5', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('191', '十年内心血管疾病危险评估', '5-6', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('192', '十年内心血管疾病危险评估', '5-7', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('193', '十年内心血管疾病危险评估', '5-8', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('194', '十年内心血管疾病危险评估', '5-9', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('195', '十年内心血管疾病危险评估', '5-10', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('196', '十年内心血管疾病危险评估', '5-11', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('197', '十年内心血管疾病危险评估', '5-12', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('198', '十年内心血管疾病危险评估', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('199', '十年内心血管疾病危险评估', '6-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('200', '十年内心血管疾病危险评估', '6-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('201', '十年内心血管疾病危险评估', '6-4', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('202', '十年内心血管疾病危险评估', '7-1', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('203', '十年内心血管疾病危险评估', '7-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('204', '十年内心血管疾病危险评估', '7-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('205', '十年内心血管疾病危险评估', '7-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('206', '十年内心血管疾病危险评估', '8-1', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('207', '十年内心血管疾病危险评估', '8-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('208', '十年内心血管疾病危险评估', '8-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('209', '十年内心血管疾病危险评估', '8-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('210', '腰椎病风险评估', '1', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('211', '腰椎病风险评估', '2', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('212', '腰椎病风险评估', '3', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('213', '腰椎病风险评估', '4', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('214', '腰椎病风险评估', '5', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('215', '腰椎病风险评估', '6', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('216', '腰椎病风险评估', '7', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('222', '高血压危险评估', '2-6', '5', '2');
INSERT INTO `plat_questionnaire` VALUES ('223', '高血压危险评估', '2-7', '5', '2');
INSERT INTO `plat_questionnaire` VALUES ('264', '脂肪肝风险评估', '1', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('265', '脂肪肝风险评估', '2', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('266', '脂肪肝风险评估', '3', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('267', '脂肪肝风险评估', '4', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('268', '脂肪肝风险评估', '5', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('269', '脂肪肝风险评估', '6', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('270', '脂肪肝风险评估', '7', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('271', '脂肪肝风险评估', '8', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('272', '脂肪肝风险评估', '9', '1', '1');
INSERT INTO `plat_questionnaire` VALUES ('276', '工具性日常生活活動能力量表', '1-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('277', '工具性日常生活活動能力量表', '1-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('278', '工具性日常生活活動能力量表', '1-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('279', '工具性日常生活活動能力量表', '1-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('280', '工具性日常生活活動能力量表', '2-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('281', '工具性日常生活活動能力量表', '2-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('282', '工具性日常生活活動能力量表', '2-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('283', '工具性日常生活活動能力量表', '2-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('284', '工具性日常生活活動能力量表', '2-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('285', '工具性日常生活活動能力量表', '3-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('286', '工具性日常生活活動能力量表', '3-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('287', '工具性日常生活活動能力量表', '3-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('288', '工具性日常生活活動能力量表', '3-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('289', '工具性日常生活活動能力量表', '4-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('290', '工具性日常生活活動能力量表', '4-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('291', '工具性日常生活活動能力量表', '4-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('292', '工具性日常生活活動能力量表', '4-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('293', '工具性日常生活活動能力量表', '4-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('294', '工具性日常生活活動能力量表', '5-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('295', '工具性日常生活活動能力量表', '5-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('296', '工具性日常生活活動能力量表', '5-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('297', '工具性日常生活活動能力量表', '6-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('298', '工具性日常生活活動能力量表', '6-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('299', '工具性日常生活活動能力量表', '6-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('300', '工具性日常生活活動能力量表', '6-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('301', '工具性日常生活活動能力量表', '7-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('302', '工具性日常生活活動能力量表', '7-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('303', '工具性日常生活活動能力量表', '7-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('304', '工具性日常生活活動能力量表', '7-3', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('305', '工具性日常生活活動能力量表', '8-0', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('306', '工具性日常生活活動能力量表', '8-1', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('307', '工具性日常生活活動能力量表', '8-2', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('308', '日常生活活动能力量表', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('319', '日常生活活动能力量表', '1-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('320', '日常生活活动能力量表', '1-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('321', '日常生活活动能力量表', '1-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('322', '日常生活活动能力量表', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('323', '日常生活活动能力量表', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('324', '日常生活活动能力量表', '2-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('325', '日常生活活动能力量表', '2-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('326', '日常生活活动能力量表', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('327', '日常生活活动能力量表', '3-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('328', '日常生活活动能力量表', '3-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('329', '日常生活活动能力量表', '3-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('330', '日常生活活动能力量表', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('331', '日常生活活动能力量表', '4-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('332', '日常生活活动能力量表', '4-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('333', '日常生活活动能力量表', '4-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('334', '日常生活活动能力量表', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('335', '日常生活活动能力量表', '5-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('336', '日常生活活动能力量表', '5-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('337', '日常生活活动能力量表', '5-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('338', '日常生活活动能力量表', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('339', '日常生活活动能力量表', '6-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('340', '日常生活活动能力量表', '6-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('341', '日常生活活动能力量表', '6-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('342', '日常生活活动能力量表', '7-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('343', '日常生活活动能力量表', '7-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('344', '日常生活活动能力量表', '7-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('345', '日常生活活动能力量表', '7-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('346', '日常生活活动能力量表', '8-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('347', '日常生活活动能力量表', '8-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('348', '日常生活活动能力量表', '8-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('349', '日常生活活动能力量表', '8-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('350', '日常生活活动能力量表', '9-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('351', '日常生活活动能力量表', '9-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('352', '日常生活活动能力量表', '9-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('353', '日常生活活动能力量表', '9-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('354', '日常生活活动能力量表', '10-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('355', '日常生活活动能力量表', '10-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('356', '日常生活活动能力量表', '10-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('357', '日常生活活动能力量表', '10-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('482', '中医体质分类与评价', '1-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('483', '中医体质分类与评价', '1-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('484', '中医体质分类与评价', '1-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('485', '中医体质分类与评价', '1-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('486', '中医体质分类与评价', '1-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('487', '中医体质分类与评价', '1-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('488', '中医体质分类与评价', '1-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('489', '中医体质分类与评价', '1-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('490', '中医体质分类与评价', '1-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('491', '中医体质分类与评价', '1-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('492', '中医体质分类与评价', '1-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('493', '中医体质分类与评价', '1-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('494', '中医体质分类与评价', '1-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('495', '中医体质分类与评价', '1-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('496', '中医体质分类与评价', '1-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('497', '中医体质分类与评价', '1-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('498', '中医体质分类与评价', '1-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('499', '中医体质分类与评价', '1-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('500', '中医体质分类与评价', '1-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('501', '中医体质分类与评价', '1-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('502', '中医体质分类与评价', '1-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('503', '中医体质分类与评价', '1-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('504', '中医体质分类与评价', '1-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('505', '中医体质分类与评价', '1-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('506', '中医体质分类与评价', '1-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('507', '中医体质分类与评价', '1-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('508', '中医体质分类与评价', '1-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('509', '中医体质分类与评价', '1-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('510', '中医体质分类与评价', '1-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('511', '中医体质分类与评价', '1-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('512', '中医体质分类与评价', '1-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('513', '中医体质分类与评价', '1-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('514', '中医体质分类与评价', '1-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('515', '中医体质分类与评价', '1-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('516', '中医体质分类与评价', '1-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('517', '中医体质分类与评价', '1-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('518', '中医体质分类与评价', '1-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('519', '中医体质分类与评价', '1-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('520', '中医体质分类与评价', '1-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('521', '中医体质分类与评价', '1-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('522', '中医体质分类与评价', '2-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('523', '中医体质分类与评价', '2-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('524', '中医体质分类与评价', '2-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('525', '中医体质分类与评价', '2-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('526', '中医体质分类与评价', '2-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('527', '中医体质分类与评价', '2-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('528', '中医体质分类与评价', '2-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('529', '中医体质分类与评价', '2-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('530', '中医体质分类与评价', '2-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('531', '中医体质分类与评价', '2-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('532', '中医体质分类与评价', '2-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('533', '中医体质分类与评价', '2-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('534', '中医体质分类与评价', '2-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('535', '中医体质分类与评价', '2-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('536', '中医体质分类与评价', '2-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('537', '中医体质分类与评价', '2-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('538', '中医体质分类与评价', '2-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('539', '中医体质分类与评价', '2-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('540', '中医体质分类与评价', '2-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('541', '中医体质分类与评价', '2-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('542', '中医体质分类与评价', '2-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('543', '中医体质分类与评价', '2-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('544', '中医体质分类与评价', '2-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('545', '中医体质分类与评价', '2-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('546', '中医体质分类与评价', '2-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('547', '中医体质分类与评价', '2-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('548', '中医体质分类与评价', '2-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('549', '中医体质分类与评价', '2-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('550', '中医体质分类与评价', '2-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('551', '中医体质分类与评价', '2-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('552', '中医体质分类与评价', '2-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('553', '中医体质分类与评价', '2-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('554', '中医体质分类与评价', '2-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('555', '中医体质分类与评价', '2-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('556', '中医体质分类与评价', '2-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('557', '中医体质分类与评价', '2-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('558', '中医体质分类与评价', '2-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('559', '中医体质分类与评价', '2-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('560', '中医体质分类与评价', '2-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('561', '中医体质分类与评价', '2-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('562', '中医体质分类与评价', '3-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('563', '中医体质分类与评价', '3-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('564', '中医体质分类与评价', '3-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('565', '中医体质分类与评价', '3-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('566', '中医体质分类与评价', '3-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('567', '中医体质分类与评价', '3-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('568', '中医体质分类与评价', '3-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('569', '中医体质分类与评价', '3-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('570', '中医体质分类与评价', '3-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('571', '中医体质分类与评价', '3-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('572', '中医体质分类与评价', '3-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('573', '中医体质分类与评价', '3-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('574', '中医体质分类与评价', '3-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('575', '中医体质分类与评价', '3-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('576', '中医体质分类与评价', '3-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('577', '中医体质分类与评价', '3-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('578', '中医体质分类与评价', '3-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('579', '中医体质分类与评价', '3-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('580', '中医体质分类与评价', '3-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('581', '中医体质分类与评价', '3-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('582', '中医体质分类与评价', '3-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('583', '中医体质分类与评价', '3-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('584', '中医体质分类与评价', '3-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('585', '中医体质分类与评价', '3-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('586', '中医体质分类与评价', '3-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('587', '中医体质分类与评价', '3-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('588', '中医体质分类与评价', '3-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('589', '中医体质分类与评价', '3-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('590', '中医体质分类与评价', '3-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('591', '中医体质分类与评价', '3-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('592', '中医体质分类与评价', '3-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('593', '中医体质分类与评价', '3-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('594', '中医体质分类与评价', '3-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('595', '中医体质分类与评价', '3-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('596', '中医体质分类与评价', '3-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('602', '中医体质分类与评价', '4-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('603', '中医体质分类与评价', '4-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('604', '中医体质分类与评价', '4-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('605', '中医体质分类与评价', '4-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('606', '中医体质分类与评价', '4-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('607', '中医体质分类与评价', '4-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('608', '中医体质分类与评价', '4-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('609', '中医体质分类与评价', '4-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('610', '中医体质分类与评价', '4-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('611', '中医体质分类与评价', '4-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('612', '中医体质分类与评价', '4-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('613', '中医体质分类与评价', '4-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('614', '中医体质分类与评价', '4-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('615', '中医体质分类与评价', '4-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('616', '中医体质分类与评价', '4-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('617', '中医体质分类与评价', '4-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('618', '中医体质分类与评价', '4-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('619', '中医体质分类与评价', '4-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('620', '中医体质分类与评价', '4-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('621', '中医体质分类与评价', '4-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('622', '中医体质分类与评价', '4-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('623', '中医体质分类与评价', '4-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('624', '中医体质分类与评价', '4-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('625', '中医体质分类与评价', '4-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('626', '中医体质分类与评价', '4-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('627', '中医体质分类与评价', '4-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('628', '中医体质分类与评价', '4-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('629', '中医体质分类与评价', '4-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('630', '中医体质分类与评价', '4-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('631', '中医体质分类与评价', '4-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('632', '中医体质分类与评价', '4-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('633', '中医体质分类与评价', '4-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('634', '中医体质分类与评价', '4-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('635', '中医体质分类与评价', '4-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('636', '中医体质分类与评价', '4-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('637', '中医体质分类与评价', '4-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('638', '中医体质分类与评价', '4-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('639', '中医体质分类与评价', '4-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('640', '中医体质分类与评价', '4-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('641', '中医体质分类与评价', '4-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('642', '中医体质分类与评价', '5-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('643', '中医体质分类与评价', '5-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('644', '中医体质分类与评价', '5-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('645', '中医体质分类与评价', '5-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('646', '中医体质分类与评价', '5-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('647', '中医体质分类与评价', '5-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('648', '中医体质分类与评价', '5-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('649', '中医体质分类与评价', '5-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('650', '中医体质分类与评价', '5-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('651', '中医体质分类与评价', '5-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('652', '中医体质分类与评价', '5-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('653', '中医体质分类与评价', '5-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('654', '中医体质分类与评价', '5-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('655', '中医体质分类与评价', '5-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('656', '中医体质分类与评价', '5-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('657', '中医体质分类与评价', '5-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('658', '中医体质分类与评价', '5-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('659', '中医体质分类与评价', '5-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('660', '中医体质分类与评价', '5-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('661', '中医体质分类与评价', '5-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('662', '中医体质分类与评价', '5-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('663', '中医体质分类与评价', '5-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('664', '中医体质分类与评价', '5-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('665', '中医体质分类与评价', '5-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('666', '中医体质分类与评价', '5-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('667', '中医体质分类与评价', '5-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('668', '中医体质分类与评价', '5-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('669', '中医体质分类与评价', '5-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('670', '中医体质分类与评价', '5-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('671', '中医体质分类与评价', '5-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('672', '中医体质分类与评价', '5-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('673', '中医体质分类与评价', '5-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('674', '中医体质分类与评价', '5-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('675', '中医体质分类与评价', '5-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('676', '中医体质分类与评价', '5-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('677', '中医体质分类与评价', '5-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('678', '中医体质分类与评价', '5-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('679', '中医体质分类与评价', '5-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('680', '中医体质分类与评价', '5-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('681', '中医体质分类与评价', '5-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('682', '中医体质分类与评价', '6-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('683', '中医体质分类与评价', '6-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('684', '中医体质分类与评价', '6-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('685', '中医体质分类与评价', '6-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('686', '中医体质分类与评价', '6-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('687', '中医体质分类与评价', '6-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('688', '中医体质分类与评价', '6-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('689', '中医体质分类与评价', '6-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('690', '中医体质分类与评价', '6-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('691', '中医体质分类与评价', '6-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('692', '中医体质分类与评价', '6-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('693', '中医体质分类与评价', '6-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('694', '中医体质分类与评价', '6-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('695', '中医体质分类与评价', '6-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('696', '中医体质分类与评价', '6-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('697', '中医体质分类与评价', '6-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('698', '中医体质分类与评价', '6-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('699', '中医体质分类与评价', '6-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('700', '中医体质分类与评价', '6-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('701', '中医体质分类与评价', '6-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('702', '中医体质分类与评价', '6-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('703', '中医体质分类与评价', '6-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('704', '中医体质分类与评价', '6-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('705', '中医体质分类与评价', '6-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('706', '中医体质分类与评价', '6-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('707', '中医体质分类与评价', '6-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('708', '中医体质分类与评价', '6-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('709', '中医体质分类与评价', '6-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('710', '中医体质分类与评价', '6-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('711', '中医体质分类与评价', '6-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('712', '中医体质分类与评价', '6-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('713', '中医体质分类与评价', '6-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('714', '中医体质分类与评价', '6-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('715', '中医体质分类与评价', '6-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('716', '中医体质分类与评价', '6-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('722', '中医体质分类与评价', '7-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('723', '中医体质分类与评价', '7-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('724', '中医体质分类与评价', '7-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('725', '中医体质分类与评价', '7-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('726', '中医体质分类与评价', '7-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('727', '中医体质分类与评价', '7-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('728', '中医体质分类与评价', '7-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('729', '中医体质分类与评价', '7-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('730', '中医体质分类与评价', '7-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('731', '中医体质分类与评价', '7-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('732', '中医体质分类与评价', '7-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('733', '中医体质分类与评价', '7-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('734', '中医体质分类与评价', '7-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('735', '中医体质分类与评价', '7-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('736', '中医体质分类与评价', '7-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('737', '中医体质分类与评价', '7-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('738', '中医体质分类与评价', '7-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('739', '中医体质分类与评价', '7-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('740', '中医体质分类与评价', '7-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('741', '中医体质分类与评价', '7-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('742', '中医体质分类与评价', '7-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('743', '中医体质分类与评价', '7-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('744', '中医体质分类与评价', '7-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('745', '中医体质分类与评价', '7-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('746', '中医体质分类与评价', '7-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('747', '中医体质分类与评价', '7-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('748', '中医体质分类与评价', '7-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('749', '中医体质分类与评价', '7-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('750', '中医体质分类与评价', '7-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('751', '中医体质分类与评价', '7-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('752', '中医体质分类与评价', '7-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('753', '中医体质分类与评价', '7-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('754', '中医体质分类与评价', '7-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('755', '中医体质分类与评价', '7-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('756', '中医体质分类与评价', '7-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('762', '中医体质分类与评价', '8-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('763', '中医体质分类与评价', '8-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('764', '中医体质分类与评价', '8-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('765', '中医体质分类与评价', '8-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('766', '中医体质分类与评价', '8-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('767', '中医体质分类与评价', '8-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('768', '中医体质分类与评价', '8-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('769', '中医体质分类与评价', '8-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('770', '中医体质分类与评价', '8-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('771', '中医体质分类与评价', '8-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('772', '中医体质分类与评价', '8-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('773', '中医体质分类与评价', '8-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('774', '中医体质分类与评价', '8-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('775', '中医体质分类与评价', '8-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('776', '中医体质分类与评价', '8-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('777', '中医体质分类与评价', '8-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('778', '中医体质分类与评价', '8-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('779', '中医体质分类与评价', '8-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('780', '中医体质分类与评价', '8-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('781', '中医体质分类与评价', '8-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('782', '中医体质分类与评价', '8-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('783', '中医体质分类与评价', '8-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('784', '中医体质分类与评价', '8-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('785', '中医体质分类与评价', '8-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('786', '中医体质分类与评价', '8-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('787', '中医体质分类与评价', '8-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('788', '中医体质分类与评价', '8-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('789', '中医体质分类与评价', '8-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('790', '中医体质分类与评价', '8-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('791', '中医体质分类与评价', '8-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('792', '中医体质分类与评价', '8-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('793', '中医体质分类与评价', '8-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('794', '中医体质分类与评价', '8-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('795', '中医体质分类与评价', '8-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('796', '中医体质分类与评价', '8-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('802', '慢性病患者生命质量测定量表', '1-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('803', '慢性病患者生命质量测定量表', '1-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('804', '慢性病患者生命质量测定量表', '1-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('805', '慢性病患者生命质量测定量表', '1-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('806', '慢性病患者生命质量测定量表', '1-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('807', '慢性病患者生命质量测定量表', '1-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('808', '慢性病患者生命质量测定量表', '1-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('809', '慢性病患者生命质量测定量表', '1-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('810', '慢性病患者生命质量测定量表', '1-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('811', '慢性病患者生命质量测定量表', '1-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('812', '慢性病患者生命质量测定量表', '1-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('813', '慢性病患者生命质量测定量表', '1-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('814', '慢性病患者生命质量测定量表', '1-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('815', '慢性病患者生命质量测定量表', '1-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('816', '慢性病患者生命质量测定量表', '1-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('817', '慢性病患者生命质量测定量表', '1-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('818', '慢性病患者生命质量测定量表', '1-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('819', '慢性病患者生命质量测定量表', '1-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('820', '慢性病患者生命质量测定量表', '1-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('821', '慢性病患者生命质量测定量表', '1-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('822', '慢性病患者生命质量测定量表', '1-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('823', '慢性病患者生命质量测定量表', '1-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('824', '慢性病患者生命质量测定量表', '1-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('825', '慢性病患者生命质量测定量表', '1-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('826', '慢性病患者生命质量测定量表', '1-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('827', '慢性病患者生命质量测定量表', '1-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('828', '慢性病患者生命质量测定量表', '1-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('829', '慢性病患者生命质量测定量表', '1-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('830', '慢性病患者生命质量测定量表', '1-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('831', '慢性病患者生命质量测定量表', '1-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('832', '慢性病患者生命质量测定量表', '1-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('833', '慢性病患者生命质量测定量表', '1-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('834', '慢性病患者生命质量测定量表', '1-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('835', '慢性病患者生命质量测定量表', '1-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('836', '慢性病患者生命质量测定量表', '1-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('837', '慢性病患者生命质量测定量表', '1-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('838', '慢性病患者生命质量测定量表', '1-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('839', '慢性病患者生命质量测定量表', '1-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('840', '慢性病患者生命质量测定量表', '1-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('841', '慢性病患者生命质量测定量表', '1-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('857', '慢性病患者生命质量测定量表', '2-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('858', '慢性病患者生命质量测定量表', '2-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('859', '慢性病患者生命质量测定量表', '2-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('860', '慢性病患者生命质量测定量表', '2-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('861', '慢性病患者生命质量测定量表', '2-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('862', '慢性病患者生命质量测定量表', '2-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('863', '慢性病患者生命质量测定量表', '2-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('864', '慢性病患者生命质量测定量表', '2-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('865', '慢性病患者生命质量测定量表', '2-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('866', '慢性病患者生命质量测定量表', '2-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('867', '慢性病患者生命质量测定量表', '2-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('868', '慢性病患者生命质量测定量表', '2-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('869', '慢性病患者生命质量测定量表', '2-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('870', '慢性病患者生命质量测定量表', '2-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('871', '慢性病患者生命质量测定量表', '2-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('872', '慢性病患者生命质量测定量表', '2-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('873', '慢性病患者生命质量测定量表', '2-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('874', '慢性病患者生命质量测定量表', '2-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('875', '慢性病患者生命质量测定量表', '2-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('876', '慢性病患者生命质量测定量表', '2-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('877', '慢性病患者生命质量测定量表', '2-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('878', '慢性病患者生命质量测定量表', '2-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('879', '慢性病患者生命质量测定量表', '2-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('880', '慢性病患者生命质量测定量表', '2-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('881', '慢性病患者生命质量测定量表', '2-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('882', '慢性病患者生命质量测定量表', '2-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('883', '慢性病患者生命质量测定量表', '2-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('884', '慢性病患者生命质量测定量表', '2-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('885', '慢性病患者生命质量测定量表', '2-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('886', '慢性病患者生命质量测定量表', '2-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('887', '慢性病患者生命质量测定量表', '2-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('888', '慢性病患者生命质量测定量表', '2-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('889', '慢性病患者生命质量测定量表', '2-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('890', '慢性病患者生命质量测定量表', '2-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('891', '慢性病患者生命质量测定量表', '2-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('892', '慢性病患者生命质量测定量表', '2-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('893', '慢性病患者生命质量测定量表', '2-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('894', '慢性病患者生命质量测定量表', '2-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('895', '慢性病患者生命质量测定量表', '2-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('896', '慢性病患者生命质量测定量表', '2-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('897', '慢性病患者生命质量测定量表', '2-9-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('898', '慢性病患者生命质量测定量表', '2-9-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('899', '慢性病患者生命质量测定量表', '2-9-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('900', '慢性病患者生命质量测定量表', '2-9-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('901', '慢性病患者生命质量测定量表', '2-9-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('902', '慢性病患者生命质量测定量表', '2-10-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('903', '慢性病患者生命质量测定量表', '2-10-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('904', '慢性病患者生命质量测定量表', '2-10-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('905', '慢性病患者生命质量测定量表', '2-10-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('906', '慢性病患者生命质量测定量表', '2-10-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('907', '慢性病患者生命质量测定量表', '2-11-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('908', '慢性病患者生命质量测定量表', '2-11-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('909', '慢性病患者生命质量测定量表', '2-11-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('910', '慢性病患者生命质量测定量表', '2-11-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('911', '慢性病患者生命质量测定量表', '2-11-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('912', '慢性病患者生命质量测定量表', '3-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('913', '慢性病患者生命质量测定量表', '3-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('914', '慢性病患者生命质量测定量表', '3-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('915', '慢性病患者生命质量测定量表', '3-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('916', '慢性病患者生命质量测定量表', '3-1-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('917', '慢性病患者生命质量测定量表', '3-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('918', '慢性病患者生命质量测定量表', '3-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('919', '慢性病患者生命质量测定量表', '3-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('920', '慢性病患者生命质量测定量表', '3-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('921', '慢性病患者生命质量测定量表', '3-2-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('922', '慢性病患者生命质量测定量表', '3-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('923', '慢性病患者生命质量测定量表', '3-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('924', '慢性病患者生命质量测定量表', '3-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('925', '慢性病患者生命质量测定量表', '3-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('926', '慢性病患者生命质量测定量表', '3-3-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('927', '慢性病患者生命质量测定量表', '3-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('928', '慢性病患者生命质量测定量表', '3-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('929', '慢性病患者生命质量测定量表', '3-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('930', '慢性病患者生命质量测定量表', '3-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('931', '慢性病患者生命质量测定量表', '3-4-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('932', '慢性病患者生命质量测定量表', '3-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('933', '慢性病患者生命质量测定量表', '3-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('934', '慢性病患者生命质量测定量表', '3-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('935', '慢性病患者生命质量测定量表', '3-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('936', '慢性病患者生命质量测定量表', '3-5-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('937', '慢性病患者生命质量测定量表', '3-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('938', '慢性病患者生命质量测定量表', '3-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('939', '慢性病患者生命质量测定量表', '3-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('940', '慢性病患者生命质量测定量表', '3-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('941', '慢性病患者生命质量测定量表', '3-6-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('942', '慢性病患者生命质量测定量表', '3-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('943', '慢性病患者生命质量测定量表', '3-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('944', '慢性病患者生命质量测定量表', '3-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('945', '慢性病患者生命质量测定量表', '3-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('946', '慢性病患者生命质量测定量表', '3-7-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('947', '慢性病患者生命质量测定量表', '3-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('948', '慢性病患者生命质量测定量表', '3-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('949', '慢性病患者生命质量测定量表', '3-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('950', '慢性病患者生命质量测定量表', '3-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('951', '慢性病患者生命质量测定量表', '3-8-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('952', '慢性病患者生命质量测定量表', '3-9-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('953', '慢性病患者生命质量测定量表', '3-9-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('954', '慢性病患者生命质量测定量表', '3-9-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('955', '慢性病患者生命质量测定量表', '3-9-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('956', '慢性病患者生命质量测定量表', '3-9-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('957', '慢性病患者生命质量测定量表', '3-10-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('958', '慢性病患者生命质量测定量表', '3-10-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('959', '慢性病患者生命质量测定量表', '3-10-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('960', '慢性病患者生命质量测定量表', '3-10-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('961', '慢性病患者生命质量测定量表', '3-10-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('962', '慢性病患者生命质量测定量表', '3-11-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('963', '慢性病患者生命质量测定量表', '3-11-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('964', '慢性病患者生命质量测定量表', '3-11-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('965', '慢性病患者生命质量测定量表', '3-11-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('966', '慢性病患者生命质量测定量表', '3-11-5', '4', '3');
INSERT INTO `plat_questionnaire` VALUES ('1605', '匹茨堡睡眠质量指数量表', '5-1-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1606', '匹茨堡睡眠质量指数量表', '5-1-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1607', '匹茨堡睡眠质量指数量表', '5-1-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1608', '匹茨堡睡眠质量指数量表', '5-1-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1609', '匹茨堡睡眠质量指数量表', '5-2-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1610', '匹茨堡睡眠质量指数量表', '5-2-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1611', '匹茨堡睡眠质量指数量表', '5-2-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1612', '匹茨堡睡眠质量指数量表', '5-2-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1613', '匹茨堡睡眠质量指数量表', '5-3-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1614', '匹茨堡睡眠质量指数量表', '5-3-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1615', '匹茨堡睡眠质量指数量表', '5-3-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1616', '匹茨堡睡眠质量指数量表', '5-3-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1617', '匹茨堡睡眠质量指数量表', '5-4-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1618', '匹茨堡睡眠质量指数量表', '5-4-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1619', '匹茨堡睡眠质量指数量表', '5-4-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1620', '匹茨堡睡眠质量指数量表', '5-4-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1621', '匹茨堡睡眠质量指数量表', '5-5-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1622', '匹茨堡睡眠质量指数量表', '5-5-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1623', '匹茨堡睡眠质量指数量表', '5-5-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1624', '匹茨堡睡眠质量指数量表', '5-5-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1625', '匹茨堡睡眠质量指数量表', '5-6-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1626', '匹茨堡睡眠质量指数量表', '5-6-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1627', '匹茨堡睡眠质量指数量表', '5-6-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1628', '匹茨堡睡眠质量指数量表', '5-6-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1629', '匹茨堡睡眠质量指数量表', '5-7-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1630', '匹茨堡睡眠质量指数量表', '5-7-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1631', '匹茨堡睡眠质量指数量表', '5-7-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1632', '匹茨堡睡眠质量指数量表', '5-7-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1633', '匹茨堡睡眠质量指数量表', '5-8-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1634', '匹茨堡睡眠质量指数量表', '5-8-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1635', '匹茨堡睡眠质量指数量表', '5-8-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1636', '匹茨堡睡眠质量指数量表', '5-8-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1637', '匹茨堡睡眠质量指数量表', '5-9-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1638', '匹茨堡睡眠质量指数量表', '5-9-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1639', '匹茨堡睡眠质量指数量表', '5-9-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1640', '匹茨堡睡眠质量指数量表', '5-9-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1641', '匹茨堡睡眠质量指数量表', '5-10-1', '0', '3');
INSERT INTO `plat_questionnaire` VALUES ('1642', '匹茨堡睡眠质量指数量表', '5-10-2', '1', '3');
INSERT INTO `plat_questionnaire` VALUES ('1643', '匹茨堡睡眠质量指数量表', '5-10-3', '2', '3');
INSERT INTO `plat_questionnaire` VALUES ('1644', '匹茨堡睡眠质量指数量表', '5-10-4', '3', '3');
INSERT INTO `plat_questionnaire` VALUES ('1645', 'Berg 平衡量表', '1-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1646', 'Berg 平衡量表', '1-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1647', 'Berg 平衡量表', '1-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1648', 'Berg 平衡量表', '1-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1649', 'Berg 平衡量表', '1-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1650', 'Berg 平衡量表', '2-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1651', 'Berg 平衡量表', '2-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1652', 'Berg 平衡量表', '2-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1653', 'Berg 平衡量表', '2-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1654', 'Berg 平衡量表', '2-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1655', 'Berg 平衡量表', '3-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1656', 'Berg 平衡量表', '3-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1657', 'Berg 平衡量表', '3-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1658', 'Berg 平衡量表', '3-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1659', 'Berg 平衡量表', '3-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1660', 'Berg 平衡量表', '4-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1661', 'Berg 平衡量表', '4-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1662', 'Berg 平衡量表', '4-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1663', 'Berg 平衡量表', '4-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1664', 'Berg 平衡量表', '4-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1665', 'Berg 平衡量表', '5-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1666', 'Berg 平衡量表', '5-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1667', 'Berg 平衡量表', '5-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1668', 'Berg 平衡量表', '5-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1669', 'Berg 平衡量表', '5-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1670', 'Berg 平衡量表', '6-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1671', 'Berg 平衡量表', '6-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1672', 'Berg 平衡量表', '6-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1673', 'Berg 平衡量表', '6-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1674', 'Berg 平衡量表', '6-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1675', 'Berg 平衡量表', '7-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1676', 'Berg 平衡量表', '7-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1677', 'Berg 平衡量表', '7-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1678', 'Berg 平衡量表', '7-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1679', 'Berg 平衡量表', '7-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1680', 'Berg 平衡量表', '8-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1681', 'Berg 平衡量表', '8-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1682', 'Berg 平衡量表', '8-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1683', 'Berg 平衡量表', '8-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1684', 'Berg 平衡量表', '8-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1685', 'Berg 平衡量表', '9-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1686', 'Berg 平衡量表', '9-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1687', 'Berg 平衡量表', '9-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1688', 'Berg 平衡量表', '9-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1689', 'Berg 平衡量表', '9-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1690', 'Berg 平衡量表', '10-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1691', 'Berg 平衡量表', '10-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1692', 'Berg 平衡量表', '10-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1693', 'Berg 平衡量表', '10-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1694', 'Berg 平衡量表', '10-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1695', 'Berg 平衡量表', '11-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1696', 'Berg 平衡量表', '11-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1697', 'Berg 平衡量表', '11-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1698', 'Berg 平衡量表', '11-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1699', 'Berg 平衡量表', '11-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1700', 'Berg 平衡量表', '12-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1701', 'Berg 平衡量表', '12-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1702', 'Berg 平衡量表', '12-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1703', 'Berg 平衡量表', '12-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1704', 'Berg 平衡量表', '12-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1705', 'Berg 平衡量表', '13-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1706', 'Berg 平衡量表', '13-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1707', 'Berg 平衡量表', '13-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1708', 'Berg 平衡量表', '13-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1709', 'Berg 平衡量表', '13-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1710', 'Berg 平衡量表', '14-0', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1711', 'Berg 平衡量表', '14-1', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1712', 'Berg 平衡量表', '14-2', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1713', 'Berg 平衡量表', '14-3', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1714', 'Berg 平衡量表', '14-4', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1715', '慢性病病人健康素养量表', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1716', '慢性病病人健康素养量表', '1-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1717', '慢性病病人健康素养量表', '1-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1718', '慢性病病人健康素养量表', '1-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1719', '慢性病病人健康素养量表', '1-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1720', '慢性病病人健康素养量表', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1721', '慢性病病人健康素养量表', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1722', '慢性病病人健康素养量表', '2-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1723', '慢性病病人健康素养量表', '2-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1724', '慢性病病人健康素养量表', '2-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1725', '慢性病病人健康素养量表', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1726', '慢性病病人健康素养量表', '3-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1727', '慢性病病人健康素养量表', '3-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1728', '慢性病病人健康素养量表', '3-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1729', '慢性病病人健康素养量表', '3-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1730', '慢性病病人健康素养量表', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1731', '慢性病病人健康素养量表', '4-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1732', '慢性病病人健康素养量表', '4-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1733', '慢性病病人健康素养量表', '4-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1734', '慢性病病人健康素养量表', '4-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1735', '慢性病病人健康素养量表', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1736', '慢性病病人健康素养量表', '5-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1737', '慢性病病人健康素养量表', '5-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1738', '慢性病病人健康素养量表', '5-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1739', '慢性病病人健康素养量表', '5-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1740', '慢性病病人健康素养量表', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1741', '慢性病病人健康素养量表', '6-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1742', '慢性病病人健康素养量表', '6-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1743', '慢性病病人健康素养量表', '6-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1744', '慢性病病人健康素养量表', '6-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1745', '慢性病病人健康素养量表', '7-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1746', '慢性病病人健康素养量表', '7-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1747', '慢性病病人健康素养量表', '7-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1748', '慢性病病人健康素养量表', '7-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1749', '慢性病病人健康素养量表', '7-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1750', '慢性病病人健康素养量表', '8-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1751', '慢性病病人健康素养量表', '8-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1752', '慢性病病人健康素养量表', '8-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1753', '慢性病病人健康素养量表', '8-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1754', '慢性病病人健康素养量表', '8-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1755', '慢性病病人健康素养量表', '9-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1756', '慢性病病人健康素养量表', '9-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1757', '慢性病病人健康素养量表', '9-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1758', '慢性病病人健康素养量表', '9-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1759', '慢性病病人健康素养量表', '9-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1760', '慢性病病人健康素养量表', '10-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1761', '慢性病病人健康素养量表', '10-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1762', '慢性病病人健康素养量表', '10-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1763', '慢性病病人健康素养量表', '10-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1764', '慢性病病人健康素养量表', '10-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1765', '慢性病病人健康素养量表', '11-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1766', '慢性病病人健康素养量表', '11-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1767', '慢性病病人健康素养量表', '11-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1768', '慢性病病人健康素养量表', '11-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1769', '慢性病病人健康素养量表', '11-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1770', '慢性病病人健康素养量表', '12-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1771', '慢性病病人健康素养量表', '12-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1772', '慢性病病人健康素养量表', '12-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1773', '慢性病病人健康素养量表', '12-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1774', '慢性病病人健康素养量表', '12-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1775', '慢性病病人健康素养量表', '13-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1776', '慢性病病人健康素养量表', '13-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1777', '慢性病病人健康素养量表', '13-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1778', '慢性病病人健康素养量表', '13-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1779', '慢性病病人健康素养量表', '13-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1780', '慢性病病人健康素养量表', '14-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1781', '慢性病病人健康素养量表', '14-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1782', '慢性病病人健康素养量表', '14-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1783', '慢性病病人健康素养量表', '14-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1784', '慢性病病人健康素养量表', '14-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1785', '慢性病病人健康素养量表', '15-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1786', '慢性病病人健康素养量表', '15-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1787', '慢性病病人健康素养量表', '15-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1788', '慢性病病人健康素养量表', '15-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1789', '慢性病病人健康素养量表', '15-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1790', '慢性病病人健康素养量表', '16-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1791', '慢性病病人健康素养量表', '16-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1792', '慢性病病人健康素养量表', '16-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1793', '慢性病病人健康素养量表', '16-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1794', '慢性病病人健康素养量表', '16-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1795', '慢性病病人健康素养量表', '17-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1796', '慢性病病人健康素养量表', '17-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1797', '慢性病病人健康素养量表', '17-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1798', '慢性病病人健康素养量表', '17-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1799', '慢性病病人健康素养量表', '17-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1800', '慢性病病人健康素养量表', '18-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1801', '慢性病病人健康素养量表', '18-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1802', '慢性病病人健康素养量表', '18-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1803', '慢性病病人健康素养量表', '18-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1804', '慢性病病人健康素养量表', '18-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1805', '慢性病病人健康素养量表', '19-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1806', '慢性病病人健康素养量表', '19-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1807', '慢性病病人健康素养量表', '19-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1808', '慢性病病人健康素养量表', '19-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1809', '慢性病病人健康素养量表', '19-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1810', '慢性病病人健康素养量表', '20-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1811', '慢性病病人健康素养量表', '20-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1812', '慢性病病人健康素养量表', '20-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1813', '慢性病病人健康素养量表', '20-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1814', '慢性病病人健康素养量表', '20-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1815', '慢性病病人健康素养量表', '21-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1816', '慢性病病人健康素养量表', '21-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1817', '慢性病病人健康素养量表', '21-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1818', '慢性病病人健康素养量表', '21-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1819', '慢性病病人健康素养量表', '21-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1820', '慢性病病人健康素养量表', '22-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1821', '慢性病病人健康素养量表', '22-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1822', '慢性病病人健康素养量表', '22-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1823', '慢性病病人健康素养量表', '22-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1824', '慢性病病人健康素养量表', '22-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1825', '慢性病病人健康素养量表', '23-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1826', '慢性病病人健康素养量表', '23-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1827', '慢性病病人健康素养量表', '23-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1828', '慢性病病人健康素养量表', '23-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1829', '慢性病病人健康素养量表', '23-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1830', '慢性病病人健康素养量表', '24-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1831', '慢性病病人健康素养量表', '24-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1832', '慢性病病人健康素养量表', '24-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1833', '慢性病病人健康素养量表', '24-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1834', '慢性病病人健康素养量表', '24-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1835', '安全感量表', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1836', '安全感量表', '1-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1837', '安全感量表', '1-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1838', '安全感量表', '1-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1839', '安全感量表', '1-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1840', '安全感量表', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1841', '安全感量表', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1842', '安全感量表', '2-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1843', '安全感量表', '2-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1844', '安全感量表', '2-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1845', '安全感量表', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1846', '安全感量表', '3-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1847', '安全感量表', '3-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1848', '安全感量表', '3-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1849', '安全感量表', '3-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1850', '安全感量表', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1851', '安全感量表', '4-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1852', '安全感量表', '4-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1853', '安全感量表', '4-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1854', '安全感量表', '4-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1855', '安全感量表', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1856', '安全感量表', '5-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1857', '安全感量表', '5-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1858', '安全感量表', '5-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1859', '安全感量表', '5-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1860', '安全感量表', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1861', '安全感量表', '6-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1862', '安全感量表', '6-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1863', '安全感量表', '6-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1864', '安全感量表', '6-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1865', '安全感量表', '7-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1866', '安全感量表', '7-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1867', '安全感量表', '7-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1868', '安全感量表', '7-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1869', '安全感量表', '7-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1870', '安全感量表', '8-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1871', '安全感量表', '8-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1872', '安全感量表', '8-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1873', '安全感量表', '8-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1874', '安全感量表', '8-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1875', '安全感量表', '9-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1876', '安全感量表', '9-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1877', '安全感量表', '9-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1878', '安全感量表', '9-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1879', '安全感量表', '9-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1880', '安全感量表', '10-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1881', '安全感量表', '10-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1882', '安全感量表', '10-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1883', '安全感量表', '10-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1884', '安全感量表', '10-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1885', '安全感量表', '11-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1886', '安全感量表', '11-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1887', '安全感量表', '11-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1888', '安全感量表', '11-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1889', '安全感量表', '11-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1890', '安全感量表', '12-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1891', '安全感量表', '12-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1892', '安全感量表', '12-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1893', '安全感量表', '12-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1894', '安全感量表', '12-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1895', '安全感量表', '13-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1896', '安全感量表', '13-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1897', '安全感量表', '13-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1898', '安全感量表', '13-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1899', '安全感量表', '13-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1900', '安全感量表', '14-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1901', '安全感量表', '14-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1902', '安全感量表', '14-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1903', '安全感量表', '14-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1904', '安全感量表', '14-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1905', '安全感量表', '15-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1906', '安全感量表', '15-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1907', '安全感量表', '15-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1908', '安全感量表', '15-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1909', '安全感量表', '15-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1910', '安全感量表', '16-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1911', '安全感量表', '16-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1912', '安全感量表', '16-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1913', '安全感量表', '16-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1914', '安全感量表', '16-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1915', 'TDL生命质量测定表', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1916', 'TDL生命质量测定表', '1-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1917', 'TDL生命质量测定表', '1-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1918', 'TDL生命质量测定表', '1-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1919', 'TDL生命质量测定表', '1-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1920', 'TDL生命质量测定表', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1921', 'TDL生命质量测定表', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1922', 'TDL生命质量测定表', '2-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1923', 'TDL生命质量测定表', '2-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1924', 'TDL生命质量测定表', '2-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1925', 'TDL生命质量测定表', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1926', 'TDL生命质量测定表', '3-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1927', 'TDL生命质量测定表', '3-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1928', 'TDL生命质量测定表', '3-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1929', 'TDL生命质量测定表', '3-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1930', 'TDL生命质量测定表', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1931', 'TDL生命质量测定表', '4-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1932', 'TDL生命质量测定表', '4-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1933', 'TDL生命质量测定表', '4-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1934', 'TDL生命质量测定表', '4-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1935', 'TDL生命质量测定表', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1936', 'TDL生命质量测定表', '5-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1937', 'TDL生命质量测定表', '5-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1938', 'TDL生命质量测定表', '5-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1939', 'TDL生命质量测定表', '5-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1940', 'TDL生命质量测定表', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1941', 'TDL生命质量测定表', '6-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1942', 'TDL生命质量测定表', '6-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1943', 'TDL生命质量测定表', '6-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1944', 'TDL生命质量测定表', '6-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1945', 'TDL生命质量测定表', '7-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1946', 'TDL生命质量测定表', '7-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1947', 'TDL生命质量测定表', '7-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1948', 'TDL生命质量测定表', '7-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1949', 'TDL生命质量测定表', '7-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1950', 'TDL生命质量测定表', '8-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1951', 'TDL生命质量测定表', '8-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1952', 'TDL生命质量测定表', '8-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1953', 'TDL生命质量测定表', '8-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1954', 'TDL生命质量测定表', '8-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1955', 'TDL生命质量测定表', '9-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1956', 'TDL生命质量测定表', '9-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1957', 'TDL生命质量测定表', '9-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1958', 'TDL生命质量测定表', '9-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1959', 'TDL生命质量测定表', '9-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1960', 'TDL生命质量测定表', '10-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1961', 'TDL生命质量测定表', '10-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1962', 'TDL生命质量测定表', '10-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1963', 'TDL生命质量测定表', '10-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1964', 'TDL生命质量测定表', '10-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1965', 'TDL生命质量测定表', '11-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1966', 'TDL生命质量测定表', '11-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1967', 'TDL生命质量测定表', '11-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1968', 'TDL生命质量测定表', '11-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1969', 'TDL生命质量测定表', '11-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1970', 'TDL生命质量测定表', '12-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1971', 'TDL生命质量测定表', '12-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1972', 'TDL生命质量测定表', '12-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1973', 'TDL生命质量测定表', '12-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1974', 'TDL生命质量测定表', '12-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1975', 'TDL生命质量测定表', '13-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1976', 'TDL生命质量测定表', '13-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1977', 'TDL生命质量测定表', '13-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1978', 'TDL生命质量测定表', '13-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1979', 'TDL生命质量测定表', '13-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1980', 'TDL生命质量测定表', '14-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1981', 'TDL生命质量测定表', '14-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1982', 'TDL生命质量测定表', '14-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1983', 'TDL生命质量测定表', '14-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1984', 'TDL生命质量测定表', '14-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1985', 'TDL生命质量测定表', '15-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1986', 'TDL生命质量测定表', '15-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1987', 'TDL生命质量测定表', '15-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1988', 'TDL生命质量测定表', '15-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1989', 'TDL生命质量测定表', '15-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1990', 'TDL生命质量测定表', '16-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1991', 'TDL生命质量测定表', '16-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1992', 'TDL生命质量测定表', '16-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1993', 'TDL生命质量测定表', '16-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1994', 'TDL生命质量测定表', '16-5', '4', '2');
INSERT INTO `plat_questionnaire` VALUES ('1995', '健康促进生活方式量表', '1-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('1996', '健康促进生活方式量表', '1-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('1997', '健康促进生活方式量表', '1-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('1998', '健康促进生活方式量表', '1-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('1999', '健康促进生活方式量表', '2-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2000', '健康促进生活方式量表', '2-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2001', '健康促进生活方式量表', '2-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2002', '健康促进生活方式量表', '2-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2003', '健康促进生活方式量表', '3-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2004', '健康促进生活方式量表', '3-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2005', '健康促进生活方式量表', '3-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2006', '健康促进生活方式量表', '3-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2007', '健康促进生活方式量表', '4-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2008', '健康促进生活方式量表', '4-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2009', '健康促进生活方式量表', '4-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2010', '健康促进生活方式量表', '4-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2011', '健康促进生活方式量表', '5-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2012', '健康促进生活方式量表', '5-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2013', '健康促进生活方式量表', '5-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2014', '健康促进生活方式量表', '5-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2015', '健康促进生活方式量表', '6-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2016', '健康促进生活方式量表', '6-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2017', '健康促进生活方式量表', '6-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2018', '健康促进生活方式量表', '6-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2019', '健康促进生活方式量表', '7-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2020', '健康促进生活方式量表', '7-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2021', '健康促进生活方式量表', '7-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2022', '健康促进生活方式量表', '7-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2023', '健康促进生活方式量表', '8-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2024', '健康促进生活方式量表', '8-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2025', '健康促进生活方式量表', '8-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2026', '健康促进生活方式量表', '8-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2027', '健康促进生活方式量表', '9-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2028', '健康促进生活方式量表', '9-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2029', '健康促进生活方式量表', '9-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2030', '健康促进生活方式量表', '9-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2031', '健康促进生活方式量表', '10-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2032', '健康促进生活方式量表', '10-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2033', '健康促进生活方式量表', '10-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2034', '健康促进生活方式量表', '10-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2035', '健康促进生活方式量表', '11-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2036', '健康促进生活方式量表', '11-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2037', '健康促进生活方式量表', '11-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2038', '健康促进生活方式量表', '11-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2039', '健康促进生活方式量表', '12-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2040', '健康促进生活方式量表', '12-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2041', '健康促进生活方式量表', '12-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2042', '健康促进生活方式量表', '12-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2043', '健康促进生活方式量表', '13-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2044', '健康促进生活方式量表', '13-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2045', '健康促进生活方式量表', '13-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2046', '健康促进生活方式量表', '13-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2047', '健康促进生活方式量表', '14-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2048', '健康促进生活方式量表', '14-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2049', '健康促进生活方式量表', '14-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2050', '健康促进生活方式量表', '14-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2051', '健康促进生活方式量表', '15-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2052', '健康促进生活方式量表', '15-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2053', '健康促进生活方式量表', '15-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2054', '健康促进生活方式量表', '15-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2055', '健康促进生活方式量表', '16-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2056', '健康促进生活方式量表', '16-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2057', '健康促进生活方式量表', '16-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2058', '健康促进生活方式量表', '16-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2059', '健康促进生活方式量表', '17-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2060', '健康促进生活方式量表', '17-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2061', '健康促进生活方式量表', '17-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2062', '健康促进生活方式量表', '17-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2063', '健康促进生活方式量表', '18-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2064', '健康促进生活方式量表', '18-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2065', '健康促进生活方式量表', '18-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2066', '健康促进生活方式量表', '18-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2067', '健康促进生活方式量表', '19-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2068', '健康促进生活方式量表', '19-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2069', '健康促进生活方式量表', '19-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2070', '健康促进生活方式量表', '19-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2071', '健康促进生活方式量表', '20-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2072', '健康促进生活方式量表', '20-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2073', '健康促进生活方式量表', '20-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2074', '健康促进生活方式量表', '20-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2075', '健康促进生活方式量表', '21-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2076', '健康促进生活方式量表', '21-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2077', '健康促进生活方式量表', '21-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2078', '健康促进生活方式量表', '21-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2079', '健康促进生活方式量表', '22-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2080', '健康促进生活方式量表', '22-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2081', '健康促进生活方式量表', '22-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2082', '健康促进生活方式量表', '22-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2083', '健康促进生活方式量表', '23-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2084', '健康促进生活方式量表', '23-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2085', '健康促进生活方式量表', '23-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2086', '健康促进生活方式量表', '23-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2087', '健康促进生活方式量表', '24-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2088', '健康促进生活方式量表', '24-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2089', '健康促进生活方式量表', '24-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2090', '健康促进生活方式量表', '24-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2091', '健康促进生活方式量表', '25-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2092', '健康促进生活方式量表', '25-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2093', '健康促进生活方式量表', '25-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2094', '健康促进生活方式量表', '25-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2095', '健康促进生活方式量表', '26-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2096', '健康促进生活方式量表', '26-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2097', '健康促进生活方式量表', '26-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2098', '健康促进生活方式量表', '26-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2099', '健康促进生活方式量表', '27-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2100', '健康促进生活方式量表', '27-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2101', '健康促进生活方式量表', '27-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2102', '健康促进生活方式量表', '27-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2103', '健康促进生活方式量表', '28-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2104', '健康促进生活方式量表', '28-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2105', '健康促进生活方式量表', '28-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2106', '健康促进生活方式量表', '28-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2107', '健康促进生活方式量表', '29-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2108', '健康促进生活方式量表', '29-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2109', '健康促进生活方式量表', '29-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2110', '健康促进生活方式量表', '29-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2111', '健康促进生活方式量表', '30-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2112', '健康促进生活方式量表', '30-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2113', '健康促进生活方式量表', '30-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2114', '健康促进生活方式量表', '30-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2115', '健康促进生活方式量表', '31-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2116', '健康促进生活方式量表', '31-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2117', '健康促进生活方式量表', '31-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2118', '健康促进生活方式量表', '31-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2119', '健康促进生活方式量表', '32-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2120', '健康促进生活方式量表', '32-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2121', '健康促进生活方式量表', '32-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2122', '健康促进生活方式量表', '32-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2123', '健康促进生活方式量表', '33-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2124', '健康促进生活方式量表', '33-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2125', '健康促进生活方式量表', '33-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2126', '健康促进生活方式量表', '33-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2127', '健康促进生活方式量表', '34-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2128', '健康促进生活方式量表', '34-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2129', '健康促进生活方式量表', '34-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2130', '健康促进生活方式量表', '34-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2131', '健康促进生活方式量表', '35-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2132', '健康促进生活方式量表', '35-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2133', '健康促进生活方式量表', '35-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2134', '健康促进生活方式量表', '35-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2135', '健康促进生活方式量表', '36-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2136', '健康促进生活方式量表', '36-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2137', '健康促进生活方式量表', '36-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2138', '健康促进生活方式量表', '36-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2139', '健康促进生活方式量表', '37-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2140', '健康促进生活方式量表', '37-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2141', '健康促进生活方式量表', '37-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2142', '健康促进生活方式量表', '37-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2143', '健康促进生活方式量表', '38-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2144', '健康促进生活方式量表', '38-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2145', '健康促进生活方式量表', '38-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2146', '健康促进生活方式量表', '38-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2147', '健康促进生活方式量表', '39-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2148', '健康促进生活方式量表', '39-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2149', '健康促进生活方式量表', '39-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2150', '健康促进生活方式量表', '39-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2151', '健康促进生活方式量表', '40-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2152', '健康促进生活方式量表', '40-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2153', '健康促进生活方式量表', '40-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2154', '健康促进生活方式量表', '40-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2155', '健康促进生活方式量表', '41-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2156', '健康促进生活方式量表', '41-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2157', '健康促进生活方式量表', '41-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2158', '健康促进生活方式量表', '41-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2159', '健康促进生活方式量表', '42-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2160', '健康促进生活方式量表', '42-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2161', '健康促进生活方式量表', '42-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2162', '健康促进生活方式量表', '42-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2163', '健康促进生活方式量表', '43-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2164', '健康促进生活方式量表', '43-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2165', '健康促进生活方式量表', '43-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2166', '健康促进生活方式量表', '43-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2167', '健康促进生活方式量表', '44-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2168', '健康促进生活方式量表', '44-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2169', '健康促进生活方式量表', '44-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2170', '健康促进生活方式量表', '44-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2171', '健康促进生活方式量表', '45-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2172', '健康促进生活方式量表', '45-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2173', '健康促进生活方式量表', '45-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2174', '健康促进生活方式量表', '45-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2175', '健康促进生活方式量表', '46-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2176', '健康促进生活方式量表', '46-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2177', '健康促进生活方式量表', '46-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2178', '健康促进生活方式量表', '46-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2179', '健康促进生活方式量表', '47-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2180', '健康促进生活方式量表', '47-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2181', '健康促进生活方式量表', '47-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2182', '健康促进生活方式量表', '47-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2183', '健康促进生活方式量表', '48-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2184', '健康促进生活方式量表', '48-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2185', '健康促进生活方式量表', '48-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2186', '健康促进生活方式量表', '48-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2187', '健康促进生活方式量表', '49-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2188', '健康促进生活方式量表', '49-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2189', '健康促进生活方式量表', '49-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2190', '健康促进生活方式量表', '49-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2191', '健康促进生活方式量表', '50-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2192', '健康促进生活方式量表', '50-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2193', '健康促进生活方式量表', '50-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2194', '健康促进生活方式量表', '50-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2195', '健康促进生活方式量表', '51-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2196', '健康促进生活方式量表', '51-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2197', '健康促进生活方式量表', '51-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2198', '健康促进生活方式量表', '51-4', '3', '2');
INSERT INTO `plat_questionnaire` VALUES ('2199', '健康促进生活方式量表', '52-1', '0', '2');
INSERT INTO `plat_questionnaire` VALUES ('2200', '健康促进生活方式量表', '52-2', '1', '2');
INSERT INTO `plat_questionnaire` VALUES ('2201', '健康促进生活方式量表', '52-3', '2', '2');
INSERT INTO `plat_questionnaire` VALUES ('2202', '健康促进生活方式量表', '52-4', '3', '2');

-- ----------------------------
-- Table structure for plat_ques_sport_study
-- ----------------------------
DROP TABLE IF EXISTS `plat_ques_sport_study`;
CREATE TABLE `plat_ques_sport_study` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `type` int(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `parent_id` bigint(32) DEFAULT NULL,
  `lesson_id` bigint(32) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=507 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_ques_sport_study
-- ----------------------------
INSERT INTO `plat_ques_sport_study` VALUES ('-1', '3', '转身运动测试.mp4', '178', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('0', '3', '米字操.mp4', '187', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('1', '1', 'BMI概念.pdf', null, '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('2', '2', '检测方法', null, '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('3', '1', '国围检测.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('4', '1', 'BMI检测.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('5', '1', '皮褶厚度.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('6', '2', '训练方法', null, '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('7', '2', '增肌运动', '6', '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('8', '1', '抗阻运动.pdf', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('9', '1', '柔韧性运动.pdf', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('10', '1', '减脂运动', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('11', '1', '其它肌肉训练.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('12', '1', '深蹲+俯卧撑.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('13', '1', '增肌运动概念.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('14', '1', '坐位抬腿.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('15', '1', '静力靠墙蹲.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('16', '1', '注意事项.pdf', null, '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('17', '1', '反应力概念.pdf', null, '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('18', '2', '检测方法', null, '402', '');
INSERT INTO `plat_ques_sport_study` VALUES ('20', '1', '十字跳测验.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('21', '1', '选择反应时测试.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('22', '1', '徒手反应性测试（抓尺实验）.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('23', '2', '训练方法', null, '402', '');
INSERT INTO `plat_ques_sport_study` VALUES ('24', '2', '初级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('25', '2', '中级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('26', '2', '高级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('27', '1', '口令式训练.pdf', '24', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('28', '1', '智力游戏.pdf', '24', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('29', '1', '口令式训练2.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('30', '1', '球类训练.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('31', '1', '球类训练.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('32', '1', '手接橡胶弹力球.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('33', '1', '限时折返跑.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('34', '1', '镜面肢体反应训练.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('35', '1', '闭眼转身后踢球或取物.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('36', '1', '肺功能理论知识.pdf', null, '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('37', '2', '检测方法', null, '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('38', '1', '\r\n徒手肺功能测试（憋气实验）.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('39', '1', '\r\n肺活量测试.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('40', '1', '\r\n\r\n日常生活能力评估.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('41', '2', '训练方法', null, '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('42', '2', '初级', '41', '403', '');
INSERT INTO `plat_ques_sport_study` VALUES ('43', '2', '中级', '41', '403', '');
INSERT INTO `plat_ques_sport_study` VALUES ('44', '2', '高级', '41', '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('45', '2', '互动游戏', '41', '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('46', '1', '坐姿深呼吸训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('47', '1', '\r\n行走深呼吸训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('48', '1', '缩唇呼气训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('49', '1', '\r\n腹式呼吸.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('50', '1', '\r\n呼气训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('51', '1', '\r\n（上肢）提重物训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('52', '1', '\r\n提重物方式与训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('53', '1', '\r\n吸气训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('54', '1', '\r\n箭步蹲.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('55', '1', '站立耸肩.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('56', '1', '自重深蹲.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('57', '1', '\r\n仰卧位双下肢屈髋屈膝训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('58', '1', '\r\n呼吸体操.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('59', '1', '\r\n\r\n全身呼吸操--适用于COPD患者\r\n.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('60', '1', '\r\n\r\n家居锻炼.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('61', '1', '\r\n\r\n仰卧腹腔负荷.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('62', '1', '\r\n\r\n仰卧胸腔负荷.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('63', '1', '\r\n\r\n有氧运动训练.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('64', '1', '\r\n\r\n\r\n吹棉花游戏.pdf', '45', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('65', '1', '\r\n\r\n\r\n吹蜡烛比赛.pdf', '45', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('66', '1', '核心力概念.pdf', null, '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('67', '2', '核心力能力检测', null, '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('68', '1', '\r\n仰卧起坐.pdf', '67', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('69', '2', '\r\n核心力能力训练方法', null, '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('70', '2', '中级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('71', '2', '高级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('72', '2', '初级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('73', '1', '\r\n双脚触球仰桥.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('74', '1', '\r\n单脚触球仰桥\r\n.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('75', '1', '\r\n后背触球仰桥\r\n.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('76', '1', '\r\n平衡垫俯卧撑.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('77', '1', '\r\n\r\n健身球俯卧撑.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('78', '1', '\r\n\r\n\r\n胸腹触球俯桥.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('79', '1', '\r\n\r\n\r\n\r\n平衡垫平衡式.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('80', '1', '\r\n\r\n\r\n\r\n\r\n躯干肌肉系统\r\n.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('81', '1', '\r\n\r\n\r\n\r\n强化腹部训练\r\n.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('82', '1', '\r\n\r\n\r\n核心肌肉.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('83', '1', '\r\n\r\n\r\n\r\n斜腹部&腹部训练.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('84', '1', '\r\n\r\n\r\n\r\n腹部练习.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('85', '1', '\r\n\r\n\r\n\r\n斜腹肌.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('86', '1', '\r\n\r\n\r\n\r\n\r\n腰部强化训练.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('87', '1', '\r\n\r\n\r\n\r\n\r\n\r\n直腿仰卧举腿\r\n.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('88', '1', '\r\n\r\n\r\n\r\n\r\n腹肌板\r\n.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('89', '1', '\r\n\r\n\r\n\r\n屈腿仰卧举腿.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('90', '1', '\r\n\r\n\r\n\r\n\r\n抱头仰卧起坐.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('91', '1', '\r\n\r\n\r\n\r\n\r\n\r\n腹肌的静力性练习.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('92', '1', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n“小飞燕”练习.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('93', '1', '局部力量概念.pdf', null, '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('94', '2', '局部力量检测方法', null, '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('95', '1', '背肌耐力评定\r\n.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('96', '1', '腹肌耐力评定\r\n.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('97', '1', '爆发力检测.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('98', '1', '握力测试.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('99', '1', '握力计测试.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('100', '1', '俯卧撑.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('101', '1', '引体向上.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('102', '2', '局部力量训练方法', null, '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('103', '2', '初级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('104', '2', '高级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('105', '2', '中级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('106', '1', '前后踮脚尖.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('107', '1', '背部练习.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('108', '1', '\r\n站姿后抬腿\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('109', '1', '站立耸肩.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('110', '1', '站姿前平举.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('111', '1', '半蹲斜下拉.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('112', '1', '\r\n直臂前下压\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('113', '1', '直腿硬拉\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('114', '1', '站姿肘碰膝.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('115', '1', '弹力带扩胸.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('116', '1', '俯身展体\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('117', '1', '\r\n俯卧两头起\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('118', '1', '\r\n负重体侧屈.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('119', '1', '\r\n俯卧腿弯举\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('120', '1', '仰卧举腿.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('121', '1', '站姿负重提踵\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('122', '1', '窄握距卧推.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('123', '1', '俯卧挺身.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('124', '1', '半蹲.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('125', '1', '俯立划船.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('126', '1', '弯举\r\n.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('127', '1', '直腿使拉\r\n.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('128', '1', '\r\n双臂屈伸.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('129', '1', '\r\n俯卧撑.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('130', '1', '\r\n纵跳.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('131', '1', '\r\n仰卧起坐.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('132', '1', '\r\n仰卧举腿.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('133', '1', '\r\n平衡能力概念.pdf', null, '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('134', '2', '\r\n\r\n平衡能力评估检测', null, '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('135', '1', '\r\n成年人闭眼单脚站立.pdf', '134', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('136', '1', '\r\n\r\n徒手平衡性测试.pdf', '134', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('137', '2', '平衡能力评估训练', null, '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('138', '2', '初级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('139', '2', '高级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('140', '2', '中级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('141', '1', '\r\n\r\n穿袜子、绑鞋带.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('142', '1', '\r\n\r\n单脚站立.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('143', '1', '\r\n\r\n垫上初级训练\r\n.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('144', '1', '\r\n端水走路.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('145', '1', '\r\n\r\n双、单脚尖脚跟站立.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('146', '1', '\r\n\r\n坐姿平衡.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('147', '1', '\r\n\r\n弹力带腿内收\r\n.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('148', '1', '\r\n弹力带腿前踢.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('149', '1', '\r\n弹力带腿后踢\r\n.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('150', '1', '弹力带腿外展.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('151', '1', '\r\n弹力带蹲起训练.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('152', '1', '\r\n倒走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('153', '1', '\r\n侧走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('154', '1', '\r\n\r\n独立前踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('155', '1', '\r\n\r\n独立后踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('156', '1', '\r\n\r\n高尔夫球摆臂训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('157', '1', '\r\n\r\n单脚蹲起.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('158', '1', '\r\n\r\n金鸡独立.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('159', '1', '\r\n\r\n弓步训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('160', '1', '\r\n\r\n腾空后踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('161', '1', '\r\n\r\n头顶物走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('162', '1', '\r\n\r\n弯腰伸臂.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('163', '1', '\r\n\r\n网球摆臂训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('164', '1', '\r\n\r\n燕式平衡.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('165', '1', '\r\n\r\n直线猫爬.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('166', '1', '\r\n\r\n足球踢腿训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('167', '1', '\r\n\r\n走直线.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('168', '1', '柔韧性功能概念.pdf', null, '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('169', '2', '测试方法', null, '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('170', '1', '肩关节柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('171', '1', '脊柱前屈柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('172', '1', '髋部和脊柱屈伸功能测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('173', '1', '脊柱转向柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('174', '1', '颈、躯干和骨盆转动的流畅性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('175', '1', '肩关节功能测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('176', '2', '\r\n训练方法', null, '407', null);
INSERT INTO `plat_ques_sport_study` VALUES ('177', '2', '初级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('178', '2', '中级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('179', '2', '高级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('180', '2', '1、颈部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('181', '2', '2、肩部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('182', '2', '3、胸，腹部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('183', '2', '4、脊柱拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('184', '2', '5、胯，腹部练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('185', '2', '6、腿部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('186', '2', '7、加大膝关节的ROM的练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('187', '2', '1、颈部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('188', '2', '2、肩部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('189', '2', '3、躯干部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('190', '2', '4、腿部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('191', '2', '5、裸关节', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('192', '2', '6、前表线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('193', '2', '7、手臂线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('194', '2', '8、前深筋膜拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('195', '2', '9、后表线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('196', '2', '10、腰肌拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('197', '2', '11、跟腱拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('198', '2', '1、前表线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('199', '2', '2、后表线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('200', '2', '3、体侧线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('201', '2', '4、臂前深线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('202', '2', '5、前深线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('203', '2', '6、肌肉加强训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('204', '2', '7、弹力带训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('205', '2', '8、弹射训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('206', '2', '9、互动游戏', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('207', '1', '注意事项.pdf', null, '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('208', '1', '颈回旋运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('209', '1', '颈部助力练习\n\n.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('210', '1', '颈屈伸运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('211', '1', '颈侧倾运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('212', '1', '颈左右旋转运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('213', '1', '爬墙练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('214', '1', '肩前屈助力练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('215', '1', '肩前屈练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('216', '1', '拉伸二：跪姿后屈拉伸.pdf', '182', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('217', '1', '拉伸一：站立后仰拉伸.pdf', '182', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('218', '1', '跪撑.pdf', '183', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('219', '1', '身体站直.pdf', '183', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('220', '1', '抬腿画圈练习.pdf', '184', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('221', '1', '鞠躬式拉伸.pdf', '184', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('222', '1', '腹股沟部拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('223', '1', '股四头肌拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('224', '1', '奈绳肌拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('225', '1', '小腿部拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('226', '1', '膝关节屈曲练习.pdf', '186', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('227', '1', '米字操.pdf', '187', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('228', '1', '背后拉手.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('229', '1', '直臂压肩.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('230', '1', '振臂运动.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('231', '1', '头后单手拉肩.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('232', '1', '肩关节的PNF练习.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('233', '1', '转体运动.pdf', '189', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('234', '1', '侧压.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('235', '1', '奈绳肌的PNFl练习.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('236', '1', '坐式体前倾.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('237', '1', '坐位体前屈.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('238', '1', '跪坐脚踝.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('239', '1', '脚跟走.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('240', '1', '脚尖走.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('241', '1', '股四头肌拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('242', '1', '伸展拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('243', '1', '前表线拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('244', '1', '肩部拉伸.pdf', '193', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('245', '1', '后表线滚动.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('246', '1', '前深线滚动.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('247', '1', '内收肌拉伸.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('248', '1', '筋膜.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('249', '1', '火烈鸟式一.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('250', '1', '猫式二.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('251', '1', '猫式一.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('252', '1', '火烈鸟式二.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('253', '1', '腰肌拉伸.pdf', '196', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('254', '1', '跟腱拉伸二.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('255', '1', '跟腱拉伸一.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('256', '1', '跟腱拉伸三.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('257', '1', '股四头肌.pdf', '198', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('258', '1', '胫前肌.pdf', '198', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('259', '1', '大腿后侧.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('260', '1', '臀部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('261', '1', '后背部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('262', '1', '枕部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('263', '1', '小腿后侧.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('264', '1', '足底筋膜.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('265', '1', '髂胫束.pdf', '200', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('266', '1', '胸廓外侧.pdf', '200', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('267', '1', '胸小肌.pdf', '201', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('268', '1', '内收肌.pdf', '202', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('269', '1', '海星式.pdf', '203', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('270', '1', '全身螺旋式.pdf', '203', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('271', '1', '二式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('272', '1', '三式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('273', '1', '一式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('274', '1', '多方向跳跃.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('275', '1', '火箭腿.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('276', '1', '后背部跳跃.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('277', '1', '屈臂撑墙.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('278', '1', '推手练习.pdf', '206', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('279', '1', '关氏操.pdf', '206', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('280', '1', '协调能力概念.pdf', null, '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('281', '2', '\r\n检测方法', null, '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('282', '1', '跟膝胫试验.pdf', '281', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('283', '1', '指鼻和指他人指试验.pdf', '281', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('284', '2', '\r\n训练方法', null, '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('285', '2', '\r\n初级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('286', '2', '\r\n中级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('287', '2', '\r\n高级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('288', '1', '交替屈肘.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('289', '1', '侧面交替摸肩.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('290', '1', '交替手上举.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('291', '1', '前臂交替旋前、旋后.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('292', '1', '屈肘交替摸肩.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('293', '1', '双脚交替触地.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('294', '1', '双腿屈膝剪刀腿.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('295', '1', '双腿直膝剪刀腿.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('296', '1', '原地踏步走.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('297', '1', '对指练习.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('298', '1', '弓箭步转身运动.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('299', '1', '五指敲击.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('300', '1', '原地高抬腿.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('301', '1', '原地小步跑.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('302', '1', '拳掌相击.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('303', '1', '左右跨跳.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('304', '1', '指鼻练习.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('305', '1', '手眼协调练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('306', '1', '节律性动作练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('307', '1', '身体动作练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('308', '1', '开合跳.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('309', '1', '心功能能力概念.pdf', null, '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('310', '2', '\r\n心功能能力训练方法', null, '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('311', '2', '心功能能力检测方法', null, '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('312', '1', '300米健身走.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('313', '1', '\r\n六分钟步行实验.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('314', '1', '\r\n徒手心功能测试.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('315', '1', '台阶实验测试方法.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('316', '2', '初级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('317', '2', '中级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('318', '2', '高级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('319', '1', '慢跑.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('320', '1', '体感游戏.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('321', '1', '健步走.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('322', '1', '跳绳.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('323', '1', '功率自行车.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('324', '1', '高抬腿.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('325', '1', '波比跳.pdf', '318', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('326', '1', '\r\nX-CO双合跳.pdf', '318', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('327', '1', '本体感觉概念.pdf', null, '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('328', '2', '本体感觉测试方法', null, '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('329', '2', '本体感觉训练方法', null, '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('330', '1', '闭眼复杂重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('331', '1', '闭眼重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('332', '1', '\r\n睁眼复杂重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('333', '1', '睁眼重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('334', '2', '初级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('335', '2', '中级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('336', '2', '高级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('337', '1', '对指练习.pdf', '334', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('338', '1', '指鼻练习.pdf', '334', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('339', '1', '“螃蟹先生”游戏.pdf', '335', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('340', '1', '“摸一摸”游戏.pdf', '335', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('341', '1', '闭目踏步训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('342', '1', '闭眼直线行走.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('343', '1', '闭目单腿站立训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('344', '1', '倒着走训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('345', '1', '脚踩鹅卵石行走训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('346', '1', '黑箱触物.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('347', '1', '\r\n跨越障碍物训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('348', '1', '\r\n上下楼梯控制训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('349', '1', '踏步训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('350', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('351', '1', '跳跃训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('352', '1', '本体感觉概念.pdf', null, '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('353', '2', '本体感觉测试方法', null, '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('354', '2', '本体感觉训练方法', null, '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('355', '1', '闭眼复杂重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('356', '1', '闭眼重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('357', '1', '\r\n睁眼复杂重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('358', '1', '睁眼重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('359', '2', '初级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('360', '2', '中级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('361', '2', '高级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('362', '1', '对指练习.pdf', '359', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('363', '1', '指鼻练习.pdf', '359', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('364', '1', '“螃蟹先生”游戏.pdf', '360', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('365', '1', '“摸一摸”游戏.pdf', '360', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('366', '1', '闭目踏步训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('367', '1', '闭眼直线行走.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('368', '1', '闭目单腿站立训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('369', '1', '倒着走训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('370', '1', '脚踩鹅卵石行走训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('371', '1', '黑箱触物.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('372', '1', '\r\n跨越障碍物训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('373', '1', '\r\n上下楼梯控制训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('374', '1', '踏步训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('375', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('376', '1', '跳跃训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('377', '1', '本体感觉概念.pdf', null, '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('378', '2', '本体感觉测试方法', null, '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('379', '2', '本体感觉训练方法', null, '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('380', '1', '闭眼复杂重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('381', '1', '闭眼重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('382', '1', '\r\n睁眼复杂重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('383', '1', '睁眼重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('384', '2', '初级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('385', '2', '中级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('386', '2', '高级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('387', '1', '对指练习.pdf', '334', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('388', '1', '指鼻练习.pdf', '334', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('389', '1', '“螃蟹先生”游戏.pdf', '335', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('390', '1', '“摸一摸”游戏.pdf', '335', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('391', '1', '闭目踏步训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('392', '1', '闭眼直线行走.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('393', '1', '闭目单腿站立训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('394', '1', '倒着走训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('395', '1', '脚踩鹅卵石行走训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('396', '1', '黑箱触物.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('397', '1', '\r\n跨越障碍物训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('398', '1', '\r\n上下楼梯控制训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('399', '1', '踏步训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('400', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('401', '1', '跳跃训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('402', '1', '本体感觉概念.pdf', null, '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('403', '2', '本体感觉测试方法', null, '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('404', '2', '本体感觉训练方法', null, '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('405', '1', '闭眼复杂重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('406', '1', '闭眼重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('407', '1', '\r\n睁眼复杂重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('408', '1', '睁眼重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('409', '2', '初级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('410', '2', '中级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('411', '2', '高级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('412', '1', '对指练习.pdf', '409', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('413', '1', '指鼻练习.pdf', '409', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('414', '1', '“螃蟹先生”游戏.pdf', '410', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('415', '1', '“摸一摸”游戏.pdf', '410', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('416', '1', '闭目踏步训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('417', '1', '闭眼直线行走.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('418', '1', '闭目单腿站立训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('419', '1', '倒着走训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('420', '1', '脚踩鹅卵石行走训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('421', '1', '黑箱触物.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('422', '1', '\r\n跨越障碍物训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('423', '1', '\r\n上下楼梯控制训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('424', '1', '踏步训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('425', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('426', '1', '跳跃训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('427', '1', '本体感觉概念.pdf', null, '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('428', '2', '本体感觉测试方法', null, '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('429', '2', '本体感觉训练方法', null, '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('430', '1', '闭眼复杂重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('431', '1', '闭眼重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('432', '1', '\r\n睁眼复杂重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('433', '1', '睁眼重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('434', '2', '初级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('435', '2', '中级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('436', '2', '高级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('437', '1', '对指练习.pdf', '434', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('438', '1', '指鼻练习.pdf', '434', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('439', '1', '“螃蟹先生”游戏.pdf', '435', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('440', '1', '“摸一摸”游戏.pdf', '435', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('441', '1', '闭目踏步训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('442', '1', '闭眼直线行走.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('443', '1', '闭目单腿站立训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('444', '1', '倒着走训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('445', '1', '脚踩鹅卵石行走训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('446', '1', '黑箱触物.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('447', '1', '\r\n跨越障碍物训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('448', '1', '\r\n上下楼梯控制训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('449', '1', '踏步训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('450', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('451', '1', '跳跃训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('452', '1', '本体感觉概念.pdf', null, '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('453', '2', '本体感觉测试方法', null, '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('454', '2', '本体感觉训练方法', null, '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('455', '1', '闭眼复杂重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('456', '1', '闭眼重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('457', '1', '\r\n睁眼复杂重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('458', '1', '睁眼重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('459', '2', '初级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('460', '2', '中级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('461', '2', '高级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('462', '1', '对指练习.pdf', '459', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('463', '1', '指鼻练习.pdf', '459', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('464', '1', '“螃蟹先生”游戏.pdf', '460', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('465', '1', '“摸一摸”游戏.pdf', '460', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('466', '1', '闭目踏步训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('467', '1', '闭眼直线行走.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('468', '1', '闭目单腿站立训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('469', '1', '倒着走训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('470', '1', '脚踩鹅卵石行走训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('471', '1', '黑箱触物.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('472', '1', '\r\n跨越障碍物训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('473', '1', '\r\n上下楼梯控制训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('474', '1', '踏步训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('475', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('476', '1', '跳跃训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('477', '1', '本体感觉概念.pdf', null, '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('478', '2', '本体感觉测试方法', null, '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('479', '2', '本体感觉训练方法', null, '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('480', '1', '闭眼复杂重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('481', '1', '闭眼重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('482', '1', '\r\n睁眼复杂重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('483', '1', '睁眼重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('484', '2', '初级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('485', '2', '中级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('486', '2', '高级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('487', '1', '对指练习.pdf', '484', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('488', '1', '指鼻练习.pdf', '484', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('489', '1', '“螃蟹先生”游戏.pdf', '485', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('490', '1', '“摸一摸”游戏.pdf', '485', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('491', '1', '闭目踏步训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('492', '1', '闭眼直线行走.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('493', '1', '闭目单腿站立训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('494', '1', '倒着走训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('495', '1', '脚踩鹅卵石行走训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('496', '1', '黑箱触物.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('497', '1', '\r\n跨越障碍物训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('498', '1', '\r\n上下楼梯控制训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('499', '1', '踏步训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('500', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('501', '1', '跳跃训练.pdf', '486', '416', 'lazyTraining/');

-- ----------------------------
-- Table structure for plat_role
-- ----------------------------
DROP TABLE IF EXISTS `plat_role`;
CREATE TABLE `plat_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `role_id` int(2) NOT NULL COMMENT '角色',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of plat_role
-- ----------------------------
INSERT INTO `plat_role` VALUES ('1', '1', '超级管理员');
INSERT INTO `plat_role` VALUES ('2', '2', '普通管理员');
INSERT INTO `plat_role` VALUES ('3', '3', '教师');
INSERT INTO `plat_role` VALUES ('4', '4', '学生');

-- ----------------------------
-- Table structure for plat_user
-- ----------------------------
DROP TABLE IF EXISTS `plat_user`;
CREATE TABLE `plat_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(2000) NOT NULL COMMENT '用户账号',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `nick_name` varchar(200) NOT NULL COMMENT '用户昵称',
  `role` int(2) NOT NULL COMMENT '角色',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `mobile` varchar(200) DEFAULT '' COMMENT '手机号码',
  `email` varchar(200) DEFAULT '' COMMENT '邮箱',
  `status` int(2) DEFAULT NULL COMMENT '帐号状态（0正常 1停用）',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `college_id` bigint(20) DEFAULT NULL COMMENT '学院id',
  `major_id` bigint(20) DEFAULT NULL COMMENT '专业id',
  `sex` varchar(5) DEFAULT NULL COMMENT '姓名',
  `class_id` bigint(20) DEFAULT NULL COMMENT '班级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of plat_user
-- ----------------------------
INSERT INTO `plat_user` VALUES ('1', 'hw_root', '96e79218965eb72c92a549dd5a330112', '王总', '1', '超级管理员', '', '1@qq.com', '1', '2025-01-14 20:00:00', '', '2024-07-03 15:03:36', 'hw_root', '2024-07-10 17:47:39', '', '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('2', 'admin1', '96e79218965eb72c92a549dd5a330112', '吕总', '2', '普通管理员', '', '', '1', '2024-09-30 09:43:29', '', '2024-07-03 15:03:36', '王总', '2024-12-13 12:55:24', '', '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('103', 'admin2', '96e79218965eb72c92a549dd5a330112', '司总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:06:17', '王总', '2024-09-13 22:32:38', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('106', 'teacher_mei', '96e79218965eb72c92a549dd5a330112', '美老师', '3', '教师', '18888888888', '', '1', '2024-12-18 20:51:18', null, '2024-07-10 18:24:32', 'teacher_mei', '2024-12-13 15:37:11', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('107', 'admin3', '96e79218965eb72c92a549dd5a330112', '孙总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:24:41', '王总', '2024-12-13 12:55:20', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('108', 'admin4', '96e79218965eb72c92a549dd5a330112', '钱总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:24:54', '王总', '2024-12-13 12:55:17', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('109', 'teacher_zhao', '96e79218965eb72c92a549dd5a330112', '赵老师', '3', '教师', '', '', '1', '2024-12-13 14:50:12', null, '2024-07-10 18:25:04', '王总', '2024-12-13 12:55:27', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('110', 'student_1', '96e79218965eb72c92a549dd5a330112', '同学1', '4', '学生', '', '', '1', '2025-01-14 20:04:38', null, '2024-07-10 20:02:21', '王总', '2024-12-18 19:19:22', null, '21', '2', null, '26');
INSERT INTO `plat_user` VALUES ('111', 'student_2', '96e79218965eb72c92a549dd5a330112', '同学2', '4', '学生', '', '243', '1', null, null, '2024-07-10 20:02:29', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('113', 'student_3', '96e79218965eb72c92a549dd5a330112', '同学3', '4', '学生', '', '1', '1', null, '关羽1111111111111111111111111111', '2024-07-11 20:11:43', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('117', 'student_4', '96e79218965eb72c92a549dd5a330112', '同学4', '4', '学生', '', '', '1', '2024-08-20 15:18:59', '关羽1111111111111111111111111111', '2024-07-12 14:45:28', '王总', '2024-12-18 19:20:23', null, '21', '2', null, '25');
INSERT INTO `plat_user` VALUES ('118', 'student_5', '96e79218965eb72c92a549dd5a330112', '同学5', '4', '学生', '', '', '1', null, '关羽1111111111111111111111111111', '2024-07-12 15:43:57', '王总', '2024-12-18 19:29:37', null, '21', '2', null, '26');
INSERT INTO `plat_user` VALUES ('119', 'student_6', '96e79218965eb72c92a549dd5a330112', '同学6', '4', '学生', '', '', '1', null, '关羽1111111111111111111111111111', '2024-07-12 15:56:10', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('120', 'student_7', '96e79218965eb72c92a549dd5a330112', '同学7', '4', '学生', '', '', '1', '2024-12-18 20:17:44', '关羽1111111111111111111111111111', '2024-07-12 15:56:14', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('121', 'student_8', '96e79218965eb72c92a549dd5a330112', '同学8', '4', '学生', '', '', '1', '2024-12-18 19:30:05', '关羽1111111111111111111111111111', '2024-07-12 15:57:06', '王总', '2024-12-18 19:19:45', null, '21', '2', null, '26');

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务详细信息表';

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
