/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2025-01-13 21:06:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for plat_class
-- ----------------------------
DROP TABLE IF EXISTS `plat_class`;
CREATE TABLE `plat_class` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT NULL COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) NOT NULL COMMENT '所属学院id',
  `major_id` bigint(32) NOT NULL COMMENT '所属专业id',
  `teacher` text COMMENT '授课教师',
  `serial_number` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_class
-- ----------------------------
INSERT INTO `plat_class` VALUES ('14', '三班', null, null, '2024-07-09 21:25:53', '2024-09-13 22:32:12', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('20', '二班', null, null, '2024-07-11 20:22:01', '2024-09-13 22:32:08', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('25', '一班', null, null, '2024-07-11 20:42:34', '2024-09-13 22:32:03', '21', '2', '@109@106@', null);
INSERT INTO `plat_class` VALUES ('26', '五班', null, null, '2024-12-10 21:40:46', null, '21', '2', '@109@106@', null);

-- ----------------------------
-- Table structure for plat_college
-- ----------------------------
DROP TABLE IF EXISTS `plat_college`;
CREATE TABLE `plat_college` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '学院id',
  `name` varchar(20) NOT NULL COMMENT '学院名称',
  `manager` varchar(20) NOT NULL COMMENT '负责人',
  `manager_mobile` varchar(20) NOT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_college
-- ----------------------------
INSERT INTO `plat_college` VALUES ('21', '计算机学院', '李老师', '18888888888', '2024-07-05 20:34:49', '2024-12-13 12:41:33');
INSERT INTO `plat_college` VALUES ('22', '教育学院', '李老师', '18888888888', '2024-12-13 12:41:04', '2024-12-13 12:41:18');
INSERT INTO `plat_college` VALUES ('23', '工业学院', '', '', '2024-12-13 12:42:00', null);

-- ----------------------------
-- Table structure for plat_interfere_health
-- ----------------------------
DROP TABLE IF EXISTS `plat_interfere_health`;
CREATE TABLE `plat_interfere_health` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `assistant_text` varchar(200) DEFAULT NULL,
  `assistant_audio_url` varchar(200) DEFAULT NULL,
  `patient_information_info` varchar(2000) DEFAULT NULL,
  `assistant_health_text` varchar(200) DEFAULT NULL,
  `assistant_health_audio_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_interfere_health
-- ----------------------------
INSERT INTO `plat_interfere_health` VALUES ('288', '李白是一名高血压患者，针对于李白这个对象，我们需要对其进行高血压健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下李白的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/2be121ee-5911-498e-9092-b4739d67xx.mp3', '李白，男，58岁，上海本地居民，IT领域高级工程师，企业高管，身高175cm，体重88kg，腰围88cm。高血压病1年，未来服药时收缩压142mmHg，舒张压95mmHg。李某按时服用降压药，平时在外应酬频繁，未在社区接受定期慢性病随访，缺少运动，常头痛，头晕，经常焦虑，睡眠质量不佳。最近的一次体检指标显示，收缩压130mmHg，舒张压83mmHg，空腹血糖控制在6.0 mmol/L, 餐后2小时血糖7.7 mmol/L；总胆固醇（TC）7.2mmol/L, 甘油三酯（TG）1.6mmol/L,高密度脂蛋白胆固醇（HDL-C）0.9mmol/L ，低密度脂蛋白胆固醇LDL5.0mmol/L，Hb1Ac5.6%，母亲有冠心病，烟龄20年，目前每天1包烟，经常饮酒，每日52度白酒至少200ml，饮食荤食为主，口味偏咸，喜欢吃腌制食品。心房纤颤，左心室肥厚。', '你作为健康管理师，针对于李白的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/2be121ee-5911-498e-9092-b4739d679300_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('289', '刘英是一名冠心病患者，针对于刘英这个对象，我们需要对其进行冠心病健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下刘英的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/e62e0e91-7819-4402-84e0-77264d42ded7_FlowPanel_node_3.mp3', '刘英，女，67岁，杭州本地居民，某邮政储蓄银行退休职工，身高165cm，体重60kg，腰围76cm。37岁被诊断为冠心病，日常服用丹参片，收缩压120mmHg，舒张压75mmHg。刘某爱吃咸菜，水果蔬菜摄入较少，不吸烟，不饮酒，平时早晨起早走步1h。最近的一次体检指标显示，空腹血糖控制在5.8 mmol/L, 餐后2小时血糖7.3 mmol/L, 总胆固醇（TC）6.2mmol/L, 甘油三酯2.2 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.8mmol/L ，低密度脂蛋白胆固醇LDL5.4mmol/L，经常会出现焦虑，睡眠质量不好，晚上多次醒来，难以入睡，有心房纤颤，无左心室肥厚。', '你作为健康管理师，针对于刘英的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/e62e0e91-7819-4402-84e0-77264d42ded7_FlowPanel_node_9_F918F85622161828.mp3');
INSERT INTO `plat_interfere_health` VALUES ('290', '陈峰是一名脑卒中患者，针对于陈峰这个对象，我们需要对其进行脑卒中健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下陈峰的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/807858bb-ffe0-4668-8376-a18b4d93474e_FlowPanel_node_3.mp3', '陈某，男，75岁，广州本地居民，某卫健委退休职工，身高173cm，体重65kg，腰围86cm。半年前曾出现过小中风，日常血压测量，收缩压145mmHg，舒张压95mmHg。陈某爱吃甜食，水果蔬菜摄入较少，吸烟年限20年，平均每天5支，每周饮用白酒2次，每次约20ml，平时基本不运动。最近的一次体检指标显示，空腹血糖6.8 mmol/L, 餐后2小时血糖8.7mmol/L, 总胆固醇（TC）5.5mmol/L, 甘油三酯2.4 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.7mmol/L ，低密度脂蛋白胆固醇LDL4.4mmol/L，睡眠质量不好，无心房纤颤，左心室肥厚。', '你作为健康管理师，针对于陈峰的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/807858bb-ffe0-4668-8376-a18b4d93474e_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('291', '张玲是一名糖尿病患者，针对于张玲这个对象，我们需要对其进行糖尿病健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下张玲的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/97fb8fc0-36ea-4e93-b6d2-2afd867b6891_FlowPanel_node_3.mp3', '张玲，女，某国企退休人员，60岁，杭州本地居民，身高160cm，体重72kg，腰围90cm，确诊糖尿病近2年，按时服用降糖药物，本次因为腿部静息痛来健康管理中心接受检查，静脉抽血测量李女士空腹血糖7.5mmol/L，收缩压132mmHg，舒张压85mmHg，总胆固醇（TC）5.2mmol/L, 甘油三酯（TG）2.2mmol/L, 高密度脂蛋白胆固醇（HDL-C）1.5mmol/L，低密度脂蛋白胆固醇LDL2.9mmol/L，血尿酸400 umol/L，HbA1c=7.1%；无心房纤颤，无左心室肥厚。父亲患有糖尿病，不吸烟，不饮酒，平时主食基本不吃，以素食为主，嗜糖，因为家中琐事繁多，心理压力很大，乏力，常无精打采，对很多事情都没有兴趣，睡眠不踏实，每天健步走4000步，请根据李女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于张玲的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/97fb8fc0-36ea-4e93-b6d2-2afd867b6891_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('292', '张军是一名肥胖患者，针对于张军这个对象，我们需要对其进行肥胖健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下张军的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/cf0f0cd9-61b1-468a-98c4-7cf9c28d55ee_FlowPanel_node_3.mp3', '张军，男性，30岁，武汉市本地人，现为某IT公司的程序员，身高175cm，因为疫情，最近1年体重增加到了100kg，腰围100cm。平时缺少运动，经常熬夜加班，喜食肥肉，油炸食品，应酬多等，嗜酒如命，喜食甜食。每周吸烟1-2次，每次1支。无慢性病家族史，性格外向，擅言谈，睡眠质量不太好，最近因为有些头晕，头痛不适，到医院进行体检主要指标如下：空腹血糖8.5mmol/L，收缩压139mmHg，舒张压89mmHg，总胆固醇（TC）8.2mmol/L, 甘油三酯（TG）1.3mmol/L,高密度脂蛋白胆固醇（HDL-C）0.7mmol/L，低密度脂蛋白胆固醇（LDL-C）3.8mmol/L，血尿酸450 umol/L，HbA1c=6.4%；无心房纤颤，左心室肥厚。请根据张先生的情况为其提供健康管理服务。  ', '你作为健康管理师，针对于张军的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/aa.mp3');
INSERT INTO `plat_interfere_health` VALUES ('293', '赵云是一名COPD患者，针对于赵云这个对象，我们需要对其进行COPD健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下赵云的个人健康信息情况。。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1b86d4ed-3902-4974-b511-10a9341671e1_FlowPanel_node_3.mp3', '赵云，北京户籍，北京市某外企行政办公室主任，男性，55岁，身高175cm，体重55kg，腰围82cm，20岁开始吸烟，每天平均吸烟10支，不喝酒，性格开朗，乐观积极，近半年白天和夜间不感冒时经常出现咳嗽、胸闷，走路时气短等症状，睡眠质量不佳，父亲有哮喘和慢性支气管炎，怀疑自己肺部有健康问题。请根据赵先生的情况为其提供健康管理服务。', '你作为健康管理师，针对于张军的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/cf0f0cd9-61b1-468a-98c4-7cf9c28d55ee_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('294', '吴霞是一名骨质疏松患者，针对于吴霞这个对象，我们需要对其进行骨质疏松健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下吴霞的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1bce064b-1f2c-490b-b8fc-429471b2f7f1_FlowPanel_node_3.mp3', '吴霞，女性，杭州人，某高校党政办公室职员，年龄49岁，身高170cm，体重50kg，腰围73，爱喝咖啡，爱喝酒，不吸烟，不爱运动，喜欢宅在办公室或家里，性格外向，睡眠质量良好，有母亲髋关节脆性骨折家族史，有皮肤病，常服用糖皮质激素类药物。最近一次下楼不小心摔倒，导致小腿骨折，她怀疑自己为骨质疏松，请根据赵女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于吴霞的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/1bce064b-1f2c-490b-b8fc-429471b2f7f1_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('295', '秦芳是一名盆底肌功能患者，针对于秦芳这个对象，我们需要对其进行盆底肌功能健康干预或健康指导，首先先收集其健康信息，然后进行风险评估，根据存在的危险因素及评估结果对其展开具体地健康干预工作，我们先来了解下秦芳的个人健康信息情况。', 'content/assets/audio/plat-questionnaire/risk_dysfunction/c0de8716-0789-41fc-8a64-70f2466484ac_FlowPanel_node_3.mp3', '秦芳，女性，北京人，家庭主妇，年龄50岁，身高160cm，体重65kg，腰围92cm，有3个孩子，其中两个孩子出生时体重超过4kg，45岁时开始抽烟，每天约1支烟，烟龄5年，不饮酒，目前有慢性支气管炎，慢性盆腔炎，平时体力活动少，性格开朗，睡眠良好，当她开怀大笑、打喷嚏等时会出现尿失禁，请根据秦女士的情况为其提供健康管理服务。', '你作为健康管理师，针对于秦芳的健康状况，你需要考虑收集哪些健康信息？也就是收集其存在的健康危险因素，为下一步地评估和干预工作提供依据，下面开始吧！', 'content/assets/audio/plat-questionnaire/risk_dysfunction/c0de8716-0789-41fc-8a64-70f2466484ac_FlowPanel_node_9.mp3');
INSERT INTO `plat_interfere_health` VALUES ('401', '上面是案例人何某的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/b2c526ab-2aac-4594-9272-68df40d075ca_FlowPanel_node_2_BFFB38DB1A203F5C.mp3', '何某，28岁，身高180，体重100kg，上班族，朝九晚五，一坐就是一天，爱喝奶茶，平时很少运动，晚上常常凌晨1-2点入睡，睡前爱吃宵夜。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('402', '上面是案例人张叔叔的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/019780fd-dbe9-47f5-9746-a62b8860f12c_FlowPanel_node_2.mp3', '56岁张叔叔，身高173cm，体重70kg，患有高血压，踝关节曾经扭伤过，现在基本可以正常行走但是跳跃等运动时会伴有疼痛感，在体检时进行了徒手的反应性测试，测试的结果是33cm。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('403', '上面是案例人张爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/d8de4211-f615-4b97-b1f8-cb4da647bbd9_FlowPanel_node_2_2162DAE75B6D6ACB.mp3', '张爷爷是一名68岁的退休军人，体重离开军队之后一直在从事军训教官直到退休，退休之后因为摔倒过一次，导致腰痛，他在一次体检的时候进行了肺功能的测试，相关结果如下（包含测试和访谈的结果）：憋气实验测得时间是20s；平常活动不会出现气短的情况；每周会有几天咳嗽，在运动或进行重体力劳动时会出现气促，爬山或在平地快速行走时，不得不减慢速度或停下来，且经常容易筋疲力尽；个人比较爱抽烟，饮酒。\r\n\r\n       张爷爷进行了COPD的问卷调查，他说自己大部分时间很正常，但是想去跑步就不行了，通常每周有几天咳嗽，一旦运动或提重物行走时就会气促，爬山或在平地快速行走时，不得不减慢速度或停下来，且经常容易筋疲力尽。', '', '');
INSERT INTO `plat_interfere_health` VALUES ('404', '上面是案例人小何的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/95188c8c-57a6-42db-858e-311c56d0f2bd_FlowPanel_node_2_269F6229B297004F.mp3', '23岁的小何是一名大学生，日常运动主要以晚间在操场慢跑10min为主，最近学习进行了体质监测考试，他其中一项俯卧撑完成了27个，测试结束之后小何问了一个问题：如果身体未降至肩与肘处于同一水平面是否会得分？', null, null);
INSERT INTO `plat_interfere_health` VALUES ('405', '上面是案例人小张的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/97974728-57e6-4c82-ad59-40dc049ead56_FlowPanel_node_2_6A8EC6D5F266286C.mp3', '28岁的小张是一名律师，身高180cm,体重75kg,日常主要的运动是晚餐过后的步行51min,无其他运动病史，在体质检测中分别进行了背肌耐力检测，握力计测试，纵跳测试。其中测试的结果如下：背肌耐力检测是130kg；握力计测试36kg；纵跳30cm。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('406', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/e9ccebd9-db0c-4104-b8d1-395a20e20d4f_FlowPanel_node_2.mp3', ' 60岁的何爷爷，身高167cm, 体重60kg, 在一次体检上进行了睁眼双足前后脚 一字型站立，他的结果是28s', null, null);
INSERT INTO `plat_interfere_health` VALUES ('407', null, null, '张叔叔，50岁，体重65kg,身高175cm,血压血糖的指标属于正常标准的范围内，在某体检机构进行了相关的柔韧性检测。平常有运动习惯，平常下班之后会选择约30分钟的慢跑，运动结束会进行2分钟的放松练习。\r\n\r\n相关的结果如下：\r\n\r\n       徒手的坐位体前屈结果为：8cm,；背后交接的检测，发现双手距离15cm ,伴有受限的情况，主诉曾经受伤过；转身测试结果是65°；屈伸功能测试，伸展测试顺利完成动作，没有卡顿的情况，主诉也没有相关的疼痛。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('408', '请按照文字指导和图片示例进行自己或他人的协调能力测试，并记录自己的测试结果，年龄需要在45岁以上，系统才能提供评分结果。', 'content/assets/audio/plat-questionnaire/risk_obstructive/32f67d93-2b27-4559-b1b6-b87833cec4dc_FlowPanel_node_3_E067A487DDD2FA77.mp3', null, null, null);
INSERT INTO `plat_interfere_health` VALUES ('409', '上面是案例人游叔叔的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/106ddd04-9931-4bda-b0e1-885a79b8b209_FlowPanel_node_2_4B40FFC2D4AB8DC1.mp3', '游某某，男，39岁，身高173cm，体重64kg。按照“国民体质测定标准”进行测试，结果为： 台阶(30cm)试验：蹬台阶持续时间200s，运动后3次心率分别为51次/30s，39次/30s、37次/30s。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('410', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('411', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('412', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('413', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('414', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('415', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);
INSERT INTO `plat_interfere_health` VALUES ('416', '上面是案例人何爷爷的案例详情，请阅读分析，完成接下来的答题。', 'content/assets/audio/plat-questionnaire/risk_obstructive/911246cf-c8ca-4970-aaa2-b09246fdd6b9_FlowPanel_node_2.mp3', '  何爷爷今年70岁了，他被邀请去实验室进行了一次闭眼复杂重建法的测试。检查者准备了一条杆子系上不同颜色、不同长短、相隔3-5cm的带线小球。何爷爷在检查者的协助下闭眼后用手、肘、肩、膝、足、头等肢体部位触碰认定多个目标并记忆，离开目标后要求能自己按不同顺序要求触碰认定目标，实验中离目标越则近能力越强。\r\n\r\n      \r\n\r\n       何爷爷最终的测试情况是触碰了旁边的3个小球。', null, null);

-- ----------------------------
-- Table structure for plat_lesson
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson`;
CREATE TABLE `plat_lesson` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) DEFAULT NULL COMMENT '所属学院id',
  `majors` varchar(2000) DEFAULT '' COMMENT '适用专业',
  `teachers` varchar(2000) DEFAULT NULL COMMENT '授课教师',
  `picture` longtext COMMENT '缩略图',
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL COMMENT '描述',
  `type_id` bigint(32) DEFAULT NULL COMMENT '类型',
  `college_name` varchar(2000) DEFAULT NULL COMMENT '所属学院名称',
  `serial_number` int(4) DEFAULT NULL COMMENT '排序',
  `student_title` varchar(200) DEFAULT NULL COMMENT '学习页面标题',
  `student_img_url` varchar(200) DEFAULT NULL COMMENT '学习页面文件路径',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson
-- ----------------------------
INSERT INTO `plat_lesson` VALUES ('1', '中医九种体质评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('2', '设计健康体检套餐--糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:22', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('3', '设计健康体检套餐--脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:29', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('4', '健康体检报告解读--乳腺癌和甲状腺', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:33', '21', '@2@4@', '@106@', null, null, '', '1', '', '4', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('5', '健康体检报告解读--脑卒中和外周血管疾病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:40', '21', '@2@4@', '@106@', null, null, '', '1', '', '5', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('6', '社区健康评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '6', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('7', '企业人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '7', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('8', '社区人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '8', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('9', '养老院人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '9', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('10', '健康促进生活方式评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '201', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('36', '慢性病饮食干预-高血压', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('37', '营养配餐课程', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('38', '慢性病饮食干预-冠心病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('39', '饮食评价报告模块', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('40', '慢性病饮食干预-脑卒中', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('41', '慢性病饮食干预-糖尿病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('42', '慢性病饮食干预-肥胖症', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('43', '慢性病饮食干预-慢性阻塞性肺病', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('44', '慢性病饮食干预-骨质疏松', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('45', '慢性病饮食干预-盆底功能障碍', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('46', '正常人群食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('47', '高血压患者食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('48', '糖尿病患者食品标签实验', '赵老师', null, '2024-11-19 14:01:07', null, '21', '@2@', '@109@', null, null, '', '3', '', '202', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('288', '慢性病健康干预-高血压', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '288', '高血压的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/gaoxueya.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('289', '慢性病健康干预-冠心病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '289', '冠状动脉粥样硬化性心脏病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/guanxibing.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('290', '慢性病健康干预-脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '290', '脑卒中的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/naocuzhong.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('291', '慢性病健康干预-糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '291', '\r\n2型糖尿病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/bf514d96-0d8d-49f5-bd4a-7ec21a684485.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('292', '慢性病健康干预-肥胖症', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '292', '肥胖症的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/b5a5dd91-b9b1-42a9-9d1c-676d0477bca9.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('293', '慢性病健康干预-慢性阻塞性肺病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', '慢性阻塞性肺疾病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/5798c351-9526-49a9-a44e-d8c8d8405d7f.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('294', '慢性病健康干预-骨质疏松', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', null, null, 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('295', '慢性病健康干预-盆底功能障碍', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', '\r\n盆底功能障碍的评估与干预', 'content/assets/img/plat-questionnaire/risk_dysfunction/a4c603af-32ec-4f52-8240-596ce4c475f5.png', 'plat-questionnaire', 'risk_common_interfere');
INSERT INTO `plat_lesson` VALUES ('296', '心血管病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('297', '脑卒中风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('298', '糖尿病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('299', '慢性阻塞性肺疾病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '299', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('305', '睡眠质量评估', '王老师', '无', '2024-12-15 16:25:18', null, null, '@2@4@', '@109@106@3@', '', null, '', '2', '', '203', null, null, null, null);
INSERT INTO `plat_lesson` VALUES ('401', 'BMI训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('402', '反应力能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('403', '肺功能能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('404', '核心力能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('405', '局部力量训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('406', '平衡能力评估', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('407', '柔软度能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('408', '协调能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('409', '心功能能力训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('410', '本体感觉训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('411', '床上操', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('412', '养生操', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('413', '家务运动训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('414', '徒手保健养生操（75岁以下）', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('415', '徒手保健养生操（75岁以上）', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');
INSERT INTO `plat_lesson` VALUES ('416', '懒人训练', '王老师', '无', '2024-12-15 16:25:18', '2025-01-07 22:09:00', '21', '@2@4@', '@109@106@3@', '', '', '', '4', '', '203', '', '', 'plat-questionnaire', 'risk_common_sport');

-- ----------------------------
-- Table structure for plat_lesson_chapter_type
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_chapter_type`;
CREATE TABLE `plat_lesson_chapter_type` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '章节类型id',
  `name` varchar(200) NOT NULL COMMENT '章节类型名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_chapter_type
-- ----------------------------
INSERT INTO `plat_lesson_chapter_type` VALUES ('1', '122222222222', null, '2024-07-17 14:43:26', '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('33', '333', '2024-07-17 14:43:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('34', '11', '2024-07-17 15:09:30', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('35', '66666', '2024-07-17 15:17:16', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('36', 'ff', '2024-07-17 15:18:16', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('37', '1', '2024-07-17 15:20:50', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('94', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('95', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('96', '2', '2024-07-17 15:24:45', null, '10');
INSERT INTO `plat_lesson_chapter_type` VALUES ('97', '2', '2024-07-17 15:24:46', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('98', '1', '2024-07-17 15:24:50', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('102', '3', '2024-07-17 15:35:41', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('103', '5', '2024-07-17 15:37:31', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('105', 'www', '2024-07-17 15:38:14', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('107', '2222t', '2024-07-17 15:40:54', '2024-07-17 15:41:04', '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('108', '1', '2024-07-17 16:31:21', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('110', '1', '2024-07-17 16:34:34', null, '32');
INSERT INTO `plat_lesson_chapter_type` VALUES ('111', '666', '2024-07-17 16:34:46', null, '32');

-- ----------------------------
-- Table structure for plat_lesson_copy
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_copy`;
CREATE TABLE `plat_lesson_copy` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(11) DEFAULT NULL COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) DEFAULT NULL COMMENT '所属学院id',
  `majors` varchar(2000) DEFAULT '' COMMENT '适用专业',
  `teachers` varchar(2000) DEFAULT NULL COMMENT '授课教师',
  `picture` longtext COMMENT '缩略图',
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL COMMENT '描述',
  `type_id` bigint(32) DEFAULT NULL COMMENT '类型',
  `college_name` varchar(2000) DEFAULT NULL COMMENT '所属学院名称',
  `serial_number` int(4) DEFAULT NULL COMMENT '排序',
  `student_title` varchar(200) DEFAULT NULL COMMENT '学习页面标题',
  `student_img_url` varchar(200) DEFAULT NULL COMMENT '学习页面文件路径',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_copy
-- ----------------------------
INSERT INTO `plat_lesson_copy` VALUES ('1', '中医九种体质评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '202', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('2', '设计健康体检套餐--糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:22', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('3', '设计健康体检套餐--脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:29', '21', '@2@4@', '@109@', null, null, '', '1', '', '3', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('4', '健康体检报告解读--乳腺癌和甲状腺', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:33', '21', '@2@4@', '@106@', null, null, '', '1', '', '4', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('5', '健康体检报告解读--脑卒中和外周血管疾病', '王老师', '无', '2024-08-28 12:49:34', '2024-12-13 15:06:40', '21', '@2@4@', '@106@', null, null, '', '1', '', '5', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('6', '社区健康评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '6', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('7', '企业人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '7', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('8', '社区人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '8', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('9', '养老院人群健康数据分析', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '1', '', '9', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('10', '健康促进生活方式评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '201', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('288', '慢性病健康干预-高血压', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '288', '高血压的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/gaoxueya.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('289', '慢性病健康干预-冠心病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '289', '冠状动脉粥样硬化性心脏病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/guanxibing.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('290', '慢性病健康干预-脑卒中', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '290', '脑卒中的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/naocuzhong.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('291', '慢性病健康干预-糖尿病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '291', '\r\n2型糖尿病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/bf514d96-0d8d-49f5-bd4a-7ec21a684485.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('292', '慢性病健康干预-肥胖症', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '292', '肥胖症的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/b5a5dd91-b9b1-42a9-9d1c-676d0477bca9.png', 'common', 'plat');
INSERT INTO `plat_lesson_copy` VALUES ('293', '慢性病健康干预-慢性阻塞性肺病', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', '慢性阻塞性肺疾病的健康干预技能', 'content/assets/img/plat-questionnaire/risk_dysfunction/5798c351-9526-49a9-a44e-d8c8d8405d7f.png', null, null);
INSERT INTO `plat_lesson_copy` VALUES ('294', '慢性病健康干预-骨质疏松', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '297', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('295', '慢性病健康干预-盆底功能障碍', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', '\r\n盆底功能障碍的评估与干预', 'content/assets/img/plat-questionnaire/risk_dysfunction/a4c603af-32ec-4f52-8240-596ce4c475f5.png', null, null);
INSERT INTO `plat_lesson_copy` VALUES ('296', '心血管病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('297', '脑卒中风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('298', '糖尿病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', '', null, '', '2', '', '298', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('299', '慢性阻塞性肺疾病风险评估', '王老师', '无', '2024-08-28 12:49:34', '2024-08-28 12:56:15', '21', '@2@4@', '@109@106@3@', null, null, '', '2', '', '299', null, null, null, null);
INSERT INTO `plat_lesson_copy` VALUES ('305', '睡眠质量评估', '王老师', '无', '2024-12-15 16:25:18', null, null, '@2@4@', '@109@106@3@', '', null, '', '2', '', '203', null, null, null, null);

-- ----------------------------
-- Table structure for plat_lesson_scale
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale`;
CREATE TABLE `plat_lesson_scale` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表id主键',
  `name` varchar(200) DEFAULT NULL COMMENT '量表名称',
  `lesson_son_id` bigint(32) DEFAULT NULL COMMENT '所属子课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale
-- ----------------------------
INSERT INTO `plat_lesson_scale` VALUES ('1', '十种慢病风险评估', '1', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '1');
INSERT INTO `plat_lesson_scale` VALUES ('2', '其它健康风险评估', '1', '2024-08-28 12:59:46', '2024-08-28 12:59:53', '2');

-- ----------------------------
-- Table structure for plat_lesson_scale_question
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_question`;
CREATE TABLE `plat_lesson_scale_question` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表主键id',
  `parent_id` bigint(32) DEFAULT NULL COMMENT '合并单元格问题，上级id',
  `name` varchar(200) DEFAULT NULL COMMENT '问题名称',
  `lesson_scale_son_id` bigint(32) DEFAULT NULL COMMENT '所属量表id',
  `lesson_scale_id` bigint(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `type` int(5) DEFAULT NULL COMMENT '类型（1单选打对勾，2填空，3标题，4中医）',
  `score` int(255) DEFAULT NULL COMMENT '分数',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_question
-- ----------------------------
INSERT INTO `plat_lesson_scale_question` VALUES ('1', null, '年龄(≥50岁)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '1', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('2', null, '使用电脑、驾驶员工作', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '2', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('3', null, '睡眠时间短(<6小时/天)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '3', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('4', null, '不合理用枕头(枕头与自己拳头比较高或低)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '4', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('5', null, '头颈部外伤史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '5', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('6', null, '缺少体育锻炼(偶尔或1-2次/周)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '6', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('7', null, '直系或旁系亲属颈椎疾病史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '7', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('8', null, '吸烟史', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '8', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('9', null, '超重/肥胖 BMI>24', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '9', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('10', null, '缺少体力锻炼(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '10', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('11', null, '血压升高 收缩压≥140或舒张压≥90', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '11', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('12', null, '脂肪摄入多(很油腻、较油腻)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '12', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('13', null, '肉类摄入多(喜欢、较喜欢)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '13', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('14', null, '蔬菜摄入少(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '14', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('15', null, '水果摄入少>(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '15', '1', '1', null);
INSERT INTO `plat_lesson_scale_question` VALUES ('16', null, '(1)您精力充沛吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '16', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('17', null, '(2)您容易疲乏吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '17', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('18', null, '(3)您说话声音无力吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '18', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('19', null, '(4)您感觉到闷闷不乐吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '19', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('20', null, '(5)您比一般人耐受不了寒冷(冬天的寒冷，夏天的冷空调电扇)吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '20', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('21', null, '(6)您能适应外界自然和社会环境的变化吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '21', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('22', null, '(7)您容易失眠吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '22', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('23', null, '(8)您容易忘事(健忘)吗?', '14', null, '2024-10-18 20:43:49', '2024-10-18 20:43:49', '23', '4', null, '1.平和质(A型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('24', null, '(1)您容易疲乏吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '24', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('25', null, '(2)您容易气短(呼吸短促，接不上气)吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '25', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('26', null, '(3)您容易心慌吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '26', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('27', null, '(4)您容易头晕或站起时晕眩吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '27', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('28', null, '(5)您比别人容易患感冒吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '28', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('29', null, '6)您喜欢安静、懒得说话吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '29', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('30', null, '(7)您说话声音无力吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '30', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('31', null, '(8)您活动量稍大就容易出虚汗吗?', '14', null, '2024-10-18 20:49:37', '2024-10-18 20:49:37', '31', '4', null, '2.气虚质 (B型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('32', null, '(1)您手脚发凉吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '32', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('33', null, '(2)您胃脘部、背部或腰膝部怕冷吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '33', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('34', null, '(3)您感到怕冷、衣服比别人穿得多吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '34', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('35', null, '(4)您比一般人耐受不了寒冷(冬天的寒冷，夏天的冷空调电扇)吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '35', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('36', null, '(5)您比别人容易患感冒吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '36', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('37', null, '(6)您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉东西吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '37', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('38', null, '(7)您受凉或吃(喝)凉的东西后，容易腹泻(拉肚子)吗?', '14', null, '2024-10-18 20:53:40', '2024-10-18 20:53:40', '38', '4', null, '3.阳虚质 (C型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('39', null, '(1)您感到手脚心发热吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '39', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('40', null, '(2)您感觉身上、脸上发热吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '40', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('41', null, '(3)您皮肤或口唇干吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '41', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('42', null, '您的口唇颜色比一般人红吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '42', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('43', null, '(5)您容易便秘或大便干燥吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '43', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('44', null, '您两面部潮红或偏红吗?(6)', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '44', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('45', null, '(7)您感到眼睛干涩吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '45', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('46', null, '(8)您活动量稍大就容易出虚汗吗?', '14', null, '2024-10-18 20:54:50', '2024-10-18 20:54:50', '46', '4', null, '4.阴虚质 (D型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('47', null, '(1)您感到胸闷或腹部胀满吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '47', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('48', null, '(2)您感到身体沉重不轻松或不爽快吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '48', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('49', null, '3您腹部肥满松软吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '49', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('50', null, '您有额部油脂多的现象吗?4)', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '50', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('51', null, '(5)您上眼睑比别人肿(上眼睑有轻微隆起的现象)吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '51', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('52', null, '(6)您嘴里有黏黏的感觉吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '52', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('53', null, '(7)您平时痰多，特别是感到咽喉部总有痰堵着吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '53', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('54', null, '您舌苔厚腻或有舌苔厚厚的8)感觉吗?', '14', null, '2024-10-18 20:55:51', '2024-10-18 20:55:51', '54', '4', null, '5.痰湿质 (E型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('55', null, '(1)您面部或鼻部有油腻感或者油亮发光吗?', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '55', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('56', null, '(2)您容易生痤疮或疮疖吗?', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '56', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('57', null, '(3)您感到口苦或嘴里有异味吗?', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '57', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('58', null, '(4)您大便粘滞不爽、有解不尽的感觉吗?', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '58', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('59', null, '(5)您小便时尿道有发热感、尿色浓(深)吗?', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '59', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('60', null, '(6)您带下色黄(白带颜色发黄)吗?(限女性回答)', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '60', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('61', null, '(7)您的阴囊潮湿吗?(限男性回答)', '14', null, '2024-10-18 20:56:41', '2024-10-18 20:56:41', '61', '4', null, '6.湿热质 (F型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('62', null, '(1)您的皮肤在不知不觉中会出现青紫瘀斑(皮下出血)吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '62', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('63', null, '(2)您两颧部有细微红丝吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '63', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('64', null, '(3)您身体上有哪里疼痛吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '64', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('65', null, '(4)您面色晦黯或容易出现褐斑吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '65', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('66', null, '(5)您容易有黑眼圈吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '66', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('67', null, '(6)您容易忘事(健忘)吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '67', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('68', null, '(7)您口唇颜色偏黯吗?', '14', null, '2024-10-18 20:57:54', '2024-10-18 20:57:54', '68', '4', null, '7.血瘀质 (G型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('69', null, '(1)您感到闷闷不乐吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '69', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('70', null, '(2)您容易精神紧张、焦虑不安吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '70', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('71', null, '(3)您多愁善感、感情脆弱吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '71', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('72', null, '(4)您容易感到害怕或受到惊吓吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '72', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('73', null, '(5)您胁肋部或乳房胀痛吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '73', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('74', null, '(6)您无缘无故叹气吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '74', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('75', null, '(7)您咽喉部有异物感，且吐之不出、咽之不下吗?', '14', null, '2024-10-18 20:59:27', '2024-10-18 20:59:27', '75', '4', null, '8.气郁质 (H型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('76', null, '(1)您没有感冒时也会打喷嚏', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '76', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('77', null, '吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '77', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('78', null, '(2)您没有感冒时也会鼻塞流鼻涕吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '78', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('79', null, '(3)您有因季节变化、温度变化或异味等原因而咳喘的现象吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '79', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('80', null, '(4)您容易过敏(对药物、食物、气味、花粉或在季节交替气候变化时)吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '80', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('81', null, '(5)您的皮肤容易起荨麻疹(风团、风疹块、风疙瘩)吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '81', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('82', null, '(6)您的皮肤因过敏出现过紫癜(紫红色瘀点、瘀斑)吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '82', '4', null, '9.特禀质(1型)');
INSERT INTO `plat_lesson_scale_question` VALUES ('83', null, '(7)您的皮肤一抓就红，并出现抓痕吗?', '14', null, '2024-10-18 21:00:46', '2024-10-18 21:00:46', '83', '4', null, '9.特禀质(1型)');

-- ----------------------------
-- Table structure for plat_lesson_scale_question_copy
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_question_copy`;
CREATE TABLE `plat_lesson_scale_question_copy` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表主键id',
  `parent_id` bigint(32) DEFAULT NULL COMMENT '合并单元格问题，上级id',
  `name` varchar(200) DEFAULT NULL COMMENT '问题名称',
  `lesson_scale_son_id` bigint(32) DEFAULT NULL COMMENT '所属量表id',
  `lesson_scale_id` bigint(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `type` int(5) DEFAULT NULL COMMENT '类型（1打对勾，2填空，3标题）',
  `score` int(255) DEFAULT NULL COMMENT '分数',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_question_copy
-- ----------------------------
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('1', null, '年龄(≥50岁)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '1', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('2', null, '使用电脑、驾驶员工作', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '2', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('3', null, '睡眠时间短(<6小时/天)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '3', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('4', null, '不合理用枕头(枕头与自己拳头比较高或低)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '4', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('5', null, '头颈部外伤史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '5', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('6', null, '缺少体育锻炼(偶尔或1-2次/周)', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '6', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('7', null, '直系或旁系亲属颈椎疾病史', '1', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '7', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('8', null, '吸烟史', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '8', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('9', null, '超重/肥胖 BMI>24', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '9', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('10', null, '缺少体力锻炼(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '10', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('11', null, '血压升高 收缩压≥140或舒张压≥90', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '11', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('12', null, '脂肪摄入多(很油腻、较油腻)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '12', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('13', null, '肉类摄入多(喜欢、较喜欢)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '13', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('14', null, '蔬菜摄入少(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '14', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('15', null, '水果摄入少>(偶尔、1-2次/周)', '2', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '15', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('16', null, '父亲或母亲痛风家族史', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '16', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('17', null, '超重/肥胖(BMI>24)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '17', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('18', null, '饮酒史(3-4次/周;5次/周)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '18', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('19', null, '高嘌呤饮食(喜欢;较喜欢)例如:内脏、海鲜、老火汤等食物', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '19', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('20', null, '高血压(收缩压≥140或舒张压>90)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '20', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('21', null, '脂代谢紊乱(胆固醇>5.2或甘油三酯>2.3)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '21', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('22', null, '糖代谢紊乱(空腹血糖>6.0或餐后血糖>7.8)', '3', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '22', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('23', null, '您的性别:', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '23', '2', '2', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('24', null, '您的体重(KG):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '24', '2', '2', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('25', null, '您的身高(M):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '25', '2', '2', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('26', null, '您的腰围(CM):', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '26', '2', '2', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('27', null, '肥胖症史', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '27', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('28', '27', '是', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '28', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('29', null, '上周运动次数', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '29', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('30', '29', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '30', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('31', '29', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '31', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('32', null, '肉类', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '32', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('33', '32', '喜欢\n', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '33', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('34', '32', '较喜欢', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '34', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('35', null, '口味', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '35', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('36', '35', '油腻', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '36', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('37', '35', '\n较油腻', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '37', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('38', null, '上周膳食纤维摄入量（水果）', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '38', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('39', null, '上周膳食纤维摄入量（蔬菜）', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '39', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('41', '38', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '41', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('42', '38', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '42', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('43', '39', '偶尔', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '43', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('44', '39', '\n1-2次/周', '4', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '44', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('45', null, '男性', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '45', '3', '3', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('46', null, '年龄(岁)', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '46', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('47', '46', '54-56', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '47', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('48', '46', '57-59', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '48', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('49', '46', '60-62', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '49', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('50', '46', '63-65', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '50', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('51', '46', '66-68', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '51', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('52', '46', '69-72', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '52', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('53', '46', '73-75', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '53', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('54', '46', '76-78', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '54', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('55', '46', '79-81', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '55', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('56', '46', '82-84', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '56', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('57', '46', '85', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '57', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('58', null, '收缩压(未服药)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '58', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('62', '58', '97-105', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '62', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('63', '58', '106-115', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '63', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('64', '58', '116-125', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '64', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('65', '58', '126-135', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '65', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('66', '58', '136-145', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '66', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('67', '58', '146-155', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '67', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('68', '58', '156-165', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '68', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('69', '58', '166-175', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '69', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('70', '58', '176-185', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '70', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('71', '58', '186-195', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '71', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('72', '58', '196-205', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '72', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('73', null, '收缩压(服药后)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '73', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('74', '73', '97-105', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '74', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('75', '73', '106-112', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '75', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('76', '73', '113-117', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '76', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('77', '73', '118-123', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '77', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('78', '73', '124-129', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '78', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('79', '73', '130-135', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '79', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('80', '73', '136-142', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '80', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('81', '73', '143-150', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '81', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('82', '73', '151-161', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '82', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('83', '73', '162-176', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '83', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('84', '73', '177-205', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '84', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('85', null, '糖尿病家族史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '85', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('86', null, '吸烟史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '86', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('87', null, '心血管疾病史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '87', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('88', null, '房颤', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '88', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('89', null, '左心室肥厚', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '89', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('90', null, '女性', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '90', '3', '3', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('91', null, '年龄(岁)', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '91', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('92', '91', '54-56', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '92', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('93', '91', '57-59', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '93', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('94', '91', '60-62', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '94', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('95', '91', '63-64', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '95', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('96', '91', '65-67', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '96', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('97', '91', '68-70', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '97', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('98', '91', '71-73', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '98', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('99', '91', '74-76', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '99', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('100', '91', '77-78', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '100', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('101', '91', '79-81', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '101', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('102', '91', '82-84', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '102', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('103', null, '收缩压(未服药)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '103', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('104', '103', '95-106', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '104', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('105', '103', '107-118', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '105', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('106', '103', '119-130', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '106', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('107', '103', '131-143', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '107', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('108', '103', '144-155', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '108', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('109', '103', '156-167', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '109', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('110', '103', '168-180', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '110', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('111', '103', '181-192', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '111', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('112', '103', '193-204', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '112', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('113', '103', '205-216', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '113', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('114', null, '收缩压(服药后)/mmHg', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '114', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('115', '114', '95-106', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '115', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('116', '114', '107-113', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '116', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('117', '114', '114-119', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '117', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('118', '114', '120-125', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '118', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('119', '114', '126-131', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '119', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('120', '114', '132-139', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '120', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('121', '114', '140-148', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '121', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('122', '114', '149-160', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '122', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('123', '114', '161-204', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '123', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('124', '114', '205-216', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '124', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('125', null, '糖尿病家族史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '125', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('126', null, '吸烟史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '126', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('127', null, '心血管疾病史', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '127', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('128', null, '房颤', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '128', '1', '1', null);
INSERT INTO `plat_lesson_scale_question_copy` VALUES ('129', null, '左心室肥厚', '5', null, '2024-08-28 12:59:56', '2024-08-28 12:59:58', '129', '1', '1', null);

-- ----------------------------
-- Table structure for plat_lesson_scale_son
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_son`;
CREATE TABLE `plat_lesson_scale_son` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表id主键',
  `name` varchar(200) DEFAULT NULL COMMENT '量表名称',
  `lesson_scale_id` bigint(32) DEFAULT NULL COMMENT '所属子课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `result_formula` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_son
-- ----------------------------
INSERT INTO `plat_lesson_scale_son` VALUES ('1', '颈椎病风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '1', '<3:初级症状，3-5:中级症状，>5:严重症状');
INSERT INTO `plat_lesson_scale_son` VALUES ('2', '高脂血症危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '2', null);
INSERT INTO `plat_lesson_scale_son` VALUES ('14', '中医体质分类与评价', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '14', null);

-- ----------------------------
-- Table structure for plat_lesson_scale_son_copy
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_scale_son_copy`;
CREATE TABLE `plat_lesson_scale_son_copy` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '量表id主键',
  `name` varchar(200) DEFAULT NULL COMMENT '量表名称',
  `lesson_scale_id` bigint(32) DEFAULT NULL COMMENT '所属子课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `serial_number` int(5) DEFAULT NULL COMMENT '排序',
  `result_formula` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_scale_son_copy
-- ----------------------------
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('1', '颈椎病风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '1', '<3:初级症状，3-5:中级症状，>5:严重症状');
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('2', '高脂血症危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '2', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('3', '高尿酸血症风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '3', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('4', '肥胖及肥胖风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '4', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('5', '脑卒中风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '5', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('6', '纪立农中国人群糖尿病风险', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '6', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('7', '十年内心血管疾病危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '7', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('8', '腰椎病风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '8', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('9', '高血压危险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '9', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('10', '脂肪肝风险评估', '1', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '10', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('11', '欧洲生存质量测定量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '11', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('12', '日常生活能力量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '12', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('13', '日常生活活动能力量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '13', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('14', '中医体质分类与评价', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '14', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('15', '慢性病患者生命质量测定量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '15', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('16', 'Berg 平衡量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '16', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('17', '匹茨堡睡眠质量指数量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '17', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('18', '慢性病病人健康素养量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '18', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('19', 'ADL量表（Barthel指数）', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '19', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('20', '自测健康评定量表（不确定）', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '20', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('21', '社会支持评定量表SSQ', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '21', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('22', '生命质量测定量表QLQ', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '22', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('23', '安全感量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '23', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('24', 'SF-36健康状况问卷', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '24', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('25', 'TDL生命质量测定表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '25', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('26', 'PHQ-9抑郁症筛查量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '26', null);
INSERT INTO `plat_lesson_scale_son_copy` VALUES ('27', '健康促进生活方式量表', '2', '2024-08-28 13:09:32', '2024-08-28 13:09:34', '27', null);

-- ----------------------------
-- Table structure for plat_lesson_schedule
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_schedule`;
CREATE TABLE `plat_lesson_schedule` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `teacher_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `class_id` bigint(32) DEFAULT NULL COMMENT '课程id',
  `lesson_time` varchar(32) DEFAULT NULL COMMENT '200',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_schedule
-- ----------------------------
INSERT INTO `plat_lesson_schedule` VALUES ('74', '2024-12-18 19:13:28', '2024-12-18 19:14:28', '305', '106', '14', '2024-12-01 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('75', '2024-12-18 19:13:55', '2024-12-18 19:16:35', '305', '106', '25', '2024-12-16 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('76', '2024-12-18 19:17:08', null, '8', '106', '25', '2024-12-23 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('77', '2024-12-18 19:18:21', '2024-12-18 19:24:41', '6', '1', '25', '2024-12-23 00:00:00');
INSERT INTO `plat_lesson_schedule` VALUES ('78', '2024-12-18 19:35:31', null, '6', '106', '25', '2024-12-24 00:00:00');

-- ----------------------------
-- Table structure for plat_lesson_son
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_son`;
CREATE TABLE `plat_lesson_son` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '子课程id主键',
  `name` varchar(200) NOT NULL COMMENT '子课程名称',
  `lesson_id` bigint(32) NOT NULL COMMENT '所属课程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `experimental_method` varchar(200) DEFAULT NULL COMMENT '实验方式',
  `suggested_duration` varchar(200) DEFAULT NULL COMMENT '分值权重占比',
  `serial_number` varchar(200) DEFAULT NULL COMMENT '分值权重占比',
  `module_folder_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `lesson_folder_name` varchar(100) DEFAULT NULL COMMENT '课程项目文件夹名称',
  `lesson_son_folder_name` varchar(100) DEFAULT NULL COMMENT '子课程文件夹名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30344 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_son
-- ----------------------------
INSERT INTO `plat_lesson_son` VALUES ('1', '自我评估', '1', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2', '中医九种体质分型', '1', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '60', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('3', '理论学习', '35', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '45', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('4', '实训流程', '35', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('5', '理论知识', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('6', '健康信息收集', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('7', '健康干预', '36', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('8', '理论知识', '37', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('9', '健康干预', '37', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('10', '理论知识', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('11', '健康信息收集', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('12', '健康干预', '38', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('13', '学习及咨询模块', '39', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('14', '饮食评价报告', '39', '2024-11-19 14:07:52', '2024-11-19 14:07:54', '个人', '30', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('15', '理论知识', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('16', '健康信息收集', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('17', '健康干预', '40', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('18', '理论知识', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('19', '健康信息收集', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('20', '健康干预', '41', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('21', '理论知识', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('22', '健康信息收集', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('23', '健康干预', '42', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('24', '理论知识', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('25', '健康信息收集', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('26', '健康干预', '43', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('27', '健康信息收集', '44', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('28', '健康干预', '44', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('29', '理论知识', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('30', '健康信息收集', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('31', '健康干预', '45', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '50%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('32', '理论学习', '46', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('33', '营养标签', '46', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('34', '理论学习', '47', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('35', '实训环节', '47', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('36', '理论学习', '48', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('37', '实训环节', '48', '2024-11-19 14:09:05', '2024-11-19 14:09:08', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2991', '理论知识', '299', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2992', '慢性阻塞性肺疾病风险评估', '299', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2993', '理论知识', '298', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2994', '糖尿病风险评估', '298', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2995', '理论知识', '297', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2996', '脑卒中风险评估', '297', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2997', '理论知识', '296', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2998', '心血管病风险评估', '296', '2024-08-28 12:58:46', '2024-08-28 12:59:53', '个人', '45', '100%', null, null, null);
INSERT INTO `plat_lesson_son` VALUES ('2999', '理论知识', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3000', '健康信息收集', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3001', '健康干预', '295', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3002', '健康信息收集', '294', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3003', '健康干预', '294', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3004', '理论知识', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3005', '健康信息收集', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3006', '健康干预', '293', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3007', '理论知识', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3008', '健康信息收集', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3009', '健康干预', '288', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3010', '理论知识', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3011', '健康信息收集', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3012', '健康干预', '292', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3013', '理论知识', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3014', '健康信息收集', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3015', '健康干预', '291', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3016', '理论知识', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3017', '健康信息收集', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3018', '健康干预', '290', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3019', '理论知识', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_interfere', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3020', '健康信息收集', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_interfere', 'health');
INSERT INTO `plat_lesson_son` VALUES ('3021', '健康干预', '289', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '90', '90%', 'plat-questionnaire', 'risk_common_interfere', 'risk');
INSERT INTO `plat_lesson_son` VALUES ('3022', '理论学习', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3023', '理论习题', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '12%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3024', '测试方法考核', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '5%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3025', '自我检测', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3026', '训练方法考核', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '27%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3027', '案例评估', '401', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '56%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3028', '理论学习', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3029', '理论习题', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '12%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3030', '测试方法考核', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '28%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3031', '自我检测', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3032', '训练方法考核', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3033', '案例评估', '402', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3034', '理论学习', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3036', '测试方法考核', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '24%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3037', '自我检测', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3038', '训练方法考核', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '8%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3039', '案例评估', '403', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '68%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3040', '理论学习', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3041', '测试方法考核', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '30%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3042', '自我检测', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3043', '训练方法考核', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3044', '案例评估', '404', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '60%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3045', '理论学习', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3046', '测试方法考核', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '15%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3047', '自我检测', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3048', '训练方法考核', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '15%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3049', '案例评估', '405', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3050', '理论学习', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3051', '测试方法考核', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '30%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3052', '自我检测', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3053', '训练方法考核', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3054', '案例评估', '406', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3055', '理论学习', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3056', '测试方法考核', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '35%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3057', '自我检测', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3058', '训练方法考核', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3059', '案例评估', '407', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '45%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3060', '理论学习', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3061', '测试方法考核', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3062', '自我检测', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3063', '训练方法考核', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '50%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3064', '案例评估', '408', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '40%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3065', '理论学习', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3066', '理论习题', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3067', '测试方法考核', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3068', '自我检测', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3069', '训练方法考核', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3070', '案例评估', '409', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '50%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3071', '理论学习', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '60', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3072', '理论习题', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3073', '测试方法考核', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3074', '自我检测', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3075', '训练方法考核', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3076', '案例评估', '410', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '60%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3077', '理论学习', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3078', '测试方法考核', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3079', '自我检测', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3080', '训练方法考核', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3081', '案例评估', '411', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3082', '理论学习', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3083', '测试方法考核', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3084', '自我检测', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3085', '训练方法考核', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3086', '案例评估', '412', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3087', '理论学习', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3088', '测试方法考核', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3089', '自我检测', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3090', '训练方法考核', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3091', '案例评估', '413', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3092', '理论学习', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3093', '测试方法考核', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3094', '自我检测', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3095', '训练方法考核', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3096', '案例评估', '414', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3097', '理论学习', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3098', '测试方法考核', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3099', '自我检测', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3100', '训练方法考核', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3101', '案例评估', '415', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');
INSERT INTO `plat_lesson_son` VALUES ('3102', '理论学习', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'study');
INSERT INTO `plat_lesson_son` VALUES ('3103', '测试方法考核', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '10%', 'plat-questionnaire', 'risk_common_sport', 'question');
INSERT INTO `plat_lesson_son` VALUES ('3104', '自我检测', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '0%', 'plat-questionnaire', 'risk_common_sport', 'self_test');
INSERT INTO `plat_lesson_son` VALUES ('3105', '训练方法考核', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '30', '20%', 'plat-questionnaire', 'risk_common_sport', 'training_method_assessment');
INSERT INTO `plat_lesson_son` VALUES ('3106', '案例评估', '416', '2024-08-28 12:59:56', '2024-08-28 12:59:58', '个人', '40', '70%', 'plat-questionnaire', 'risk_common_sport', 'case_evaluation');

-- ----------------------------
-- Table structure for plat_lesson_student
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_student`;
CREATE TABLE `plat_lesson_student` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '学生课程id',
  `user_id` bigint(32) NOT NULL COMMENT '学生id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `lesson_id` bigint(32) NOT NULL COMMENT '课程id',
  `state` int(1) NOT NULL COMMENT '0未开始，1已结束',
  `schedule_id` bigint(32) NOT NULL COMMENT '课程id',
  `score` varchar(200) DEFAULT '0' COMMENT '分数',
  `complete_time` varchar(200) DEFAULT NULL COMMENT '完成时间',
  `class_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_student
-- ----------------------------
INSERT INTO `plat_lesson_student` VALUES ('161', '111', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('162', '113', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('163', '117', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('164', '119', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');
INSERT INTO `plat_lesson_student` VALUES ('165', '120', '2024-12-18 19:35:31', null, '6', '0', '78', '', '', '25');

-- ----------------------------
-- Table structure for plat_lesson_type
-- ----------------------------
DROP TABLE IF EXISTS `plat_lesson_type`;
CREATE TABLE `plat_lesson_type` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '课程类型',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_lesson_type
-- ----------------------------
INSERT INTO `plat_lesson_type` VALUES ('1', '基于大数据技术的区域性健康风险评估');
INSERT INTO `plat_lesson_type` VALUES ('2', '慢性病人生活质量评估');
INSERT INTO `plat_lesson_type` VALUES ('3', '饮食综合评估及干预虚拟仿真实验软件');
INSERT INTO `plat_lesson_type` VALUES ('4', '运动功能检测与评估');

-- ----------------------------
-- Table structure for plat_major
-- ----------------------------
DROP TABLE IF EXISTS `plat_major`;
CREATE TABLE `plat_major` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '专业id',
  `name` varchar(200) NOT NULL COMMENT '专业名称',
  `manager` varchar(200) DEFAULT '' COMMENT '负责人',
  `manager_mobile` varchar(20) DEFAULT '' COMMENT '负责人联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `college_id` bigint(32) NOT NULL COMMENT '所属学院id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_major
-- ----------------------------
INSERT INTO `plat_major` VALUES ('1', '信息管理与信息系统', '', '', '2024-07-07 12:21:14', '2024-12-13 12:47:21', '21');
INSERT INTO `plat_major` VALUES ('2', '计算机科学与技术', '', '', '2024-07-07 12:53:29', '2024-12-13 12:47:18', '21');

-- ----------------------------
-- Table structure for plat_menu
-- ----------------------------
DROP TABLE IF EXISTS `plat_menu`;
CREATE TABLE `plat_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（1正常 0停用）',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `roles` varchar(500) DEFAULT '' COMMENT '可查看角色',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of plat_menu
-- ----------------------------
INSERT INTO `plat_menu` VALUES ('1', '客户管理', '0', '2', 'plat-manager', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('2', '教职工管理', '1', '2', 'teacher', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@');
INSERT INTO `plat_menu` VALUES ('3', '学生管理', '1', '2', 'student', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('4', '组织管理', '0', '1', 'plat-organization', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('5', '学院管理', '4', '1', 'college', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('6', '专业管理', '4', '1', 'major', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('7', '班级管理', '4', '1', 'class', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('8', '课程资源', '0', '3', 'plat-lesson', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@3@4@');
INSERT INTO `plat_menu` VALUES ('9', '课程列表', '8', '3', 'lesson', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('10', '章节类型列表', '8', '4', 'chapterType', '0', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('11', '章节列表', '8', '5', 'chapter', '0', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@1@2@');
INSERT INTO `plat_menu` VALUES ('12', '课程管理', '8', '6', 'plat-teacher-lesson-manager', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@3@');
INSERT INTO `plat_menu` VALUES ('13', '课程安排', '8', '6', 'plat-teacher-lesson-schedule', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@3@');
INSERT INTO `plat_menu` VALUES ('14', '课程列表', '8', '6', 'plat-student-lesson-manager', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');
INSERT INTO `plat_menu` VALUES ('15', '成绩管理', '0', '6', 'plat-achievement', '1', 'layui-icon-form', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');
INSERT INTO `plat_menu` VALUES ('16', '成绩查询', '15', '6', 'student', '1', '', 'admin', '2024-07-01 19:10:51', '', '2024-07-03 15:24:48', '', '@4@');

-- ----------------------------
-- Table structure for plat_ques_sport_study
-- ----------------------------
DROP TABLE IF EXISTS `plat_ques_sport_study`;
CREATE TABLE `plat_ques_sport_study` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `type` int(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `parent_id` bigint(32) DEFAULT NULL,
  `lesson_id` bigint(32) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=507 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plat_ques_sport_study
-- ----------------------------
INSERT INTO `plat_ques_sport_study` VALUES ('-1', '3', '转身运动测试.mp4', '178', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('0', '3', '米字操.mp4', '187', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('1', '1', 'BMI概念.pdf', null, '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('2', '2', '检测方法', null, '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('3', '1', '国围检测.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('4', '1', 'BMI检测.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('5', '1', '皮褶厚度.pdf', '2', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('6', '2', '训练方法', null, '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('7', '2', '增肌运动', '6', '401', null);
INSERT INTO `plat_ques_sport_study` VALUES ('8', '1', '抗阻运动.pdf', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('9', '1', '柔韧性运动.pdf', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('10', '1', '减脂运动', '6', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('11', '1', '其它肌肉训练.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('12', '1', '深蹲+俯卧撑.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('13', '1', '增肌运动概念.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('14', '1', '坐位抬腿.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('15', '1', '静力靠墙蹲.pdf', '10', '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('16', '1', '注意事项.pdf', null, '401', 'bmi/');
INSERT INTO `plat_ques_sport_study` VALUES ('17', '1', '反应力概念.pdf', null, '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('18', '2', '检测方法', null, '402', '');
INSERT INTO `plat_ques_sport_study` VALUES ('20', '1', '十字跳测验.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('21', '1', '选择反应时测试.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('22', '1', '徒手反应性测试（抓尺实验）.pdf', '18', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('23', '2', '训练方法', null, '402', '');
INSERT INTO `plat_ques_sport_study` VALUES ('24', '2', '初级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('25', '2', '中级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('26', '2', '高级', '23', '402', null);
INSERT INTO `plat_ques_sport_study` VALUES ('27', '1', '口令式训练.pdf', '24', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('28', '1', '智力游戏.pdf', '24', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('29', '1', '口令式训练2.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('30', '1', '球类训练.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('31', '1', '球类训练.pdf', '25', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('32', '1', '手接橡胶弹力球.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('33', '1', '限时折返跑.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('34', '1', '镜面肢体反应训练.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('35', '1', '闭眼转身后踢球或取物.pdf', '26', '402', 'responsiveness/');
INSERT INTO `plat_ques_sport_study` VALUES ('36', '1', '肺功能理论知识.pdf', null, '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('37', '2', '检测方法', null, '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('38', '1', '\r\n徒手肺功能测试（憋气实验）.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('39', '1', '\r\n肺活量测试.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('40', '1', '\r\n\r\n日常生活能力评估.pdf', '37', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('41', '2', '训练方法', null, '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('42', '2', '初级', '41', '403', '');
INSERT INTO `plat_ques_sport_study` VALUES ('43', '2', '中级', '41', '403', '');
INSERT INTO `plat_ques_sport_study` VALUES ('44', '2', '高级', '41', '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('45', '2', '互动游戏', '41', '403', null);
INSERT INTO `plat_ques_sport_study` VALUES ('46', '1', '坐姿深呼吸训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('47', '1', '\r\n行走深呼吸训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('48', '1', '缩唇呼气训练.pdf', '42', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('49', '1', '\r\n腹式呼吸.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('50', '1', '\r\n呼气训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('51', '1', '\r\n（上肢）提重物训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('52', '1', '\r\n提重物方式与训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('53', '1', '\r\n吸气训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('54', '1', '\r\n箭步蹲.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('55', '1', '站立耸肩.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('56', '1', '自重深蹲.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('57', '1', '\r\n仰卧位双下肢屈髋屈膝训练.pdf', '43', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('58', '1', '\r\n呼吸体操.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('59', '1', '\r\n\r\n全身呼吸操--适用于COPD患者\r\n.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('60', '1', '\r\n\r\n家居锻炼.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('61', '1', '\r\n\r\n仰卧腹腔负荷.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('62', '1', '\r\n\r\n仰卧胸腔负荷.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('63', '1', '\r\n\r\n有氧运动训练.pdf', '44', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('64', '1', '\r\n\r\n\r\n吹棉花游戏.pdf', '45', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('65', '1', '\r\n\r\n\r\n吹蜡烛比赛.pdf', '45', '403', 'lungs/');
INSERT INTO `plat_ques_sport_study` VALUES ('66', '1', '核心力概念.pdf', null, '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('67', '2', '核心力能力检测', null, '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('68', '1', '\r\n仰卧起坐.pdf', '67', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('69', '2', '\r\n核心力能力训练方法', null, '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('70', '2', '中级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('71', '2', '高级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('72', '2', '初级', '69', '404', '');
INSERT INTO `plat_ques_sport_study` VALUES ('73', '1', '\r\n双脚触球仰桥.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('74', '1', '\r\n单脚触球仰桥\r\n.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('75', '1', '\r\n后背触球仰桥\r\n.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('76', '1', '\r\n平衡垫俯卧撑.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('77', '1', '\r\n\r\n健身球俯卧撑.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('78', '1', '\r\n\r\n\r\n胸腹触球俯桥.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('79', '1', '\r\n\r\n\r\n\r\n平衡垫平衡式.pdf', '70', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('80', '1', '\r\n\r\n\r\n\r\n\r\n躯干肌肉系统\r\n.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('81', '1', '\r\n\r\n\r\n\r\n强化腹部训练\r\n.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('82', '1', '\r\n\r\n\r\n核心肌肉.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('83', '1', '\r\n\r\n\r\n\r\n斜腹部&腹部训练.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('84', '1', '\r\n\r\n\r\n\r\n腹部练习.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('85', '1', '\r\n\r\n\r\n\r\n斜腹肌.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('86', '1', '\r\n\r\n\r\n\r\n\r\n腰部强化训练.pdf', '71', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('87', '1', '\r\n\r\n\r\n\r\n\r\n\r\n直腿仰卧举腿\r\n.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('88', '1', '\r\n\r\n\r\n\r\n\r\n腹肌板\r\n.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('89', '1', '\r\n\r\n\r\n\r\n屈腿仰卧举腿.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('90', '1', '\r\n\r\n\r\n\r\n\r\n抱头仰卧起坐.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('91', '1', '\r\n\r\n\r\n\r\n\r\n\r\n腹肌的静力性练习.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('92', '1', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n“小飞燕”练习.pdf', '72', '404', 'core/');
INSERT INTO `plat_ques_sport_study` VALUES ('93', '1', '局部力量概念.pdf', null, '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('94', '2', '局部力量检测方法', null, '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('95', '1', '背肌耐力评定\r\n.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('96', '1', '腹肌耐力评定\r\n.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('97', '1', '爆发力检测.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('98', '1', '握力测试.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('99', '1', '握力计测试.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('100', '1', '俯卧撑.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('101', '1', '引体向上.pdf', '94', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('102', '2', '局部力量训练方法', null, '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('103', '2', '初级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('104', '2', '高级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('105', '2', '中级', '102', '405', '');
INSERT INTO `plat_ques_sport_study` VALUES ('106', '1', '前后踮脚尖.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('107', '1', '背部练习.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('108', '1', '\r\n站姿后抬腿\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('109', '1', '站立耸肩.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('110', '1', '站姿前平举.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('111', '1', '半蹲斜下拉.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('112', '1', '\r\n直臂前下压\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('113', '1', '直腿硬拉\r\n.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('114', '1', '站姿肘碰膝.pdf', '103', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('115', '1', '弹力带扩胸.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('116', '1', '俯身展体\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('117', '1', '\r\n俯卧两头起\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('118', '1', '\r\n负重体侧屈.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('119', '1', '\r\n俯卧腿弯举\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('120', '1', '仰卧举腿.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('121', '1', '站姿负重提踵\r\n.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('122', '1', '窄握距卧推.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('123', '1', '俯卧挺身.pdf', '104', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('124', '1', '半蹲.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('125', '1', '俯立划船.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('126', '1', '弯举\r\n.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('127', '1', '直腿使拉\r\n.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('128', '1', '\r\n双臂屈伸.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('129', '1', '\r\n俯卧撑.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('130', '1', '\r\n纵跳.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('131', '1', '\r\n仰卧起坐.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('132', '1', '\r\n仰卧举腿.pdf', '105', '405', 'part/');
INSERT INTO `plat_ques_sport_study` VALUES ('133', '1', '\r\n平衡能力概念.pdf', null, '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('134', '2', '\r\n\r\n平衡能力评估检测', null, '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('135', '1', '\r\n成年人闭眼单脚站立.pdf', '134', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('136', '1', '\r\n\r\n徒手平衡性测试.pdf', '134', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('137', '2', '平衡能力评估训练', null, '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('138', '2', '初级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('139', '2', '高级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('140', '2', '中级', '137', '406', '');
INSERT INTO `plat_ques_sport_study` VALUES ('141', '1', '\r\n\r\n穿袜子、绑鞋带.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('142', '1', '\r\n\r\n单脚站立.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('143', '1', '\r\n\r\n垫上初级训练\r\n.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('144', '1', '\r\n端水走路.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('145', '1', '\r\n\r\n双、单脚尖脚跟站立.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('146', '1', '\r\n\r\n坐姿平衡.pdf', '138', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('147', '1', '\r\n\r\n弹力带腿内收\r\n.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('148', '1', '\r\n弹力带腿前踢.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('149', '1', '\r\n弹力带腿后踢\r\n.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('150', '1', '弹力带腿外展.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('151', '1', '\r\n弹力带蹲起训练.pdf', '139', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('152', '1', '\r\n倒走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('153', '1', '\r\n侧走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('154', '1', '\r\n\r\n独立前踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('155', '1', '\r\n\r\n独立后踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('156', '1', '\r\n\r\n高尔夫球摆臂训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('157', '1', '\r\n\r\n单脚蹲起.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('158', '1', '\r\n\r\n金鸡独立.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('159', '1', '\r\n\r\n弓步训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('160', '1', '\r\n\r\n腾空后踢.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('161', '1', '\r\n\r\n头顶物走.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('162', '1', '\r\n\r\n弯腰伸臂.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('163', '1', '\r\n\r\n网球摆臂训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('164', '1', '\r\n\r\n燕式平衡.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('165', '1', '\r\n\r\n直线猫爬.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('166', '1', '\r\n\r\n足球踢腿训练.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('167', '1', '\r\n\r\n走直线.pdf', '140', '406', 'balance/');
INSERT INTO `plat_ques_sport_study` VALUES ('168', '1', '柔韧性功能概念.pdf', null, '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('169', '2', '测试方法', null, '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('170', '1', '肩关节柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('171', '1', '脊柱前屈柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('172', '1', '髋部和脊柱屈伸功能测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('173', '1', '脊柱转向柔韧性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('174', '1', '颈、躯干和骨盆转动的流畅性测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('175', '1', '肩关节功能测试.pdf', '169', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('176', '2', '\r\n训练方法', null, '407', null);
INSERT INTO `plat_ques_sport_study` VALUES ('177', '2', '初级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('178', '2', '中级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('179', '2', '高级', '176', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('180', '2', '1、颈部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('181', '2', '2、肩部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('182', '2', '3、胸，腹部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('183', '2', '4、脊柱拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('184', '2', '5、胯，腹部练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('185', '2', '6、腿部拉伸练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('186', '2', '7、加大膝关节的ROM的练习', '177', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('187', '2', '1、颈部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('188', '2', '2、肩部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('189', '2', '3、躯干部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('190', '2', '4、腿部', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('191', '2', '5、裸关节', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('192', '2', '6、前表线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('193', '2', '7、手臂线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('194', '2', '8、前深筋膜拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('195', '2', '9、后表线拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('196', '2', '10、腰肌拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('197', '2', '11、跟腱拉伸', '178', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('198', '2', '1、前表线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('199', '2', '2、后表线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('200', '2', '3、体侧线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('201', '2', '4、臂前深线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('202', '2', '5、前深线滚动', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('203', '2', '6、肌肉加强训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('204', '2', '7、弹力带训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('205', '2', '8、弹射训练', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('206', '2', '9、互动游戏', '179', '407', '');
INSERT INTO `plat_ques_sport_study` VALUES ('207', '1', '注意事项.pdf', null, '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('208', '1', '颈回旋运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('209', '1', '颈部助力练习\n\n.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('210', '1', '颈屈伸运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('211', '1', '颈侧倾运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('212', '1', '颈左右旋转运动.pdf', '180', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('213', '1', '爬墙练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('214', '1', '肩前屈助力练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('215', '1', '肩前屈练习.pdf', '181', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('216', '1', '拉伸二：跪姿后屈拉伸.pdf', '182', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('217', '1', '拉伸一：站立后仰拉伸.pdf', '182', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('218', '1', '跪撑.pdf', '183', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('219', '1', '身体站直.pdf', '183', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('220', '1', '抬腿画圈练习.pdf', '184', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('221', '1', '鞠躬式拉伸.pdf', '184', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('222', '1', '腹股沟部拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('223', '1', '股四头肌拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('224', '1', '奈绳肌拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('225', '1', '小腿部拉伸练习.pdf', '185', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('226', '1', '膝关节屈曲练习.pdf', '186', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('227', '1', '米字操.pdf', '187', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('228', '1', '背后拉手.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('229', '1', '直臂压肩.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('230', '1', '振臂运动.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('231', '1', '头后单手拉肩.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('232', '1', '肩关节的PNF练习.pdf', '188', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('233', '1', '转体运动.pdf', '189', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('234', '1', '侧压.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('235', '1', '奈绳肌的PNFl练习.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('236', '1', '坐式体前倾.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('237', '1', '坐位体前屈.pdf', '190', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('238', '1', '跪坐脚踝.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('239', '1', '脚跟走.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('240', '1', '脚尖走.pdf', '191', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('241', '1', '股四头肌拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('242', '1', '伸展拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('243', '1', '前表线拉伸.pdf', '192', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('244', '1', '肩部拉伸.pdf', '193', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('245', '1', '后表线滚动.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('246', '1', '前深线滚动.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('247', '1', '内收肌拉伸.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('248', '1', '筋膜.pdf', '194', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('249', '1', '火烈鸟式一.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('250', '1', '猫式二.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('251', '1', '猫式一.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('252', '1', '火烈鸟式二.pdf', '195', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('253', '1', '腰肌拉伸.pdf', '196', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('254', '1', '跟腱拉伸二.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('255', '1', '跟腱拉伸一.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('256', '1', '跟腱拉伸三.pdf', '197', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('257', '1', '股四头肌.pdf', '198', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('258', '1', '胫前肌.pdf', '198', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('259', '1', '大腿后侧.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('260', '1', '臀部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('261', '1', '后背部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('262', '1', '枕部.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('263', '1', '小腿后侧.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('264', '1', '足底筋膜.pdf', '199', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('265', '1', '髂胫束.pdf', '200', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('266', '1', '胸廓外侧.pdf', '200', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('267', '1', '胸小肌.pdf', '201', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('268', '1', '内收肌.pdf', '202', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('269', '1', '海星式.pdf', '203', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('270', '1', '全身螺旋式.pdf', '203', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('271', '1', '二式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('272', '1', '三式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('273', '1', '一式.pdf', '204', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('274', '1', '多方向跳跃.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('275', '1', '火箭腿.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('276', '1', '后背部跳跃.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('277', '1', '屈臂撑墙.pdf', '205', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('278', '1', '推手练习.pdf', '206', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('279', '1', '关氏操.pdf', '206', '407', 'soft/');
INSERT INTO `plat_ques_sport_study` VALUES ('280', '1', '协调能力概念.pdf', null, '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('281', '2', '\r\n检测方法', null, '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('282', '1', '跟膝胫试验.pdf', '281', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('283', '1', '指鼻和指他人指试验.pdf', '281', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('284', '2', '\r\n训练方法', null, '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('285', '2', '\r\n初级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('286', '2', '\r\n中级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('287', '2', '\r\n高级', '284', '408', '\n');
INSERT INTO `plat_ques_sport_study` VALUES ('288', '1', '交替屈肘.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('289', '1', '侧面交替摸肩.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('290', '1', '交替手上举.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('291', '1', '前臂交替旋前、旋后.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('292', '1', '屈肘交替摸肩.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('293', '1', '双脚交替触地.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('294', '1', '双腿屈膝剪刀腿.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('295', '1', '双腿直膝剪刀腿.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('296', '1', '原地踏步走.pdf', '285', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('297', '1', '对指练习.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('298', '1', '弓箭步转身运动.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('299', '1', '五指敲击.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('300', '1', '原地高抬腿.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('301', '1', '原地小步跑.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('302', '1', '拳掌相击.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('303', '1', '左右跨跳.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('304', '1', '指鼻练习.pdf', '286', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('305', '1', '手眼协调练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('306', '1', '节律性动作练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('307', '1', '身体动作练习.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('308', '1', '开合跳.pdf', '287', '408', '\ncoordinate/');
INSERT INTO `plat_ques_sport_study` VALUES ('309', '1', '心功能能力概念.pdf', null, '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('310', '2', '\r\n心功能能力训练方法', null, '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('311', '2', '心功能能力检测方法', null, '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('312', '1', '300米健身走.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('313', '1', '\r\n六分钟步行实验.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('314', '1', '\r\n徒手心功能测试.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('315', '1', '台阶实验测试方法.pdf', '311', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('316', '2', '初级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('317', '2', '中级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('318', '2', '高级', '310', '409', null);
INSERT INTO `plat_ques_sport_study` VALUES ('319', '1', '慢跑.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('320', '1', '体感游戏.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('321', '1', '健步走.pdf', '316', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('322', '1', '跳绳.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('323', '1', '功率自行车.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('324', '1', '高抬腿.pdf', '317', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('325', '1', '波比跳.pdf', '318', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('326', '1', '\r\nX-CO双合跳.pdf', '318', '409', 'cardiacFunctionalAbility/');
INSERT INTO `plat_ques_sport_study` VALUES ('327', '1', '本体感觉概念.pdf', null, '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('328', '2', '本体感觉测试方法', null, '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('329', '2', '本体感觉训练方法', null, '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('330', '1', '闭眼复杂重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('331', '1', '闭眼重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('332', '1', '\r\n睁眼复杂重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('333', '1', '睁眼重建法.pdf', '328', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('334', '2', '初级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('335', '2', '中级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('336', '2', '高级', '329', '410', null);
INSERT INTO `plat_ques_sport_study` VALUES ('337', '1', '对指练习.pdf', '334', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('338', '1', '指鼻练习.pdf', '334', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('339', '1', '“螃蟹先生”游戏.pdf', '335', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('340', '1', '“摸一摸”游戏.pdf', '335', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('341', '1', '闭目踏步训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('342', '1', '闭眼直线行走.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('343', '1', '闭目单腿站立训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('344', '1', '倒着走训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('345', '1', '脚踩鹅卵石行走训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('346', '1', '黑箱触物.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('347', '1', '\r\n跨越障碍物训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('348', '1', '\r\n上下楼梯控制训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('349', '1', '踏步训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('350', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('351', '1', '跳跃训练.pdf', '336', '410', 'proprioception/');
INSERT INTO `plat_ques_sport_study` VALUES ('352', '1', '本体感觉概念.pdf', null, '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('353', '2', '本体感觉测试方法', null, '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('354', '2', '本体感觉训练方法', null, '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('355', '1', '闭眼复杂重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('356', '1', '闭眼重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('357', '1', '\r\n睁眼复杂重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('358', '1', '睁眼重建法.pdf', '353', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('359', '2', '初级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('360', '2', '中级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('361', '2', '高级', '354', '411', '');
INSERT INTO `plat_ques_sport_study` VALUES ('362', '1', '对指练习.pdf', '359', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('363', '1', '指鼻练习.pdf', '359', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('364', '1', '“螃蟹先生”游戏.pdf', '360', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('365', '1', '“摸一摸”游戏.pdf', '360', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('366', '1', '闭目踏步训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('367', '1', '闭眼直线行走.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('368', '1', '闭目单腿站立训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('369', '1', '倒着走训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('370', '1', '脚踩鹅卵石行走训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('371', '1', '黑箱触物.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('372', '1', '\r\n跨越障碍物训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('373', '1', '\r\n上下楼梯控制训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('374', '1', '踏步训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('375', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('376', '1', '跳跃训练.pdf', '361', '411', 'bedJumpingExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('377', '1', '本体感觉概念.pdf', null, '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('378', '2', '本体感觉测试方法', null, '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('379', '2', '本体感觉训练方法', null, '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('380', '1', '闭眼复杂重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('381', '1', '闭眼重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('382', '1', '\r\n睁眼复杂重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('383', '1', '睁眼重建法.pdf', '328', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('384', '2', '初级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('385', '2', '中级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('386', '2', '高级', '329', '412', '');
INSERT INTO `plat_ques_sport_study` VALUES ('387', '1', '对指练习.pdf', '334', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('388', '1', '指鼻练习.pdf', '334', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('389', '1', '“螃蟹先生”游戏.pdf', '335', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('390', '1', '“摸一摸”游戏.pdf', '335', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('391', '1', '闭目踏步训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('392', '1', '闭眼直线行走.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('393', '1', '闭目单腿站立训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('394', '1', '倒着走训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('395', '1', '脚踩鹅卵石行走训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('396', '1', '黑箱触物.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('397', '1', '\r\n跨越障碍物训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('398', '1', '\r\n上下楼梯控制训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('399', '1', '踏步训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('400', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('401', '1', '跳跃训练.pdf', '336', '412', 'healthExercises/');
INSERT INTO `plat_ques_sport_study` VALUES ('402', '1', '本体感觉概念.pdf', null, '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('403', '2', '本体感觉测试方法', null, '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('404', '2', '本体感觉训练方法', null, '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('405', '1', '闭眼复杂重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('406', '1', '闭眼重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('407', '1', '\r\n睁眼复杂重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('408', '1', '睁眼重建法.pdf', '403', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('409', '2', '初级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('410', '2', '中级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('411', '2', '高级', '404', '413', '');
INSERT INTO `plat_ques_sport_study` VALUES ('412', '1', '对指练习.pdf', '409', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('413', '1', '指鼻练习.pdf', '409', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('414', '1', '“螃蟹先生”游戏.pdf', '410', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('415', '1', '“摸一摸”游戏.pdf', '410', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('416', '1', '闭目踏步训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('417', '1', '闭眼直线行走.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('418', '1', '闭目单腿站立训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('419', '1', '倒着走训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('420', '1', '脚踩鹅卵石行走训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('421', '1', '黑箱触物.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('422', '1', '\r\n跨越障碍物训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('423', '1', '\r\n上下楼梯控制训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('424', '1', '踏步训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('425', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('426', '1', '跳跃训练.pdf', '411', '413', 'householdExercise/');
INSERT INTO `plat_ques_sport_study` VALUES ('427', '1', '本体感觉概念.pdf', null, '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('428', '2', '本体感觉测试方法', null, '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('429', '2', '本体感觉训练方法', null, '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('430', '1', '闭眼复杂重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('431', '1', '闭眼重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('432', '1', '\r\n睁眼复杂重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('433', '1', '睁眼重建法.pdf', '428', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('434', '2', '初级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('435', '2', '中级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('436', '2', '高级', '429', '414', null);
INSERT INTO `plat_ques_sport_study` VALUES ('437', '1', '对指练习.pdf', '434', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('438', '1', '指鼻练习.pdf', '434', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('439', '1', '“螃蟹先生”游戏.pdf', '435', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('440', '1', '“摸一摸”游戏.pdf', '435', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('441', '1', '闭目踏步训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('442', '1', '闭眼直线行走.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('443', '1', '闭目单腿站立训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('444', '1', '倒着走训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('445', '1', '脚踩鹅卵石行走训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('446', '1', '黑箱触物.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('447', '1', '\r\n跨越障碍物训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('448', '1', '\r\n上下楼梯控制训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('449', '1', '踏步训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('450', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('451', '1', '跳跃训练.pdf', '436', '414', 'handsOnHealthExercises1/');
INSERT INTO `plat_ques_sport_study` VALUES ('452', '1', '本体感觉概念.pdf', null, '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('453', '2', '本体感觉测试方法', null, '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('454', '2', '本体感觉训练方法', null, '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('455', '1', '闭眼复杂重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('456', '1', '闭眼重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('457', '1', '\r\n睁眼复杂重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('458', '1', '睁眼重建法.pdf', '453', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('459', '2', '初级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('460', '2', '中级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('461', '2', '高级', '454', '415', null);
INSERT INTO `plat_ques_sport_study` VALUES ('462', '1', '对指练习.pdf', '459', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('463', '1', '指鼻练习.pdf', '459', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('464', '1', '“螃蟹先生”游戏.pdf', '460', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('465', '1', '“摸一摸”游戏.pdf', '460', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('466', '1', '闭目踏步训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('467', '1', '闭眼直线行走.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('468', '1', '闭目单腿站立训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('469', '1', '倒着走训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('470', '1', '脚踩鹅卵石行走训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('471', '1', '黑箱触物.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('472', '1', '\r\n跨越障碍物训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('473', '1', '\r\n上下楼梯控制训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('474', '1', '踏步训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('475', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('476', '1', '跳跃训练.pdf', '461', '415', 'handsOnHealthExercises2/');
INSERT INTO `plat_ques_sport_study` VALUES ('477', '1', '本体感觉概念.pdf', null, '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('478', '2', '本体感觉测试方法', null, '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('479', '2', '本体感觉训练方法', null, '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('480', '1', '闭眼复杂重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('481', '1', '闭眼重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('482', '1', '\r\n睁眼复杂重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('483', '1', '睁眼重建法.pdf', '478', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('484', '2', '初级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('485', '2', '中级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('486', '2', '高级', '479', '416', null);
INSERT INTO `plat_ques_sport_study` VALUES ('487', '1', '对指练习.pdf', '484', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('488', '1', '指鼻练习.pdf', '484', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('489', '1', '“螃蟹先生”游戏.pdf', '485', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('490', '1', '“摸一摸”游戏.pdf', '485', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('491', '1', '闭目踏步训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('492', '1', '闭眼直线行走.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('493', '1', '闭目单腿站立训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('494', '1', '倒着走训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('495', '1', '脚踩鹅卵石行走训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('496', '1', '黑箱触物.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('497', '1', '\r\n跨越障碍物训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('498', '1', '\r\n上下楼梯控制训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('499', '1', '踏步训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('500', '1', '\r\n仰卧足底靠墙滑动训练.pdf', '486', '416', 'lazyTraining/');
INSERT INTO `plat_ques_sport_study` VALUES ('501', '1', '跳跃训练.pdf', '486', '416', 'lazyTraining/');

-- ----------------------------
-- Table structure for plat_role
-- ----------------------------
DROP TABLE IF EXISTS `plat_role`;
CREATE TABLE `plat_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `role_id` int(2) NOT NULL COMMENT '角色',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of plat_role
-- ----------------------------
INSERT INTO `plat_role` VALUES ('1', '1', '超级管理员');
INSERT INTO `plat_role` VALUES ('2', '2', '普通管理员');
INSERT INTO `plat_role` VALUES ('3', '3', '教师');
INSERT INTO `plat_role` VALUES ('4', '4', '学生');

-- ----------------------------
-- Table structure for plat_user
-- ----------------------------
DROP TABLE IF EXISTS `plat_user`;
CREATE TABLE `plat_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(2000) NOT NULL COMMENT '用户账号',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `nick_name` varchar(200) NOT NULL COMMENT '用户昵称',
  `role` int(2) NOT NULL COMMENT '角色',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `mobile` varchar(200) DEFAULT '' COMMENT '手机号码',
  `email` varchar(200) DEFAULT '' COMMENT '邮箱',
  `status` int(2) DEFAULT NULL COMMENT '帐号状态（0正常 1停用）',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `college_id` bigint(20) DEFAULT NULL COMMENT '学院id',
  `major_id` bigint(20) DEFAULT NULL COMMENT '专业id',
  `sex` varchar(5) DEFAULT NULL COMMENT '姓名',
  `class_id` bigint(20) DEFAULT NULL COMMENT '班级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of plat_user
-- ----------------------------
INSERT INTO `plat_user` VALUES ('1', 'hw_root', '96e79218965eb72c92a549dd5a330112', '王总', '1', '超级管理员', '', '1@qq.com', '1', '2025-01-13 20:44:52', '', '2024-07-03 15:03:36', 'hw_root', '2024-07-10 17:47:39', '', '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('2', 'admin1', '96e79218965eb72c92a549dd5a330112', '吕总', '2', '普通管理员', '', '', '1', '2024-09-30 09:43:29', '', '2024-07-03 15:03:36', '王总', '2024-12-13 12:55:24', '', '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('103', 'admin2', '96e79218965eb72c92a549dd5a330112', '司总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:06:17', '王总', '2024-09-13 22:32:38', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('106', 'teacher_mei', '96e79218965eb72c92a549dd5a330112', '美老师', '3', '教师', '18888888888', '', '1', '2024-12-18 20:51:18', null, '2024-07-10 18:24:32', 'teacher_mei', '2024-12-13 15:37:11', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('107', 'admin3', '96e79218965eb72c92a549dd5a330112', '孙总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:24:41', '王总', '2024-12-13 12:55:20', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('108', 'admin4', '96e79218965eb72c92a549dd5a330112', '钱总', '2', '普通管理员', '', '', '1', null, null, '2024-07-10 18:24:54', '王总', '2024-12-13 12:55:17', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('109', 'teacher_zhao', '96e79218965eb72c92a549dd5a330112', '赵老师', '3', '教师', '', '', '1', '2024-12-13 14:50:12', null, '2024-07-10 18:25:04', '王总', '2024-12-13 12:55:27', null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('110', 'student_1', '96e79218965eb72c92a549dd5a330112', '同学1', '4', '学生', '', '', '1', '2024-09-13 22:37:29', null, '2024-07-10 20:02:21', '王总', '2024-12-18 19:19:22', null, '21', '2', null, '26');
INSERT INTO `plat_user` VALUES ('111', 'student_2', '96e79218965eb72c92a549dd5a330112', '同学2', '4', '学生', '', '243', '1', null, null, '2024-07-10 20:02:29', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('113', 'student_3', '96e79218965eb72c92a549dd5a330112', '同学3', '4', '学生', '', '1', '1', null, '关羽1111111111111111111111111111', '2024-07-11 20:11:43', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('117', 'student_4', '96e79218965eb72c92a549dd5a330112', '同学4', '4', '学生', '', '', '1', '2024-08-20 15:18:59', '关羽1111111111111111111111111111', '2024-07-12 14:45:28', '王总', '2024-12-18 19:20:23', null, '21', '2', null, '25');
INSERT INTO `plat_user` VALUES ('118', 'student_5', '96e79218965eb72c92a549dd5a330112', '同学5', '4', '学生', '', '', '1', null, '关羽1111111111111111111111111111', '2024-07-12 15:43:57', '王总', '2024-12-18 19:29:37', null, '21', '2', null, '26');
INSERT INTO `plat_user` VALUES ('119', 'student_6', '96e79218965eb72c92a549dd5a330112', '同学6', '4', '学生', '', '', '1', null, '关羽1111111111111111111111111111', '2024-07-12 15:56:10', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('120', 'student_7', '96e79218965eb72c92a549dd5a330112', '同学7', '4', '学生', '', '', '1', '2024-12-18 20:17:44', '关羽1111111111111111111111111111', '2024-07-12 15:56:14', null, null, null, '21', '1', null, '25');
INSERT INTO `plat_user` VALUES ('121', 'student_8', '96e79218965eb72c92a549dd5a330112', '同学8', '4', '学生', '', '', '1', '2024-12-18 19:30:05', '关羽1111111111111111111111111111', '2024-07-12 15:57:06', '王总', '2024-12-18 19:19:45', null, '21', '2', null, '26');

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务详细信息表';

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
