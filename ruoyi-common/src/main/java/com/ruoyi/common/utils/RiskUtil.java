package com.ruoyi.common.utils;

import java.util.HashMap;
import java.util.Map;

public class RiskUtil {
    public static String DOUHAO = ",";
    public static String DANXUAN = "1";
    public static String SHUANGGOU = "2";

    public static void main(String[] args) {
        for(int i=1;i<=3;i++){
            for(int j=1;j<=11;j++){
                for(int k=1;k<=5;k++){
                    String ss = i+"-"+j+"-"+k;
                    //System.out.println("INSERT INTO `plat_questionnaire` VALUES (null, 'Berg 平衡量表', '"+ss+"', "+(k-1)+", '3');");
                }
            }
        }

        for(int i=1;i<=52;i++){
                for(int k=1;k<=4;k++){
                    System.out.println("INSERT INTO `plat_questionnaire` VALUES (null, '健康促进生活方式量表', '"+i+"-"+k+"', "+(k-1)+", '2');");
            }
        }
    }

    public static Float getBmi(String weight, String height) {
        if(StringUtils.isEmpty(weight)||StringUtils.isEmpty(height)){
            return null;
        }

        if (isFloat(weight) && isFloat(height)) {
            return Float.parseFloat(weight)/(Float.parseFloat(height)/100*Float.parseFloat(height)/100);
        }
        return null;
    }

    public static String BMIAnakyse(Float bmi) {
        if (bmi == null) {
            return "无";
        }
        if (bmi < 18.5) {
            return "偏瘦";
        } else if (bmi >= 24 && bmi < 28) {
            return "偏胖";
        } else if (bmi >= 28) {
            return "肥胖";
        }
        return "正常";
    }

    public static boolean isFloat(String str) {
        try {
            Float.parseFloat(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
