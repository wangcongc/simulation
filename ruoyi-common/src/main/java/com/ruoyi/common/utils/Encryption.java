package com.ruoyi.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

public class Encryption {

    // 普通加密
    public static String encryptAndUncrypt(String value) {
        char secret = '#';
        byte[] bt = value.getBytes();

        for (int i = 0; i < bt.length; i++) {
            bt[i] = (byte) (bt[i] ^ (int) secret);
        }
        return new String(bt, 0, bt.length);
    }

    public static void main(String[] args) {

        String value = "2014-12-11 01:57:39";
        System.out.println(md5s(value));
    }

    // MD5加密
    public static String md5s(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            // str = buf.toString();
            // System.out.println("result: " + buf.toString());// 32位的加密
            // System.out.println("result: " + buf.toString().substring(8,
            // 24));// 16位的加密
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取用户的BMI
     *
     * @param height
     * @param weight
     * @return
     * @author pany
     */
    public String getBMI(String height, String weight) {
        if ("0".equals(height))
            return "0";
        double heightDou = Double.valueOf(height);
        double weightDou = Double.valueOf(weight);
        heightDou = heightDou / 100;
        String BMI = "0";
        DecimalFormat format = new DecimalFormat("0.0");
        BMI = format.format(Float.parseFloat(format.format(weightDou
                / (heightDou * heightDou))));
        return BMI;
    }

}
