package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.ResistanceVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 17:44
 **/
public class TensileConstant {
    public static final List<ResistanceVO> TENSILE_VO_LIST = new ArrayList<>();

    static {
        TENSILE_VO_LIST.add(new ResistanceVO(1,"颈部拉伸练习--颈屈神旋转运动","站立位或坐位，尽量低头，停留，还原；慢慢抬头，停留，还原。注意练习时调节呼吸，不要憋气","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(2,"颈部拉伸练习--颈左右旋转运动","站立位或坐位，收下颌，头尽量左转，停留，还原；头尽量右转，停留，还原， 注意练习时调节呼吸，不要憋气","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(3,"颈部拉伸练习--颈侧倾运动","站立位或坐位，尽量侧头，停留，还原；做另一侧。注意练习时动作不要过快，避免拉伤肌肉韧带","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(4,"颈部拉伸练习--颈回旋运动","站立位或坐位，头顺时针、逆时针各转动3-4周。注意练习时动作不要过快，避免拉伤肌肉韧带","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(5,"颈部拉伸练习--米字操","头部进行向前，向后，向左，向右，转左上，转右下，转左下，转右下的方向转动，注意转向时动作缓慢进行，不要太快，否则容易发生肌肉韧带拉伤的问题","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(6,"肩膀上臂部拉伸练习--肩前屈练习","两手平放在地面上，尽量前伸，臂部靠近双足。注意练习时前屈速度不要过快，避免拉伤肌肉韧带","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(7,"肩膀上臂部拉伸练习--肩前屈助力练习","右手将左臂拉向背部的中心线，左右交换练习。此方法常用于拉伸肩关节何肱三头肌。注意练习时要遵循循序渐进的原则，避免拉伤肌肉韧带","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(8,"肩膀上臂部拉伸练习--爬墙练习","面对墙(或肋木)站立，做手指爬墙运动。尽量达到所能达到的高度(不要耸肩，上体保持正直)，做10次;身体稍侧转，做10次;再稍侧转，做10次，直至身体侧对墙壁。此练习可作为肩周炎患者的常规练习。注意练习时调节呼吸不要憋气","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(9,"肩部拉伸练习--直臂压肩","训练者双脚开立，手臂与肩同宽压在稳固的椅子上，低头向下压肩至最大幅度，在极限处维持5-10s","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(10,"肩部拉伸练习--振臂运动","一侧手臂上举后伸，另外一侧手臂从腰部向后伸，慢慢向后振至最大幅度，在极限位置维持5-10s，交替进行练习，注意在练习时调节呼吸，不要憋气","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(11,"肩部拉伸练习--背后拉手","一手手心贴颈部沿脊柱向下，一手手背贴背部沿脊柱向上，双手拉着毛巾上下移动，注意拉伸时适可而止，不要过度拉伸。推荐每天进行练习","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(12,"肩部拉伸练习--头后单手拉肩","训练者一侧手从肩关节处缓慢向头部后侧拉，另外一侧手臂从腰部向后拉伸，注意头部不要前伸，两侧手臂指尖尽量触碰在一起","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(13,"胸，腹部拉伸练习--站立后仰拉伸","身体双脚开立与肩同宽，两手撑腰，使躯干向后撑成弓形，头尽量后仰，感到胸腹受到拉伸后，停留10-15s","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(14,"胸，腹部拉伸练习--跪姿后屈拉伸","①跪立位，两手放在腰部，上体后屈； ②柔韧性提高后，身体进一步后倒，两手后倒，两手撑地，停留； ③柔韧性很好的人，可以尝试不撑地，头部顶着地面","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(15,"脊柱拉伸练习--身体站直","吸气两腿分开，两臂侧平举，呼气身体右后转达同时右手放在腰后，左手扶在右肩上，保持呼吸，吸气身体转正，两臂放下，之后反方向","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(16,"脊柱拉伸练习--跪撑","吸气低头整个脊部上拱，低头，收腹;呼气背部下塌头上抬，臀上伸，腰放松， 重复10～12次","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(17,"胯，腹部拉伸练习--抬腿画圈练习","身体躺平，单腿上抬，顺时针做画圈运动，再逆时针方向做，每个方向做12次。注意练习时速度不要过快，左右侧交替练习","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(18,"胯，腹部拉伸练习--鞠躬式拉伸","身体坐直，两脚对外撑，吸气头向上，脊柱立直；呼气身体向前压，保持呼吸，停20-30s，重复2-3次。注意练习时速度不要过快，避免拉伤，左右侧交替练习","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(19,"腿部拉伸练习--小腿部拉伸练习","可以利用台阶，楼梯等进行练习，将一个脚掌踏在选定的物体边缘上，伸膝，身体重心向上，足跟向下。感到拉伸后，停留5-10s，同样的方法做另外一侧。重复2-3次。","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(20,"腿部拉伸练习--腘绳肌拉伸练习","仰卧位，双手抱一侧膝关节，使其尽量靠近胸部，感到拉伸时，停留；进一步使膝关节尽量靠近头部，有时需要助手帮助完成练习。用同样方法做另外一侧练习，注意左右侧交替练习","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(21,"腿部拉伸练习--股四头肌拉伸练习","用右手握住左脚，将左脚尽量拉向左臀部，感到股四头肌受到拉伸后，停留10-15s。用同样的方法做另外一侧的练习。注意练习时速度不要过快，避免拉伤肌肉韧带，左右侧交替练习","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(22,"腿部拉伸练习--腹股沟部位拉伸练习","盘腿而坐，两足相对，足跟尽量靠近臀部，上体前倾，大腿内侧感到拉伸后，停留。注意练习时感到肌肉韧带有拉伸感，或有轻微的疼痛感即可，左右侧交替练习","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(23,"加大膝关节ROM练习","俯卧位，两手握毛巾两端，中间套在患侧踝关节处，手拉毛巾，帮助患侧膝屈曲。在极限处维持5-10s，注意拉伸时要循序渐进，不要过度拉伸","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(24,"腘绳肌PNF练习","仰卧位，一侧下肢平放，在助手帮助下，另一侧下肢尽力直膝抬腿至最大限度后，在助手用力顶住的同时，练习者用力下压(伸髋)，做伸髋肌群的静力性收缩，放松。这时下肢可以很容易地抬高到一个新的最大限度，再用力下压，反复进行","初级"));
        TENSILE_VO_LIST.add(new ResistanceVO(25,"肩关节PNF练习","坐位，助手位于练习者身后。练习者在助手的帮助下，上肢屈肘上举（肩关节屈曲（到最大限度后，练习者向前下方用（肩关节伸），助手用对抗，持续10s左右，放松。上肢上举到新的最大限度，再用力下压，重复3-5次练习","中级"));
        TENSILE_VO_LIST.add(new ResistanceVO(26,"前表线滚动--股四头肌","趴在瑜伽垫上，足尖触地，将泡沫轴置于一侧大腿下方，且将重心转移至该侧，另一侧腿屈髋屈膝以提供支撑，做前后方向的水平运动","高级"));
        TENSILE_VO_LIST.add(new ResistanceVO(27,"前表线滚动--胫前肌","跪坐于瑜伽垫上，脚背朝地，将泡沫轴置于一侧踝部，重心转移至泡沫轴侧使身体重量压于泡沫轴上，然后向前弯腰，用双手撑地，再缓慢将重心向臀后部挪动，使泡沫轴在小腿正前方来回滚动","高级"));
        TENSILE_VO_LIST.add(new ResistanceVO(28,"后表线滚动--大腿后侧","长坐位于瑜伽垫上，双手后伸以支撑躯干，使臀部抬离地面，一侧腿屈髋屈膝，脚掌触地以提供支撑，另一侧腿置于泡沫轴上，做前后方向的来回滚动，从踝关节滚到膝关节处，注意左右侧交替进行练习","高级"));
        TENSILE_VO_LIST.add(new ResistanceVO(29,"后表线滚动--后背部","仰卧，泡沫轴置于双肩下方，双腿屈曲以提供支撑，然后伸直双腿使泡沫轴在后背部滚动","高级"));
        TENSILE_VO_LIST.add(new ResistanceVO(30,"后表线滚动--臀部","长坐位于瑜伽垫上，双手后伸以支撑躯干，使臀部抬离地面，将泡沫轴置于臀部一侧，对侧腿屈髋屈膝，脚掌触地以提供支撑，同侧踝关节置于对侧膝关节处，做环转运动","高级"));
        TENSILE_VO_LIST.add(new ResistanceVO(31,"后表线滚动--小腿后侧","长坐位于瑜伽垫上，双手后伸以支撑躯干，使臀部抬离地面，一侧腿屈髋屈膝，脚掌触地以提供支撑，另一侧腿置于泡沫轴上，做前后方向的来回滚动，从踝关节滚到膝关节处。注意臀部要抬起，左右侧交替进行练习","高级"));
    }
}
