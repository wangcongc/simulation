package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.FoodLabelsOptions;
import com.ruoyi.system.domain.vo.FoodLabelsVO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ruoyi.system.constant.StaticPath.FOODLABELS_PREFIX;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-28 16:47
 **/
public class FoodLabelsConstant {
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_ONE = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_TWO = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_THREE = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_FORE = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_FIVE = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_SIX = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_SEVEN = new ArrayList<>();
    public static final List<FoodLabelsOptions> QUESTION_OPTIONS_EIGHT = new ArrayList<>();

    public static final List<FoodLabelsVO> FOOD_LABELS_VO_LIST = new ArrayList<>();
    public static final List<FoodLabelsVO> HYPERTENSION_FOOD_LABELS_VO_LIST = new ArrayList<>();
    public static final List<FoodLabelsVO> DIABETES_FOOD_LABELS_VO_LIST = new ArrayList<>();



    static {
        QUESTION_OPTIONS_ONE.add(new FoodLabelsOptions(1,"A","1","蛋糕",FOODLABELS_PREFIX + "蛋糕.png"));
        QUESTION_OPTIONS_ONE.add(new FoodLabelsOptions(2,"B","2","苦荞麦饼干",FOODLABELS_PREFIX + "苦荞麦饼干.png"));
        QUESTION_OPTIONS_ONE.add(new FoodLabelsOptions(3,"C","3","苏打饼干",FOODLABELS_PREFIX + "苏打饼干.png"));
        QUESTION_OPTIONS_ONE.add(new FoodLabelsOptions(4,"D","4","威化饼干",FOODLABELS_PREFIX + "威化饼干.png"));

        QUESTION_OPTIONS_TWO.add(new FoodLabelsOptions(1,"A","1","黑椒汁",FOODLABELS_PREFIX + "黑椒汁.png"));
        QUESTION_OPTIONS_TWO.add(new FoodLabelsOptions(2,"B","2","黑芝麻酱",FOODLABELS_PREFIX + "黑芝麻酱.png"));
        QUESTION_OPTIONS_TWO.add(new FoodLabelsOptions(3,"C","3","花生酱",FOODLABELS_PREFIX + "花生酱.png"));
        QUESTION_OPTIONS_TWO.add(new FoodLabelsOptions(4,"D","4","蓝莓酱",FOODLABELS_PREFIX + "蓝莓酱.png"));

        QUESTION_OPTIONS_THREE.add(new FoodLabelsOptions(1,"A","1","牛肉干",FOODLABELS_PREFIX + "牛肉干.png"));
        QUESTION_OPTIONS_THREE.add(new FoodLabelsOptions(2,"B","2","蒜蓉辣面片",FOODLABELS_PREFIX + "蒜蓉辣面片.png"));
        QUESTION_OPTIONS_THREE.add(new FoodLabelsOptions(3,"C","3","盐焗鸡翅",FOODLABELS_PREFIX + "盐焗鸡翅.png"));
        QUESTION_OPTIONS_THREE.add(new FoodLabelsOptions(4,"D","4","豆干",FOODLABELS_PREFIX + "豆干.png"));

        QUESTION_OPTIONS_FORE.add(new FoodLabelsOptions(1,"A","1","有机牛奶",FOODLABELS_PREFIX + "有机牛奶.png"));
        QUESTION_OPTIONS_FORE.add(new FoodLabelsOptions(2,"B","2","零乳糖牛奶",FOODLABELS_PREFIX + "零乳糖牛奶.png"));
        QUESTION_OPTIONS_FORE.add(new FoodLabelsOptions(3,"C","3","高钙奶",FOODLABELS_PREFIX + "高钙奶.png"));
        QUESTION_OPTIONS_FORE.add(new FoodLabelsOptions(4,"D","4","生牛乳",FOODLABELS_PREFIX + "生牛乳.png"));

        QUESTION_OPTIONS_FIVE.add(new FoodLabelsOptions(1,"A","1","安慕希",FOODLABELS_PREFIX + "安慕希.png"));
        QUESTION_OPTIONS_FIVE.add(new FoodLabelsOptions(2,"B","2","水果酸奶",FOODLABELS_PREFIX + "水果酸奶.png"));
        QUESTION_OPTIONS_FIVE.add(new FoodLabelsOptions(3,"C","3","优酸乳",FOODLABELS_PREFIX + "优酸乳.png"));
        QUESTION_OPTIONS_FIVE.add(new FoodLabelsOptions(4,"D","4","乳酸菌饮品",FOODLABELS_PREFIX + "乳酸菌饮品.png"));

        QUESTION_OPTIONS_SIX.add(new FoodLabelsOptions(1,"A","1","果冻",FOODLABELS_PREFIX + "果冻.png"));
        QUESTION_OPTIONS_SIX.add(new FoodLabelsOptions(2,"B","2","炒花生仁",FOODLABELS_PREFIX + "炒花生仁.png"));
        QUESTION_OPTIONS_SIX.add(new FoodLabelsOptions(3,"C","3","水果软糖",FOODLABELS_PREFIX + "水果软糖.png"));
        QUESTION_OPTIONS_SIX.add(new FoodLabelsOptions(4,"D","4","巧克力棒",FOODLABELS_PREFIX + "巧克力棒.png"));

        QUESTION_OPTIONS_SEVEN.add(new FoodLabelsOptions(1,"A","1","话梅",FOODLABELS_PREFIX + "话梅.png"));
        QUESTION_OPTIONS_SEVEN.add(new FoodLabelsOptions(2,"B","2","紫苏梅",FOODLABELS_PREFIX + "紫苏梅.png"));
        QUESTION_OPTIONS_SEVEN.add(new FoodLabelsOptions(3,"C","3","蓝莓",FOODLABELS_PREFIX + "蓝莓.png"));
        QUESTION_OPTIONS_SEVEN.add(new FoodLabelsOptions(4,"D","4","芒果干",FOODLABELS_PREFIX + "芒果干.png"));


        QUESTION_OPTIONS_EIGHT.add(new FoodLabelsOptions(1,"A","1","茉莉花茶",FOODLABELS_PREFIX + "茉莉花茶.png"));
        QUESTION_OPTIONS_EIGHT.add(new FoodLabelsOptions(2,"B","2","一日饮料",FOODLABELS_PREFIX + "一日饮料.png"));
        QUESTION_OPTIONS_EIGHT.add(new FoodLabelsOptions(3,"C","3","拿铁咖啡",FOODLABELS_PREFIX + "拿铁咖啡.png"));
        QUESTION_OPTIONS_EIGHT.add(new FoodLabelsOptions(4,"D","4","维生素饮料",FOODLABELS_PREFIX + "维生素饮料.png"));



        FOOD_LABELS_VO_LIST.addAll(
                Arrays.asList(
                        new FoodLabelsVO(1,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_ONE,"D","能量较其他偏低，脂肪含量较其他偏低，无氢化植物油或反式脂肪酸。"),
                        new FoodLabelsVO(2,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_TWO,"D","能量偏低，且不含钠盐、氢化植物油或反式脂肪酸。"),
                        new FoodLabelsVO(3,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_THREE,"C","虽然钠含量或脂肪含量较三者相近，但是能量较三者偏低。"),
                        new FoodLabelsVO(4,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FORE,"A","蛋白质含量、钙元素含量较三者偏高，且脂肪含量偏低"),
                        new FoodLabelsVO(5,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FIVE,"A","蛋白质含量最高且碳水含量最少。"),
                        new FoodLabelsVO(6,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SIX,"A","能量远低于其他三者，且碳水含量最低。"),
                        new FoodLabelsVO(7,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SEVEN,"B","能量较三者偏低，碳水较三者偏低，但钠含量偏高严重，如果是高血压人群应尽量避免。"),
                        new FoodLabelsVO(8,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_EIGHT,"A","能量远低于其他三者，且无过分宣传成分，无加入特殊成分。")
                )
        );
        HYPERTENSION_FOOD_LABELS_VO_LIST.addAll(
                Arrays.asList(
                        new FoodLabelsVO(1,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_ONE,"B","钠元素含量较有优势，脂肪含量较其他偏低，无氢化植物油或反式脂肪酸。"),
                        new FoodLabelsVO(2,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_TWO,"D","钠元素含量占优势，能量偏低，且不含钠盐、氢化植物油或反式脂肪酸。"),
                        new FoodLabelsVO(3,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_THREE,"A","钠含量占有优势，但是腌制类食品普遍富含钠元素，故均不建议食用。"),
                        new FoodLabelsVO(4,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FORE,"A","蛋白质含量、钙元素含量较三者偏高，且脂肪含量偏低"),
                        new FoodLabelsVO(5,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FIVE,"D","钠元素占有优势"),
                        new FoodLabelsVO(6,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SIX,"A","钠含量占有优势，能量远低于其他三者，且碳水含量最低。"),
                        new FoodLabelsVO(7,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SEVEN,"C","钠元素占有优势，但是干果类食品部分富含钠元素，故建议少食用。"),
                        new FoodLabelsVO(8,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为高血压患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_EIGHT,"A","钠元素含量占优势，能量远低于其他三者，且无过分宣传成分，无加入特殊成分。")
                )
        );

        DIABETES_FOOD_LABELS_VO_LIST.addAll(
                Arrays.asList(
                        new FoodLabelsVO(1,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_ONE,"B","碳水含量相仿的情况下较其他偏低，脂肪含量稍偏高，无氢化植物油或反式脂肪酸，控制饮食用量为前提可作为糖尿病人零食。"),
                        new FoodLabelsVO(2,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_TWO,"B","碳水含量较有优势，且不含钠盐、氢化植物油或反式脂肪酸。"),
                        new FoodLabelsVO(3,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_THREE,"C","虽然钠含量或脂肪含量较三者相近，但是能量较三者偏低。"),
                        new FoodLabelsVO(4,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FORE,"A","蛋白质含量、钙元素含量较三者偏高，且脂肪含量偏低。"),
                        new FoodLabelsVO(5,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_FIVE,"A","蛋白质含量最高且碳水含量最少"),
                        new FoodLabelsVO(6,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SIX,"A","能量远低于其他三者，且碳水含量最低。"),
                        new FoodLabelsVO(7,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_SEVEN,"B","能量较三者偏低，碳水较三者偏低，但钠含量偏高严重，如果是高血压人群应尽量避免。"),
                        new FoodLabelsVO(8,"根据包装食品营养标签所提供的信息，请挑选出类别中健康风险较低的食品。","请为糖尿病患者挑选出类别中健康风险较低的食品。",QUESTION_OPTIONS_EIGHT,"A","能量远低于其他三者，且无过分宣传成分，无加入特殊成分。")
                )
        );


    }
}
