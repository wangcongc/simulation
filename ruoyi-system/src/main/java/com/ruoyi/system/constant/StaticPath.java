package com.ruoyi.system.constant;

/**
 * @program: simulation
 * @description: 静态资源前缀
 * @author: 马瑞龙
 * @create: 2024-11-21 10:29
 **/
public class StaticPath {
    /*食材分类游戏 静态资源前缀*/
    public static final String FOOD_PREFIX = "/profile/食材分类游戏/";

    /*慢性病饮食干预-高血压 静态资源前缀*/
    public static final String INTERVENEHYPERTENSION_PREFIX = "/profile/慢性病饮食干预-高血压/";
    /*慢性病饮食干预-冠心病 静态资源前缀*/
    public static final String INTERVENECORONARY_PREFIX = "/profile/慢性病饮食干预-冠心病/";
    /*慢性病饮食干预-脑卒中 静态资源前缀*/
    public static final String STROKE_PREFIX = "/profile/慢性病饮食干预-脑卒中/";
    /*慢性病饮食干预-糖尿病 静态资源前缀*/
    public static final String DIABETES_PREFIX = "/profile/慢性病饮食干预-糖尿病/";
    /*慢性病饮食干预-糖尿病 静态资源前缀*/
    public static final String OBESITY_PREFIX = "/profile/慢性病饮食干预-肥胖症/";
    /*慢性病饮食干预-慢阻肺 静态资源前缀*/
    public static final String COPD_PREFIX = "/profile/慢性病饮食干预-慢阻肺/";
    /*慢性病饮食干预-盆底功能障碍 静态资源前缀*/
    public static final String PELVICFLOOR_PREFIX = "/profile/慢性病饮食干预-盆底功能障碍/";
    /*慢性病饮食干预-骨质疏松 静态资源前缀*/
    public static final String OSTEOPOROSIS_PREFIX = "/profile/慢性病饮食干预-骨质疏松/";
    /*营养配餐课程 静态资源前缀*/
    public static final String NUTRITIONALCATERING_PREFIX = "/profile/营养配餐课程/";
    /*饮食评价报告 静态资源前缀*/
    public static final String DIETEVALUATIONREPORT_PREFIX = "/profile/饮食评价报告模块/";

    /*正常人群食品标签实验 静态资源前缀*/
    public static final String FOODLABELS_PREFIX = "/profile/正常人群食品标签实验/";
    /*高血压患者食品标签实验 静态资源前缀*/
    public static final String HYPERTENSION_PREFIX = "/profile/高血压患者食品标签实验/";
    /*糖尿病患者食品标签实验 静态资源前缀*/
    public static final String DIABETES_FOODLABELS_PREFIX = "/profile/糖尿病患者食品标签实验/";
}
