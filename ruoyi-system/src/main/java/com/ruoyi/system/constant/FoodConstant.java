package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.FoodVO;

import java.util.ArrayList;
import java.util.List;

import static com.ruoyi.system.constant.StaticPath.FOOD_PREFIX;

/**
 * @program: simulation
 * @description: 食物
 * @author: 马瑞龙
 * @create: 2024-11-19 23:43
 **/
public class FoodConstant {
    /**
     *
     * 主食类：大米；番薯；黑米；栗子；马铃薯 ；面条；檽米；藕；葡萄干；荞麦；山药；燕麦片；薏米；玉米；芋头；
     * 蛋白质类：鹌鹑蛋；大头虾；带鱼；豆浆；干香菇；干银耳；海参；花生仁；黄豆；鸡蛋；鸡肝；鸡爪；鲢鱼；牛奶；螃蟹；平菇；鳝鱼；生蚝；酸奶；鲜蘑菇；鲜木耳；猪肚；猪脑；猪蹄；猪血；
     * 水果类：哈密瓜；梨；荔枝；苹果；葡萄；桃子；西瓜；香蕉；
     * 蔬菜类：菠菜；大白菜；大葱；花菜；茄子；节瓜；韭菜；空心菜；苦瓜；南瓜；芹菜；圆白菜；
     *
     *
     * 鲜木耳、鹌鹑蛋、鸡蛋、哈密瓜、干银耳、花生仁、干香菇、葡萄、栗子、玉米、糯米、牛奶、梨、鳝鱼、平菇、酸奶、豆浆
     * 鸡爪、大头虾、鲜蘑菇、猪蹄、藕、大葱、猪血、菠菜、大白菜、猪肚、花菜、黑米、节瓜、燕麦片、空心菜、带鱼、猪脑
     * 芋头、圆白菜、西瓜、苹果、茄子、苦瓜、生蚝、桃子、荔枝、香蕉、芹菜、番薯、大米、马铃薯、黄豆、面条、山药、螃蟹、
     * 葡萄干、荞麦、南瓜、韭菜、薏米、鲶鱼、鸡肝
     *
     *
     *
     */
    public static final List<FoodVO> ALL_FOOD_LIST = new ArrayList<>();
    /**主食类 分类区*/
    public static final List<FoodVO> STAPLE_FOOD_LIST = new ArrayList<>();
    /**蛋白质 分类区*/
    public static final List<FoodVO> PROTEIN_FOOD_LIST = new ArrayList<>();
    /**水果 分类区*/
    public static final List<FoodVO> FRUIT_FOOD_LIST = new ArrayList<>();
    /**蔬菜 分类区*/
    public static final List<FoodVO> VEGETABLES_FOOD_LIST = new ArrayList<>();

    static {
        ALL_FOOD_LIST.add(new FoodVO("1","大头虾","","",FOOD_PREFIX + "大头虾.png",0));
        ALL_FOOD_LIST.add(new FoodVO("2","鹌鹑蛋","","",FOOD_PREFIX + "鹌鹑蛋.png",0));
        ALL_FOOD_LIST.add(new FoodVO("3","带鱼","","",FOOD_PREFIX + "带鱼.png",0));
        ALL_FOOD_LIST.add(new FoodVO("4","豆浆","","",FOOD_PREFIX + "豆浆.png",0));
        ALL_FOOD_LIST.add(new FoodVO("5","干银耳","","",FOOD_PREFIX + "干银耳.png",0));
        ALL_FOOD_LIST.add(new FoodVO("6","花生仁","","",FOOD_PREFIX + "花生仁.png",0));
        ALL_FOOD_LIST.add(new FoodVO("7","黄豆","","",FOOD_PREFIX + "黄豆.png",0));
        ALL_FOOD_LIST.add(new FoodVO("8","鸡蛋","","",FOOD_PREFIX + "鸡蛋.png",0));
        ALL_FOOD_LIST.add(new FoodVO("9","鸡肝","","",FOOD_PREFIX + "鸡肝.png",0));
        ALL_FOOD_LIST.add(new FoodVO("10","鸡爪","","",FOOD_PREFIX + "鸡爪.png",0));
        ALL_FOOD_LIST.add(new FoodVO("11","牛奶","","",FOOD_PREFIX + "牛奶.png",0));
        ALL_FOOD_LIST.add(new FoodVO("12","螃蟹","","",FOOD_PREFIX + "螃蟹.png",0));
        ALL_FOOD_LIST.add(new FoodVO("13","平菇","","",FOOD_PREFIX + "平菇.png",0));
        ALL_FOOD_LIST.add(new FoodVO("14","鳝鱼","","",FOOD_PREFIX + "鳝鱼.png",0));
        ALL_FOOD_LIST.add(new FoodVO("15","生蚝","","",FOOD_PREFIX + "生蚝.png",0));
        ALL_FOOD_LIST.add(new FoodVO("16","酸奶","","",FOOD_PREFIX + "酸奶.png",0));
        ALL_FOOD_LIST.add(new FoodVO("17","鲜蘑菇","","",FOOD_PREFIX + "鲜蘑菇.png",0));
        ALL_FOOD_LIST.add(new FoodVO("18","鲜木耳","","",FOOD_PREFIX + "鲜木耳.png",0));
        ALL_FOOD_LIST.add(new FoodVO("19","猪肚","","",FOOD_PREFIX + "猪肚.png",0));
        ALL_FOOD_LIST.add(new FoodVO("20","猪脑","","",FOOD_PREFIX + "猪脑.png",0));
        ALL_FOOD_LIST.add(new FoodVO("21","猪蹄","","",FOOD_PREFIX + "猪蹄.png",0));
        ALL_FOOD_LIST.add(new FoodVO("22","猪血","","",FOOD_PREFIX + "猪血.png",0));
        ALL_FOOD_LIST.add(new FoodVO("23","干香菇","","",FOOD_PREFIX + "干香菇.png",0));
        ALL_FOOD_LIST.add(new FoodVO("24","鲢鱼","","",FOOD_PREFIX + "鲢鱼.png",0));
        ALL_FOOD_LIST.add(new FoodVO("25","菠菜","","",FOOD_PREFIX + "菠菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("26","大白菜","","",FOOD_PREFIX + "大白菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("27","大葱","","",FOOD_PREFIX + "大葱.png",0));
        ALL_FOOD_LIST.add(new FoodVO("28","花菜","","",FOOD_PREFIX + "花菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("29","茄子","","",FOOD_PREFIX + "茄子.png",0));
        ALL_FOOD_LIST.add(new FoodVO("30","节瓜","","",FOOD_PREFIX + "节瓜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("31","韭菜","","",FOOD_PREFIX + "韭菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("32","空心菜","","",FOOD_PREFIX + "空心菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("33","苦瓜","","",FOOD_PREFIX + "苦瓜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("34","南瓜","","",FOOD_PREFIX + "南瓜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("35","芹菜","","",FOOD_PREFIX + "芹菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("36","圆白菜","","",FOOD_PREFIX + "圆白菜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("37","哈密瓜","","",FOOD_PREFIX + "哈密瓜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("38","梨","","",FOOD_PREFIX + "梨.png",0));
        ALL_FOOD_LIST.add(new FoodVO("39","荔枝","","",FOOD_PREFIX + "荔枝.png",0));
        ALL_FOOD_LIST.add(new FoodVO("40","苹果","","",FOOD_PREFIX + "苹果.png",0));
        ALL_FOOD_LIST.add(new FoodVO("41","葡萄","","",FOOD_PREFIX + "葡萄.png",0));
        ALL_FOOD_LIST.add(new FoodVO("42","桃子","","",FOOD_PREFIX + "桃子.png",0));
        ALL_FOOD_LIST.add(new FoodVO("43","西瓜","","",FOOD_PREFIX + "西瓜.png",0));
        ALL_FOOD_LIST.add(new FoodVO("44","香蕉","","",FOOD_PREFIX + "香蕉.png",0));
        ALL_FOOD_LIST.add(new FoodVO("45","大米","","",FOOD_PREFIX + "大米.png",0));
        ALL_FOOD_LIST.add(new FoodVO("46","番薯","","",FOOD_PREFIX + "番薯.png",0));
        ALL_FOOD_LIST.add(new FoodVO("47","黑米","","",FOOD_PREFIX + "黑米.png",0));
        ALL_FOOD_LIST.add(new FoodVO("48","栗子","","",FOOD_PREFIX + "栗子.png",0));
        ALL_FOOD_LIST.add(new FoodVO("49","马铃薯","","",FOOD_PREFIX + "马铃薯.png",0));
        ALL_FOOD_LIST.add(new FoodVO("50","面条","","",FOOD_PREFIX + "面条.png",0));
        ALL_FOOD_LIST.add(new FoodVO("51","檽米","","",FOOD_PREFIX + "檽米.png",0));
        ALL_FOOD_LIST.add(new FoodVO("52","藕","","",FOOD_PREFIX + "藕.png",0));
        ALL_FOOD_LIST.add(new FoodVO("53","葡萄干","","",FOOD_PREFIX + "葡萄干.png",0));
        ALL_FOOD_LIST.add(new FoodVO("54","荞麦","","",FOOD_PREFIX + "荞麦.png",0));
        ALL_FOOD_LIST.add(new FoodVO("55","山药","","",FOOD_PREFIX + "山药.png",0));
        ALL_FOOD_LIST.add(new FoodVO("56","燕麦片","","",FOOD_PREFIX + "燕麦片.png",0));
        ALL_FOOD_LIST.add(new FoodVO("57","薏米","","",FOOD_PREFIX + "薏米.png",0));
        ALL_FOOD_LIST.add(new FoodVO("58","玉米","","",FOOD_PREFIX + "玉米.png",0));
        ALL_FOOD_LIST.add(new FoodVO("59","芋头","","",FOOD_PREFIX + "芋头.png",0));

        STAPLE_FOOD_LIST.add(new FoodVO("45","大米","","",FOOD_PREFIX + "大米.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("46","番薯","","",FOOD_PREFIX + "番薯.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("47","黑米","","",FOOD_PREFIX + "黑米.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("48","栗子","","",FOOD_PREFIX + "栗子.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("49","马铃薯","","",FOOD_PREFIX + "马铃薯.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("50","面条","","",FOOD_PREFIX + "面条.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("51","檽米","","",FOOD_PREFIX + "檽米.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("52","藕","","",FOOD_PREFIX + "藕.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("53","葡萄干","","",FOOD_PREFIX + "葡萄干.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("54","荞麦","","",FOOD_PREFIX + "荞麦.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("55","山药","","",FOOD_PREFIX + "山药.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("56","燕麦片","","",FOOD_PREFIX + "燕麦片.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("57","薏米","","",FOOD_PREFIX + "薏米.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("58","玉米","","",FOOD_PREFIX + "玉米.png",0));
        STAPLE_FOOD_LIST.add(new FoodVO("59","芋头","","",FOOD_PREFIX + "芋头.png",0));


        PROTEIN_FOOD_LIST.add(new FoodVO("1","大头虾","","",FOOD_PREFIX + "大头虾.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("2","鹌鹑蛋","","",FOOD_PREFIX + "鹌鹑蛋.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("3","带鱼","","",FOOD_PREFIX + "带鱼.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("4","豆浆","","",FOOD_PREFIX + "豆浆.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("5","干银耳","","",FOOD_PREFIX + "干银耳.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("6","花生仁","","",FOOD_PREFIX + "花生仁.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("7","黄豆","","",FOOD_PREFIX + "黄豆.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("8","鸡蛋","","",FOOD_PREFIX + "鸡蛋.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("9","鸡肝","","",FOOD_PREFIX + "鸡肝.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("10","鸡爪","","",FOOD_PREFIX + "鸡爪.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("11","牛奶","","",FOOD_PREFIX + "牛奶.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("12","螃蟹","","",FOOD_PREFIX + "螃蟹.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("13","平菇","","",FOOD_PREFIX + "平菇.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("14","鳝鱼","","",FOOD_PREFIX + "鳝鱼.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("15","生蚝","","",FOOD_PREFIX + "生蚝.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("16","酸奶","","",FOOD_PREFIX + "酸奶.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("17","鲜蘑菇","","",FOOD_PREFIX + "鲜蘑菇.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("18","鲜木耳","","",FOOD_PREFIX + "鲜木耳.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("19","猪肚","","",FOOD_PREFIX + "猪肚.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("20","猪脑","","",FOOD_PREFIX + "猪脑.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("21","猪蹄","","",FOOD_PREFIX + "猪蹄.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("22","猪血","","",FOOD_PREFIX + "猪血.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("23","干香菇","","",FOOD_PREFIX + "干香菇.png",0));
        PROTEIN_FOOD_LIST.add(new FoodVO("24","鲢鱼","","",FOOD_PREFIX + "鲢鱼.png",0));


        FRUIT_FOOD_LIST.add(new FoodVO("37","哈密瓜","","",FOOD_PREFIX + "哈密瓜.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("38","梨","","",FOOD_PREFIX + "梨.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("39","荔枝","","",FOOD_PREFIX + "荔枝.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("40","苹果","","",FOOD_PREFIX + "苹果.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("41","葡萄","","",FOOD_PREFIX + "葡萄.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("42","桃子","","",FOOD_PREFIX + "桃子.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("43","西瓜","","",FOOD_PREFIX + "西瓜.png",0));
        FRUIT_FOOD_LIST.add(new FoodVO("44","香蕉","","",FOOD_PREFIX + "香蕉.png",0));

        VEGETABLES_FOOD_LIST.add(new FoodVO("25","菠菜","","",FOOD_PREFIX + "菠菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("26","大白菜","","",FOOD_PREFIX + "大白菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("27","大葱","","",FOOD_PREFIX + "大葱.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("28","花菜","","",FOOD_PREFIX + "花菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("29","茄子","","",FOOD_PREFIX + "茄子.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("30","节瓜","","",FOOD_PREFIX + "节瓜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("31","韭菜","","",FOOD_PREFIX + "韭菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("32","空心菜","","",FOOD_PREFIX + "空心菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("33","苦瓜","","",FOOD_PREFIX + "苦瓜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("34","南瓜","","",FOOD_PREFIX + "南瓜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("35","芹菜","","",FOOD_PREFIX + "芹菜.png",0));
        VEGETABLES_FOOD_LIST.add(new FoodVO("36","圆白菜","","",FOOD_PREFIX + "圆白菜.png",0));
    }

}
