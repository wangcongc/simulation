package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.MotionVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 17:23
 **/
public class MotionConstant {
    public static final List<MotionVO> MOTION_VO_LIST = new ArrayList<>();

    static {
        MOTION_VO_LIST.add(new MotionVO(1,"步行<3.2 km/h（普通散步），水平硬表面","2.5","低强度"));
        MOTION_VO_LIST.add(new MotionVO(2,"步行4.8km/h，水平硬表面（上班速度）","3.3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(3,"步行4km/h，水平硬表面（购物、遛狗等）；下楼；下山","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(4,"步行5.6km/h，水平硬表面","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(5,"步行5.6km/h上山","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(6,"步行6.4km/h，水平硬表面","5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(7,"步行7.2km/h，水平硬表面","6.3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(8,"负重和上下楼0.5-7kg负重上楼","5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(9,"负重和上下楼7.5-11kg负重上楼","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(10,"负重和上下楼搬重物（搬砖）","7.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(11,"负重和上下楼爬台阶（10厘米，每分30级； 20厘米，每分20级）","4.8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(12,"负重和上下楼爬台阶（30厘米，每分20级）； 爬台阶（20厘米，每分30级）","6.3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(13,"负重和上下楼爬台阶（30厘米，每分30级）","9","中强度"));
        MOTION_VO_LIST.add(new MotionVO(14,"负重和上下楼爬台阶（每级台阶10cm，每分钟20级）","3.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(15,"负重和上下楼跑步上楼梯","15","中强度"));
        MOTION_VO_LIST.add(new MotionVO(16,"负重和上下楼下楼梯","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(17,"负重和上下楼中慢速上楼","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(18,"健身器材划船器100W","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(19,"健身器材划船器150W（较强力量）","8.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(20,"健身器材划船器200W（非常强力）","12","中强度"));
        MOTION_VO_LIST.add(new MotionVO(21,"健身器材健身单车100W（轻力量）","5.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(22,"健身器材健身单车150W","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(23,"健身器材健身单车200W（非常强力）","10.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(24,"健身器材健身单车250W（非常强力）","12.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(25,"健身器材健身单车50W","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(26,"跑步10.8km/h","11","中强度"));
        MOTION_VO_LIST.add(new MotionVO(27,"跑步11.2km/h","11.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(28,"跑步8km/h","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(29,"跑步9.6km/h","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(30,"骑车＜12-16km/h","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(31,"骑车＜12km/h","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(32,"骑车＞16.1-19.2km/h","6.8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(33,"骑车20km/h","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(34,"球类传接球练习（足球）","2.5","低强度"));
        MOTION_VO_LIST.add(new MotionVO(35,"球类打保龄球","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(36,"球类打高尔夫","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(37,"球类打篮球（中强度）","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(38,"球类打垒球或棒球","5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(39,"球类橄榄球","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(40,"球类篮球（比赛）","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(41,"球类篮球练习","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(42,"球类排球练习","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(43,"球类乒乓球练习","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(44,"球类踢足球（比赛性）","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(45,"球类踢足球（练习或者娱乐性）","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(46,"球类投篮","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(47,"球类网球单打","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(48,"球类网球练习","5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(49,"球类羽毛球（比赛型）","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(50,"球类羽毛球练习（娱乐性）","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(51,"跳绳和溜冰滑冰","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(52,"跳绳和溜冰轮滑旱冰","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(53,"跳绳和溜冰跳绳（慢）","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(54,"跳绳和溜冰跳绳（中速）","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(55,"跳绳和溜冰越野滑雪（10km/h，较强的速度和力量）","9","中强度"));
        MOTION_VO_LIST.add(new MotionVO(56,"跳绳和溜冰越野滑雪（中等速度和力量）","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(57,"舞蹈和体操芭蕾舞","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(58,"舞蹈和体操集体舞（骑兵舞、邀请舞）","5.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(59,"舞蹈和体操健身操","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(60,"舞蹈和体操爵士舞","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(61,"舞蹈和体操起蹲","5.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(62,"舞蹈和体操柔道，泰拳","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(63,"舞蹈和体操水中柔软体操","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(64,"舞蹈和体操踢踏舞","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(65,"舞蹈和体操跳有氧操","6.3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(66,"舞蹈和体操舞厅舞，慢（华尔兹、狐步舞、慢速舞蹈）","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(67,"舞蹈和体操有氧踏板操（踏板高15-20厘米）","8.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(68,"舞蹈和体操有氧踏板操（踏板高20-25厘米）","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(69,"舞蹈和体操有氧舞蹈（低冲击）","5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(70,"舞蹈和体操有氧舞蹈（较强冲击）","7","中强度"));
        MOTION_VO_LIST.add(new MotionVO(71,"舞蹈和体操早操，工间操","3.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(72,"游泳踩水（中等用力）","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(73,"游泳蝶泳，自由泳（快速70米/分钟）","11","中强度"));
        MOTION_VO_LIST.add(new MotionVO(74,"游泳游泳（轻划水）","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(75,"游泳游泳（蛙泳）","10","中强度"));
        MOTION_VO_LIST.add(new MotionVO(76,"游泳游泳（娱乐性）","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(77,"游泳游泳（自由泳中速）","8","中强度"));
        MOTION_VO_LIST.add(new MotionVO(78,"娱乐打架子鼓","4","中强度"));
        MOTION_VO_LIST.add(new MotionVO(79,"娱乐玩飞盘","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(80,"整理家务清洗地毯、扫地、拖地、扫院子、拖地板、吸尘","3.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(81,"整理家务手洗衣服","3.3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(82,"整理家务铁锹铲雪，移动家具","6","中强度"));
        MOTION_VO_LIST.add(new MotionVO(83,"整理家务用电动机割草","5.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(84,"整理家务整理床铺，搬桌椅，擦窗户、擦车、木工活","3","中强度"));
        MOTION_VO_LIST.add(new MotionVO(85,"整理家务种植树苗，院子拔草，耕作，农活，给家畜喂食等","4.5","中强度"));
        MOTION_VO_LIST.add(new MotionVO(86,"舞蹈和体操太极拳","3.5","中强度"));
    }
}
