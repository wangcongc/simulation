package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.FoodVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description: 慢性病饮食干预-高血压 标签信息
 * @author: 马瑞龙
 * @create: 2024-11-20 17:52
 **/
public class TagsConstant {
    /*患者资料-拖拽项*/
    public static final List<FoodVO> ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> PRESENT_DISEASE_TAGS = new ArrayList<>();


    /*患者资料-拖拽项*/
    public static final List<FoodVO> CORONARY_ALL_TAGS = new ArrayList<>();

    /*基本情况-放置区*/
    public static final List<FoodVO> CORONARY_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> CORONARY_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> CORONARY_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> CORONARY_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> CORONARY_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> CORONARY_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> CORONARY_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> CORONARY_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> CORONARY_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> CORONARY_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> CORONARY_PRESENT_DISEASE_TAGS = new ArrayList<>();



    /*患者资料-拖拽项*/
    public static final List<FoodVO> STROKE_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> STROKE_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> STROKE_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> STROKE_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> STROKE_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> STROKE_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> STROKE_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> STROKE_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> STROKE_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> STROKE_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> STROKE_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> STROKE_PRESENT_DISEASE_TAGS = new ArrayList<>();



    /*患者资料-拖拽项*/
    public static final List<FoodVO> DIABETES_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> DIABETES_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> DIABETES_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> DIABETES_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> DIABETES_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> DIABETES_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> DIABETES_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> DIABETES_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> DIABETES_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> DIABETES_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> DIABETES_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> DIABETES_PRESENT_DISEASE_TAGS = new ArrayList<>();


    /*患者资料-拖拽项*/
    public static final List<FoodVO> OBESITY_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> OBESITY_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> OBESITY_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> OBESITY_PRESENT_DISEASE_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> OBESITY_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> OBESITY_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> OBESITY_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> OBESITY_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> OBESITY_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> OBESITY_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> OBESITY_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> OBESITY_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();


    /*患者资料-拖拽项*/
    public static final List<FoodVO> COPD_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> COPD_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> COPD_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> COPD_PRESENT_DISEASE_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> COPD_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> COPD_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> COPD_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> COPD_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> COPD_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> COPD_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> COPD_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> COPD_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();


    /*患者资料-拖拽项*/
    public static final List<FoodVO> OSTEOPOROSIS_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_PRESENT_DISEASE_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> OSTEOPOROSIS_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();


    /*患者资料-拖拽项*/
    public static final List<FoodVO> PELVICFLOOR_ALL_TAGS = new ArrayList<>();
    /*基本情况-放置区*/
    public static final List<FoodVO> PELVICFLOOR_BASIC_SITUATION_TAGS = new ArrayList<>();
    /*家族病史-放置区*/
    public static final List<FoodVO> PELVICFLOOR_FAMILY_HISTORY_TAGS = new ArrayList<>();
    /*现患疾病-放置区*/
    public static final List<FoodVO> PELVICFLOOR_PRESENT_DISEASE_TAGS = new ArrayList<>();
    /*体重-放置区*/
    public static final List<FoodVO> PELVICFLOOR_WEIGHT_TAGS = new ArrayList<>();
    /*心理-放置区*/
    public static final List<FoodVO> PELVICFLOOR_MIND_TAGS = new ArrayList<>();
    /*睡眠-放置区*/
    public static final List<FoodVO> PELVICFLOOR_SLEEP_TAGS = new ArrayList<>();
    /*生活方式1-放置区*/
    public static final List<FoodVO> PELVICFLOOR_LIFESTYLE_ONE_TAGS = new ArrayList<>();
    /*生活方式2-放置区*/
    public static final List<FoodVO> PELVICFLOOR_LIFESTYLE_TWO_TAGS = new ArrayList<>();
    /*生活方式3-放置区*/
    public static final List<FoodVO> PELVICFLOOR_LIFESTYLE_THREE_TAGS = new ArrayList<>();
    /*生活方式4-放置区*/
    public static final List<FoodVO> PELVICFLOOR_LIFESTYLE_FORE_TAGS = new ArrayList<>();
    /*重要健康体检结果-放置区*/
    public static final List<FoodVO> PELVICFLOOR_PHYSICAL_EXAMINATION_TAGS = new ArrayList<>();




    static {
        ALL_TAGS.add(new FoodVO("1","李白","","","",0));
        ALL_TAGS.add(new FoodVO("2","男","","","",0));
        ALL_TAGS.add(new FoodVO("3","58岁","","","",0));
        ALL_TAGS.add(new FoodVO("4","上海本地居民","","","",0));
        ALL_TAGS.add(new FoodVO("5","IT领域高级工程师","","","",0));
        ALL_TAGS.add(new FoodVO("6","企业高管","","","",0));
        ALL_TAGS.add(new FoodVO("7","身高175cm","","","",0));
        ALL_TAGS.add(new FoodVO("8","体重88kg","","","",0));
        ALL_TAGS.add(new FoodVO("9","腰围88cm","","","",0));
        ALL_TAGS.add(new FoodVO("10","高血压病1年","","","",0));
        ALL_TAGS.add(new FoodVO("11","未来服药时收缩压142mmHg","","","",0));
        ALL_TAGS.add(new FoodVO("12","舒张压95mmHg","","","",0));
        ALL_TAGS.add(new FoodVO("13","李某按时服用降压药","","","",0));
        ALL_TAGS.add(new FoodVO("14","平时在外应酬频繁","","","",0));
        ALL_TAGS.add(new FoodVO("15","未在社区接受定期慢性病随访","","","",0));
        ALL_TAGS.add(new FoodVO("16","缺少运动","","","",0));

        ALL_TAGS.add(new FoodVO("17","常头痛","","","",0));
        ALL_TAGS.add(new FoodVO("18","头晕","","","",0));
        ALL_TAGS.add(new FoodVO("19","经常焦虑","","","",0));
        ALL_TAGS.add(new FoodVO("20","睡眠质量不佳","","","",0));
        ALL_TAGS.add(new FoodVO("21","最近的一次体检指标显示","","","",0));
        ALL_TAGS.add(new FoodVO("22","收缩压130mmHg","","","",0));
        ALL_TAGS.add(new FoodVO("23","舒张压83mmHg","","","",0));
        ALL_TAGS.add(new FoodVO("24","空腹血糖控制在6.0 mmol/L","","","",0));
        ALL_TAGS.add(new FoodVO("25","餐后2小时血糖7.7 mmol/l","","","",0));
        ALL_TAGS.add(new FoodVO("26","总胆固醇(TC)7.2mmol/L","","","",0));
        ALL_TAGS.add(new FoodVO("27","甘油三酯(TG)1.6mmol/L","","","",0));
        ALL_TAGS.add(new FoodVO("28","高密度脂蛋白胆固醇(HDL-C)0.9mmol/L","","","",0));
        ALL_TAGS.add(new FoodVO("29","低密度脂蛋白胆固醇LDL5.0mmol/L","","","",0));
        ALL_TAGS.add(new FoodVO("30","Hb1Ac5.6%","","","",0));
        ALL_TAGS.add(new FoodVO("31","母亲有冠心病","","","",0));
        ALL_TAGS.add(new FoodVO("32","烟龄20年","","","",0));
        ALL_TAGS.add(new FoodVO("33","日前每天1包烟","","","",0));
        ALL_TAGS.add(new FoodVO("34","经常饮酒","","","",0));
        ALL_TAGS.add(new FoodVO("35","每日52度白酒至少200ml","","","",0));
        ALL_TAGS.add(new FoodVO("36","饮食荤食为主","","","",0));
        ALL_TAGS.add(new FoodVO("37","口味偏咸","","","",0));
        ALL_TAGS.add(new FoodVO("38","喜欢吃腌制食品","","","",0));
        ALL_TAGS.add(new FoodVO("39","心房纤颤","","","",0));
        ALL_TAGS.add(new FoodVO("40","左心室肥厚","","","",0));


        BASIC_SITUATION_TAGS.add(new FoodVO("1","李白","","","",0));
        BASIC_SITUATION_TAGS.add(new FoodVO("2","男","","","",0));
        BASIC_SITUATION_TAGS.add(new FoodVO("3","58岁","","","",0));

        FAMILY_HISTORY_TAGS.add(new FoodVO("31","母亲有冠心病","","","",0));

        PRESENT_DISEASE_TAGS.add(new FoodVO("10","高血压病1年","","","",0));

        WEIGHT_TAGS.add(new FoodVO("7","身高175cm","","","",0));
        WEIGHT_TAGS.add(new FoodVO("8","体重88kg","","","",0));
        WEIGHT_TAGS.add(new FoodVO("9","腰围88cm","","","",0));

        MIND_TAGS.add(new FoodVO("19","经常焦虑","","","",0));

        SLEEP_TAGS.add(new FoodVO("20","睡眠质量不佳","","","",0));

        LIFESTYLE_ONE_TAGS.add(new FoodVO("32","烟龄20年","","","",0));
        LIFESTYLE_ONE_TAGS.add(new FoodVO("33","日前每天1包烟","","","",0));

        LIFESTYLE_TWO_TAGS.add(new FoodVO("34","经常饮酒","","","",0));
        LIFESTYLE_TWO_TAGS.add(new FoodVO("35","每日52度白酒至少200ml","","","",0));

        LIFESTYLE_THREE_TAGS.add(new FoodVO("36","饮食荤食为主","","","",0));
        LIFESTYLE_THREE_TAGS.add(new FoodVO("37","口味偏咸","","","",0));
        LIFESTYLE_THREE_TAGS.add(new FoodVO("38","喜欢吃腌制食品","","","",0));

        LIFESTYLE_FORE_TAGS.add(new FoodVO("16","缺少运动","","","",0));

        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("25","餐后2小时血糖7.7 mmol/l","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("26","总胆固醇(TC)7.2mmol/L","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("27","甘油三酯(TG)1.6mmol/L","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("28","高密度脂蛋白胆固醇(HDL-C)0.9mmol/L","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("29","低密度脂蛋白胆固醇LDL5.0mmol/L","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("30","Hb1Ac5.6%","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("39","心房纤颤","","","",0));
        PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("40","左心室肥厚","","","",0));





        CORONARY_ALL_TAGS.add(new FoodVO("1","刘英","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("2","女","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("3","67岁","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("4","杭州本地居民","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("5","某邮政储蓄银行退休职工","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("6","收缩压120mmHg","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("7","身高165cm","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("8","体重60kg","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("9","腰围76cm","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("10","37岁被诊断为冠心病","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("11","日常服用丹参片","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("12","舒张压75mmHg","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("13","刘某爱吃咸菜","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("14","水果蔬菜摄入较少","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("15","不吸烟","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("16","不饮酒","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("17","平时早晨起早走步1h","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("18","最近的一次体检指标显示","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("19","空腹血糖控制在5.8 mmol/L","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("20","餐后2小时血糖7.3 mmol/l","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("21","总胆固醇(TC)6.2mmol/L","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("22","甘油三酯2.2 mmol/L","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("23","高密度脂蛋白胆固醇(HDL-C)0.8mmol/l","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("24","低密度脂蛋白胆固醇LDL5.4mmol/l","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("25","经常会出现焦虑","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("26","睡眠质量不好","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("27","晚上多次醒来","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("28","难以入睡","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("29","有心房纤颤","","","",0));
        CORONARY_ALL_TAGS.add(new FoodVO("30","无左心室肥厚","","","",0));
        CORONARY_BASIC_SITUATION_TAGS.add(new FoodVO("1","刘英","","","",0));
        CORONARY_BASIC_SITUATION_TAGS.add(new FoodVO("2","女","","","",0));
        CORONARY_BASIC_SITUATION_TAGS.add(new FoodVO("3","67岁","","","",0));
        CORONARY_BASIC_SITUATION_TAGS.add(new FoodVO("4","杭州本地居民","","","",0));
        CORONARY_PRESENT_DISEASE_TAGS.add(new FoodVO("10","37岁被诊断为冠心病","","","",0));
        CORONARY_WEIGHT_TAGS.add(new FoodVO("8","体重60kg","","","",0));
        CORONARY_MIND_TAGS.add(new FoodVO("25","经常会出现焦虑","","","",0));
        CORONARY_SLEEP_TAGS.add(new FoodVO("26","睡眠质量不好","","","",0));
        CORONARY_SLEEP_TAGS.add(new FoodVO("27","晚上多次醒来","","","",0));
        CORONARY_SLEEP_TAGS.add(new FoodVO("28","难以入睡","","","",0));
        CORONARY_LIFESTYLE_ONE_TAGS.add(new FoodVO("15","不吸烟","","","",0));
        CORONARY_LIFESTYLE_TWO_TAGS.add(new FoodVO("16","不饮酒","","","",0));
        CORONARY_LIFESTYLE_THREE_TAGS.add(new FoodVO("13","刘某爱吃咸菜","","","",0));
        CORONARY_LIFESTYLE_THREE_TAGS.add(new FoodVO("14","水果蔬菜摄入较少","","","",0));
        CORONARY_LIFESTYLE_FORE_TAGS.add(new FoodVO("17","平时早晨起早走步1h","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("6","收缩压120mmHg","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("12","舒张压75mmHg","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("19","空腹血糖控制在5.8 mmol/L","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("20","餐后2小时血糖7.3 mmol/l","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("21","总胆固醇(TC)6.2mmol/L","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("22","甘油三酯2.2 mmol/L","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("23","高密度脂蛋白胆固醇(HDL-C)0.8mmol/l","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("24","低密度脂蛋白胆固醇LDL5.4mmol/l","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("29","有心房纤颤","","","",0));
        CORONARY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("30","无左心室肥厚","","","",0));


        STROKE_ALL_TAGS.add(new FoodVO("1","陈峰","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("2","男","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("3","75岁","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("4","广州本地居民","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("5","某卫健委退休职工","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("6","身高173cm","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("7","体重65kg","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("8","腰围86cm","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("9","半年前曾出现过小中风","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("10","日常血压测量","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("11","收缩压145mmHg","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("12","舒张压95mmHg","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("13","陈峰爱吃甜食","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("14","水果蔬菜摄入较少","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("15","吸烟年限20年","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("16","平均每天5支","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("17","每周饮用白酒2次","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("18","每次约20ml","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("19","平时基本不运动","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("20","最近的-次体检指标显示","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("21","空腹血糖6.8 mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("22","餐后2小时血糖8.7mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("23","总胆固醇(TC)5.5mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("24","甘油三酯2.4 mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("25","高密度脂蛋白胆固醇(HDL-C)0.7mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("26","低密度脂蛋白胆固醇LDL4.4mmol/L","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("27","睡眠质量不好","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("28","无心房纤颤","","","",0));
        STROKE_ALL_TAGS.add(new FoodVO("29","左心室肥厚","","","",0));
        STROKE_BASIC_SITUATION_TAGS.add(new FoodVO("1","陈峰","","","",0));
        STROKE_BASIC_SITUATION_TAGS.add(new FoodVO("2","男","","","",0));
        STROKE_BASIC_SITUATION_TAGS.add(new FoodVO("3","75岁","","","",0));
        STROKE_BASIC_SITUATION_TAGS.add(new FoodVO("4","广州本地居民","","","",0));
        STROKE_PRESENT_DISEASE_TAGS.add(new FoodVO("9","半年前曾出现过小中风","","","",0));
        STROKE_WEIGHT_TAGS.add(new FoodVO("7","体重65kg","","","",0));
        STROKE_WEIGHT_TAGS.add(new FoodVO("7","体重65kg","","","",0));
        STROKE_WEIGHT_TAGS.add(new FoodVO("8","腰围86cm","","","",0));
        STROKE_SLEEP_TAGS.add(new FoodVO("27","睡眠质量不好","","","",0));
        STROKE_LIFESTYLE_ONE_TAGS.add(new FoodVO("15","吸烟年限20年","","","",0));
        STROKE_LIFESTYLE_ONE_TAGS.add(new FoodVO("16","平均每天5支","","","",0));
        STROKE_LIFESTYLE_TWO_TAGS.add(new FoodVO("17","每周饮用白酒2次","","","",0));
        STROKE_LIFESTYLE_TWO_TAGS.add(new FoodVO("18","每次约20ml","","","",0));
        STROKE_LIFESTYLE_THREE_TAGS.add(new FoodVO("13","陈峰爱吃甜食","","","",0));
        STROKE_LIFESTYLE_THREE_TAGS.add(new FoodVO("14","水果蔬菜摄入较少","","","",0));
        STROKE_LIFESTYLE_FORE_TAGS.add(new FoodVO("19","平时基本不运动","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("11","收缩压145mmHg","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("12","舒张压95mmHg","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("21","空腹血糖6.8 mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("22","餐后2小时血糖8.7mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("23","总胆固醇(TC)5.5mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("24","甘油三酯2.4 mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("25","高密度脂蛋白胆固醇(HDL-C)0.7mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("26","低密度脂蛋白胆固醇LDL4.4mmol/L","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("28","无心房纤颤","","","",0));
        STROKE_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("29","左心室肥厚","","","",0));


        DIABETES_ALL_TAGS.add(new FoodVO("1","张玲","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("2","女","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("3","某国企退休人员","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("4","60岁","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("5","杭州本地居民","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("6","身高160cm","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("7","体重72kg","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("8","腰围90cm","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("9","确诊糖尿病近2年","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("10","按时服用降糖药物","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("11","本次因为腿部静息痛来健康管理中心接受检查","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("12","静脉抽血测量李女士空腹血糖7.5mmol/L","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("13","收缩压132mmHg","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("14","舒张压85mmHg","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("15","总胆固醇(TC)5.2mmol/L","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("16","甘油三酯(TG)2.2mmol/L","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("17","高密度脂蛋白胆固醇(HDL-C)1.5mmol/L","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("18","低密度脂蛋白胆固醇LDL2.9mmol/l","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("19","血尿酸400 umol/L","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("20","HbA1c=7.1%","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("21","无心房纤颤","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("22","无左心室肥厚","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("23","父亲患有糖尿病","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("24","不吸烟","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("25","不饮酒","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("26","平时主食基本不吃","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("27","以素食为主","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("28","嗜糖","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("29","因为家中琐事繁多","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("30","心理压力很大","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("31","乏力","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("32","常无精打采","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("33","对很多事情都没有兴趣","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("34","睡眠不踏实","","","",0));
        DIABETES_ALL_TAGS.add(new FoodVO("35","每天健步走4000步","","","",0));
        DIABETES_BASIC_SITUATION_TAGS.add(new FoodVO("2","女","","","",0));
        DIABETES_BASIC_SITUATION_TAGS.add(new FoodVO("3","某国企退休人员","","","",0));
        DIABETES_BASIC_SITUATION_TAGS.add(new FoodVO("4","60岁","","","",0));
        DIABETES_BASIC_SITUATION_TAGS.add(new FoodVO("5","杭州本地居民","","","",0));
        DIABETES_FAMILY_HISTORY_TAGS.add(new FoodVO("23","父亲患有糖尿病","","","",0));
        DIABETES_PRESENT_DISEASE_TAGS.add(new FoodVO("9","确诊糖尿病近2年","","","",0));
        DIABETES_WEIGHT_TAGS.add(new FoodVO("6","身高160cm","","","",0));
        DIABETES_WEIGHT_TAGS.add(new FoodVO("7","体重72kg","","","",0));
        DIABETES_WEIGHT_TAGS.add(new FoodVO("8","腰围90cm","","","",0));
        DIABETES_MIND_TAGS.add(new FoodVO("30","心理压力很大","","","",0));
        DIABETES_MIND_TAGS.add(new FoodVO("31","乏力","","","",0));
        DIABETES_MIND_TAGS.add(new FoodVO("32","常无精打采","","","",0));
        DIABETES_MIND_TAGS.add(new FoodVO("33","对很多事情都没有兴趣","","","",0));
        DIABETES_SLEEP_TAGS.add(new FoodVO("34","睡眠不踏实","","","",0));
        DIABETES_LIFESTYLE_ONE_TAGS.add(new FoodVO("24","不吸烟","","","",0));
        DIABETES_LIFESTYLE_TWO_TAGS.add(new FoodVO("25","不饮酒","","","",0));
        DIABETES_LIFESTYLE_THREE_TAGS.add(new FoodVO("26","平时主食基本不吃","","","",0));
        DIABETES_LIFESTYLE_THREE_TAGS.add(new FoodVO("27","以素食为主","","","",0));
        DIABETES_LIFESTYLE_THREE_TAGS.add(new FoodVO("28","嗜糖","","","",0));
        DIABETES_LIFESTYLE_FORE_TAGS.add(new FoodVO("35","每天健步走4000步","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("12","静脉抽血测量李女士空腹血糖7.5mmol/L","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("13","收缩压132mmHg","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("14","舒张压85mmHg","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("15","总胆固醇(TC)5.2mmol/L","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("16","甘油三酯(TG)2.2mmol/L","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("17","高密度脂蛋白胆固醇(HDL-C)1.5mmol/L","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("18","低密度脂蛋白胆固醇LDL2.9mmol/l","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("19","血尿酸400 umol/L","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("20","HbA1c=7.1%","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("21","无心房纤颤","","","",0));
        DIABETES_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("22","无左心室肥厚","","","",0));



        OBESITY_ALL_TAGS.add(new FoodVO("1","张军","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("2","男性","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("3","30岁","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("4","武汉市本地人","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("5","现为某IT公司的程序员","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("6","身高175cm","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("7","因为疫情","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("8","最近1年体重增加到了100kg","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("9","腰围100cm","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("10","平时缺少运动","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("11","经常熬夜加班","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("12","喜食肥肉","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("13","油炸食品","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("14","应酬多等","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("15","嗜酒如命","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("16","喜食甜食","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("17","每周吸烟1-2次","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("18","每次1支","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("19","无慢性病家族史","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("20","性格外向","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("21","擅言谈","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("22","睡眠质量不太好","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("23","最近因为有些头晕","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("24","头痛不适","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("25","到医院进行体检主要指标如下","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("26","空腹血糖8.5mmol/L","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("27","收缩压139mmHg","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("28","舒张压89mmHg","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("29","总胆固醇(TC)8.2mmol/L","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("30","甘油三酯(TG)1.3mmol/L","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("31","高密度脂蛋白胆固醇(HDL-C)0.7mmol/L","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("32","低密度脂蛋白胆固醇(LDL-C)3.8mmol/l","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("33","血尿酸450 umol/L","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("34","HbA1c=6.4%","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("35","无心房纤颤","","","",0));
        OBESITY_ALL_TAGS.add(new FoodVO("36","左心室肥厚","","","",0));
        OBESITY_BASIC_SITUATION_TAGS.add(new FoodVO("1","张军","","","",0));
        OBESITY_BASIC_SITUATION_TAGS.add(new FoodVO("2","男性","","","",0));
        OBESITY_BASIC_SITUATION_TAGS.add(new FoodVO("3","30岁","","","",0));
        OBESITY_BASIC_SITUATION_TAGS.add(new FoodVO("4","武汉市本地人","","","",0));
        OBESITY_BASIC_SITUATION_TAGS.add(new FoodVO("5","现为某IT公司的程序员","","","",0));
        OBESITY_FAMILY_HISTORY_TAGS.add(new FoodVO("19","无慢性病家族史","","","",0));
        OBESITY_PRESENT_DISEASE_TAGS.add(new FoodVO("23","最近因为有些头晕","","","",0));
        OBESITY_PRESENT_DISEASE_TAGS.add(new FoodVO("24","头痛不适","","","",0));
        OBESITY_WEIGHT_TAGS.add(new FoodVO("6","身高175cm","","","",0));
        OBESITY_WEIGHT_TAGS.add(new FoodVO("8","最近1年体重增加到了100kg","","","",0));
        OBESITY_WEIGHT_TAGS.add(new FoodVO("9","腰围100cm","","","",0));
        OBESITY_MIND_TAGS.add(new FoodVO("20","性格外向","","","",0));
        OBESITY_MIND_TAGS.add(new FoodVO("21","擅言谈","","","",0));
        OBESITY_SLEEP_TAGS.add(new FoodVO("11","经常熬夜加班","","","",0));
        OBESITY_SLEEP_TAGS.add(new FoodVO("22","睡眠质量不太好","","","",0));
        OBESITY_LIFESTYLE_ONE_TAGS.add(new FoodVO("17","每周吸烟1-2次","","","",0));
        OBESITY_LIFESTYLE_ONE_TAGS.add(new FoodVO("18","每次1支","","","",0));
        OBESITY_LIFESTYLE_TWO_TAGS.add(new FoodVO("15","嗜酒如命","","","",0));
        OBESITY_LIFESTYLE_THREE_TAGS.add(new FoodVO("16","喜食甜食","","","",0));
        OBESITY_LIFESTYLE_THREE_TAGS.add(new FoodVO("13","油炸食品","","","",0));
        OBESITY_LIFESTYLE_FORE_TAGS.add(new FoodVO("10","平时缺少运动","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("26","空腹血糖8.5mmol/L","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("27","收缩压139mmHg","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("28","舒张压89mmHg","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("29","总胆固醇(TC)8.2mmol/L","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("30","甘油三酯(TG)1.3mmol/L","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("31","高密度脂蛋白胆固醇(HDL-C)0.7mmol/L","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("32","低密度脂蛋白胆固醇(LDL-C)3.8mmol/l","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("33","血尿酸450 umol/L","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("34","HbA1c=6.4%","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("35","无心房纤颤","","","",0));
        OBESITY_PHYSICAL_EXAMINATION_TAGS.add(new FoodVO("36","左心室肥厚","","","",0));


        COPD_ALL_TAGS.add(new FoodVO("1","赵云","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("2","北京户籍","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("3","北京市某外企行政办公室主任","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("4","男性","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("5","55岁","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("6","身高175cm","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("7","体重55kg","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("8","腰围82cm","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("9","20岁开始吸烟","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("10","每天平均吸烟10支","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("11","不喝酒","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("12","性格开朗","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("13","乐观积极","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("14","近半年白天和夜间不感冒时经常出现咳嗽、胸闷","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("15","走路时气短等症状","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("16","睡眠质量不佳","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("17","父亲有哮喘和慢性支气管炎","","","",0));
        COPD_ALL_TAGS.add(new FoodVO("18","怀疑自己肺部有健康问题","","","",0));
        COPD_BASIC_SITUATION_TAGS.add(new FoodVO("1","赵云","","","",0));
        COPD_BASIC_SITUATION_TAGS.add(new FoodVO("2","北京户籍","","","",0));
        COPD_BASIC_SITUATION_TAGS.add(new FoodVO("3","北京市某外企行政办公室主任","","","",0));
        COPD_BASIC_SITUATION_TAGS.add(new FoodVO("4","男性","","","",0));
        COPD_BASIC_SITUATION_TAGS.add(new FoodVO("5","55岁","","","",0));
        COPD_FAMILY_HISTORY_TAGS.add(new FoodVO("17","父亲有哮喘和慢性支气管炎","","","",0));
        COPD_PRESENT_DISEASE_TAGS.add(new FoodVO("14","近半年白天和夜间不感冒时经常出现咳嗽、胸闷","","","",0));
        COPD_PRESENT_DISEASE_TAGS.add(new FoodVO("15","走路时气短等症状","","","",0));
        COPD_WEIGHT_TAGS.add(new FoodVO("6","身高175cm","","","",0));
        COPD_WEIGHT_TAGS.add(new FoodVO("7","体重55kg","","","",0));
        COPD_WEIGHT_TAGS.add(new FoodVO("8","腰围82cm","","","",0));
        COPD_MIND_TAGS.add(new FoodVO("12","性格开朗","","","",0));
        COPD_MIND_TAGS.add(new FoodVO("13","乐观积极","","","",0));
        COPD_SLEEP_TAGS.add(new FoodVO("16","睡眠质量不佳","","","",0));
        COPD_LIFESTYLE_ONE_TAGS.add(new FoodVO("9","20岁开始吸烟","","","",0));
        COPD_LIFESTYLE_ONE_TAGS.add(new FoodVO("10","每天平均吸烟10支","","","",0));
        COPD_LIFESTYLE_TWO_TAGS.add(new FoodVO("11","不喝酒","","","",0));

        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("1","吴霞","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("2","女性","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("3","杭州人","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("4","某高校党政办公室职员","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("5","年龄49岁","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("6","身高170cm","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("7","体重50kg","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("8","腰围73","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("9","爱喝咖啡","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("10","爱喝酒","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("11","不吸烟","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("12","不爱运动","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("13","喜欢宅在办公室或家里","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("14","性格外向","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("15","睡眠质量良好","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("16","有母亲髋关节脆性骨折家族史","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("17","有皮肤病","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("18","常服用糖皮质激素类药物","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("19","最近一次下楼不小心摔倒","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("20","导致小腿骨折","","","",0));
        OSTEOPOROSIS_ALL_TAGS.add(new FoodVO("21","她怀疑自己为骨质疏松","","","",0));
        OSTEOPOROSIS_BASIC_SITUATION_TAGS.add(new FoodVO("1","吴霞","","","",0));
        OSTEOPOROSIS_BASIC_SITUATION_TAGS.add(new FoodVO("2","女性","","","",0));
        OSTEOPOROSIS_BASIC_SITUATION_TAGS.add(new FoodVO("3","杭州人","","","",0));
        OSTEOPOROSIS_BASIC_SITUATION_TAGS.add(new FoodVO("4","某高校党政办公室职员","","","",0));
        OSTEOPOROSIS_BASIC_SITUATION_TAGS.add(new FoodVO("5","年龄49岁","","","",0));
        OSTEOPOROSIS_FAMILY_HISTORY_TAGS.add(new FoodVO("16","有母亲髋关节脆性骨折家族史","","","",0));
        OSTEOPOROSIS_PRESENT_DISEASE_TAGS.add(new FoodVO("17","有皮肤病","","","",0));
        OSTEOPOROSIS_PRESENT_DISEASE_TAGS.add(new FoodVO("18","常服用糖皮质激素类药物","","","",0));
        OSTEOPOROSIS_PRESENT_DISEASE_TAGS.add(new FoodVO("20","导致小腿骨折","","","",0));
        OSTEOPOROSIS_WEIGHT_TAGS.add(new FoodVO("6","身高170cm","","","",0));
        OSTEOPOROSIS_WEIGHT_TAGS.add(new FoodVO("7","体重50kg","","","",0));
        OSTEOPOROSIS_WEIGHT_TAGS.add(new FoodVO("8","腰围73","","","",0));
        OSTEOPOROSIS_MIND_TAGS.add(new FoodVO("14","性格外向","","","",0));
        OSTEOPOROSIS_SLEEP_TAGS.add(new FoodVO("15","睡眠质量良好","","","",0));
        OSTEOPOROSIS_LIFESTYLE_ONE_TAGS.add(new FoodVO("11","不吸烟","","","",0));
        OSTEOPOROSIS_LIFESTYLE_TWO_TAGS.add(new FoodVO("10","爱喝酒","","","",0));
        OSTEOPOROSIS_LIFESTYLE_THREE_TAGS.add(new FoodVO("9","爱喝咖啡","","","",0));
        OSTEOPOROSIS_LIFESTYLE_FORE_TAGS.add(new FoodVO("12","不爱运动","","","",0));
        OSTEOPOROSIS_LIFESTYLE_FORE_TAGS.add(new FoodVO("13","喜欢宅在办公室或家里","","","",0));


        PELVICFLOOR_ALL_TAGS.add(new FoodVO("1","秦芳","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("2","女性","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("3","北京人","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("4","家庭主妇","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("5","年龄50岁","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("6","身高160cm","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("7","体重65kg","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("8","腰围92cm","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("9","有3个孩子","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("10","其中两个孩子出生时体重超过4kg","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("11","45岁时开始抽烟","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("12","每天约1支烟","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("13","烟龄5年","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("14","不饮酒","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("15","目前有慢性支气管炎","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("16","慢性岔腔炎","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("17","平时体力活动少","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("18","性格开朗","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("19","睡眠良好","","","",0));
        PELVICFLOOR_ALL_TAGS.add(new FoodVO("20","当她开怀大笑、打喷嚏等时会出现尿失禁","","","",0));
        PELVICFLOOR_BASIC_SITUATION_TAGS.add(new FoodVO("1","秦芳","","","",0));
        PELVICFLOOR_BASIC_SITUATION_TAGS.add(new FoodVO("2","女性","","","",0));
        PELVICFLOOR_BASIC_SITUATION_TAGS.add(new FoodVO("3","北京人","","","",0));
        PELVICFLOOR_BASIC_SITUATION_TAGS.add(new FoodVO("4","家庭主妇","","","",0));
        PELVICFLOOR_BASIC_SITUATION_TAGS.add(new FoodVO("5","年龄50岁","","","",0));
        PELVICFLOOR_PRESENT_DISEASE_TAGS.add(new FoodVO("15","目前有慢性支气管炎","","","",0));
        PELVICFLOOR_PRESENT_DISEASE_TAGS.add(new FoodVO("16","慢性岔腔炎","","","",0));
        PELVICFLOOR_PRESENT_DISEASE_TAGS.add(new FoodVO("20","当她开怀大笑、打喷嚏等时会出现尿失禁","","","",0));
        PELVICFLOOR_WEIGHT_TAGS.add(new FoodVO("6","身高160cm","","","",0));
        PELVICFLOOR_WEIGHT_TAGS.add(new FoodVO("7","体重65kg","","","",0));
        PELVICFLOOR_WEIGHT_TAGS.add(new FoodVO("8","腰围92cm","","","",0));
        PELVICFLOOR_MIND_TAGS.add(new FoodVO("18","性格开朗","","","",0));
        PELVICFLOOR_SLEEP_TAGS.add(new FoodVO("19","睡眠良好","","","",0));
        PELVICFLOOR_LIFESTYLE_ONE_TAGS.add(new FoodVO("11","45岁时开始抽烟","","","",0));
        PELVICFLOOR_LIFESTYLE_ONE_TAGS.add(new FoodVO("12","每天约1支烟","","","",0));
        PELVICFLOOR_LIFESTYLE_ONE_TAGS.add(new FoodVO("13","烟龄5年","","","",0));
        PELVICFLOOR_LIFESTYLE_TWO_TAGS.add(new FoodVO("14","不饮酒","","","",0));
        PELVICFLOOR_LIFESTYLE_FORE_TAGS.add(new FoodVO("17","平时体力活动少","","","",0));
    }
}
