package com.ruoyi.system.constant;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2025-02-10 11:21
 **/
public class LessonConstant {
    /*住院患者自理能力评估表*/
    public static final String ZYHZZLNL_PGB = "住院患者自理能力评估表";
    /*Morse跌倒危险因素评估量表*/
    public static final String MORSEDDWXYS_PGB = "Morse跌倒危险因素评估量表";
    public static final String BRADENYC_PFB = "Braden压疮评分表";
}
