package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.ResistanceVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 17:39
 **/
public class ResistanceConstant {
    public static final List<ResistanceVO> RESISTANCE_VO_LIST = new ArrayList<>();

    static {
        RESISTANCE_VO_LIST.add(new ResistanceVO(1,"抗阻训练","高强度力量训练，阻力训练、举重（较强的力量）"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(2,"躯干肌群—平板支撑","手肘与脚尖做支撑点，双手臂与肩膀同宽、手肘保持90度，力量集中在腹部，臀部肌群以及股四头肌持续保持，臀部、上背部和头部保持一条直线"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(3,"躯干肌群—抱头仰卧起坐","仰卧起坐的难度，准备姿势为两手放于体侧，两手抱头，腿部逐渐屈曲，腹部用力将上身支撑起至膝盖处，注意身体进行协调训练"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(4,"躯干肌群—直腿仰卧举腿","仰卧位，两手放于体侧，膝关节保持伸直，两腿上抬至垂直位；慢慢放下，还原，开始下次练习。如果两腿慢慢放下至接近台面时，即开始做下一次练习，练习难度将增大。注意腿部保持伸直"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(5,"腰背肌群—小飞燕","预备姿势取俯卧位或俯卧台位，固定下肢，上体下垂。抬起上体；或俯卧位，抬起下肢，还原。此方法为束脊肌力量练习，难度较大的是上体，下肢同时抬起。注意手脚同时运动，推荐练习5-8次。"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(6,"上肢肌群—站立位哑铃屈肘练习","两脚左右开立，双手正握杠铃，持杠铃下垂于腿前（或双手持哑铃于腿侧），先向上直臂提肩将杠铃提至最高点，稍停后，再还原，如此重复"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(7,"下肢肌群—靠墙静蹲","浅蹲：角度约在120度以上，适合膝关节疼痛明显、长时间卧床肌肉力量差的人群。半蹲：角度约在100-120度，适合膝关节疼痛可控、力量相对较差但不太影响日常生活的人群。深蹲：角度约为90-100度，大腿与地面平行，适合运动时膝痛、力量响度较好但仍然有待提高的人群"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(8,"下肢肌群—左侧箭步蹲","挺直腰背，不要含胸驼背，建议一侧训练力竭后，再换另一条腿。每次动作结束后都收回腿，恢复站立姿势，然后再重复动作。下蹲的时候，保持2个肩部的宽度，避免膝盖内扣，保持前脚膝盖不超过脚尖，后脚膝盖不要碰地，才能减轻关节伤害。下蹲的同时身体重心要往前移，站起的时候重心要后移"));
        RESISTANCE_VO_LIST.add(new ResistanceVO(9,"下肢肌群—交替侧弓步","首先站立，双脚平行，与肩同宽。向一侧迈出一大步，确保躯干尽可能直立，降低前腿的膝盖弯曲约 90°，保持后腿伸直。向上推并返回起始位置"));
    }
}
