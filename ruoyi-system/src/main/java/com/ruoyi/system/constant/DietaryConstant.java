package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.DietaryVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description: 膳食方案制定食品常量类
 * @author: 马瑞龙
 * @create: 2024-11-22 12:18
 **/
public class DietaryConstant {
    public static final List<DietaryVO> DIETARY_VO_LIST = new ArrayList<>();

    static {
        DIETARY_VO_LIST.add(new DietaryVO(1,"大米",1,100.0,347.0,7.4,0.8,77.9,0.7,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(2,"番薯",1,100.0,100.0,1.1,0.2,24.7,1.6,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(3,"黑米",1,100.0,300.0,9.0,2.5,64.0,3.9,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(4,"栗子",1,100.0,175.0,4.0,1.2,37.0,1.2,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(5,"藕",1,100.0,290.0,0.2,0.0,72.3,0.1,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(6,"马铃薯",1,100.0,100.0,2.0,0.2,22.5,0.7,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(7,"面条",1,100.0,286.0,8.3,0.7,61.9,0.8,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(8,"糯米",1,100.0,350.0,7.3,1.0,78.3,0.8,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(9,"葡萄干",1,100.0,280.0,2.5,0.4,66.6,1.6,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(10,"荞麦",1,100.0,250.0,8.0,2.0,55.0,6.5,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(11,"山药",1,100.0,45.0,1.9,0.2,8.9,0.8,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(12,"燕麦片",1,100.0,250.0,10.0,6.0,40.0,5.3,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(13,"薏米",1,100.0,300.0,11.0,3.3,56.5,2.0,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(14,"玉米",1,100.0,112.0,4.0,1.2,22.8,2.9,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(15,"芋头",1,100.0,100.0,2.2,0.2,22.3,1.0,"主食类"));
        DIETARY_VO_LIST.add(new DietaryVO(16,"鹌鹑蛋",2,100.0,150.0,12.0,10.5,2.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(17,"带鱼",2,100.0,100.0,15.0,4.0,2.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(18,"含钙豆浆",5,100.0,100.0,1.8,0.7,15.1,11.0,"乳制品类"));
        DIETARY_VO_LIST.add(new DietaryVO(19,"对虾",2,100.0,90.0,18.6,0.8,2.8,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(20,"干蘑菇",2,100.0,195.0,20.0,1.2,26.1,31.6,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(21,"干银耳",2,100.0,195.0,10.0,1.4,35.6,30.4,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(22,"海参",2,100.0,55.0,16.5,0.2,2.5,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(23,"花生仁",2,100.0,560.0,24.8,44.3,16.2,5.5,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(24,"黄豆",2,100.0,150.0,15.0,14.0,8.5,15.5,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(25,"鸡蛋",2,100.0,150.0,13.3,8.8,2.8,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(26,"鸡肝",2,100.0,120.0,16.6,4.8,2.8,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(27,"鸡爪",2,100.0,80.0,3.0,7.0,1.5,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(28,"鲢鱼",2,100.0,90.0,15.8,3.0,0.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(29,"牛奶",5,100.0,55.0,3.0,3.2,3.4,0.0,"乳制品类"));
        DIETARY_VO_LIST.add(new DietaryVO(30,"螃蟹",2,100.0,80.0,14.6,1.6,1.7,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(31,"平菇",2,100.0,15.0,1.5,0.3,1.6,2.3,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(32,"鳝鱼",2,100.0,90.0,18.0,1.4,1.2,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(33,"生蚝",2,100.0,300.0,6.5,1.0,0.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(34,"酸奶",5,100.0,75.0,2.5,2.7,9.3,0.0,"乳制品类"));
        DIETARY_VO_LIST.add(new DietaryVO(35,"鲜蘑菇",2,100.0,15.0,2.2,0.1,1.4,2.1,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(36,"鲜木耳",2,100.0,25.0,1.5,0.2,4.3,2.6,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(37,"猪肚",2,100.0,120.0,15.2,5.1,0.7,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(38,"猪脑",2,100.0,140.0,10.8,9.8,0.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(39,"猪蹄",2,100.0,100.0,8.0,7.5,0.0,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(40,"猪血",2,100.0,55.0,12.2,0.3,0.9,0.0,"蛋白质类"));
        DIETARY_VO_LIST.add(new DietaryVO(41,"菠菜",3,100.0,25.0,2.0,0.3,3.8,1.7,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(42,"菜花",3,100.0,25.0,2.1,0.2,3.4,1.2,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(43,"大白菜",3,100.0,15.0,1.3,0.1,2.2,0.8,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(44,"大葱",3,100.0,25.0,1.6,0.3,4.0,1.3,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(45,"节瓜",3,100.0,15.0,0.6,0.1,2.9,1.2,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(46,"韭菜",3,100.0,25.0,2.4,0.4,3.2,1.4,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(47,"空心菜",3,100.0,15.0,1.2,0.3,1.9,1.4,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(48,"苦瓜",3,100.0,15.0,1.0,0.1,2.7,1.4,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(49,"南瓜",3,100.0,15.0,0.7,0.1,3.0,0.8,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(50,"茄子",3,100.0,15.0,0.8,0.2,2.5,1.3,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(51,"芹菜",3,100.0,15.0,0.8,0.1,2.5,1.4,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(52,"圆白菜",3,100.0,15.0,1.5,0.2,2.0,1.0,"蔬菜类"));
        DIETARY_VO_LIST.add(new DietaryVO(53,"哈密瓜",4,100.0,25.0,0.5,0.1,5.5,0.2,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(54,"梨",4,100.0,35.0,0.4,0.2,7.9,3.1,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(55,"荔枝",4,100.0,65.0,0.9,0.2,14.9,0.5,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(56,"苹果",4,100.0,45.0,0.2,0.2,10.6,1.2,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(57,"葡萄",4,100.0,35.0,0.5,0.2,7.8,0.4,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(58,"桃子",4,100.0,35.0,0.9,0.1,7.7,1.3,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(59,"西瓜",4,100.0,25.0,0.6,0.1,5.8,0.3,"水果类"));
        DIETARY_VO_LIST.add(new DietaryVO(60,"香蕉",4,100.0,95.0,1.2,0.1,22.3,3.1,"水果类"));
    }
}
