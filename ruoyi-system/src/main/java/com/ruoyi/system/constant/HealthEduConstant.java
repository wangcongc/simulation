package com.ruoyi.system.constant;

import com.ruoyi.system.domain.vo.HealthEduVO;

import java.util.ArrayList;
import java.util.List;

import static com.ruoyi.system.constant.StaticPath.COPD_PREFIX;
import static com.ruoyi.system.constant.StaticPath.DIABETES_PREFIX;
import static com.ruoyi.system.constant.StaticPath.INTERVENECORONARY_PREFIX;
import static com.ruoyi.system.constant.StaticPath.INTERVENEHYPERTENSION_PREFIX;
import static com.ruoyi.system.constant.StaticPath.NUTRITIONALCATERING_PREFIX;
import static com.ruoyi.system.constant.StaticPath.OSTEOPOROSIS_PREFIX;
import static com.ruoyi.system.constant.StaticPath.PELVICFLOOR_PREFIX;
import static com.ruoyi.system.constant.StaticPath.STROKE_PREFIX;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 18:07
 **/
public class HealthEduConstant {

    /*慢性病饮食干预-高血压 健康教育*/
    public static final List<HealthEduVO> HEALTH_EDU_VO_LIST = new ArrayList<>();

    /*慢性病饮食干预-冠心病 健康教育*/
    public static final List<HealthEduVO> CORONARY_EDU_VO_LIST = new ArrayList<>();


    /*慢性病饮食干预-脑卒中 健康教育*/
    public static final List<HealthEduVO> STROKE_EDU_VO_LIST = new ArrayList<>();

    /*慢性病饮食干预-糖尿病 健康教育*/
    public static final List<HealthEduVO> DIABETES_EDU_VO_LIST = new ArrayList<>();
    /*慢性病饮食干预-慢阻肺 健康教育*/
    public static final List<HealthEduVO> COPD_EDU_VO_LIST = new ArrayList<>();
    /*慢性病饮食干预-慢阻肺 健康教育*/
    public static final List<HealthEduVO> OSTEOPOROSIS_EDU_VO_LIST = new ArrayList<>();

    /*慢性病饮食干预-慢阻肺 健康教育*/
    public static final List<HealthEduVO> PELVICFLOOR_EDU_VO_LIST = new ArrayList<>();

    /*营养配餐课程 健康教育*/
    public static final List<HealthEduVO> NUTRITIONALCATERING_HEALTH_EDU_VO_LIST = new ArrayList<>();

    static {
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("2020 年最新高血压饮食大全，一张图教你怎么吃",INTERVENEHYPERTENSION_PREFIX + "t-1.png",INTERVENEHYPERTENSION_PREFIX + "4-1.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("高血压悄悄改变大脑结构！你可能不知不觉就变“笨”了",INTERVENEHYPERTENSION_PREFIX + "t-2.png",INTERVENEHYPERTENSION_PREFIX + "4-2.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("别把头晕不当回事，原发性高血压的病情预警别忽略",INTERVENEHYPERTENSION_PREFIX + "t-3.png",INTERVENEHYPERTENSION_PREFIX + "4-3.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("高血压头痛的几大特点，紧急处理很重要！",INTERVENEHYPERTENSION_PREFIX + "t-4.png",INTERVENEHYPERTENSION_PREFIX + "4-4.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("高血压的那些事儿，想知道的都在这儿！",INTERVENEHYPERTENSION_PREFIX + "t-5.png",INTERVENEHYPERTENSION_PREFIX + "4-5.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("可怕！8张动图揭秘血管堵的全过程！建议每个人都该看看！",INTERVENEHYPERTENSION_PREFIX + "t-6.png",INTERVENEHYPERTENSION_PREFIX + "4-6.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("高血压的头号元凶，除了盐还有它！",INTERVENEHYPERTENSION_PREFIX + "t-7.png",INTERVENEHYPERTENSION_PREFIX + "4-7.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("全国高血压日 _ 合理运动能降压，信不信由你！",INTERVENEHYPERTENSION_PREFIX + "t-8.png",INTERVENEHYPERTENSION_PREFIX + "4-8.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("高血压就是血压高吗？这些高血压病的基础知识你我他都应该知道",INTERVENEHYPERTENSION_PREFIX + "t-9.png",INTERVENEHYPERTENSION_PREFIX + "4-9.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("万万没想到！高血压的罪魁祸首竟然是它…很多人都忽略了",INTERVENEHYPERTENSION_PREFIX + "t-10.png",INTERVENEHYPERTENSION_PREFIX + "4-10.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("这些高血压病的基础知识你我他都应该知道",INTERVENEHYPERTENSION_PREFIX + "t-11.png",INTERVENEHYPERTENSION_PREFIX + "4-11.png"));
        HEALTH_EDU_VO_LIST.add(new HealthEduVO("午睡让高血压悄悄回落！身体还会用这5种好处回报你",INTERVENEHYPERTENSION_PREFIX + "t-12.png",INTERVENEHYPERTENSION_PREFIX + "4-12.png"));


        NUTRITIONALCATERING_HEALTH_EDU_VO_LIST.add(new HealthEduVO("肥胖会导致血压升高，要警惕",NUTRITIONALCATERING_PREFIX +  "t-1.png",NUTRITIONALCATERING_PREFIX + "4-1.png"));
        NUTRITIONALCATERING_HEALTH_EDU_VO_LIST.add(new HealthEduVO("危机！隐形的肥胖更伤人",NUTRITIONALCATERING_PREFIX +  "t-2.png",NUTRITIONALCATERING_PREFIX + "4-2.png"));
        NUTRITIONALCATERING_HEALTH_EDU_VO_LIST.add(new HealthEduVO("老年人肥胖症常伴有脑血管疾病和糖尿病",NUTRITIONALCATERING_PREFIX +  "t-3.png",NUTRITIONALCATERING_PREFIX + "4-3.png"));
        NUTRITIONALCATERING_HEALTH_EDU_VO_LIST.add(new HealthEduVO("我到底是超重还是肥胖？",NUTRITIONALCATERING_PREFIX +  "t-4.png",NUTRITIONALCATERING_PREFIX + "4-4.png"));
        NUTRITIONALCATERING_HEALTH_EDU_VO_LIST.add(new HealthEduVO("此“胖”非彼“胖”，你是“胖”友吗？",NUTRITIONALCATERING_PREFIX +  "t-5.png",NUTRITIONALCATERING_PREFIX + "4-5.png"));

        STROKE_EDU_VO_LIST.add(new HealthEduVO("到底什么是脑卒中？",STROKE_PREFIX +  "t-1.png",STROKE_PREFIX + "4-1.png"));
        STROKE_EDU_VO_LIST.add(new HealthEduVO("脑卒中分秒“B”争",STROKE_PREFIX +  "t-2.png",STROKE_PREFIX + "4-2.png"));
        STROKE_EDU_VO_LIST.add(new HealthEduVO("脑卒中的九大误区",STROKE_PREFIX +  "t-3.png",STROKE_PREFIX + "4-3.png"));
        STROKE_EDU_VO_LIST.add(new HealthEduVO("脑卒中患者饮食指导",STROKE_PREFIX +  "t-4.png",STROKE_PREFIX + "4-4.png"));
        STROKE_EDU_VO_LIST.add(new HealthEduVO("脑卒中防治",STROKE_PREFIX +  "t-5.png",STROKE_PREFIX + "4-5.png"));
        STROKE_EDU_VO_LIST.add(new HealthEduVO("预防脑卒中，您该这样做！",STROKE_PREFIX +  "t-6.png",STROKE_PREFIX + "4-6.png"));

        CORONARY_EDU_VO_LIST.add(new HealthEduVO("心脏病≠冠心病 全面了解冠心病",INTERVENECORONARY_PREFIX +  "t-1.png",INTERVENECORONARY_PREFIX + "4-1.png"));
        CORONARY_EDU_VO_LIST.add(new HealthEduVO("冠心病的注意事项：冠心病饮食六大禁忌",INTERVENECORONARY_PREFIX +  "t-2.png",INTERVENECORONARY_PREFIX + "4-2.png"));
        CORONARY_EDU_VO_LIST.add(new HealthEduVO("并非老年人的“专利病”，细数冠心病的8大误区",INTERVENECORONARY_PREFIX +  "t-3.png",INTERVENECORONARY_PREFIX + "4-3.png"));
        CORONARY_EDU_VO_LIST.add(new HealthEduVO("冠心病发病年轻化，为什么年轻人的心脏越来越差？",INTERVENECORONARY_PREFIX +  "t-4.png",INTERVENECORONARY_PREFIX + "4-4.png"));
        CORONARY_EDU_VO_LIST.add(new HealthEduVO("冠心病，那么远，那么近",INTERVENECORONARY_PREFIX +  "t-5.png",INTERVENECORONARY_PREFIX + "4-5.png"));
        CORONARY_EDU_VO_LIST.add(new HealthEduVO("冠心病患者的日常饮食原则",INTERVENECORONARY_PREFIX +  "t-6.png",INTERVENECORONARY_PREFIX + "4-6.png"));

        DIABETES_EDU_VO_LIST.addAll(STROKE_EDU_VO_LIST);

        COPD_EDU_VO_LIST.add(new HealthEduVO("世界慢性阻塞性肺炎日｜早防早治，始终不晚！",COPD_PREFIX +  "t-1.png",COPD_PREFIX + "4-1.png"));
        COPD_EDU_VO_LIST.add(new HealthEduVO("注意啦！这个季节你可能会被这个病盯上……",COPD_PREFIX +  "t-2.png",COPD_PREFIX + "4-2.png"));
        COPD_EDU_VO_LIST.add(new HealthEduVO("专家解惑：慢阻肺治疗常见三大误区，最容易忽视的是它？",COPD_PREFIX +  "t-3.png",COPD_PREFIX + "4-3.png"));

        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("骨质疏松“静悄悄”，做好预防是关键",OSTEOPOROSIS_PREFIX +  "t-1.png",OSTEOPOROSIS_PREFIX + "4-1.png"));
        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("骨质疏松防治指南来了",OSTEOPOROSIS_PREFIX +  "t-2.png",OSTEOPOROSIS_PREFIX + "4-2.png"));
        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("骨质疏松症，一种“静悄悄”的疾病",OSTEOPOROSIS_PREFIX +  "t-3.png",OSTEOPOROSIS_PREFIX + "4-3.png"));
        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("骨质疏松症是老年人的“专利”吗？补钙能否管用？今天全部告诉你",OSTEOPOROSIS_PREFIX +  "t-4.png",OSTEOPOROSIS_PREFIX + "4-4.png"));
        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("远离骨质疏松，争当“硬骨头”",OSTEOPOROSIS_PREFIX +  "t-5.png",OSTEOPOROSIS_PREFIX + "4-5.png"));
        OSTEOPOROSIS_EDU_VO_LIST.add(new HealthEduVO("指南说，骨质疏松患者该这么运动！",OSTEOPOROSIS_PREFIX +  "t-6.png",OSTEOPOROSIS_PREFIX + "4-6.png"));


        PELVICFLOOR_EDU_VO_LIST.add(new HealthEduVO("产后的盆底功能康复治疗有多重要？",PELVICFLOOR_PREFIX +  "t-1.png",PELVICFLOOR_PREFIX + "4-1.png"));
        PELVICFLOOR_EDU_VO_LIST.add(new HealthEduVO("产后妈妈的噩梦-盆底功能障碍",PELVICFLOOR_PREFIX +  "t-2.png",PELVICFLOOR_PREFIX + "4-2.png"));
        PELVICFLOOR_EDU_VO_LIST.add(new HealthEduVO("关爱盆底健康--做幸福女神",PELVICFLOOR_PREFIX +  "t-3.png",PELVICFLOOR_PREFIX + "4-3.png"));
        PELVICFLOOR_EDU_VO_LIST.add(new HealthEduVO("怀孕&生孩子为什么会损伤盆底肌？",PELVICFLOOR_PREFIX +  "t-4.png",PELVICFLOOR_PREFIX + "4-4.png"));
    }
}
