package com.ruoyi.system.domain.vo;

import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-21 17:41
 **/
public class HealthInterventionSon {
    private String id;

    private String name;

    private List<HealthInterventionRight> healthInterventionRight;

    public HealthInterventionSon() {
    }

    public HealthInterventionSon(String id, String name, List<HealthInterventionRight> healthInterventionRight) {
        this.id = id;
        this.name = name;
        this.healthInterventionRight = healthInterventionRight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HealthInterventionRight> getHealthInterventionRight() {
        return healthInterventionRight;
    }

    public void setHealthInterventionRight(List<HealthInterventionRight> healthInterventionRight) {
        this.healthInterventionRight = healthInterventionRight;
    }
}
