package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description: 理论学习相关实体类
 * @author: 马瑞龙
 * @create: 2024-11-20 17:12
 **/
public class BaseInfo {
    private String title;
    private String paperName = "";
    private String paperId = "";

    public BaseInfo() {
    }

    public BaseInfo(String title, String paperName, String paperId) {
        this.title = title;
        this.paperName = paperName;
        this.paperId = paperId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperId() {
        return paperId;
    }

    public void setPaperId(String paperId) {
        this.paperId = paperId;
    }
}
