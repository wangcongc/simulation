package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description: 理论学习相关实体类-文件返回
 * @author: 马瑞龙
 * @create: 2024-11-20 17:13
 **/
public class FileList {
    private String fileName;
    private String path;
    private String fileType;
    private String folderId = "";

    public FileList() {
    }

    public FileList(String fileName, String path, String fileType, String folderId) {
        this.fileName = fileName;
        this.path = path;
        this.fileType = fileType;
        this.folderId = folderId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }
}
