package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 16:07
 **/
public class ResultVO {
    private String result;
    private String thisRisk;

    private String averageRisk;
    private String bestState;

    private String canReduceRisk;

    public ResultVO() {
    }

    public ResultVO(String result, String thisRisk, String averageRisk, String bestState, String canReduceRisk) {
        this.result = result;
        this.thisRisk = thisRisk;
        this.averageRisk = averageRisk;
        this.bestState = bestState;
        this.canReduceRisk = canReduceRisk;
    }

    public ResultVO(String thisRisk, String averageRisk, String bestState, String canReduceRisk) {
        this.thisRisk = thisRisk;
        this.averageRisk = averageRisk;
        this.bestState = bestState;
        this.canReduceRisk = canReduceRisk;
    }


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getThisRisk() {
        return thisRisk;
    }

    public void setThisRisk(String thisRisk) {
        this.thisRisk = thisRisk;
    }

    public String getAverageRisk() {
        return averageRisk;
    }

    public void setAverageRisk(String averageRisk) {
        this.averageRisk = averageRisk;
    }

    public String getBestState() {
        return bestState;
    }

    public void setBestState(String bestState) {
        this.bestState = bestState;
    }

    public String getCanReduceRisk() {
        return canReduceRisk;
    }

    public void setCanReduceRisk(String canReduceRisk) {
        this.canReduceRisk = canReduceRisk;
    }
}
