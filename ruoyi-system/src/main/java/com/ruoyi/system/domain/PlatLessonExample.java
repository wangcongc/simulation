package com.ruoyi.system.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlatLessonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlatLessonExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andManagerIsNull() {
            addCriterion("manager is null");
            return (Criteria) this;
        }

        public Criteria andManagerIsNotNull() {
            addCriterion("manager is not null");
            return (Criteria) this;
        }

        public Criteria andManagerEqualTo(String value) {
            addCriterion("manager =", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotEqualTo(String value) {
            addCriterion("manager <>", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThan(String value) {
            addCriterion("manager >", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThanOrEqualTo(String value) {
            addCriterion("manager >=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThan(String value) {
            addCriterion("manager <", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThanOrEqualTo(String value) {
            addCriterion("manager <=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLike(String value) {
            addCriterion("manager like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotLike(String value) {
            addCriterion("manager not like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerIn(List<String> values) {
            addCriterion("manager in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotIn(List<String> values) {
            addCriterion("manager not in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerBetween(String value1, String value2) {
            addCriterion("manager between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotBetween(String value1, String value2) {
            addCriterion("manager not between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerMobileIsNull() {
            addCriterion("manager_mobile is null");
            return (Criteria) this;
        }

        public Criteria andManagerMobileIsNotNull() {
            addCriterion("manager_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andManagerMobileEqualTo(String value) {
            addCriterion("manager_mobile =", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileNotEqualTo(String value) {
            addCriterion("manager_mobile <>", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileGreaterThan(String value) {
            addCriterion("manager_mobile >", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileGreaterThanOrEqualTo(String value) {
            addCriterion("manager_mobile >=", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileLessThan(String value) {
            addCriterion("manager_mobile <", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileLessThanOrEqualTo(String value) {
            addCriterion("manager_mobile <=", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileLike(String value) {
            addCriterion("manager_mobile like", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileNotLike(String value) {
            addCriterion("manager_mobile not like", value, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileIn(List<String> values) {
            addCriterion("manager_mobile in", values, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileNotIn(List<String> values) {
            addCriterion("manager_mobile not in", values, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileBetween(String value1, String value2) {
            addCriterion("manager_mobile between", value1, value2, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andManagerMobileNotBetween(String value1, String value2) {
            addCriterion("manager_mobile not between", value1, value2, "managerMobile");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCollegeIdIsNull() {
            addCriterion("college_id is null");
            return (Criteria) this;
        }

        public Criteria andCollegeIdIsNotNull() {
            addCriterion("college_id is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeIdEqualTo(Long value) {
            addCriterion("college_id =", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdNotEqualTo(Long value) {
            addCriterion("college_id <>", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdGreaterThan(Long value) {
            addCriterion("college_id >", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("college_id >=", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdLessThan(Long value) {
            addCriterion("college_id <", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdLessThanOrEqualTo(Long value) {
            addCriterion("college_id <=", value, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdIn(List<Long> values) {
            addCriterion("college_id in", values, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdNotIn(List<Long> values) {
            addCriterion("college_id not in", values, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdBetween(Long value1, Long value2) {
            addCriterion("college_id between", value1, value2, "collegeId");
            return (Criteria) this;
        }

        public Criteria andCollegeIdNotBetween(Long value1, Long value2) {
            addCriterion("college_id not between", value1, value2, "collegeId");
            return (Criteria) this;
        }

        public Criteria andMajorsIsNull() {
            addCriterion("majors is null");
            return (Criteria) this;
        }

        public Criteria andMajorsIsNotNull() {
            addCriterion("majors is not null");
            return (Criteria) this;
        }

        public Criteria andMajorsEqualTo(String value) {
            addCriterion("majors =", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsNotEqualTo(String value) {
            addCriterion("majors <>", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsGreaterThan(String value) {
            addCriterion("majors >", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsGreaterThanOrEqualTo(String value) {
            addCriterion("majors >=", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsLessThan(String value) {
            addCriterion("majors <", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsLessThanOrEqualTo(String value) {
            addCriterion("majors <=", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsLike(String value) {
            addCriterion("majors like", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsNotLike(String value) {
            addCriterion("majors not like", value, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsIn(List<String> values) {
            addCriterion("majors in", values, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsNotIn(List<String> values) {
            addCriterion("majors not in", values, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsBetween(String value1, String value2) {
            addCriterion("majors between", value1, value2, "majors");
            return (Criteria) this;
        }

        public Criteria andMajorsNotBetween(String value1, String value2) {
            addCriterion("majors not between", value1, value2, "majors");
            return (Criteria) this;
        }

        public Criteria andTeachersIsNull() {
            addCriterion("teachers is null");
            return (Criteria) this;
        }

        public Criteria andTeachersIsNotNull() {
            addCriterion("teachers is not null");
            return (Criteria) this;
        }

        public Criteria andTeachersEqualTo(String value) {
            addCriterion("teachers =", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersNotEqualTo(String value) {
            addCriterion("teachers <>", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersGreaterThan(String value) {
            addCriterion("teachers >", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersGreaterThanOrEqualTo(String value) {
            addCriterion("teachers >=", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersLessThan(String value) {
            addCriterion("teachers <", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersLessThanOrEqualTo(String value) {
            addCriterion("teachers <=", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersLike(String value) {
            addCriterion("teachers like", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersNotLike(String value) {
            addCriterion("teachers not like", value, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersIn(List<String> values) {
            addCriterion("teachers in", values, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersNotIn(List<String> values) {
            addCriterion("teachers not in", values, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersBetween(String value1, String value2) {
            addCriterion("teachers between", value1, value2, "teachers");
            return (Criteria) this;
        }

        public Criteria andTeachersNotBetween(String value1, String value2) {
            addCriterion("teachers not between", value1, value2, "teachers");
            return (Criteria) this;
        }

        public Criteria andLinkIsNull() {
            addCriterion("link is null");
            return (Criteria) this;
        }

        public Criteria andLinkIsNotNull() {
            addCriterion("link is not null");
            return (Criteria) this;
        }

        public Criteria andLinkEqualTo(String value) {
            addCriterion("link =", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkNotEqualTo(String value) {
            addCriterion("link <>", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkGreaterThan(String value) {
            addCriterion("link >", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkGreaterThanOrEqualTo(String value) {
            addCriterion("link >=", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkLessThan(String value) {
            addCriterion("link <", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkLessThanOrEqualTo(String value) {
            addCriterion("link <=", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkLike(String value) {
            addCriterion("link like", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkNotLike(String value) {
            addCriterion("link not like", value, "link");
            return (Criteria) this;
        }

        public Criteria andLinkIn(List<String> values) {
            addCriterion("link in", values, "link");
            return (Criteria) this;
        }

        public Criteria andLinkNotIn(List<String> values) {
            addCriterion("link not in", values, "link");
            return (Criteria) this;
        }

        public Criteria andLinkBetween(String value1, String value2) {
            addCriterion("link between", value1, value2, "link");
            return (Criteria) this;
        }

        public Criteria andLinkNotBetween(String value1, String value2) {
            addCriterion("link not between", value1, value2, "link");
            return (Criteria) this;
        }

        public Criteria andInfoIsNull() {
            addCriterion("info is null");
            return (Criteria) this;
        }

        public Criteria andInfoIsNotNull() {
            addCriterion("info is not null");
            return (Criteria) this;
        }

        public Criteria andInfoEqualTo(String value) {
            addCriterion("info =", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoNotEqualTo(String value) {
            addCriterion("info <>", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoGreaterThan(String value) {
            addCriterion("info >", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoGreaterThanOrEqualTo(String value) {
            addCriterion("info >=", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoLessThan(String value) {
            addCriterion("info <", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoLessThanOrEqualTo(String value) {
            addCriterion("info <=", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoLike(String value) {
            addCriterion("info like", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoNotLike(String value) {
            addCriterion("info not like", value, "info");
            return (Criteria) this;
        }

        public Criteria andInfoIn(List<String> values) {
            addCriterion("info in", values, "info");
            return (Criteria) this;
        }

        public Criteria andInfoNotIn(List<String> values) {
            addCriterion("info not in", values, "info");
            return (Criteria) this;
        }

        public Criteria andInfoBetween(String value1, String value2) {
            addCriterion("info between", value1, value2, "info");
            return (Criteria) this;
        }

        public Criteria andInfoNotBetween(String value1, String value2) {
            addCriterion("info not between", value1, value2, "info");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNull() {
            addCriterion("type_id is null");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNotNull() {
            addCriterion("type_id is not null");
            return (Criteria) this;
        }

        public Criteria andTypeIdEqualTo(Long value) {
            addCriterion("type_id =", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotEqualTo(Long value) {
            addCriterion("type_id <>", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThan(Long value) {
            addCriterion("type_id >", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("type_id >=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThan(Long value) {
            addCriterion("type_id <", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThanOrEqualTo(Long value) {
            addCriterion("type_id <=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdIn(List<Long> values) {
            addCriterion("type_id in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotIn(List<Long> values) {
            addCriterion("type_id not in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdBetween(Long value1, Long value2) {
            addCriterion("type_id between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotBetween(Long value1, Long value2) {
            addCriterion("type_id not between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andCollegeNameIsNull() {
            addCriterion("college_name is null");
            return (Criteria) this;
        }

        public Criteria andCollegeNameIsNotNull() {
            addCriterion("college_name is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeNameEqualTo(String value) {
            addCriterion("college_name =", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameNotEqualTo(String value) {
            addCriterion("college_name <>", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameGreaterThan(String value) {
            addCriterion("college_name >", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameGreaterThanOrEqualTo(String value) {
            addCriterion("college_name >=", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameLessThan(String value) {
            addCriterion("college_name <", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameLessThanOrEqualTo(String value) {
            addCriterion("college_name <=", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameLike(String value) {
            addCriterion("college_name like", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameNotLike(String value) {
            addCriterion("college_name not like", value, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameIn(List<String> values) {
            addCriterion("college_name in", values, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameNotIn(List<String> values) {
            addCriterion("college_name not in", values, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameBetween(String value1, String value2) {
            addCriterion("college_name between", value1, value2, "collegeName");
            return (Criteria) this;
        }

        public Criteria andCollegeNameNotBetween(String value1, String value2) {
            addCriterion("college_name not between", value1, value2, "collegeName");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIsNull() {
            addCriterion("serial_number is null");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIsNotNull() {
            addCriterion("serial_number is not null");
            return (Criteria) this;
        }

        public Criteria andSerialNumberEqualTo(Integer value) {
            addCriterion("serial_number =", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotEqualTo(Integer value) {
            addCriterion("serial_number <>", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberGreaterThan(Integer value) {
            addCriterion("serial_number >", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("serial_number >=", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberLessThan(Integer value) {
            addCriterion("serial_number <", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberLessThanOrEqualTo(Integer value) {
            addCriterion("serial_number <=", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIn(List<Integer> values) {
            addCriterion("serial_number in", values, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotIn(List<Integer> values) {
            addCriterion("serial_number not in", values, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberBetween(Integer value1, Integer value2) {
            addCriterion("serial_number between", value1, value2, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("serial_number not between", value1, value2, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andStudentTitleIsNull() {
            addCriterion("student_title is null");
            return (Criteria) this;
        }

        public Criteria andStudentTitleIsNotNull() {
            addCriterion("student_title is not null");
            return (Criteria) this;
        }

        public Criteria andStudentTitleEqualTo(String value) {
            addCriterion("student_title =", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleNotEqualTo(String value) {
            addCriterion("student_title <>", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleGreaterThan(String value) {
            addCriterion("student_title >", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleGreaterThanOrEqualTo(String value) {
            addCriterion("student_title >=", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleLessThan(String value) {
            addCriterion("student_title <", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleLessThanOrEqualTo(String value) {
            addCriterion("student_title <=", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleLike(String value) {
            addCriterion("student_title like", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleNotLike(String value) {
            addCriterion("student_title not like", value, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleIn(List<String> values) {
            addCriterion("student_title in", values, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleNotIn(List<String> values) {
            addCriterion("student_title not in", values, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleBetween(String value1, String value2) {
            addCriterion("student_title between", value1, value2, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentTitleNotBetween(String value1, String value2) {
            addCriterion("student_title not between", value1, value2, "studentTitle");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlIsNull() {
            addCriterion("student_img_url is null");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlIsNotNull() {
            addCriterion("student_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlEqualTo(String value) {
            addCriterion("student_img_url =", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlNotEqualTo(String value) {
            addCriterion("student_img_url <>", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlGreaterThan(String value) {
            addCriterion("student_img_url >", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("student_img_url >=", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlLessThan(String value) {
            addCriterion("student_img_url <", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlLessThanOrEqualTo(String value) {
            addCriterion("student_img_url <=", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlLike(String value) {
            addCriterion("student_img_url like", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlNotLike(String value) {
            addCriterion("student_img_url not like", value, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlIn(List<String> values) {
            addCriterion("student_img_url in", values, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlNotIn(List<String> values) {
            addCriterion("student_img_url not in", values, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlBetween(String value1, String value2) {
            addCriterion("student_img_url between", value1, value2, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andStudentImgUrlNotBetween(String value1, String value2) {
            addCriterion("student_img_url not between", value1, value2, "studentImgUrl");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIsNull() {
            addCriterion("module_folder_name is null");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIsNotNull() {
            addCriterion("module_folder_name is not null");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameEqualTo(String value) {
            addCriterion("module_folder_name =", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotEqualTo(String value) {
            addCriterion("module_folder_name <>", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameGreaterThan(String value) {
            addCriterion("module_folder_name >", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameGreaterThanOrEqualTo(String value) {
            addCriterion("module_folder_name >=", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLessThan(String value) {
            addCriterion("module_folder_name <", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLessThanOrEqualTo(String value) {
            addCriterion("module_folder_name <=", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLike(String value) {
            addCriterion("module_folder_name like", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotLike(String value) {
            addCriterion("module_folder_name not like", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIn(List<String> values) {
            addCriterion("module_folder_name in", values, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotIn(List<String> values) {
            addCriterion("module_folder_name not in", values, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameBetween(String value1, String value2) {
            addCriterion("module_folder_name between", value1, value2, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotBetween(String value1, String value2) {
            addCriterion("module_folder_name not between", value1, value2, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIsNull() {
            addCriterion("lesson_folder_name is null");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIsNotNull() {
            addCriterion("lesson_folder_name is not null");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameEqualTo(String value) {
            addCriterion("lesson_folder_name =", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotEqualTo(String value) {
            addCriterion("lesson_folder_name <>", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameGreaterThan(String value) {
            addCriterion("lesson_folder_name >", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameGreaterThanOrEqualTo(String value) {
            addCriterion("lesson_folder_name >=", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLessThan(String value) {
            addCriterion("lesson_folder_name <", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLessThanOrEqualTo(String value) {
            addCriterion("lesson_folder_name <=", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLike(String value) {
            addCriterion("lesson_folder_name like", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotLike(String value) {
            addCriterion("lesson_folder_name not like", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIn(List<String> values) {
            addCriterion("lesson_folder_name in", values, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotIn(List<String> values) {
            addCriterion("lesson_folder_name not in", values, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameBetween(String value1, String value2) {
            addCriterion("lesson_folder_name between", value1, value2, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotBetween(String value1, String value2) {
            addCriterion("lesson_folder_name not between", value1, value2, "lessonFolderName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}