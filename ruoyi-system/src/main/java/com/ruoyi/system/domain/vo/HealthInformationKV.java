package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-21 18:27
 **/
public class HealthInformationKV<T> {
    private String key;
    private T value;

    public HealthInformationKV() {
    }

    public HealthInformationKV(String key, T value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
