package com.ruoyi.system.domain;

import java.util.List;

public class PlatQuesSportStudy {
    private Long id;

    private Integer type;

    private String name;

    private Long parentId;

    private Long lessonId;

    private String url;

    private List<PlatQuesSportStudy> list;

    public List<PlatQuesSportStudy> getList() {
        return list;
    }

    public void setList(List<PlatQuesSportStudy> list) {
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }
}