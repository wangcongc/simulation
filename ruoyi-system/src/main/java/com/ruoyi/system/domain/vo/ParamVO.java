package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 15:55
 **/
public class ParamVO {
    private String paramName;

    private String result;

    private String reference;

    private String units;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public ParamVO() {
    }

    public ParamVO(String paramName, String result, String reference, String units) {
        this.paramName = paramName;
        this.result = result;
        this.reference = reference;
        this.units = units;
    }
}
