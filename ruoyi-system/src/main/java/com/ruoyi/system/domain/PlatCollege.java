package com.ruoyi.system.domain;

import java.util.Date;

public class PlatCollege {
    private Long id;

    private String name;

    private String manager;

    private String managerMobile;

    private Date createTime;

    private Date updateTime;

    private int platMajorNum = 0;

    public int getPlatMajorNum() {
        return platMajorNum;
    }

    public void setPlatMajorNum(int platMajorNum) {
        this.platMajorNum = platMajorNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager == null ? null : manager.trim();
    }

    public String getManagerMobile() {
        return managerMobile;
    }

    public void setManagerMobile(String managerMobile) {
        this.managerMobile = managerMobile == null ? null : managerMobile.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}