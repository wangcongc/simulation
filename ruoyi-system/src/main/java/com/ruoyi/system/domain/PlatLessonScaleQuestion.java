package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

public class PlatLessonScaleQuestion {
    private Long id;

    private Long parentId;

    private String name;

    private List<PlatLessonScaleQuestion> item;

    private Long lessonScaleSonId;

    private Long lessonScaleId;

    private Date createTime;

    private Date updateTime;

    private Integer serialNumber;

    private Integer type;

    private Integer score;

    public List<PlatLessonScaleQuestion> getItem() {
        return item;
    }

    public void setItem(List<PlatLessonScaleQuestion> item) {
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getLessonScaleSonId() {
        return lessonScaleSonId;
    }

    public void setLessonScaleSonId(Long lessonScaleSonId) {
        this.lessonScaleSonId = lessonScaleSonId;
    }

    public Long getLessonScaleId() {
        return lessonScaleId;
    }

    public void setLessonScaleId(Long lessonScaleId) {
        this.lessonScaleId = lessonScaleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}