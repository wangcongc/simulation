package com.ruoyi.system.domain.vo;

import java.util.List;

public class EhmsAnswerVO {
    private List<EhmsAnswerListVO> list;

    //量表健康指导
    private String gauge;

    public List<EhmsAnswerListVO> getList() {
        return list;
    }

    public void setList(List<EhmsAnswerListVO> list) {
        this.list = list;
    }

    public String getGauge() {
        return gauge;
    }

    public void setGauge(String gauge) {
        this.gauge = gauge;
    }
}
