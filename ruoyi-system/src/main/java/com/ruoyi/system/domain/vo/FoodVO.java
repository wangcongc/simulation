package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description: 食物信息
 * @author: 马瑞龙
 * @create: 2024-11-19 23:27
 **/
public class FoodVO {
    private String id;

    private String name;

    private String tooltip;

    private String tips;

    private String imgSrc;

    private Integer defaults;

    public FoodVO() {
    }

    public FoodVO(String id, String name, String tooltip, String tips, String imgSrc, Integer defaults) {
        this.id = id;
        this.name = name;
        this.tooltip = tooltip;
        this.tips = tips;
        this.imgSrc = imgSrc;
        this.defaults = defaults;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public Integer getDefaults() {
        return defaults;
    }

    public void setDefaults(Integer defaults) {
        this.defaults = defaults;
    }
}
