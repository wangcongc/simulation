package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 17:58
 **/
public class PsychologicalVO {
    private String title;

    private String titleImg;

    private String detail;

    public PsychologicalVO() {
    }

    public PsychologicalVO(String title, String titleImg, String detail) {
        this.title = title;
        this.titleImg = titleImg;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
