package com.ruoyi.system.domain.vo;

import java.util.List;

public class EhmsScaleVO {
    private Long id;
    private String name;
    private List<EhmsScaleVO> list;

    public List<EhmsScaleVO> getList() {
        return list;
    }

    public void setList(List<EhmsScaleVO> list) {
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
