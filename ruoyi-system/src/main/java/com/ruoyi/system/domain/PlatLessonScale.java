package com.ruoyi.system.domain;

import java.util.Date;

public class PlatLessonScale {
    private Long id;

    private String name;

    private Long lessonSonId;

    private Date createTime;

    private Date updateTime;

    private Integer serialNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getLessonSonId() {
        return lessonSonId;
    }

    public void setLessonSonId(Long lessonSonId) {
        this.lessonSonId = lessonSonId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }
}