package com.ruoyi.system.domain.vo;

public class EhmsAnswerListVO {
    private String name;
    //评估结果
    private String riskResult;
    //评估方案
    private String riskPlan;

    public String getRiskResult() {
        return riskResult;
    }

    public void setRiskResult(String riskResult) {
        this.riskResult = riskResult;
    }

    public String getRiskPlan() {
        return riskPlan;
    }

    public void setRiskPlan(String riskPlan) {
        this.riskPlan = riskPlan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
