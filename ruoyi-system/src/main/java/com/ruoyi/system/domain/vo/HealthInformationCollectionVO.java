package com.ruoyi.system.domain.vo;

import com.ruoyi.system.constant.TagsConstant;

import java.util.Arrays;
import java.util.List;

/**
 * @program: simulation
 * @description: 慢性病饮食干预-高血压 健康信息收集
 * @author: 马瑞龙
 * @create: 2024-11-20 18:23
 **/
public class HealthInformationCollectionVO {
    /**课程 基本信息*/
    private TheoreticalLearningVO theoreticalLearningVO;

    /**标签信息*/
    private List<ClassificationVO> healthInformationList = Arrays.asList(
            new ClassificationVO("1","drag","患者资料-拖拽项", TagsConstant.ALL_TAGS),
            new ClassificationVO("2","drop","基本情况-放置区", TagsConstant.BASIC_SITUATION_TAGS),
            new ClassificationVO("3","drop","家族病史-放置区", TagsConstant.FAMILY_HISTORY_TAGS),
            new ClassificationVO("4","drop","体重-放置区", TagsConstant.WEIGHT_TAGS),
            new ClassificationVO("5","drop","心理-放置区", TagsConstant.MIND_TAGS),
            new ClassificationVO("6","drop","睡眠-放置区", TagsConstant.SLEEP_TAGS),
            new ClassificationVO("7","drop","生活方式1-放置区", TagsConstant.LIFESTYLE_ONE_TAGS),
            new ClassificationVO("8","drop","生活方式2-放置区", TagsConstant.LIFESTYLE_TWO_TAGS),
            new ClassificationVO("9","drop","生活方式3-放置区", TagsConstant.LIFESTYLE_THREE_TAGS),
            new ClassificationVO("10","drop","生活方式4-放置区", TagsConstant.LIFESTYLE_FORE_TAGS),
            new ClassificationVO("11","drop","重要健康体检结果-放置区", TagsConstant.PHYSICAL_EXAMINATION_TAGS),
            new ClassificationVO("12","drop","现患疾病-放置区", TagsConstant.PRESENT_DISEASE_TAGS)
    );

    public HealthInformationCollectionVO() {
    }

    public HealthInformationCollectionVO(TheoreticalLearningVO theoreticalLearningVO, List<ClassificationVO> healthInformationList) {
        this.theoreticalLearningVO = theoreticalLearningVO;
        this.healthInformationList = healthInformationList;
    }

    public TheoreticalLearningVO getTheoreticalLearningVO() {
        return theoreticalLearningVO;
    }

    public void setTheoreticalLearningVO(TheoreticalLearningVO theoreticalLearningVO) {
        this.theoreticalLearningVO = theoreticalLearningVO;
    }

    public List<ClassificationVO> getHealthInformationList() {
        return healthInformationList;
    }

    public void setHealthInformationList(List<ClassificationVO> healthInformationList) {
        this.healthInformationList = healthInformationList;
    }
}
