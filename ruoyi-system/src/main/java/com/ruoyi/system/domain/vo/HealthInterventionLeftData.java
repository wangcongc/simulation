package com.ruoyi.system.domain.vo;

import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-21 17:40
 **/
public class HealthInterventionLeftData {
    private String name;

    private List<HealthInterventionSon> healthInterventionSon;


    public HealthInterventionLeftData() {
    }

    public HealthInterventionLeftData(String name, List<HealthInterventionSon> healthInterventionSon) {
        this.name = name;
        this.healthInterventionSon = healthInterventionSon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HealthInterventionSon> getHealthInterventionSon() {
        return healthInterventionSon;
    }

    public void setHealthInterventionSon(List<HealthInterventionSon> healthInterventionSon) {
        this.healthInterventionSon = healthInterventionSon;
    }
}
