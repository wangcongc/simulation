package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.PlatLessonScaleQuestion;

import java.util.List;

public class EhmsQuestionVO {
    private List<List<PlatLessonScaleQuestion>> list;

    public List<List<PlatLessonScaleQuestion>> getList() {
        return list;
    }

    public void setList(List<List<PlatLessonScaleQuestion>> list) {
        this.list = list;
    }
}
