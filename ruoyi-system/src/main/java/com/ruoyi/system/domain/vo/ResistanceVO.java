package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 17:38
 **/
public class ResistanceVO {
    private Integer id;

    private String item;

    private String desc;

    private String grate;

    public ResistanceVO() {
    }

    public ResistanceVO(Integer id, String item, String desc) {
        this.id = id;
        this.item = item;
        this.desc = desc;
    }

    public ResistanceVO(Integer id, String item, String desc, String grate) {
        this.id = id;
        this.item = item;
        this.desc = desc;
        this.grate = grate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGrate() {
        return grate;
    }

    public void setGrate(String grate) {
        this.grate = grate;
    }
}
