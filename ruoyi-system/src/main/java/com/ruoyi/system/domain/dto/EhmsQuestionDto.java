package com.ruoyi.system.domain.dto;

public class EhmsQuestionDto {
    private String scaleSonIds;

    public String getScaleSonIds() {
        return scaleSonIds;
    }

    public void setScaleSonIds(String scaleSonIds) {
        this.scaleSonIds = scaleSonIds;
    }
}
