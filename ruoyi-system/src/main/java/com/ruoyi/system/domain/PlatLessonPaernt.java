package com.ruoyi.system.domain;

public class PlatLessonPaernt {
    private String lessonTime;
    private String studentState;
    private Integer studentStateCountans;
    private Long platLessonStudentId;
    private Long typeId;
    private String typeName;
    private int chapterTypeNum = 0;

    public int getChapterTypeNum() {
        return chapterTypeNum;
    }

    public void setChapterTypeNum(int chapterTypeNum) {
        this.chapterTypeNum = chapterTypeNum;
    }

    public Integer getStudentStateCountans() {
        return studentStateCountans;
    }

    public void setStudentStateCountans(Integer studentStateCountans) {
        this.studentStateCountans = studentStateCountans;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getPlatLessonStudentId() {
        return platLessonStudentId;
    }

    public void setPlatLessonStudentId(Long platLessonStudentId) {
        this.platLessonStudentId = platLessonStudentId;
    }

    public String getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(String lessonTime) {
        this.lessonTime = lessonTime;
    }

    public String getStudentState() {
        return studentState;
    }

    public void setStudentState(String studentState) {
        this.studentState = studentState;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
