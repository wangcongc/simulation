package com.ruoyi.system.domain.vo;

import com.ruoyi.system.constant.FoodConstant;
import com.ruoyi.system.domain.PlatLesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: simulation
 * @description:食材分类返回类
 * @author: 马瑞龙
 * @create: 2024-11-19 23:13
 **/
public class FoodClassificationVO {
    /**课程 基本信息*/
    private PlatLesson platLesson;
    /**食材信息*/
    private List<ClassificationVO> classificationList = Arrays.asList(
            new ClassificationVO("0","drag","拖拽项", FoodConstant.ALL_FOOD_LIST),
            new ClassificationVO("1","drop","蛋白类-放置区", FoodConstant.PROTEIN_FOOD_LIST),
            new ClassificationVO("2","drop","蔬菜类-放置区", FoodConstant.VEGETABLES_FOOD_LIST),
            new ClassificationVO("3","drop","水果类-放置区", FoodConstant.FRUIT_FOOD_LIST),
            new ClassificationVO("4","drop","主食类-放置区", FoodConstant.STAPLE_FOOD_LIST)
    );

    public FoodClassificationVO() {
    }

    public FoodClassificationVO(PlatLesson platLesson, List<ClassificationVO> classificationList) {
        this.platLesson = platLesson;
        this.classificationList = classificationList;
    }

    public PlatLesson getPlatLesson() {
        return platLesson;
    }

    public void setPlatLesson(PlatLesson platLesson) {
        this.platLesson = platLesson;
    }

    public List<ClassificationVO> getClassificationList() {
        if (null == classificationList) {
            classificationList = new ArrayList<>();
            classificationList.add(new ClassificationVO("0","drag","拖拽项", FoodConstant.ALL_FOOD_LIST));
            classificationList.add(new ClassificationVO("1","drop","蛋白类-放置区", FoodConstant.PROTEIN_FOOD_LIST));
            classificationList.add(new ClassificationVO("2","drop","蔬菜类-放置区", FoodConstant.VEGETABLES_FOOD_LIST));
            classificationList.add(new ClassificationVO("3","drop","水果类-放置区", FoodConstant.FRUIT_FOOD_LIST));
            classificationList.add(new ClassificationVO("4","drop","主食类-放置区", FoodConstant.STAPLE_FOOD_LIST));
        }
        return classificationList;
    }

    public void setClassificationList(List<ClassificationVO> classificationList) {
        this.classificationList = classificationList;
    }
}
