package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-28 16:31
 **/
public class FoodLabelsOptions {
    private Integer id;

    private String optionName;

    private String optionSort;
    private String optionTitle;

    private String optionImage;


    public FoodLabelsOptions() {
    }

    public FoodLabelsOptions(Integer id, String optionName, String optionSort, String optionTitle, String optionImage) {
        this.id = id;
        this.optionName = optionName;
        this.optionSort = optionSort;
        this.optionTitle = optionTitle;
        this.optionImage = optionImage;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionSort() {
        return optionSort;
    }

    public void setOptionSort(String optionSort) {
        this.optionSort = optionSort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOptionTitle() {
        return optionTitle;
    }

    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }

    public String getOptionImage() {
        return optionImage;
    }

    public void setOptionImage(String optionImage) {
        this.optionImage = optionImage;
    }
}
