package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-22 18:06
 **/
public class HealthEduVO {
    private String title;

    private String titleImg;

    private String descImg;

    public HealthEduVO() {
    }

    public HealthEduVO(String title, String titleImg, String descImg) {
        this.title = title;
        this.titleImg = titleImg;
        this.descImg = descImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }

    public String getDescImg() {
        return descImg;
    }

    public void setDescImg(String descImg) {
        this.descImg = descImg;
    }
}
