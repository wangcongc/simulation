package com.ruoyi.system.domain;

public class PlatInterfereHealth {
    private Long id;

    private String assistantText;

    private String assistantAudioUrl;

    private String patientInformationInfo;

    private String assistantHealthText;

    private String assistantHealthAudioUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssistantText() {
        return assistantText;
    }

    public void setAssistantText(String assistantText) {
        this.assistantText = assistantText == null ? null : assistantText.trim();
    }

    public String getAssistantAudioUrl() {
        return assistantAudioUrl;
    }

    public void setAssistantAudioUrl(String assistantAudioUrl) {
        this.assistantAudioUrl = assistantAudioUrl == null ? null : assistantAudioUrl.trim();
    }

    public String getPatientInformationInfo() {
        return patientInformationInfo;
    }

    public void setPatientInformationInfo(String patientInformationInfo) {
        this.patientInformationInfo = patientInformationInfo == null ? null : patientInformationInfo.trim();
    }

    public String getAssistantHealthText() {
        return assistantHealthText;
    }

    public void setAssistantHealthText(String assistantHealthText) {
        this.assistantHealthText = assistantHealthText == null ? null : assistantHealthText.trim();
    }

    public String getAssistantHealthAudioUrl() {
        return assistantHealthAudioUrl;
    }

    public void setAssistantHealthAudioUrl(String assistantHealthAudioUrl) {
        this.assistantHealthAudioUrl = assistantHealthAudioUrl == null ? null : assistantHealthAudioUrl.trim();
    }
}