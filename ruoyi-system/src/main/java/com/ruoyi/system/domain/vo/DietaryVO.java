package com.ruoyi.system.domain.vo;

/**
 * @program: simulation
 * @description: 膳食实体类
 * @author: 马瑞龙
 * @create: 2024-11-22 12:15
 **/
public class DietaryVO {
    private Integer id;
    private String name;
    private Integer category;
    private Double scale;
    private Double heat;
    private Double protein;
    private Double fat;
    private Double carbohydrate;
    private Double dietaryfiber;
    private String classify;

    public DietaryVO() {
    }

    public DietaryVO(Integer id, String name, Integer category, Double scale, Double heat, Double protein, Double fat, Double carbohydrate, Double dietaryfiber, String classify) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.scale = scale;
        this.heat = heat;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
        this.dietaryfiber = dietaryfiber;
        this.classify = classify;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Double getScale() {
        return scale;
    }

    public void setScale(Double scale) {
        this.scale = scale;
    }

    public Double getHeat() {
        return heat;
    }

    public void setHeat(Double heat) {
        this.heat = heat;
    }

    public Double getProtein() {
        return protein;
    }

    public void setProtein(Double protein) {
        this.protein = protein;
    }

    public Double getFat() {
        return fat;
    }

    public void setFat(Double fat) {
        this.fat = fat;
    }

    public Double getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(Double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public Double getDietaryfiber() {
        return dietaryfiber;
    }

    public void setDietaryfiber(Double dietaryfiber) {
        this.dietaryfiber = dietaryfiber;
    }

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify;
    }
}
