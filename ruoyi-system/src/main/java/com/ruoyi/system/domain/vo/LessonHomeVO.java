package com.ruoyi.system.domain.vo;

public class LessonHomeVO {
    //课程数量
    private int allLessonNum;
    //已安排课程数量
    private int lessonIngNum;
    //已安排课程数量-本周
    private int lessonWeekIngNum;
    //已安排课程数量-本月
    private int lessonMonthIngNum;

    //学院总数
    private int collegeNum;
    //专业总数
    private int majorNum;
    //班级总数
    private int classNum;
    //学院人数
    private int userNum;

    //超管数量
    private int superNum;
    //普管数量
    private int adminNum;
    //教师数量
    private int teacherNum;
    //学生数量
    private int studentNum;

    //学生 未开始的课程
    private int notStartLessonNum;
    //学生 已结束的课程
    private int endLessonNum;

    public int getEndLessonNum() {
        return endLessonNum;
    }

    public void setEndLessonNum(int endLessonNum) {
        this.endLessonNum = endLessonNum;
    }

    public int getNotStartLessonNum() {
        return notStartLessonNum;
    }

    public void setNotStartLessonNum(int notStartLessonNum) {
        this.notStartLessonNum = notStartLessonNum;
    }

    public int getLessonMonthIngNum() {
        return lessonMonthIngNum;
    }

    public void setLessonMonthIngNum(int lessonMonthIngNum) {
        this.lessonMonthIngNum = lessonMonthIngNum;
    }

    public int getLessonWeekIngNum() {
        return lessonWeekIngNum;
    }

    public void setLessonWeekIngNum(int lessonWeekIngNum) {
        this.lessonWeekIngNum = lessonWeekIngNum;
    }

    public int getSuperNum() {
        return superNum;
    }

    public void setSuperNum(int superNum) {
        this.superNum = superNum;
    }

    public int getAdminNum() {
        return adminNum;
    }

    public void setAdminNum(int adminNum) {
        this.adminNum = adminNum;
    }

    public int getTeacherNum() {
        return teacherNum;
    }

    public void setTeacherNum(int teacherNum) {
        this.teacherNum = teacherNum;
    }

    public int getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(int studentNum) {
        this.studentNum = studentNum;
    }

    public int getUserNum() {
        return userNum;
    }

    public void setUserNum(int userNum) {
        this.userNum = userNum;
    }

    public int getClassNum() {
        return classNum;
    }

    public void setClassNum(int classNum) {
        this.classNum = classNum;
    }

    public int getCollegeNum() {
        return collegeNum;
    }

    public int getMajorNum() {
        return majorNum;
    }

    public void setMajorNum(int majorNum) {
        this.majorNum = majorNum;
    }

    public void setCollegeNum(int collegeNum) {
        this.collegeNum = collegeNum;
    }

    public int getAllLessonNum() {
        return allLessonNum;
    }

    public void setAllLessonNum(int allLessonNum) {
        this.allLessonNum = allLessonNum;
    }

    public int getLessonIngNum() {
        return lessonIngNum;
    }

    public void setLessonIngNum(int lessonIngNum) {
        this.lessonIngNum = lessonIngNum;
    }
}
