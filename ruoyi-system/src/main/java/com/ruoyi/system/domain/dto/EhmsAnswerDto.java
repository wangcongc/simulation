package com.ruoyi.system.domain.dto;

public class EhmsAnswerDto {
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
