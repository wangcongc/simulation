package com.ruoyi.system.domain.vo;

import java.util.List;

public class PlatMenuVO {

    private String name;

    private String title;

    private String icon;

    private List<PlatMenusVO> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<PlatMenusVO> getList() {
        return list;
    }

    public void setList(List<PlatMenusVO> list) {
        this.list = list;
    }
}