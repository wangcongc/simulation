package com.ruoyi.system.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlatLessonSonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlatLessonSonExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andLessonIdIsNull() {
            addCriterion("lesson_id is null");
            return (Criteria) this;
        }

        public Criteria andLessonIdIsNotNull() {
            addCriterion("lesson_id is not null");
            return (Criteria) this;
        }

        public Criteria andLessonIdEqualTo(Long value) {
            addCriterion("lesson_id =", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdNotEqualTo(Long value) {
            addCriterion("lesson_id <>", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdGreaterThan(Long value) {
            addCriterion("lesson_id >", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdGreaterThanOrEqualTo(Long value) {
            addCriterion("lesson_id >=", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdLessThan(Long value) {
            addCriterion("lesson_id <", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdLessThanOrEqualTo(Long value) {
            addCriterion("lesson_id <=", value, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdIn(List<Long> values) {
            addCriterion("lesson_id in", values, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdNotIn(List<Long> values) {
            addCriterion("lesson_id not in", values, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdBetween(Long value1, Long value2) {
            addCriterion("lesson_id between", value1, value2, "lessonId");
            return (Criteria) this;
        }

        public Criteria andLessonIdNotBetween(Long value1, Long value2) {
            addCriterion("lesson_id not between", value1, value2, "lessonId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodIsNull() {
            addCriterion("experimental_method is null");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodIsNotNull() {
            addCriterion("experimental_method is not null");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodEqualTo(String value) {
            addCriterion("experimental_method =", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodNotEqualTo(String value) {
            addCriterion("experimental_method <>", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodGreaterThan(String value) {
            addCriterion("experimental_method >", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodGreaterThanOrEqualTo(String value) {
            addCriterion("experimental_method >=", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodLessThan(String value) {
            addCriterion("experimental_method <", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodLessThanOrEqualTo(String value) {
            addCriterion("experimental_method <=", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodLike(String value) {
            addCriterion("experimental_method like", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodNotLike(String value) {
            addCriterion("experimental_method not like", value, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodIn(List<String> values) {
            addCriterion("experimental_method in", values, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodNotIn(List<String> values) {
            addCriterion("experimental_method not in", values, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodBetween(String value1, String value2) {
            addCriterion("experimental_method between", value1, value2, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andExperimentalMethodNotBetween(String value1, String value2) {
            addCriterion("experimental_method not between", value1, value2, "experimentalMethod");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationIsNull() {
            addCriterion("suggested_duration is null");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationIsNotNull() {
            addCriterion("suggested_duration is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationEqualTo(String value) {
            addCriterion("suggested_duration =", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationNotEqualTo(String value) {
            addCriterion("suggested_duration <>", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationGreaterThan(String value) {
            addCriterion("suggested_duration >", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationGreaterThanOrEqualTo(String value) {
            addCriterion("suggested_duration >=", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationLessThan(String value) {
            addCriterion("suggested_duration <", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationLessThanOrEqualTo(String value) {
            addCriterion("suggested_duration <=", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationLike(String value) {
            addCriterion("suggested_duration like", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationNotLike(String value) {
            addCriterion("suggested_duration not like", value, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationIn(List<String> values) {
            addCriterion("suggested_duration in", values, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationNotIn(List<String> values) {
            addCriterion("suggested_duration not in", values, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationBetween(String value1, String value2) {
            addCriterion("suggested_duration between", value1, value2, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSuggestedDurationNotBetween(String value1, String value2) {
            addCriterion("suggested_duration not between", value1, value2, "suggestedDuration");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIsNull() {
            addCriterion("serial_number is null");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIsNotNull() {
            addCriterion("serial_number is not null");
            return (Criteria) this;
        }

        public Criteria andSerialNumberEqualTo(String value) {
            addCriterion("serial_number =", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotEqualTo(String value) {
            addCriterion("serial_number <>", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberGreaterThan(String value) {
            addCriterion("serial_number >", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberGreaterThanOrEqualTo(String value) {
            addCriterion("serial_number >=", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberLessThan(String value) {
            addCriterion("serial_number <", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberLessThanOrEqualTo(String value) {
            addCriterion("serial_number <=", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberLike(String value) {
            addCriterion("serial_number like", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotLike(String value) {
            addCriterion("serial_number not like", value, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberIn(List<String> values) {
            addCriterion("serial_number in", values, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotIn(List<String> values) {
            addCriterion("serial_number not in", values, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberBetween(String value1, String value2) {
            addCriterion("serial_number between", value1, value2, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andSerialNumberNotBetween(String value1, String value2) {
            addCriterion("serial_number not between", value1, value2, "serialNumber");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIsNull() {
            addCriterion("module_folder_name is null");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIsNotNull() {
            addCriterion("module_folder_name is not null");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameEqualTo(String value) {
            addCriterion("module_folder_name =", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotEqualTo(String value) {
            addCriterion("module_folder_name <>", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameGreaterThan(String value) {
            addCriterion("module_folder_name >", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameGreaterThanOrEqualTo(String value) {
            addCriterion("module_folder_name >=", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLessThan(String value) {
            addCriterion("module_folder_name <", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLessThanOrEqualTo(String value) {
            addCriterion("module_folder_name <=", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameLike(String value) {
            addCriterion("module_folder_name like", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotLike(String value) {
            addCriterion("module_folder_name not like", value, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameIn(List<String> values) {
            addCriterion("module_folder_name in", values, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotIn(List<String> values) {
            addCriterion("module_folder_name not in", values, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameBetween(String value1, String value2) {
            addCriterion("module_folder_name between", value1, value2, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andModuleFolderNameNotBetween(String value1, String value2) {
            addCriterion("module_folder_name not between", value1, value2, "moduleFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIsNull() {
            addCriterion("lesson_folder_name is null");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIsNotNull() {
            addCriterion("lesson_folder_name is not null");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameEqualTo(String value) {
            addCriterion("lesson_folder_name =", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotEqualTo(String value) {
            addCriterion("lesson_folder_name <>", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameGreaterThan(String value) {
            addCriterion("lesson_folder_name >", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameGreaterThanOrEqualTo(String value) {
            addCriterion("lesson_folder_name >=", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLessThan(String value) {
            addCriterion("lesson_folder_name <", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLessThanOrEqualTo(String value) {
            addCriterion("lesson_folder_name <=", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameLike(String value) {
            addCriterion("lesson_folder_name like", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotLike(String value) {
            addCriterion("lesson_folder_name not like", value, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameIn(List<String> values) {
            addCriterion("lesson_folder_name in", values, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotIn(List<String> values) {
            addCriterion("lesson_folder_name not in", values, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameBetween(String value1, String value2) {
            addCriterion("lesson_folder_name between", value1, value2, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonFolderNameNotBetween(String value1, String value2) {
            addCriterion("lesson_folder_name not between", value1, value2, "lessonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameIsNull() {
            addCriterion("lesson_son_folder_name is null");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameIsNotNull() {
            addCriterion("lesson_son_folder_name is not null");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameEqualTo(String value) {
            addCriterion("lesson_son_folder_name =", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameNotEqualTo(String value) {
            addCriterion("lesson_son_folder_name <>", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameGreaterThan(String value) {
            addCriterion("lesson_son_folder_name >", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameGreaterThanOrEqualTo(String value) {
            addCriterion("lesson_son_folder_name >=", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameLessThan(String value) {
            addCriterion("lesson_son_folder_name <", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameLessThanOrEqualTo(String value) {
            addCriterion("lesson_son_folder_name <=", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameLike(String value) {
            addCriterion("lesson_son_folder_name like", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameNotLike(String value) {
            addCriterion("lesson_son_folder_name not like", value, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameIn(List<String> values) {
            addCriterion("lesson_son_folder_name in", values, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameNotIn(List<String> values) {
            addCriterion("lesson_son_folder_name not in", values, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameBetween(String value1, String value2) {
            addCriterion("lesson_son_folder_name between", value1, value2, "lessonSonFolderName");
            return (Criteria) this;
        }

        public Criteria andLessonSonFolderNameNotBetween(String value1, String value2) {
            addCriterion("lesson_son_folder_name not between", value1, value2, "lessonSonFolderName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}