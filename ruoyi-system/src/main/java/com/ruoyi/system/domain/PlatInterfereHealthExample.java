package com.ruoyi.system.domain;

import java.util.ArrayList;
import java.util.List;

public class PlatInterfereHealthExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlatInterfereHealthExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAssistantTextIsNull() {
            addCriterion("assistant_text is null");
            return (Criteria) this;
        }

        public Criteria andAssistantTextIsNotNull() {
            addCriterion("assistant_text is not null");
            return (Criteria) this;
        }

        public Criteria andAssistantTextEqualTo(String value) {
            addCriterion("assistant_text =", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextNotEqualTo(String value) {
            addCriterion("assistant_text <>", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextGreaterThan(String value) {
            addCriterion("assistant_text >", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextGreaterThanOrEqualTo(String value) {
            addCriterion("assistant_text >=", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextLessThan(String value) {
            addCriterion("assistant_text <", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextLessThanOrEqualTo(String value) {
            addCriterion("assistant_text <=", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextLike(String value) {
            addCriterion("assistant_text like", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextNotLike(String value) {
            addCriterion("assistant_text not like", value, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextIn(List<String> values) {
            addCriterion("assistant_text in", values, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextNotIn(List<String> values) {
            addCriterion("assistant_text not in", values, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextBetween(String value1, String value2) {
            addCriterion("assistant_text between", value1, value2, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantTextNotBetween(String value1, String value2) {
            addCriterion("assistant_text not between", value1, value2, "assistantText");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlIsNull() {
            addCriterion("assistant_audio_url is null");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlIsNotNull() {
            addCriterion("assistant_audio_url is not null");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlEqualTo(String value) {
            addCriterion("assistant_audio_url =", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlNotEqualTo(String value) {
            addCriterion("assistant_audio_url <>", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlGreaterThan(String value) {
            addCriterion("assistant_audio_url >", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlGreaterThanOrEqualTo(String value) {
            addCriterion("assistant_audio_url >=", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlLessThan(String value) {
            addCriterion("assistant_audio_url <", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlLessThanOrEqualTo(String value) {
            addCriterion("assistant_audio_url <=", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlLike(String value) {
            addCriterion("assistant_audio_url like", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlNotLike(String value) {
            addCriterion("assistant_audio_url not like", value, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlIn(List<String> values) {
            addCriterion("assistant_audio_url in", values, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlNotIn(List<String> values) {
            addCriterion("assistant_audio_url not in", values, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlBetween(String value1, String value2) {
            addCriterion("assistant_audio_url between", value1, value2, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantAudioUrlNotBetween(String value1, String value2) {
            addCriterion("assistant_audio_url not between", value1, value2, "assistantAudioUrl");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoIsNull() {
            addCriterion("patient_information_info is null");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoIsNotNull() {
            addCriterion("patient_information_info is not null");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoEqualTo(String value) {
            addCriterion("patient_information_info =", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoNotEqualTo(String value) {
            addCriterion("patient_information_info <>", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoGreaterThan(String value) {
            addCriterion("patient_information_info >", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoGreaterThanOrEqualTo(String value) {
            addCriterion("patient_information_info >=", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoLessThan(String value) {
            addCriterion("patient_information_info <", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoLessThanOrEqualTo(String value) {
            addCriterion("patient_information_info <=", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoLike(String value) {
            addCriterion("patient_information_info like", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoNotLike(String value) {
            addCriterion("patient_information_info not like", value, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoIn(List<String> values) {
            addCriterion("patient_information_info in", values, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoNotIn(List<String> values) {
            addCriterion("patient_information_info not in", values, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoBetween(String value1, String value2) {
            addCriterion("patient_information_info between", value1, value2, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andPatientInformationInfoNotBetween(String value1, String value2) {
            addCriterion("patient_information_info not between", value1, value2, "patientInformationInfo");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextIsNull() {
            addCriterion("assistant_health_text is null");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextIsNotNull() {
            addCriterion("assistant_health_text is not null");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextEqualTo(String value) {
            addCriterion("assistant_health_text =", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextNotEqualTo(String value) {
            addCriterion("assistant_health_text <>", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextGreaterThan(String value) {
            addCriterion("assistant_health_text >", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextGreaterThanOrEqualTo(String value) {
            addCriterion("assistant_health_text >=", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextLessThan(String value) {
            addCriterion("assistant_health_text <", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextLessThanOrEqualTo(String value) {
            addCriterion("assistant_health_text <=", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextLike(String value) {
            addCriterion("assistant_health_text like", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextNotLike(String value) {
            addCriterion("assistant_health_text not like", value, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextIn(List<String> values) {
            addCriterion("assistant_health_text in", values, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextNotIn(List<String> values) {
            addCriterion("assistant_health_text not in", values, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextBetween(String value1, String value2) {
            addCriterion("assistant_health_text between", value1, value2, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthTextNotBetween(String value1, String value2) {
            addCriterion("assistant_health_text not between", value1, value2, "assistantHealthText");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlIsNull() {
            addCriterion("assistant_health_audio_url is null");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlIsNotNull() {
            addCriterion("assistant_health_audio_url is not null");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlEqualTo(String value) {
            addCriterion("assistant_health_audio_url =", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlNotEqualTo(String value) {
            addCriterion("assistant_health_audio_url <>", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlGreaterThan(String value) {
            addCriterion("assistant_health_audio_url >", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlGreaterThanOrEqualTo(String value) {
            addCriterion("assistant_health_audio_url >=", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlLessThan(String value) {
            addCriterion("assistant_health_audio_url <", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlLessThanOrEqualTo(String value) {
            addCriterion("assistant_health_audio_url <=", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlLike(String value) {
            addCriterion("assistant_health_audio_url like", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlNotLike(String value) {
            addCriterion("assistant_health_audio_url not like", value, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlIn(List<String> values) {
            addCriterion("assistant_health_audio_url in", values, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlNotIn(List<String> values) {
            addCriterion("assistant_health_audio_url not in", values, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlBetween(String value1, String value2) {
            addCriterion("assistant_health_audio_url between", value1, value2, "assistantHealthAudioUrl");
            return (Criteria) this;
        }

        public Criteria andAssistantHealthAudioUrlNotBetween(String value1, String value2) {
            addCriterion("assistant_health_audio_url not between", value1, value2, "assistantHealthAudioUrl");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}