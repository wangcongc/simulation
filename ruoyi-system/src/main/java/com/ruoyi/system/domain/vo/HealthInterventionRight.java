package com.ruoyi.system.domain.vo;

import java.util.List;
import java.util.Map;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-21 17:42
 **/
public class HealthInterventionRight {
    private String name;

    private List<HealthInformationKV> datas;

    public HealthInterventionRight() {
    }


    public HealthInterventionRight(String name, List<HealthInformationKV> datas) {
        this.name = name;
        this.datas = datas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HealthInformationKV> getDatas() {
        return datas;
    }

    public void setDatas(List<HealthInformationKV> datas) {
        this.datas = datas;
    }
}
