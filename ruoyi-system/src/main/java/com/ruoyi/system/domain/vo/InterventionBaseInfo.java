package com.ruoyi.system.domain.vo;

import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-26 11:26
 **/
public class InterventionBaseInfo {
    private String img;
    private String name;
    private String sex;
    private String age;
    private String number;
    private String info;
    private String medicalHistory;
    private String disease;
    private String weight;
    private String psychological;
    private String sleep;
    private String lifeStyle;
    private String physical;
    private String generalRisk;
    private List<String> lowRisk;
    private List<String> intermediateRisk;
    private List<String> highRisk;

    public InterventionBaseInfo() {
    }

    public InterventionBaseInfo(String img, String name, String sex, String age, String number, String info, String medicalHistory, String disease, String weight, String psychological, String sleep, String lifeStyle, String physical, String generalRisk, List<String> lowRisk, List<String> intermediateRisk, List<String> highRisk) {
        this.img = img;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.number = number;
        this.info = info;
        this.medicalHistory = medicalHistory;
        this.disease = disease;
        this.weight = weight;
        this.psychological = psychological;
        this.sleep = sleep;
        this.lifeStyle = lifeStyle;
        this.physical = physical;
        this.generalRisk = generalRisk;
        this.lowRisk = lowRisk;
        this.intermediateRisk = intermediateRisk;
        this.highRisk = highRisk;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPsychological() {
        return psychological;
    }

    public void setPsychological(String psychological) {
        this.psychological = psychological;
    }

    public String getSleep() {
        return sleep;
    }

    public void setSleep(String sleep) {
        this.sleep = sleep;
    }

    public String getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(String lifeStyle) {
        this.lifeStyle = lifeStyle;
    }

    public String getPhysical() {
        return physical;
    }

    public void setPhysical(String physical) {
        this.physical = physical;
    }

    public String getGeneralRisk() {
        return generalRisk;
    }

    public void setGeneralRisk(String generalRisk) {
        this.generalRisk = generalRisk;
    }

    public List<String> getLowRisk() {
        return lowRisk;
    }

    public void setLowRisk(List<String> lowRisk) {
        this.lowRisk = lowRisk;
    }

    public List<String> getIntermediateRisk() {
        return intermediateRisk;
    }

    public void setIntermediateRisk(List<String> intermediateRisk) {
        this.intermediateRisk = intermediateRisk;
    }

    public List<String> getHighRisk() {
        return highRisk;
    }

    public void setHighRisk(List<String> highRisk) {
        this.highRisk = highRisk;
    }
}
