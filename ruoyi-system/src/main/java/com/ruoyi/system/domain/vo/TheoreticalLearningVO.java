package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.PlatLesson;

import java.util.List;

/**
 * @program: simulation
 * @description: 理论学习
 * @author: 马瑞龙
 * @create: 2024-11-20 17:14
 **/
public class TheoreticalLearningVO {
    private PlatLesson platLesson;
    private BaseInfo baseInfo;

    private List<FileList> fileList;

    public TheoreticalLearningVO() {
    }

    public TheoreticalLearningVO(BaseInfo baseInfo, List<FileList> fileList) {
        this.baseInfo = baseInfo;
        this.fileList = fileList;
    }

    public TheoreticalLearningVO(PlatLesson platLesson, BaseInfo baseInfo, List<FileList> fileList) {
        this.platLesson = platLesson;
        this.baseInfo = baseInfo;
        this.fileList = fileList;
    }

    public BaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(BaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public List<FileList> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileList> fileList) {
        this.fileList = fileList;
    }

    public PlatLesson getPlatLesson() {
        return platLesson;
    }

    public void setPlatLesson(PlatLesson platLesson) {
        this.platLesson = platLesson;
    }
}
