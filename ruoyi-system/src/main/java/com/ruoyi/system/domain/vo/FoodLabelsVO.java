package com.ruoyi.system.domain.vo;

import java.util.List;

/**
 * @program: simulation
 * @description:
 * @author: 马瑞龙
 * @create: 2024-11-28 16:28
 **/
public class FoodLabelsVO {
    private Integer id;
    private String questionDesc;
    private String questionTitle;

    private List<FoodLabelsOptions> foodLabelsOptionsList;

    private String answer;
    private String answerDesc;


    public FoodLabelsVO() {
    }

    public FoodLabelsVO(Integer id, String questionDesc, String questionTitle, List<FoodLabelsOptions> foodLabelsOptionsList, String answer, String answerDesc) {
        this.id = id;
        this.questionDesc = questionDesc;
        this.questionTitle = questionTitle;
        this.foodLabelsOptionsList = foodLabelsOptionsList;
        this.answer = answer;
        this.answerDesc = answerDesc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public List<FoodLabelsOptions> getFoodLabelsOptionsList() {
        return foodLabelsOptionsList;
    }

    public void setFoodLabelsOptionsList(List<FoodLabelsOptions> foodLabelsOptionsList) {
        this.foodLabelsOptionsList = foodLabelsOptionsList;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerDesc() {
        return answerDesc;
    }

    public void setAnswerDesc(String answerDesc) {
        this.answerDesc = answerDesc;
    }
}
