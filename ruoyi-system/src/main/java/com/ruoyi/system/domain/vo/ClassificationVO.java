package com.ruoyi.system.domain.vo;

import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.system.constant.FoodConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: simulation
 * @description: 食物分类相关信息
 * @author: 马瑞龙
 * @create: 2024-11-19 23:28
 **/
public class ClassificationVO {
    private String id;

    private String type;

    private String label;

    private List<FoodVO> dataList;

    public ClassificationVO() {
    }

    public ClassificationVO(String id, String type, String label, List<FoodVO> dataList) {
        this.id = id;
        this.type = type;
        this.label = label;
        this.dataList = dataList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<FoodVO> getDataList() {
        return dataList;
    }

    public void setDataList(List<FoodVO> dataList) {
        this.dataList = dataList;
    }
}
