package com.ruoyi.system.domain;

import java.util.Date;

public class PlatLessonSon {
    private Long id;

    private String name;

    private Long lessonId;

    private Date createTime;

    private Date updateTime;

    private String experimentalMethod;

    private String suggestedDuration;

    private String serialNumber;

    private String moduleFolderName;

    private String lessonFolderName;

    private String lessonSonFolderName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getExperimentalMethod() {
        return experimentalMethod;
    }

    public void setExperimentalMethod(String experimentalMethod) {
        this.experimentalMethod = experimentalMethod == null ? null : experimentalMethod.trim();
    }

    public String getSuggestedDuration() {
        return suggestedDuration;
    }

    public void setSuggestedDuration(String suggestedDuration) {
        this.suggestedDuration = suggestedDuration == null ? null : suggestedDuration.trim();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public String getModuleFolderName() {
        return moduleFolderName;
    }

    public void setModuleFolderName(String moduleFolderName) {
        this.moduleFolderName = moduleFolderName == null ? null : moduleFolderName.trim();
    }

    public String getLessonFolderName() {
        return lessonFolderName;
    }

    public void setLessonFolderName(String lessonFolderName) {
        this.lessonFolderName = lessonFolderName == null ? null : lessonFolderName.trim();
    }

    public String getLessonSonFolderName() {
        return lessonSonFolderName;
    }

    public void setLessonSonFolderName(String lessonSonFolderName) {
        this.lessonSonFolderName = lessonSonFolderName == null ? null : lessonSonFolderName.trim();
    }
}