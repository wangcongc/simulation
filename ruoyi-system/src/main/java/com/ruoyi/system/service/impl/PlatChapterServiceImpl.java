package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.PlatLessonChapter;
import com.ruoyi.system.domain.PlatLessonChapterExample;
import com.ruoyi.system.domain.PlatLessonChapterType;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IPlatChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 章节管理
 */
@Service
public class PlatChapterServiceImpl extends PlatCommonServiceImpl implements IPlatChapterService {

    @Autowired
    private PlatMajorMapper platMajorMapper;
    @Autowired
    private PlatCollegeMapper platCollegeMapper;
    @Autowired
    private PlatLessonChapterMapper platLessonChapterMapper;
    @Autowired
    private PlatUserMapper platUserMapper;
    @Autowired
    private PlatLessonChapterTypeMapper platLessonChapterTypeMapper;

    @Override
    public void addChapter(PlatLessonChapter platLessonChapter) {
        if (platLessonChapter.getId() != null) {
            platLessonChapter.setUpdateTime(new Date());
            platLessonChapterMapper.updateByPrimaryKeySelective(platLessonChapter);
            return;
        }

        PlatLessonChapterType platLessonChapterType = platLessonChapterTypeMapper.selectByPrimaryKey(platLessonChapter.getChapterTypeId());
        if(platLessonChapterType!=null){
            platLessonChapter.setLessonId(platLessonChapterType.getLessonId());
        }

        platLessonChapter.setCreateTime(new Date());
        platLessonChapterMapper.insert(platLessonChapter);
    }

    @Override
    public List<PlatLessonChapter> chapterList(PlatLessonChapter platLessonChapter) {
        PlatLessonChapterExample platLessonExample = new PlatLessonChapterExample();
        platLessonExample.setOrderByClause(orderByClause);
        PlatLessonChapterExample.Criteria criteria = platLessonExample.createCriteria();

        if (platLessonChapter.getLessonId() != null) {
            criteria.andLessonIdEqualTo(platLessonChapter.getLessonId());
        }
        if (platLessonChapter.getChapterTypeId() != null) {
            criteria.andChapterTypeIdEqualTo(platLessonChapter.getChapterTypeId());
        }
        List<PlatLessonChapter> list = platLessonChapterMapper.selectByExample(platLessonExample);
        return list;
    }

    @Override
    public void delChapter(Long id) {
        platLessonChapterMapper.deleteByPrimaryKey(id);
    }
}
