package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatMajor;
import com.ruoyi.system.domain.vo.PlatMajorVO;

import java.util.List;

/**
 * 专业管理
 */
public interface IPlatMajorService {
    //添加专业
    void addMajor(PlatMajor platMajor);

    //专业列表
    List<PlatMajor> majorList(PlatMajor platMajor);

    //删除专业
    void delMajor(Long id);

    //课程适用专业
    List<PlatMajorVO> lecturerMajorList(Long lessonId,Long collegeId);
}
