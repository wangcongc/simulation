package com.ruoyi.system.service;

import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.LessonFullCalendarVO;
import com.ruoyi.system.domain.vo.LessonHomeVO;
import com.ruoyi.system.domain.vo.LessonScheduleVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 课程管理
 */
public interface IPlatLessonService {
    //添加班级
    void addLesson(PlatLesson platLesson);

    //班级列表
    List<PlatLesson> lessonList(PlatLesson platLesson);

    //删除班级
    void delLesson(Long id);

    //首页课程数量(教师)
    LessonHomeVO lessonHome(HttpServletRequest request);

    //我的课程(教师)
    List<PlatLesson> myLessonList(PlatLesson platLesson, HttpServletRequest request,PlatUser platUser);

    //我的课程(学生)
    List<PlatLessonStudent> myStudentLessonList(PlatLesson platLesson, PlatUser loginUser);
    List<PlatLesson> myStudentLessonListData(List<PlatLessonStudent> lessonStudentList);

    //我的排课列表(教师)
    List<PlatLessonSchedule> myLessonScheduleList(PlatUser platUser, PlatUser loginUser);
    PlatTableDataInfo myLessonScheduleListData(PlatTableDataInfo platTableDataInfo);

    //添加排课记录
    void addLessonSchedule(PlatLessonSchedule platLessonSchedule, HttpServletRequest request);

    //删除排课
    void delLessonSchedule(Long id);

    //我的排课行程(首页日程)
    List<LessonFullCalendarVO> myLessonFullCalendarList(HttpServletRequest request);

    //结束课程（学生）
    void endLesson(Long id);

    //课程类别列表
    List<PlatLessonType> lessonTypeList(PlatLessonType platLessonType);

    //设置分数
    void lessonInScore(PlatLessonStudent platLessonStudent,HttpServletRequest request);
}
