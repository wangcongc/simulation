package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatLessonChapterType;

import java.util.List;

/**
 * 章节类型
 */
public interface IPlatChapterTypeService {
    //添加章节类型
    void addChapterType(PlatLessonChapterType platLessonChapterType);

    //章节类型列表
    List<PlatLessonChapterType> chapterTypeList(PlatLessonChapterType platLessonChapterType);

    //删除章节类型
    void delChapterType(Long id);
}
