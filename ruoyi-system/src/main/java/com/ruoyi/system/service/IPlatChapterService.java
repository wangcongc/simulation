package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatLessonChapter;

import java.util.List;

/**
 * 章节
 */
public interface IPlatChapterService {
    //添加章节
    void addChapter(PlatLessonChapter platLessonChapter);

    //章节列表
    List<PlatLessonChapter> chapterList(PlatLessonChapter platLessonChapter);

    //删除章节
    void delChapter(Long id);
}
