package com.ruoyi.system.service.impl;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.PlatClassMapper;
import com.ruoyi.system.mapper.PlatCollegeMapper;
import com.ruoyi.system.mapper.PlatMajorMapper;
import com.ruoyi.system.mapper.PlatUserMapper;
import com.ruoyi.system.service.IPlatClassService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 班级管理
 */
@Service
public class PlatClassServiceImpl extends PlatCommonServiceImpl implements IPlatClassService {

    @Autowired
    private PlatMajorMapper platMajorMapper;
    @Autowired
    private PlatCollegeMapper platCollegeMapper;
    @Autowired
    private PlatClassMapper platClassMapper;
    @Autowired
    private PlatUserMapper platUserMapper;

    @Override
    public void addClass(PlatClass platClass) {
        //校验名称是否重复
        PlatClassExample example = new PlatClassExample();
        PlatClassExample.Criteria criteria = example.createCriteria();
        criteria.andNameEqualTo(platClass.getName());
        List<PlatClass> list = platClassMapper.selectByExample(example);
        if (platClass.getId() != null && list.size() > 0 && !String.valueOf(list.get(0).getId()).equals(String.valueOf(platClass.getId()))) {
            throw new BusinessException("班级名称已存在!");
        } else if (platClass.getId() == null && list.size() > 0) {
            throw new BusinessException("班级名称已存在!");
        }

        if (!StringUtils.isEmpty(platClass.getTeacher())) {
            String[] arrays = platClass.getTeacher().split(DOUHAO);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(IDENTIFICATION);
            for (String teacherId : arrays) {
                stringBuffer.append(teacherId);
                stringBuffer.append(IDENTIFICATION);
            }
            platClass.setTeacher(stringBuffer.toString());
        }


        if (platClass.getId() != null) {
            platClass.setUpdateTime(new Date());
            platClassMapper.updateByPrimaryKeySelective(platClass);
            return;
        }
        platClass.setCreateTime(new Date());
        platClassMapper.insert(platClass);
    }

    @Override
    public List<PlatClass> classList(PlatClass platClass) {
        PlatClassExample platCollegeExample = new PlatClassExample();
        platCollegeExample.setOrderByClause(orderByClause);
        PlatClassExample.Criteria criteria = null;
        if (!StringUtils.isEmpty(platClass.getName())) {
            if (criteria == null) {
                criteria = platCollegeExample.createCriteria();
            }
            criteria.andNameLike(LIKE + platClass.getName() + LIKE);
        }
        if (!StringUtils.isEmpty(platClass.getTeacher())) {
            if (criteria == null) {
                criteria = platCollegeExample.createCriteria();
            }
            criteria.andTeacherLike(LIKE + AITT + platClass.getTeacher() + AITT + LIKE);
        }
        List<PlatClass> list = platClassMapper.selectByExample(platCollegeExample);

        for (PlatClass platClassPO : list) {
            PlatCollege platCollege = platCollegeMapper.selectByPrimaryKey(platClassPO.getCollegeId());
            if (platCollege != null) {
                platClassPO.setCollegeName(platCollege.getName());
            }
            PlatMajor platMajor = platMajorMapper.selectByPrimaryKey(platClassPO.getMajorId());
            if (platMajor != null) {
                platClassPO.setMajorName(platMajor.getName());
            }

            String teacher = "";
            if (!StringUtils.isEmpty(platClassPO.getTeacher())) {
                String[] arrays = platClassPO.getTeacher().split(IDENTIFICATION);
                for (String teacherId : arrays) {
                    try {
                        if(StringUtils.isEmpty(teacherId)){
                            continue;
                        }
                        PlatUser platUser = platUserMapper.selectByPrimaryKey(Long.parseLong(teacherId));
                        if (platUser != null) {
                            if (StringUtils.isEmpty(teacher)) {
                                teacher = platUser.getNickName();
                            } else {
                                teacher += DOUHAO + platUser.getNickName();
                            }
                        }
                    } catch (Exception e) {
                        error("查询授课教师失败", e);
                    }
                }

            }
            platClassPO.setTeacher(teacher);

            //学生人数
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_STUDENT);
            exampleCriteria.andClassIdEqualTo(platClassPO.getId());
            platClassPO.setUserNum(platUserMapper.selectByExample(example).size());
        }
        return list;
    }

    @Override
    public void delClass(Long id) {
        platClassMapper.deleteByPrimaryKey(id);
    }
}
