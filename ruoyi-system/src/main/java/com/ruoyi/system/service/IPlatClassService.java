package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatClass;
import com.ruoyi.system.domain.PlatCollege;

import java.util.List;

/**
 * 班级管理
 */
public interface IPlatClassService {
    //添加班级
    void addClass(PlatClass platClass);

    //班级列表
    List<PlatClass> classList(PlatClass platClass);

    //删除班级
    void delClass(Long id);
}
