package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatUser;
import com.ruoyi.system.domain.vo.PlatMenuVO;
import com.ruoyi.system.domain.vo.PlatTecturer;
import jxl.Cell;
import jxl.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface IPlatUserService {
    //登录
    String login(String username, String password);

    //获取菜单
    List<PlatMenuVO> getMenu(PlatUser loginUser);

    //修改个人信息
    void updateUserInfo(HttpServletRequest request, PlatUser platUser);

    //获取个人信息
    PlatUser getLoginUser(HttpServletRequest request);

    //修改密码
    void updatePassword(HttpServletRequest request, PlatUser platUser);

    //教职工列表
    List<PlatUser> teacherList(HttpServletRequest request, PlatUser platUser);

    //授课教师列表
    List<PlatTecturer> lecturerList(Long classId);

    //授课教师列表
    List<PlatTecturer> lecturerLessonList(Long lessonId);

    //添加教师
    void addTeacher(HttpServletRequest request, PlatUser platUser);

    //学生列表
    List<PlatUser> studentList(HttpServletRequest request, PlatUser platUser);

    //删除学生
    void delUser(Long id);

    //上传学生excel
    String uploadFileStudent(Workbook workbook, Cell cell, HttpServletRequest request);
}
