package com.ruoyi.system.service.impl;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.RiskUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.constant.*;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.dto.EhmsAnswerDto;
import com.ruoyi.system.domain.dto.EhmsQuestionDto;
import com.ruoyi.system.domain.vo.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IEhmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.*;

import static com.ruoyi.system.constant.FoodLabelsConstant.*;
import static com.ruoyi.system.constant.LessonConstant.*;
import static com.ruoyi.system.constant.StaticPath.*;

/**
 * 进入课程
 */
@Service
public class EhmsServiceImpl extends PlatCommonServiceImpl implements IEhmsService {

    @Autowired
    private PlatQuestionnaireMapper platQuestionnaireMapper;
    @Autowired
    private PlatLessonScaleQuestionMapper platLessonScaleQuestionMapper;
    @Autowired
    private PlatLessonSonMapper platLessonSonMapper;
    @Autowired
    private PlatLessonScaleMapper platLessonScaleMapper;
    @Autowired
    private PlatLessonScaleSonMapper platLessonScaleSonMapper;
    @Autowired
    private PlatLessonMapper platLessonMapper;
    @Autowired
    private PlatInterfereHealthMapper platInterfereHealthMapper;
    @Autowired
    private PlatQuesSportStudyMapper platQuesSportStudyMapper;

    @Override
    public PlatLesson lessonInfo(Long id) {
        return platLessonMapper.selectByPrimaryKey(id);
    }

    @Override
    public PlatLessonSon lessonSonInfo(Long id) {
        return platLessonSonMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<PlatLessonSon> lessonSonList(Long id) {
        PlatLessonSonExample platLessonExample = new PlatLessonSonExample();
        platLessonExample.setOrderByClause(orderByClause);
        PlatLessonSonExample.Criteria criteria = platLessonExample.createCriteria();
        criteria.andLessonIdEqualTo(id);
        List<PlatLessonSon> list = platLessonSonMapper.selectByExample(platLessonExample);
        return list;
    }

    @Override
    public List<EhmsScaleVO> lessonScaleList(Long lessonSonId) {
        List<EhmsScaleVO> result = new ArrayList<>();

        PlatLessonScaleExample platLessonExample = new PlatLessonScaleExample();
        platLessonExample.setOrderByClause(orderByNumber);
        PlatLessonScaleExample.Criteria criteria = platLessonExample.createCriteria();
        criteria.andLessonSonIdEqualTo(lessonSonId);
        List<PlatLessonScale> list = platLessonScaleMapper.selectByExample(platLessonExample);
        for (PlatLessonScale platLessonScale : list) {
            EhmsScaleVO ehmsScaleVO = new EhmsScaleVO();
            ehmsScaleVO.setId(platLessonScale.getId());
            ehmsScaleVO.setName(platLessonScale.getName());

            PlatLessonScaleSonExample platLessonScaleSonExample = new PlatLessonScaleSonExample();
            platLessonScaleSonExample.setOrderByClause(orderByNumber);
            PlatLessonScaleSonExample.Criteria platLessonScaleSonCriteria = platLessonScaleSonExample.createCriteria();
            platLessonScaleSonCriteria.andLessonScaleIdEqualTo(platLessonScale.getId());
            List<PlatLessonScaleSon> platLessonScaleSonList = platLessonScaleSonMapper.selectByExample(platLessonScaleSonExample);

            List<EhmsScaleVO> ehmsScaleSonResult = new ArrayList<>();
            for (PlatLessonScaleSon platLessonScaleSon : platLessonScaleSonList) {
                EhmsScaleVO ehmsScaleSonVO = new EhmsScaleVO();
                BeanUtils.copyProperties(platLessonScaleSon, ehmsScaleSonVO);
                ehmsScaleSonResult.add(ehmsScaleSonVO);
            }
            ehmsScaleVO.setList(ehmsScaleSonResult);
            result.add(ehmsScaleVO);
        }
        return result;
    }

    @Override
    public List<PlatLessonScaleQuestion> lessonQuestionList(Long scaleSonId) {
        PlatLessonScaleQuestionExample platLessonExample = new PlatLessonScaleQuestionExample();
        platLessonExample.setOrderByClause(orderByNumber);
        PlatLessonScaleQuestionExample.Criteria criteria = platLessonExample.createCriteria();
        criteria.andLessonScaleSonIdEqualTo(scaleSonId);
        criteria.andParentIdIsNull();
        List<PlatLessonScaleQuestion> list = platLessonScaleQuestionMapper.selectByExample(platLessonExample);
        for (PlatLessonScaleQuestion platLessonScaleQuestion : list) {
            PlatLessonScaleQuestionExample example = new PlatLessonScaleQuestionExample();
            example.setOrderByClause(orderByNumber);
            PlatLessonScaleQuestionExample.Criteria sonCriteria = example.createCriteria();
            sonCriteria.andParentIdEqualTo(platLessonScaleQuestion.getId());
            List<PlatLessonScaleQuestion> sonList = platLessonScaleQuestionMapper.selectByExample(example);
            if (sonList.size() > 0) {
                platLessonScaleQuestion.setItem(sonList);
            }
        }
        return list;
    }

    @Override
    public EhmsQuestionVO lessonQuestionList(EhmsQuestionDto ehmsQuestionDto) {
        if (StringUtils.isEmpty(ehmsQuestionDto.getScaleSonIds())) {
            throw new BusinessException("问卷id不能为空!");
        }

        EhmsQuestionVO ehmsQuestionVO = new EhmsQuestionVO();
        List<List<PlatLessonScaleQuestion>> resultList = new ArrayList<>();

        String ScaleSonId[] = ehmsQuestionDto.getScaleSonIds().split("，");
        for (String scaleSonId : ScaleSonId) {
            List<PlatLessonScaleQuestion> lessonScaleQuestionList = lessonQuestionList(Long.parseLong(scaleSonId));
            resultList.add(lessonScaleQuestionList);
        }
        ehmsQuestionVO.setList(resultList);
        return ehmsQuestionVO;
    }

    //弃用
    @Override
    public EhmsAnswerVO subQuestion(EhmsAnswerDto ehmsAnswerDto) {
        EhmsAnswerVO ehmsAnswerVO = new EhmsAnswerVO();
        return ehmsAnswerVO;
    }

    private Set<String> getQuestionnaire(String type) {
        PlatQuestionnaireExample example = new PlatQuestionnaireExample();
        PlatQuestionnaireExample.Criteria criteria = example.createCriteria();
        criteria.andTypeEqualTo(type);
        List<PlatQuestionnaire> list = platQuestionnaireMapper.selectByExample(example);
        Set<String> set = new HashSet<>();
        for (PlatQuestionnaire platQuestionnaire : list) {
            set.add(platQuestionnaire.getTitle());
        }
        return set;
    }

    private int getQuestionnaireScore(String myTitle, String answer) {
        PlatQuestionnaireExample example = new PlatQuestionnaireExample();
        PlatQuestionnaireExample.Criteria criteria = example.createCriteria();
        criteria.andTitleEqualTo(myTitle);
        criteria.andNumberEqualTo(answer);
        List<PlatQuestionnaire> list = platQuestionnaireMapper.selectByExample(example);
        int score = 0;
        for (PlatQuestionnaire platQuestionnaire : list) {
            score += platQuestionnaire.getScore();
        }
        return score;
    }

    @Override
    public EhmsAnswerVO subQuestions(Map map) {
        //返回
        EhmsAnswerVO ehmsAnswerVO = new EhmsAnswerVO();
        List<EhmsAnswerListVO> resultList = new ArrayList<>();

        //前端传过来的
        LinkedHashMap<String, Object> myMap = (LinkedHashMap<String, Object>) map.get("answer");

        //单选
        for (String myTitle : getQuestionnaire(RiskUtil.DANXUAN)) {
            if (myMap.get(myTitle) != null) {
                EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
                ehmsAnswerListVO.setName(myTitle);

                //算分
                int score = 0;
                //答案
                String[] answers = myMap.get(myTitle).toString().split(DOUHAO);
                for (String answer : answers) {
                    score += getQuestionnaireScore(myTitle, answer);
                }
                PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);
                if (score >= platLessonScaleSon.getRiskScore()) {
                    String result = platLessonScaleSon.getResult().replace(XINGHAO, myTitle);
                    ehmsAnswerListVO.setRiskResult(result);
                } else {
                    ehmsAnswerListVO.setRiskResult("无");
                }
                ehmsAnswerListVO.setRiskPlan(platLessonScaleSon.getResultFormula());
                resultList.add(ehmsAnswerListVO);
            }
        }

        if (myMap.get("肥胖及肥胖风险评估") != null) {
            String myTitle = "肥胖及肥胖风险评估";
            EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
            ehmsAnswerListVO.setName(myTitle);
            Map<String,Object> hashMap = (Map<String, Object>) myMap.get(myTitle);

            String weight = (String) hashMap.get("weight");
            String height = (String) hashMap.get("height");
            Float bmi = RiskUtil.getBmi(weight, height);
            if (bmi == null) {
                ehmsAnswerListVO.setRiskResult("无");
            } else {
                ehmsAnswerListVO.setRiskResult(RiskUtil.BMIAnakyse(bmi));
            }

            PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);
            ehmsAnswerListVO.setRiskPlan(platLessonScaleSon.getResultFormula());

            resultList.add(ehmsAnswerListVO);
        }

        //双个勾选
        for (String myTitle : getQuestionnaire(RiskUtil.SHUANGGOU)) {
            if (myMap.get(myTitle) != null) {
                EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
                ehmsAnswerListVO.setName(myTitle);

                //算分
                int score = 0;

                LinkedHashMap<String, Integer> shuangMap = (LinkedHashMap<String, Integer>) myMap.get(myTitle);
                StringBuffer stringBuffer = new StringBuffer();
                for (String key : shuangMap.keySet()) {
                    if (stringBuffer.length() != 0) {
                        stringBuffer.append(DOUHAO);
                    }
                    stringBuffer.append(key + "-" + shuangMap.get(key));
                }

                //答案
                String[] answers = stringBuffer.toString().split(DOUHAO);
                for (String answer : answers) {
                    score += getQuestionnaireScore(myTitle, answer);
                }

                switch (myTitle){
                    case ZYHZZLNL_PGB:
                    {
                        if (score >= 0 && score <= 3){
                            ehmsAnswerListVO.setRiskResult("可自理");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分极高，表明其自理能力完全正常，能够独立完成所有日常活动；建议措施：1、常规监测：虽然患者自理能力正常，但仍需进行常规监测，以确保其健康状况稳定；2、健康指导：向患者提供健康生活方式和疾病预防方面的指导，帮助其保持健康状态；3、鼓励活动：鼓励患者积极参与各种社交和体育活动，以提高其生活质量和社会参与度。");

                        }else if (score >= 4 && score <= 8) {
                            ehmsAnswerListVO.setRiskResult("轻度依赖");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分较高，表明其自理能力相对较好，能够独立完成大部分日常活动，但仍需要部分帮助；建议措施：1、鼓励自主：鼓励患者自主完成日常活动，如进食、穿衣等，以提高其自理能力和自信心；2、提供指导：在患者需要帮助时，提供必要的指导和支持，确保其能够正确、安全地完成活动；3、康复锻炼：继续开展康复锻炼，进一步提高患者的自理能力和生活质量；4、家庭支持：鼓励患者家属积极参与患者的康复过程，提供必要的家庭支持和照顾。");
                        }else if (score >= 9 && score <= 18) {
                            ehmsAnswerListVO.setRiskResult("中度依赖");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分处于中等水平，表明其具有一定的自理能力，但仍需要他人的帮助才能完成部分日常活动；建议措施：1、辅助护理：在患者需要帮助时，提供必要的辅助和支持，如协助进食、穿衣等；2、康复指导：根据患者的具体情况，制定个性化的康复计划，并指导患者进行康复锻炼；3、安全教育：向患者及其家属普及安全知识，避免患者因自理能力不足而发生意外；4、定期评估：定期对患者进行自理能力评估，以监测其自理能力的变化，并调整护理措施");
                        }else {
                            ehmsAnswerListVO.setRiskResult("重度依赖（不能自理）");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分极低，表明其自理能力严重受限，几乎无法独立完成日常活动;建议措施：1、全面护理：提供24小时全天候护理，确保患者的基本生活需求得到满足；2、定期翻身：定时为患者翻身，避免压疮等并发症的发生；3、营养支持：根据患者的营养需求，提供合适的饮食和营养补充；4、康复锻炼：在医护人员的指导下，逐步开展康复锻炼，提高患者的自理能力；5、心理支持：关注患者的心理状态，提供必要的心理支持和安慰。");
                        }
                        resultList.add(ehmsAnswerListVO);
                        break;
                    }
                    case MORSEDDWXYS_PGB:
                    {
                        if (score >= 0 && score <= 24){
                            ehmsAnswerListVO.setRiskResult("无危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分较低，表明其跌倒风险相对较低;建议措施：1、常规监测：虽然患者跌倒风险较低，但仍需进行常规监测，以确保其安全；2、环境保持：保持患者所处环境整洁、干燥，避免潜在的安全隐患；3、健康教育：向患者及其家属提供基本的跌倒预防知识，鼓励其保持健康的生活方式。");
                        }else if (score >= 25 && score <= 45) {
                            ehmsAnswerListVO.setRiskResult("低度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分处于中等水平，表明其存在一定的跌倒风险;建议措施：1、定期评估：定期对患者进行跌倒风险评估，以监测其跌倒风险的变化；2、环境优化：对患者所处环境进行适当优化，如增加防滑措施、提供足够的座椅和床位等；3、辅助工具使用：鼓励患者使用合适的辅助工具，以提高其行走和活动的稳定性；4、健康教育：继续向患者及其家属普及跌倒预防知识，并强调其重要性。");
                        }else {
                            ehmsAnswerListVO.setRiskResult("高度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分较高，表明其跌倒风险极大，处于易受伤的危险中;建议措施：1、加强监护：对患者进行密切监护，特别是在其活动或转移时，确保有专人陪同；2、环境调整：确保患者所处环境安全，如地面保持干燥、无障碍物，提供足够的照明和扶手等；3、使用辅助工具：根据患者的需要，提供合适的行走辅助工具，如拐杖、助行器等，并确保其正确使用；4、药物管理：评估患者是否正在使用可能导致跌倒的药物，如镇静剂、安眠药等，并与医生协商调整用药方案；5、健康教育：向患者及其家属普及跌倒预防知识，提高其自我防护意识");
                        }
                        resultList.add(ehmsAnswerListVO);
                        break;
                    }
                    case BRADENYC_PFB:
                    {
                        if (score >= 18) {
                            ehmsAnswerListVO.setRiskResult("无危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析:患者得分极高，表明其压疮风险极低;建议措施:1、常规监测：继续关注患者的皮肤状况，但评估频率可适当降低；2、健康教育：继续向患者普及压疮预防知识，巩固其自我防护意识；3、鼓励活动：鼓励患者保持适量活动，维持良好的身体状态。");
                        }else if (score >= 15 && score < 18) {
                            ehmsAnswerListVO.setRiskResult("轻度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析:患者得分较高，表明其压疮风险相对较低;建议措施:1、常规监测：每月至少评估一次患者的皮肤状况；2、日常护理：保持患者皮肤清洁干燥，定期更换床单和衣物；3、健康教育：向患者普及压疮预防知识，提高其自我防护意识；3、鼓励活动：鼓励患者适当活动，促进血液循环和皮肤新陈代谢。");
                        }else if (score >= 13 && score <= 14) {
                            ehmsAnswerListVO.setRiskResult("中度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分处于中等水平，表明其存在一定的压疮风险;建议措施：1、定期监测：每周至少评估一次患者的皮肤状况；2、日常护理：保持患者皮肤清洁干燥，定期更换床单和衣物；3、体位管理：鼓励患者适当活动，避免长时间卧床或久坐；4、营养监测：关注患者的营养摄入情况，必要时给予营养补充。");
                        }else if (score >= 10 && score <= 12) {
                            ehmsAnswerListVO.setRiskResult("高度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分较低，表明其存在较高的压疮风险;建议措施：1、定期评估：每天评估患者的皮肤状况，记录变化情况；2、适当翻身：至少每3~4小时为患者翻身一次，以减少局部受压时间；3、皮肤清洁与干燥：保持患者皮肤清洁干燥，避免潮湿环境对皮肤的不良影响；4、营养指导：根据患者的营养状况，给予适当的营养支持；5、体位调整：协助患者调整体位，避免长时间保持同一姿势。");
                        }else {
                            ehmsAnswerListVO.setRiskResult("极度危险");
                            ehmsAnswerListVO.setRiskPlan("结果分析：患者得分极低，表明其存在极高的压疮风险;建议措施：1、加强护理观察：每2~4小时评估一次患者的皮肤状况，及时发现并处理潜在的压疮问题；2、定期翻身：至少每2小时为患者翻身一次，避免同一部位长时间受压；3、使用缓压设备：如气垫床、泡沫垫等，以减轻局部压力；4、保持皮肤干燥：及时清洁患者的皮肤，避免尿液、汗液等长时间浸湿皮肤；5、营养支持：确保患者摄入足够的营养，以维持皮肤弹性和修复能力；6、家属教育：向患者家属普及压疮预防知识，共同参与患者的护理工作。");
                        }
                        resultList.add(ehmsAnswerListVO);
                        break;
                    }
                    default:
                    {
                        PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);
                        if (score >= platLessonScaleSon.getRiskScore()) {
                            String result = platLessonScaleSon.getResult().replace(XINGHAO, myTitle);
                            ehmsAnswerListVO.setRiskResult(result);
                        } else {
                            ehmsAnswerListVO.setRiskResult("无");
                        }
                        ehmsAnswerListVO.setRiskPlan(platLessonScaleSon.getResultFormula());
                        resultList.add(ehmsAnswerListVO);
                        break;
                    }
                }
            }
        }

        //三个勾选
        if (myMap.get("脑卒中风险评估") != null) {
            String myTitle = "脑卒中风险评估";
            EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
            ehmsAnswerListVO.setName(myTitle);

            //算分
            int scoreMan = 0;
            //算分
            int scoreWoman = 0;

            LinkedHashMap<String, LinkedHashMap<String, Integer>> sanMap = (LinkedHashMap<String, LinkedHashMap<String, Integer>>) myMap.get(myTitle);
            LinkedHashMap<String, Integer> manMap = sanMap.get("man");
            LinkedHashMap<String, Integer> womanMap = sanMap.get("woman");

            //男
            StringBuffer stringBufferMan = new StringBuffer();
            for (String key : manMap.keySet()) {
                if (stringBufferMan.length() != 0) {
                    stringBufferMan.append(DOUHAO);
                }
                stringBufferMan.append("男-"+key + "-" + manMap.get(key));
            }

            //女
            StringBuffer stringBufferWoman = new StringBuffer();
            for (String key : womanMap.keySet()) {
                if (stringBufferWoman.length() != 0) {
                    stringBufferWoman.append(DOUHAO);
                }
                stringBufferWoman.append("女-"+key + "-" + womanMap.get(key));
            }

            //答案
            String[] answersMan = stringBufferMan.toString().split(DOUHAO);
            //答案
            String[] answersWoman = stringBufferWoman.toString().split(DOUHAO);

            for (String answer : answersMan) {
                scoreMan += getQuestionnaireScore(myTitle, answer);
            }
            for (String answer : answersWoman) {
                scoreWoman += getQuestionnaireScore(myTitle, answer);
            }

            String resultText = "";
            PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);
            if (scoreMan >= platLessonScaleSon.getRiskScore()) {
                String result = platLessonScaleSon.getResult().replace(XINGHAO, myTitle);
                resultText = "男:" + result;
            } else {
                resultText = "男:" + "无";
            }

            if (scoreWoman >= platLessonScaleSon.getRiskScore()) {
                String result = platLessonScaleSon.getResult().replace(XINGHAO, myTitle);
                resultText += "，女:" + result;
            } else {
                resultText += "，女:" + "无";
            }
            ehmsAnswerListVO.setRiskResult(resultText);
            ehmsAnswerListVO.setRiskPlan(platLessonScaleSon.getResultFormula());
            resultList.add(ehmsAnswerListVO);
        }

        //中医体质分类与评价
        if (myMap.get("中医体质分类与评价") != null) {
            String type[] = {"平和质","气虚质","阳虚质","阴虚质","痰湿质","湿热质","血瘀质","气郁质","特禀质"};
            String myTitle = "中医体质分类与评价";

            PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);

            EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
            ehmsAnswerListVO.setName(myTitle);

            LinkedHashMap<String, LinkedHashMap<String, Integer>> sanMap = (LinkedHashMap<String, LinkedHashMap<String, Integer>>) myMap.get(myTitle);
            for(int i=1;i<=9;i++){
                LinkedHashMap<String, Integer> hashMap = sanMap.get(String.valueOf(i));
                int score = 0;
                for (String key : hashMap.keySet()) {
                    String answer = i + "-" + key +"-"+ hashMap.get(key);
                    score += getQuestionnaireScore(myTitle, answer);
                }
                if(score >= platLessonScaleSon.getRiskScore()){
                    ehmsAnswerListVO.setRiskResult(type[i-1]);
                    break;
                }
            }
            PlatLessonScaleSon platLessonPlan= platLessonScaleSonMapper.selectByPrimaryKey(ehmsAnswerListVO.getRiskResult());
            ehmsAnswerListVO.setRiskPlan(platLessonPlan!=null?platLessonPlan.getResultFormula():"");
            resultList.add(ehmsAnswerListVO);
        }

        //慢性病患者生命质量测定量表
        if (myMap.get("慢性病患者生命质量测定量表") != null) {
            String myTitle = "慢性病患者生命质量测定量表";

            EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
            ehmsAnswerListVO.setName(myTitle);
            String resurl = "";

            LinkedHashMap<String, LinkedHashMap<String, Integer>> sanMap = (LinkedHashMap<String, LinkedHashMap<String, Integer>>) myMap.get(myTitle);
            for(int i=1;i<=3;i++){
                LinkedHashMap<String, Integer> hashMap = sanMap.get(String.valueOf(i));
                double score = 0;
                for (String key : hashMap.keySet()) {
                    String answer = i + "-" + key +"-"+ hashMap.get(key);
                    score += getQuestionnaireScore(myTitle, answer);
                }

                if(i==1){
                    double percentage = (score / 32d) * 100;
                    resurl = "生理状况得分："+( new DecimalFormat("0").format(percentage))+"%";
                }
                if(i==2){
                    double percentage = (score / 44d) * 100;
                    resurl += "，心理功能得分："+( new DecimalFormat("0").format(percentage))+"%";
                }
                if(i==3){
                    double percentage = (score / 44d) * 100;
                    resurl += "，社会功能得分："+( new DecimalFormat("0").format(percentage))+"%";
                }
            }
            ehmsAnswerListVO.setRiskResult(resurl);
            ehmsAnswerListVO.setRiskPlan("");
            resultList.add(ehmsAnswerListVO);
        }

        //匹茨堡睡眠质量指数量表
        if (myMap.get("匹茨堡睡眠质量指数量表") != null) {
            String myTitle = "匹茨堡睡眠质量指数量表";

            PlatLessonScaleSon platLessonScaleSon = platLessonScaleSonMapper.selectByPrimaryKey(myTitle);

            EhmsAnswerListVO ehmsAnswerListVO = new EhmsAnswerListVO();
            ehmsAnswerListVO.setName(myTitle);
            String resurl = "";

            LinkedHashMap<String, LinkedHashMap<String, Integer>> sanMap = (LinkedHashMap<String, LinkedHashMap<String, Integer>>) myMap.get(myTitle);
            LinkedHashMap<String, Integer> hashMap = sanMap.get("5");
            int score = 0;
            for (String key : hashMap.keySet()) {
                String answer = 5 + "-" + key +"-"+ hashMap.get(key);
                score += getQuestionnaireScore(myTitle, answer);
            }
            if(score >= platLessonScaleSon.getRiskScore()){
                ehmsAnswerListVO.setRiskResult(platLessonScaleSon.getResult());
            }
            ehmsAnswerListVO.setRiskResult(resurl);
            ehmsAnswerListVO.setRiskPlan(platLessonScaleSon.getResultFormula());
            resultList.add(ehmsAnswerListVO);
        }

        ehmsAnswerVO.setList(resultList);
        return ehmsAnswerVO;
    }

    @Override
    public TheoreticalLearningVO foodClassificationGameTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("食材分类游戏.png", FOOD_PREFIX + "食材分类游戏.png", "png", ""),
                new FileList("食材分类视频.mp4", FOOD_PREFIX + "食材分类视频.mp4", "mp4", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<Map<String, Object>> healthIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();
        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                INTERVENEHYPERTENSION_PREFIX + "baseInfo.png",
                "李白", "男", "58岁", "A002415",
                "男性，上海本地居民，58岁",
                "母亲有冠心病",
                "高血压病1年",
                "身高175cm，体重88kg，腰围88cm",
                "经常焦虑",
                "睡眠质量不佳",
                null,
                "收缩压130mmHg，舒张压83mmHg；空腹血糖控制在6.0 mmol/L, 餐后2小时血糖7.7 mmol/L；总胆固醇（TC）7.2mmol/L，甘油三酯（TG）1.6mmol/L,高密度脂蛋白胆固醇（HDL-C）0.9mmol/L ，低密度脂蛋白胆固醇LDL5.0mmol/L，Hb1Ac5.6%，心房纤颤，左心室肥厚",
                null, null, Arrays.asList("内心血管疾病"), Arrays.asList("高脂血症", "脑卒中")

        ));
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                INTERVENEHYPERTENSION_PREFIX + "riskAssessment.png",
                "脑卒中风险评估报告",
                Arrays.asList(
                        new ParamVO("年龄（岁）", "58", null, "岁"),
                        new ParamVO("治疗后收缩压", "130", "90-139", "mmHg"),
                        new ParamVO("糖尿病家族史", "无", null, null),
                        new ParamVO("吸烟", "有", null, null),
                        new ParamVO("心血管疾病", "无", null, null),
                        new ParamVO("心房纤颤", "有", null, null),
                        new ParamVO("左心室肥厚", "有", null, null)
                ),
                new ResultVO("高风险", "18", "0", "1", "17")
        );

        Map<String, Object> data2 = getRiskAssessment(
                INTERVENEHYPERTENSION_PREFIX + "riskAssessment.png",
                "心血管疾病危险评估",
                Arrays.asList(
                        new ParamVO("BMI", "28.73", "24-28", null),
                        new ParamVO("吸烟", "有", null, null),
                        new ParamVO("腰围", "88", "男=85-90cm；女=80-85cm", "cm"),
                        new ParamVO("收缩压", "130", "90-139", "mmHg"),
                        new ParamVO("总胆固醇", "7.2", "＜5.20", "mmol/L"),
                        new ParamVO("糖尿病", "无", null, null),
                        new ParamVO("家族史", "有", null, null)
                ),
                new ResultVO("中度风险", "13", "21.7", "8", "5")
        );

        Map<String, Object> data3 = getRiskAssessment(
                INTERVENEHYPERTENSION_PREFIX + "riskAssessment.png",
                "高血压疾病危险评估",
                Arrays.asList(
                        new ParamVO("性别", "男", null, null),
                        new ParamVO("收缩压", "130", "90-139", "mmHg"),
                        new ParamVO("BMI", "28.73", "24-28", null),
                        new ParamVO("舒张压", "83", "＜70", "mmHg"),
                        new ParamVO("吸烟", "有", null, null),
                        new ParamVO("父母高血压", "无", null, null)
                ),
                new ResultVO("中度风险", "18", "", "", "")
        );

        list.add(0, data1);
        list.add(1, data2);
        list.add(2, data3);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);

        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("高血压-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 高血压--膳食干预方案 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 70),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", 0.26),
                new HealthInformationKV<>("每日摄入热量", 1750)
        ));

        //构建 高血压--膳食干预方案 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 525),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 700),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 525),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 高血压--膳食干预方案 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 109.38),
                new HealthInformationKV<>("oneImg", INTERVENEHYPERTENSION_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 48.61),
                new HealthInformationKV<>("twoImg", INTERVENEHYPERTENSION_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 875),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 218.75),
                new HealthInformationKV<>("threeImg", INTERVENEHYPERTENSION_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 高血压--膳食干预方案 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 高血压--膳食干预方案 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1750),
                new HealthInformationKV<>("蛋白质推荐量", 109.38),
                new HealthInformationKV<>("脂肪推荐量", 48.61),
                new HealthInformationKV<>("碳水化合物推荐量", 218.75)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.HEALTH_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "高血压--膳食干预方案", right1List),
                new HealthInterventionSon("2", "高血压--运动干预方案", right2List),
                new HealthInterventionSon("3", "高血压--心理干预方案", right3List),
                new HealthInterventionSon("4", "高血压--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO nutritionalCateringCourse(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("营养配餐课程.png", NUTRITIONALCATERING_PREFIX + "营养配餐课程.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<Map<String, Object>> nutritionalCateringIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                NUTRITIONALCATERING_PREFIX + "baseInfo.png",
                "张军", "男", "30岁", "A002418",
                "张军，男性，武汉市常住人口，30岁，现为某IT公司的程序员",
                "无慢性家族病史",
                "最近因为有些头晕，头痛不适",
                "身高175cm，最近一年体重增加到了100kg，腰围100cm",
                "性格外向，擅言谈",
                "经常熬夜，睡眠质量不太好",
                "每周吸烟1-2次，每次一支;嗜酒如命;喜食肥肉，油炸食品等;平时缺少运动",
                "无心房纤颤，左心室肥厚;HbA1c=6.4%;血尿酸450 umol/L;收缩压139mmHg;舒张压89mmHg;总胆固醇(TC)8.2mmol/L;甘油三酯(TG)1.3mmol/L;高密度脂蛋白胆固醇(HDL-C)0.7mmol/L;低密度脂蛋白胆固醇(LDL-C)3.8mmol/L",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                NUTRITIONALCATERING_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("肥胖症-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 70),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", 0.43),
                new HealthInformationKV<>("每日摄入热量", 1750)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 525),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 700),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 525),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 109.38),
                new HealthInformationKV<>("oneImg", NUTRITIONALCATERING_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 48.61),
                new HealthInformationKV<>("twoImg", NUTRITIONALCATERING_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 875),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 218.75),
                new HealthInformationKV<>("threeImg", NUTRITIONALCATERING_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1750),
                new HealthInformationKV<>("蛋白质推荐量", 109.38),
                new HealthInformationKV<>("脂肪推荐量", 48.61),
                new HealthInformationKV<>("碳水化合物推荐量", 218.75)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.NUTRITIONALCATERING_HEALTH_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "肥胖症--膳食干预方案", right1List),
                new HealthInterventionSon("2", "肥胖症--运动干预方案", right2List),
                new HealthInterventionSon("3", "肥胖症--心理干预方案", right3List),
                new HealthInterventionSon("4", "肥胖症--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public FoodClassificationVO foodClassificationGame(Long lessonId) {
        FoodClassificationVO foodClassificationVO = new FoodClassificationVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        foodClassificationVO.setPlatLesson(platLesson);
        return foodClassificationVO;
    }

    @Override
    public TheoreticalLearningVO interveneHypertensionTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("高血压的健康干预技能.png", INTERVENEHYPERTENSION_PREFIX + "高血压的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationCollection(Long lessonId) {
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-高血压-个人基本信息表.pdf", INTERVENEHYPERTENSION_PREFIX + "慢性病饮食干预-高血压-个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-高血压-个人生活行为习惯表.pdf", INTERVENEHYPERTENSION_PREFIX + "慢性病饮食干预-高血压-个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-高血压-健康风险评估结果表.pdf", INTERVENEHYPERTENSION_PREFIX + "慢性病饮食干预-高血压-健康风险评估结果表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-高血压-健康体检表.pdf", INTERVENEHYPERTENSION_PREFIX + "慢性病饮食干预-高血压-健康体检表.pdf", "pdf", "")
        );
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public TheoreticalLearningVO interveneCoronaryTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("冠状动脉粥样硬化性心脏病的健康干预技能.png", INTERVENECORONARY_PREFIX + "冠状动脉粥样硬化性心脏病的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationCoronaryCollection(Long lessonId) {
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-冠心病-个人基本信息表.pdf", INTERVENECORONARY_PREFIX + "慢性病饮食干预-冠心病-个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-冠心病-个人生活行为习惯表.pdf", INTERVENECORONARY_PREFIX + "慢性病饮食干预-冠心病-个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-冠心病-健康体检表.pdf", INTERVENECORONARY_PREFIX + "慢性病饮食干预-冠心病-健康体检表.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.CORONARY_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.CORONARY_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.CORONARY_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.CORONARY_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.CORONARY_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.CORONARY_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.CORONARY_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.CORONARY_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.CORONARY_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.CORONARY_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.CORONARY_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.CORONARY_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthCoronaryIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                NUTRITIONALCATERING_PREFIX + "baseInfo.png",
                "刘英", "女", "67岁", "A002420",
                "女性，杭州本地居民，67岁",
                "",
                "37岁被诊断为冠心病",
                "身高165cm，体重60kg，腰围76cm",
                "经常会出现焦虑",
                "睡眠质量不好",
                "不吸烟不饮酒；刘某爱吃咸菜，水果蔬菜摄入较少；平时早晨起早走步1h",
                "收缩压120mmHg，舒张压75mmHg；空腹血糖控制在5.8 mmol/L, 餐后2小时血糖7.3 mmol/L；总胆固醇（TC）6.2mmol/L, 甘油三收缩压120mmHg，舒张压75mmHg；空腹血糖控制在5.8 mmol/L, 餐后2小时血糖7.3 mmol/L；总胆固醇（TC）6.2mmol/L, 甘油三酯2.2 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.8mmol/L ，低密度脂蛋白胆固醇LDL5.4mmol/L；心房纤颤，无左心室肥厚",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                INTERVENECORONARY_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("慢性病饮食干预-冠心病");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 60),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", null),
                new HealthInformationKV<>("每日摄入热量", 1500)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 450),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 600),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 450),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 375),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 93.75),
                new HealthInformationKV<>("oneImg", INTERVENECORONARY_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 375),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 41.67),
                new HealthInformationKV<>("twoImg", INTERVENECORONARY_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 750),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 187.5),
                new HealthInformationKV<>("threeImg", INTERVENECORONARY_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1500),
                new HealthInformationKV<>("蛋白质推荐量", 93.75),
                new HealthInformationKV<>("脂肪推荐量", 41.67),
                new HealthInformationKV<>("碳水化合物推荐量", 187.5)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.CORONARY_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "冠心病--膳食干预方案", right1List),
                new HealthInterventionSon("2", "冠心病--运动干预方案", right2List),
                new HealthInterventionSon("3", "冠心病--心理干预方案", right3List),
                new HealthInterventionSon("4", "冠心病--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO dietEvaluationReportTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("学习及咨询模块");
        List<FileList> fileList = Arrays.asList(
                new FileList("肥胖症的健康干预技能.png", DIETEVALUATIONREPORT_PREFIX + "肥胖症的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<Map<String, Object>> dietEvaluationReport(Long lessonId) {
        return this.nutritionalCateringIntervention(lessonId);
    }


    @Override
    public TheoreticalLearningVO interveneStrokeTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论知识");
        List<FileList> fileList = Arrays.asList(
                new FileList("脑卒中的健康干预技能.png", STROKE_PREFIX + "脑卒中的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationStrokeCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-脑卒中-个人基本信息表.pdf", STROKE_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-脑卒中-个人生活行为习惯表.pdf", STROKE_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-脑卒中-健康体检表.pdf", STROKE_PREFIX + "健康体检表.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.STROKE_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.STROKE_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.STROKE_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.STROKE_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.STROKE_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.STROKE_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.STROKE_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.STROKE_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.STROKE_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.STROKE_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.STROKE_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.STROKE_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthStrokeIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                STROKE_PREFIX + "baseInfo.png",
                "陈某", "男", "75岁", "A002416",
                "男，75岁，广州本地居民",
                "",
                "半年前曾出现过小中风",
                "身高173cm，体重65kg，腰围86cm",
                "",
                "睡眠质量不好",
                "吸烟年限20年，平均每天5支；每周饮用白酒2次，每次约20ml；喜吃甜食，蔬菜水果摄入少；平时基本不运动 ",
                "收缩压145mmHg，舒张压95mmHg；空腹血糖6.8 mmol/L, 餐后2小时血糖8.7mmol/L, 总胆固醇（TC）5.5mmol/L, 甘油三酯2.4 mmol/L，高密度脂蛋白胆固醇（HDL-C）0.7mmol/L ，低密度脂蛋白胆固醇LDL4.4mmol/L；无心房纤颤，左心室肥厚",
                null, null, null, Arrays.asList("脑卒中"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                STROKE_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("脑卒中-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 68),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", -0.04),
                new HealthInformationKV<>("每日摄入热量", 1700)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 510),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 680),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 510),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 425),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 106.25),
                new HealthInformationKV<>("oneImg", STROKE_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 425),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 47.22),
                new HealthInformationKV<>("twoImg", STROKE_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 850),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 212.5),
                new HealthInformationKV<>("threeImg", STROKE_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1700),
                new HealthInformationKV<>("蛋白质推荐量", 106.25),
                new HealthInformationKV<>("脂肪推荐量", 47.22),
                new HealthInformationKV<>("碳水化合物推荐量", 212.5)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.STROKE_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "脑卒中--膳食干预方案", right1List),
                new HealthInterventionSon("2", "脑卒中--运动干预方案", right2List),
                new HealthInterventionSon("3", "脑卒中--心理干预方案", right3List),
                new HealthInterventionSon("4", "脑卒中--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO interveneDiabetesTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论知识");
        List<FileList> fileList = Arrays.asList(
                new FileList("2型糖尿病的健康干预技能.png", DIABETES_PREFIX + "2型糖尿病的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationDiabetesCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-糖尿病-个人基本信息表.pdf", DIABETES_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-糖尿病-个人生活行为习惯表.pdf", DIABETES_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-糖尿病-健康体检表.pdf", DIABETES_PREFIX + "健康体检表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-糖尿病-健康风险评估结果汇总.pdf", DIABETES_PREFIX + "健康风险评估结果汇总.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.DIABETES_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.DIABETES_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.DIABETES_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.DIABETES_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.DIABETES_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.DIABETES_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.DIABETES_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.DIABETES_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.DIABETES_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.DIABETES_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.DIABETES_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.DIABETES_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthDiabetesIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                DIABETES_PREFIX + "baseInfo.png",
                "张玲", "女", "60岁", "A002423",
                "女，杭州本地居民，某国企退休人员，60岁",
                "父亲患有糖尿病",
                "糖尿病近2年",
                "身高160cm，体重72kg，腰围90cm",
                "心理压力很大，乏力，常无精打采，对很多事情都没有兴趣",
                "睡眠不踏实",
                "不吸烟；不饮酒；平时主食基本不吃，以素食为主，嗜糖；每天健步走4000布",
                "空腹血糖7.5mmol/L，收缩压132mmHg，舒张压85mmHg，总胆固醇（TC）5.2mmol/L, 甘油三酯（TG）2.2mmol/L,高密度脂蛋白胆固醇（HDL-C）1.5mmol/L，低密度脂蛋白胆固醇LDL2.9mmol/L，血尿酸400 umol/L，HbA1c=7.1%；无心房纤颤，无左心室肥厚",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                DIABETES_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("糖尿病-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 55),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", 0.31),
                new HealthInformationKV<>("每日摄入热量", 1375)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 412.5),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 550),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 412.5),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 343.75),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 85.94),
                new HealthInformationKV<>("oneImg", DIABETES_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 343.75),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 38.19),
                new HealthInformationKV<>("twoImg", DIABETES_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 687.5),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 171.88),
                new HealthInformationKV<>("threeImg", DIABETES_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1375),
                new HealthInformationKV<>("蛋白质推荐量", 85.94),
                new HealthInformationKV<>("脂肪推荐量", 38.19),
                new HealthInformationKV<>("碳水化合物推荐量", 171.88)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.DIABETES_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "糖尿病--膳食干预方案", right1List),
                new HealthInterventionSon("2", "糖尿病--运动干预方案", right2List),
                new HealthInterventionSon("3", "糖尿病--心理干预方案", right3List),
                new HealthInterventionSon("4", "糖尿病--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO interveneObesityTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论知识");
        List<FileList> fileList = Arrays.asList(
                new FileList("肥胖症的健康干预技能.png", OBESITY_PREFIX + "肥胖症的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationObesityCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-肥胖症-个人基本信息表.pdf", OBESITY_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-肥胖症-个人生活行为习惯表.pdf", OBESITY_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-肥胖症-健康体检表.pdf", OBESITY_PREFIX + "健康体检表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-肥胖症-健康风险评估结果汇总.pdf", OBESITY_PREFIX + "健康风险评估结果汇总.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.OBESITY_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.OBESITY_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.OBESITY_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.OBESITY_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.OBESITY_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.OBESITY_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.OBESITY_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.OBESITY_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.OBESITY_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.OBESITY_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.OBESITY_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.OBESITY_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthObesityIntervention(Long lessonId) {
        return this.nutritionalCateringIntervention(lessonId);
    }

    @Override
    public TheoreticalLearningVO interveneCopdTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论知识");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性阻塞性肺疾病的健康干预技能.png", COPD_PREFIX + "慢性阻塞性肺疾病的健康干预技能.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationCopdCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-慢性阻塞性肺病-个人基本信息表.pdf", COPD_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-慢性阻塞性肺病-个人生活行为习惯表.pdf", COPD_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-慢性阻塞性肺病-健康体检表.pdf", COPD_PREFIX + "健康体检表.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.COPD_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.COPD_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.COPD_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.COPD_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.COPD_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.COPD_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.COPD_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.COPD_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.COPD_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.COPD_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.COPD_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.COPD_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthCopdIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                COPD_PREFIX + "baseInfo.png",
                "赵云", "男", "55岁", "A002417",
                "男性，北京市某外企行政办公室主任，55岁",
                "父亲有哮喘和慢性支气管炎",
                "近半年白天和夜间不感冒时经常出现咳嗽、胸闷、气短等症状",
                "身高175cm，体重55kg，腰围82cm",
                "性格开朗，乐观积极",
                "睡眠质量不佳",
                "20岁开始吸烟，每天平均吸烟10支 ；不喝酒",
                "",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                COPD_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("慢性阻塞性肺病-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 70),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", -0.21),
                new HealthInformationKV<>("每日摄入热量", 1750)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 525),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 700),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 525),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 109.38),
                new HealthInformationKV<>("oneImg", COPD_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 437.5),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 48.61),
                new HealthInformationKV<>("twoImg", COPD_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 875),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 218.75),
                new HealthInformationKV<>("threeImg", COPD_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1750),
                new HealthInformationKV<>("蛋白质推荐量", 109.38),
                new HealthInformationKV<>("脂肪推荐量", 48.61),
                new HealthInformationKV<>("碳水化合物推荐量", 218.75)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.COPD_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "慢性病饮食干预--膳食干预方案", right1List),
                new HealthInterventionSon("2", "慢性病饮食干预--运动干预方案", right2List),
                new HealthInterventionSon("3", "慢性病饮食干预--心理干预方案", right3List),
                new HealthInterventionSon("4", "慢性病饮食干预--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public HealthInformationCollectionVO healthInformationOsteoporosisCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-骨质疏松-个人基本信息表.pdf", OSTEOPOROSIS_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-骨质疏松-个人生活行为习惯表.pdf", OSTEOPOROSIS_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-骨质疏松-健康体检表.pdf", OSTEOPOROSIS_PREFIX + "健康体检表.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.OSTEOPOROSIS_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.OSTEOPOROSIS_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.OSTEOPOROSIS_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.OSTEOPOROSIS_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.OSTEOPOROSIS_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.OSTEOPOROSIS_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.OSTEOPOROSIS_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.OSTEOPOROSIS_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.OSTEOPOROSIS_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.OSTEOPOROSIS_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.OSTEOPOROSIS_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.OSTEOPOROSIS_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthOsteoporosisIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                OSTEOPOROSIS_PREFIX + "baseInfo.png",
                "吴霞", "女", "49岁", "A002419",
                "女性，杭州人，某高校党政办公室职员，年龄49岁",
                "有母亲髋关节脆性骨折家族史",
                "有皮肤病，常服用糖皮质激素类药物，导致小腿骨折",
                "身高170cm，体重50kg ，腰围73",
                "性格外向",
                "睡眠质量良好",
                "不吸烟；爱喝酒；爱喝咖啡；喜欢宅在办公室或家里，不爱运动",
                "",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                OSTEOPOROSIS_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("骨质疏松-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 65),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", -0.23),
                new HealthInformationKV<>("每日摄入热量", 1625)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 487.5),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 650),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 487.5),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 406.25),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 101.56),
                new HealthInformationKV<>("oneImg", OSTEOPOROSIS_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 406.25),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 45.14),
                new HealthInformationKV<>("twoImg", OSTEOPOROSIS_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 812.5),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 203.13),
                new HealthInformationKV<>("threeImg", OSTEOPOROSIS_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1625),
                new HealthInformationKV<>("蛋白质推荐量", 101.56),
                new HealthInformationKV<>("脂肪推荐量", 45.14),
                new HealthInformationKV<>("碳水化合物推荐量", 203.13)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.OSTEOPOROSIS_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "骨质疏松--膳食干预方案", right1List),
                new HealthInterventionSon("2", "骨质疏松--运动干预方案", right2List),
                new HealthInterventionSon("3", "骨质疏松--心理干预方案", right3List),
                new HealthInterventionSon("4", "骨质疏松--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO intervenePelvicFloorTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论知识");
        List<FileList> fileList = Arrays.asList(
                new FileList("盆底功能障碍的评估与干预.png", PELVICFLOOR_PREFIX + "盆底功能障碍的评估与干预.png", "png", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public HealthInformationCollectionVO healthInformationPelvicFloorCollection(Long lessonId) {
        /*个人基本信息表.pdf   个人生活行为习惯表.pdf  健康体检表.pdf*/
        HealthInformationCollectionVO healthInformationCollection = new HealthInformationCollectionVO();
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("健康信息收集");
        List<FileList> fileList = Arrays.asList(
                new FileList("慢性病饮食干预-盆底功能障碍-个人基本信息表.pdf", PELVICFLOOR_PREFIX + "个人基本信息表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-盆底功能障碍-个人生活行为习惯表.pdf", PELVICFLOOR_PREFIX + "个人生活行为习惯表.pdf", "pdf", ""),
                new FileList("慢性病饮食干预-盆底功能障碍-健康体检表.pdf", PELVICFLOOR_PREFIX + "健康体检表.pdf", "pdf", "")
        );
        List<ClassificationVO> healthInformationList = Arrays.asList(
                new ClassificationVO("1", "drag", "患者资料-拖拽项", TagsConstant.PELVICFLOOR_ALL_TAGS),
                new ClassificationVO("2", "drop", "基本情况-放置区", TagsConstant.PELVICFLOOR_BASIC_SITUATION_TAGS),
                new ClassificationVO("3", "drop", "家族病史-放置区", TagsConstant.PELVICFLOOR_FAMILY_HISTORY_TAGS),
                new ClassificationVO("4", "drop", "体重-放置区", TagsConstant.PELVICFLOOR_WEIGHT_TAGS),
                new ClassificationVO("5", "drop", "心理-放置区", TagsConstant.PELVICFLOOR_MIND_TAGS),
                new ClassificationVO("6", "drop", "睡眠-放置区", TagsConstant.PELVICFLOOR_SLEEP_TAGS),
                new ClassificationVO("7", "drop", "生活方式1-放置区", TagsConstant.PELVICFLOOR_LIFESTYLE_ONE_TAGS),
                new ClassificationVO("8", "drop", "生活方式2-放置区", TagsConstant.PELVICFLOOR_LIFESTYLE_TWO_TAGS),
                new ClassificationVO("9", "drop", "生活方式3-放置区", TagsConstant.PELVICFLOOR_LIFESTYLE_THREE_TAGS),
                new ClassificationVO("10", "drop", "生活方式4-放置区", TagsConstant.PELVICFLOOR_LIFESTYLE_FORE_TAGS),
                new ClassificationVO("11", "drop", "重要健康体检结果-放置区", TagsConstant.PELVICFLOOR_PHYSICAL_EXAMINATION_TAGS),
                new ClassificationVO("12", "drop", "现患疾病-放置区", TagsConstant.PELVICFLOOR_PRESENT_DISEASE_TAGS)
        );
        healthInformationCollection.setHealthInformationList(healthInformationList);
        healthInformationCollection.setTheoreticalLearningVO(new TheoreticalLearningVO(platLesson, baseInfo, fileList));
        return healthInformationCollection;
    }

    @Override
    public List<Map<String, Object>> healthPelvicFloorIntervention(Long lessonId) {
        List<Map<String, Object>> result = new ArrayList<>();


        Map<String, Object> rightMap = new HashMap<>();

        Map<String, Object> baseInfoMap = getBaseInfoMap(new InterventionBaseInfo(
                PELVICFLOOR_PREFIX + "baseInfo.png",
                "吴霞", "女", "49岁", "A002419",
                "女性，杭州人，某高校党政办公室职员，年龄49岁",
                "有母亲髋关节脆性骨折家族史",
                "有皮肤病，常服用糖皮质激素类药物，导致小腿骨折",
                "身高170cm，体重50kg ，腰围73",
                "性格外向",
                "睡眠质量良好",
                "不吸烟；爱喝酒；爱喝咖啡；喜欢宅在办公室或家里，不爱运动",
                "",
                null, null, null, Arrays.asList("高脂血症"))
        );
        rightMap.put("baseInfo", baseInfoMap);

        Map<String, List<Map<String, Object>>> riskAssessmentMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> data1 = getRiskAssessment(
                PELVICFLOOR_PREFIX + "riskAssessment.png",
                null,
                null,
                null
        );
        list.add(0, data1);
        riskAssessmentMap.put("data", list);
        rightMap.put("riskAssessment", riskAssessmentMap);


        Map<String, Object> dataMap = new HashMap<>();
        HealthInterventionLeftData healthInterventionLeftData = new HealthInterventionLeftData();
        healthInterventionLeftData.setName("盆底功能障碍-健康干预");
        //右侧数据
        List<HealthInterventionRight> right1List = new ArrayList<>();
        //构建 营养配餐课程 右侧 每日摄入热量计算 的数据
        HealthInterventionRight left1Right1 = new HealthInterventionRight();
        left1Right1.setName("每日摄入热量计算");
        left1Right1.setDatas(Arrays.asList(
                new HealthInformationKV<>("标准体重", 65),
                new HealthInformationKV<>("体力劳动量取值", 25),
                new HealthInformationKV<>("肥胖程度", -0.23),
                new HealthInformationKV<>("每日摄入热量", 1625)
        ));

        //构建 营养配餐课程 右侧 三餐热量计算  的数据
        HealthInterventionRight left1Right2 = new HealthInterventionRight();
        left1Right2.setName("三餐热量计算");
        left1Right2.setDatas(Arrays.asList(
                new HealthInformationKV<>("早餐理想热量配比为", 30),
                new HealthInformationKV<>("早餐摄入的热量应为", 487.5),
                new HealthInformationKV<>("午餐理想热量配比为", 40),
                new HealthInformationKV<>("午餐摄入的热量应为", 650),
                new HealthInformationKV<>("晚餐理想热量配比为", 30),
                new HealthInformationKV<>("晚餐摄入的热量应为", 487.5),
                new HealthInformationKV<>("tips", "理想热量配比为 早餐：午餐：晚餐 =3：4：3")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right3 = new HealthInterventionRight();
        left1Right3.setName("每日摄入三大营养素计算");
        left1Right3.setDatas(Arrays.asList(
                new HealthInformationKV<>("蛋白质占总热量的", 25),
                new HealthInformationKV<>("蛋白质的热量应为", 406.25),
                new HealthInformationKV<>("热量换算为纯蛋白质质量为", 101.56),
                new HealthInformationKV<>("oneImg", PELVICFLOOR_PREFIX + "one.png"),
                new HealthInformationKV<>("oneTip", "蛋白质占总热量的20-25%,1g约等于4kcal"),
                new HealthInformationKV<>("饱和脂肪酸占总热量的", 25),
                new HealthInformationKV<>("饱和脂肪酸的热量应为", 406.25),
                new HealthInformationKV<>("热量换算为饱和脂肪酸质量为", 45.14),
                new HealthInformationKV<>("twoImg", PELVICFLOOR_PREFIX + "two.png"),
                new HealthInformationKV<>("twoTip", "脂肪占总热量的20-30%,1g约等于9kcal"),
                new HealthInformationKV<>("维生素及矿物质占总热量的", 50),
                new HealthInformationKV<>("维生素及矿物质的热量应为", 812.5),
                new HealthInformationKV<>("热量换算为维生素及矿物质质量为", 203.13),
                new HealthInformationKV<>("threeImg", PELVICFLOOR_PREFIX + "three.png"),
                new HealthInformationKV<>("threeTip", "碳水化合物占总热量的50-60%,1g约等于4kcal")
        ));

        //构建 营养配餐课程 右侧 每日摄入三大营养素计算  的数据
        HealthInterventionRight left1Right4 = new HealthInterventionRight();
        left1Right4.setName("营养素三餐热量分配");
        left1Right4.setDatas(Arrays.asList(
                new HealthInformationKV<>("脂肪-早餐的占比应为", "20|30"),
                new HealthInformationKV<>("脂肪-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("脂肪-晚餐的占比应为", "40"),
                new HealthInformationKV<>("oneTip", "脂肪三餐合理分配比例2:4:4或3:3:4"),
                new HealthInformationKV<>("蛋白质-早餐的占比应为", "30|40"),
                new HealthInformationKV<>("蛋白质-午餐的占比应为", "40|30"),
                new HealthInformationKV<>("蛋白质-晚餐的占比应为", "30"),
                new HealthInformationKV<>("twoTip", "蛋白质三餐合理分配比例3:4:3或4:3:3"),
                new HealthInformationKV<>("碳水化合物-早餐的占比应为", "30"),
                new HealthInformationKV<>("碳水化合物-午餐的占比应为", "50"),
                new HealthInformationKV<>("碳水化合物-晚餐的占比应为", "20"),
                new HealthInformationKV<>("threeTip", "碳水化合物三餐合理分配比例3:5:2")
        ));


        //构建 营养配餐课程 右侧 膳食方案制定  的数据
        HealthInterventionRight left1Right5 = new HealthInterventionRight();
        left1Right5.setName("膳食方案制定");
        left1Right5.setDatas(Arrays.asList(
                new HealthInformationKV<>("foodList", DietaryConstant.DIETARY_VO_LIST),
                new HealthInformationKV<>("能量推荐量", 1625),
                new HealthInformationKV<>("蛋白质推荐量", 101.56),
                new HealthInformationKV<>("脂肪推荐量", 45.14),
                new HealthInformationKV<>("碳水化合物推荐量", 203.13)
        ));


        right1List.add(0, left1Right1);
        right1List.add(1, left1Right2);
        right1List.add(2, left1Right3);
        right1List.add(3, left1Right4);
        right1List.add(4, left1Right5);

        //右侧数据
        List<HealthInterventionRight> right2List = new ArrayList<>();
        HealthInterventionRight left2Right1 = new HealthInterventionRight();
        left2Right1.setName("有氧运动");
        left2Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", MotionConstant.MOTION_VO_LIST)
        ));
        HealthInterventionRight left2Right2 = new HealthInterventionRight();
        left2Right2.setName("抗阻训练");
        left2Right2.setDatas(Arrays.asList(
                new HealthInformationKV("data", ResistanceConstant.RESISTANCE_VO_LIST)
        ));
        HealthInterventionRight left2Right3 = new HealthInterventionRight();
        left2Right3.setName("柔韧训练");
        left2Right3.setDatas(Arrays.asList(
                new HealthInformationKV("data", TensileConstant.TENSILE_VO_LIST)
        ));


        right2List.add(0, left2Right1);
        right2List.add(1, left2Right2);
        right2List.add(2, left2Right3);


        //右侧数据
        List<HealthInterventionRight> right3List = new ArrayList<>();
        HealthInterventionRight left3Right1 = new HealthInterventionRight();
        left3Right1.setName("心理干预");
        left3Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", PsychologicalConstant.PSYCHOLOGICAL_VO_LIST)
        ));
        right3List.add(0, left3Right1);


        //右侧数据
        List<HealthInterventionRight> right4List = new ArrayList<>();
        HealthInterventionRight left4Right1 = new HealthInterventionRight();
        left4Right1.setName("健康教育");
        left4Right1.setDatas(Arrays.asList(
                new HealthInformationKV("data", HealthEduConstant.PELVICFLOOR_EDU_VO_LIST)
        ));
        right4List.add(0, left4Right1);

        List<HealthInterventionSon> leftData = Arrays.asList(
                new HealthInterventionSon("1", "盆底功能障碍--膳食干预方案", right1List),
                new HealthInterventionSon("2", "盆底功能障碍--运动干预方案", right2List),
                new HealthInterventionSon("3", "盆底功能障碍--心理干预方案", right3List),
                new HealthInterventionSon("4", "盆底功能障碍--健康教育", right4List)
        );
        healthInterventionLeftData.setHealthInterventionSon(leftData);
        dataMap.put("data", healthInterventionLeftData);
        result.add(0, rightMap);
        result.add(1, dataMap);

        return result;
    }

    @Override
    public TheoreticalLearningVO foodLabelsTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("食品选择与营养标签.png", FOODLABELS_PREFIX + "食品选择与营养标签.png", "png", ""),
                new FileList("食品选择与营养标签.mp4", FOODLABELS_PREFIX + "食品选择与营养标签.mp4", "mp4", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<FoodLabelsVO> foodLabels(Long lessonId) {
        return FOOD_LABELS_VO_LIST;
    }

    @Override
    public TheoreticalLearningVO hypertensionFoodLabelsTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("食品选择与营养标签.png", HYPERTENSION_PREFIX + "食品选择与营养标签.png", "png", ""),
                new FileList("食品选择与营养标签.mp4", HYPERTENSION_PREFIX + "食品选择与营养标签.mp4", "mp4", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<FoodLabelsVO> hypertensionFoodLabels(Long lessonId) {
        return HYPERTENSION_FOOD_LABELS_VO_LIST;
    }

    @Override
    public TheoreticalLearningVO diabetesFoodLabelsTheoreticalLearning(Long lessonId) {
        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setTitle("理论学习");
        List<FileList> fileList = Arrays.asList(
                new FileList("食品选择与营养标签.png", DIABETES_FOODLABELS_PREFIX + "食品选择与营养标签.png", "png", ""),
                new FileList("食品选择与营养标签.mp4", DIABETES_FOODLABELS_PREFIX + "食品选择与营养标签.mp4", "mp4", "")
        );
        return new TheoreticalLearningVO(platLesson, baseInfo, fileList);
    }

    @Override
    public List<FoodLabelsVO> diabetesFoodLabels(Long lessonId) {
        return DIABETES_FOOD_LABELS_VO_LIST;
    }

    @Override
    public PlatInterfereHealth interfereHealth(Long lessonId) {
        return platInterfereHealthMapper.selectByPrimaryKey(lessonId);
    }

    @Override
    public List<PlatQuesSportStudy> getSportStudy(Long lessonId) {
        PlatQuesSportStudyExample platQuesSportStudyExample = new PlatQuesSportStudyExample();
        PlatQuesSportStudyExample.Criteria criteria = platQuesSportStudyExample.createCriteria();
        criteria.andLessonIdEqualTo(lessonId);
        List<PlatQuesSportStudy> list = platQuesSportStudyMapper.selectByExample(platQuesSportStudyExample);
        ArrayList<PlatQuesSportStudy> rootNode = new ArrayList<>();
        for (PlatQuesSportStudy node : list) {
            if (node.getParentId() == null) {
                rootNode.add(node);
            }
        }
        for (PlatQuesSportStudy node : rootNode) {
            List<PlatQuesSportStudy> child = getChild(String.valueOf(node.getId()), list);
            node.setList(child);
        }
        return rootNode;
    }

    public static List<PlatQuesSportStudy> getChild(String id, List<PlatQuesSportStudy> allNode) {
        // 存放子菜单的集合
        ArrayList<PlatQuesSportStudy> listChild = new ArrayList<>();
        for (PlatQuesSportStudy node : allNode) {
            if (id.equals(String.valueOf(node.getParentId()))) {
                listChild.add(node);
            }
        }
        // 递归：
        for (PlatQuesSportStudy node : listChild) {
            node.setList(getChild(String.valueOf(node.getId()), allNode));
        }
        if (listChild.size() == 0) {
            return null;
        }
        return listChild;
    }


    private Map<String, Object> getBaseInfoMap(InterventionBaseInfo interventionBaseInfo) {
        Map<String, Object> baseInfoMap = new HashMap<>();
        baseInfoMap.put("img", interventionBaseInfo.getImg());
        baseInfoMap.put("name", interventionBaseInfo.getName());
        baseInfoMap.put("sex", interventionBaseInfo.getSex());
        baseInfoMap.put("age", interventionBaseInfo.getAge());
        baseInfoMap.put("number", interventionBaseInfo.getNumber());
        baseInfoMap.put("info", interventionBaseInfo.getInfo());
        baseInfoMap.put("medicalHistory", interventionBaseInfo.getMedicalHistory());
        baseInfoMap.put("disease", interventionBaseInfo.getDisease());
        baseInfoMap.put("weight", interventionBaseInfo.getWeight());
        baseInfoMap.put("psychological", interventionBaseInfo.getPsychological());
        baseInfoMap.put("sleep", interventionBaseInfo.getSleep());
        baseInfoMap.put("lifeStyle", interventionBaseInfo.getLifeStyle());
        baseInfoMap.put("physical", interventionBaseInfo.getPhysical());
        baseInfoMap.put("generalRisk", interventionBaseInfo.getGeneralRisk());
        baseInfoMap.put("lowRisk", interventionBaseInfo.getLowRisk());
        baseInfoMap.put("intermediateRisk", interventionBaseInfo.getIntermediateRisk());
        baseInfoMap.put("highRisk", interventionBaseInfo.getHighRisk());
        return baseInfoMap;
    }

    private Map<String, Object> getRiskAssessment(String img, String title, List<ParamVO> paramVOList, ResultVO resultVO) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("img", img);
        dataMap.put("title", title);
        dataMap.put("paramNameList", paramVOList);
        dataMap.put("result", resultVO);
        return dataMap;
    }
}
