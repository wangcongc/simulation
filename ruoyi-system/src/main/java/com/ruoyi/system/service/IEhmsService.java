package com.ruoyi.system.service;

import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.dto.EhmsAnswerDto;
import com.ruoyi.system.domain.dto.EhmsQuestionDto;
import com.ruoyi.system.domain.vo.EhmsAnswerVO;
import com.ruoyi.system.domain.vo.EhmsQuestionVO;
import com.ruoyi.system.domain.vo.EhmsScaleVO;
import com.ruoyi.system.domain.vo.FoodClassificationVO;
import com.ruoyi.system.domain.vo.FoodLabelsVO;
import com.ruoyi.system.domain.vo.HealthInformationCollectionVO;
import com.ruoyi.system.domain.vo.TheoreticalLearningVO;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 进入课程
 */
public interface IEhmsService {
    PlatLesson lessonInfo(Long id);
    PlatLessonSon lessonSonInfo(Long id);

    //获取子课程列表
    List<PlatLessonSon> lessonSonList(Long id);

    //获取量表
    List<EhmsScaleVO> lessonScaleList(Long lessonSonId);

    //获取问卷
    List<PlatLessonScaleQuestion> lessonQuestionList(Long scaleSonId);
    EhmsQuestionVO lessonQuestionList(EhmsQuestionDto ehmsQuestionDto);

    //提交问卷
    EhmsAnswerVO subQuestion(EhmsAnswerDto ehmsAnswerDto);
    EhmsAnswerVO subQuestions(Map map);

    FoodClassificationVO foodClassificationGame(Long lessonId);

    TheoreticalLearningVO interveneHypertensionTheoreticalLearning(Long lessonId);

    HealthInformationCollectionVO healthInformationCollection(Long lessonId);

    TheoreticalLearningVO foodClassificationGameTheoreticalLearning(Long lessonId);

    List<Map<String, Object>> healthIntervention(Long lessonId);

    TheoreticalLearningVO nutritionalCateringCourse(Long lessonId);

    List<Map<String, Object>> nutritionalCateringIntervention(Long lessonId);

    TheoreticalLearningVO interveneCoronaryTheoreticalLearning(Long lessonId);


    HealthInformationCollectionVO healthInformationCoronaryCollection(Long lessonId);


    List<Map<String, Object>> healthCoronaryIntervention(Long lessonId);

    TheoreticalLearningVO dietEvaluationReportTheoreticalLearning(Long lessonId);

    List<Map<String, Object>> dietEvaluationReport(Long lessonId);

    TheoreticalLearningVO interveneStrokeTheoreticalLearning(Long lessonId);


    HealthInformationCollectionVO healthInformationStrokeCollection(Long lessonId);


    List<Map<String, Object>> healthStrokeIntervention(Long lessonId);

    TheoreticalLearningVO interveneDiabetesTheoreticalLearning(Long lessonId);

    HealthInformationCollectionVO healthInformationDiabetesCollection(Long lessonId);


    List<Map<String, Object>> healthDiabetesIntervention(Long lessonId);

    TheoreticalLearningVO interveneObesityTheoreticalLearning(Long lessonId);

    HealthInformationCollectionVO healthInformationObesityCollection(Long lessonId);


    List<Map<String, Object>> healthObesityIntervention(Long lessonId);

    TheoreticalLearningVO interveneCopdTheoreticalLearning(Long lessonId);


    HealthInformationCollectionVO healthInformationCopdCollection(Long lessonId);


    List<Map<String, Object>> healthCopdIntervention(Long lessonId);

    HealthInformationCollectionVO healthInformationOsteoporosisCollection(Long lessonId);

    List<Map<String, Object>> healthOsteoporosisIntervention(Long lessonId);

    TheoreticalLearningVO intervenePelvicFloorTheoreticalLearning(Long lessonId);

    HealthInformationCollectionVO healthInformationPelvicFloorCollection(Long lessonId);

    List<Map<String, Object>> healthPelvicFloorIntervention(Long lessonId);

    TheoreticalLearningVO foodLabelsTheoreticalLearning(Long lessonId);

    List<FoodLabelsVO> foodLabels(Long lessonId);

    TheoreticalLearningVO hypertensionFoodLabelsTheoreticalLearning(Long lessonId);

    List<FoodLabelsVO> hypertensionFoodLabels(Long lessonId);

    TheoreticalLearningVO diabetesFoodLabelsTheoreticalLearning(Long lessonId);

    List<FoodLabelsVO> diabetesFoodLabels(Long lessonId);

    PlatInterfereHealth interfereHealth(Long lessonId);

    List<PlatQuesSportStudy> getSportStudy(Long lessonId);
}
