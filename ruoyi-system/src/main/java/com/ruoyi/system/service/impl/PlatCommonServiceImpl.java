package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.system.domain.PlatUser;
import jxl.Cell;
import jxl.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.Mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public abstract class PlatCommonServiceImpl {

    //排序字段
    public String orderByClause = "create_time desc";
    public String orderByNumber = "serial_number asc";
    //模糊查询字符
    public String LIKE = "%";
    //角色分隔符
    public String AITT = "@";

    //=================用户角色=================
    //超级管理员
    public static int ROLE_SUPER_ADMIN = 1;
    //普通管理员
    public static int ROLE_ADMIN = 2;
    //教师
    public static int ROLE_TEACHER = 3;
    //学生
    public static int ROLE_STUDENT = 4;

    public static Map<Integer, String> roleMap;

    static {
        roleMap = new HashMap<>();
        roleMap.put(ROLE_SUPER_ADMIN, "超级管理员");
        roleMap.put(ROLE_ADMIN, "普通管理员");
        roleMap.put(ROLE_TEACHER, "教师");
        roleMap.put(ROLE_STUDENT, "学生");
    }

    //角色，教师多字段存储分隔符
    public static String IDENTIFICATION = "@";
    public static String DOUHAO = ",";
    public static String XINGHAO = "*";

    //状态
    public static Integer STATE_DELETE = -1;
    public static Integer STATE_END = 1;

    //默认密码
    public String DEFAULT_PASSWORD = "111111";

    private static final Logger log = LoggerFactory.getLogger(PlatCommonServiceImpl.class);

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 获取用户缓存信息
     */
    public PlatUser getLoginTokenUser() {
        try {
            return (PlatUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new ServiceException("获取用户信息异常", HttpStatus.UNAUTHORIZED);
        }
    }


    /**
     * 获取登录用户名
     */
    public String getTokenUser() {
        return getLoginTokenUser().getUserName();
    }


    //获取Excel总行数
    @Transactional
    public int getHang(jxl.Cell cell, Sheet sheet, int selectCols) {
        int res = 0;
        try {
            int rowsNum = 12; //得到行数
            for (int i = 0; i < rowsNum; i++) {//我的excel第一行是标题,所以 i从1开始  列 行


                String fs = cell.getContents();
                if (i == rowsNum) {
                    break;
                }
                if (fs != null && !fs.equals("")) {
                    res = i;
                } else {
                    continue;
                }
            }
        } catch (Exception e) {
            log.error("操作失败", e);
        }
        return res;
    }

    /**
     * 导入excel
     *
     * @param cell
     * @param sheet
     * @param name
     * @param allColsNum
     * @return
     */
    @Transactional
    public List<String> getExcelList(jxl.Cell cell, Sheet sheet, String name, int allColsNum) {
        //目标列
        int myCols = -1;
        for (int i = 0; i < allColsNum; i++) {
            cell = sheet.getCell(i, 0);
            String val = cell.getContents();
            if (name.equals(val)) {
                myCols = i;
                break;
            }
        }

        if (myCols == -1) {
            return new ArrayList<>();
        }

        List<String> list = new ArrayList<String>();

        //获取第1列所有的单元格
        Cell[] cellList = sheet.getColumn(myCols);
        for (int i = 0; i < cellList.length; i++) {
            String val = cellList[i].getContents();
            if (i == 0) {
                continue;
            } else {
                list.add(val);
            }
        }
        return list;
    }

    public static void error(String msg, Exception e) {
        log.error(msg, e);
        e.printStackTrace();
    }
}
