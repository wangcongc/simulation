package com.ruoyi.system.service.impl;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.PlatMajorVO;
import com.ruoyi.system.domain.vo.PlatTecturer;
import com.ruoyi.system.mapper.PlatCollegeMapper;
import com.ruoyi.system.mapper.PlatLessonMapper;
import com.ruoyi.system.mapper.PlatMajorMapper;
import com.ruoyi.system.service.IPlatMajorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 专业管理
 */
@Service
public class PlatMajorServiceImpl extends PlatCommonServiceImpl implements IPlatMajorService {

    @Autowired
    private PlatMajorMapper platMajorMapper;
    @Autowired
    private PlatCollegeMapper platCollegeMapper;
    @Autowired
    private PlatLessonMapper platLessonMapper;

    @Override
    public void addMajor(PlatMajor platMajor) {
        //校验名称是否重复
        PlatMajorExample example = new PlatMajorExample();
        PlatMajorExample.Criteria criteria = example.createCriteria();
        criteria.andNameEqualTo(platMajor.getName());
        List<PlatMajor> list = platMajorMapper.selectByExample(example);
        if (platMajor.getId() != null && list.size() > 0 && !String.valueOf(list.get(0).getId()).equals(String.valueOf(platMajor.getId()))) {
            throw new BusinessException("专业名称已存在!");
        } else if (platMajor.getId() == null && list.size() > 0) {
            throw new BusinessException("专业名称已存在!");
        }

        if (platMajor.getId() != null) {
            platMajor.setUpdateTime(new Date());
            platMajorMapper.updateByPrimaryKeySelective(platMajor);
            return;
        }
        platMajor.setCreateTime(new Date());
        platMajorMapper.insert(platMajor);
    }

    @Override
    public List<PlatMajor> majorList(PlatMajor platMajor) {
        PlatMajorExample platCollegeExample = new PlatMajorExample();
        platCollegeExample.setOrderByClause(orderByClause);
        if (!StringUtils.isEmpty(platMajor.getName())) {
            PlatMajorExample.Criteria criteria = platCollegeExample.createCriteria();
            criteria.andNameLike(LIKE + platMajor.getName() + LIKE);
        }
        List<PlatMajor> list = platMajorMapper.selectByExample(platCollegeExample);

        for (PlatMajor platMajorPO : list) {
            PlatCollege platCollege = platCollegeMapper.selectByPrimaryKey(platMajorPO.getCollegeId());
            if (platCollege != null) {
                platMajorPO.setCollegeName(platCollege.getName());
            }
        }
        return list;
    }

    @Override
    public void delMajor(Long id) {
        platMajorMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<PlatMajorVO> lecturerMajorList(Long lessonId,Long collegeId) {
        //返回集合
        List<PlatMajorVO> resultList = new ArrayList<>();

        PlatLesson platLesson = platLessonMapper.selectByPrimaryKey(lessonId);

        PlatMajorExample platMajorExample = new PlatMajorExample();
        PlatMajorExample.Criteria criteria = platMajorExample.createCriteria();
        criteria.andCollegeIdEqualTo(collegeId);
        List<PlatMajor> list = platMajorMapper.selectByExample(platMajorExample);
        for (PlatMajor platMajor : list) {
            PlatMajorVO platMajorVO = new PlatMajorVO();
            platMajorVO.setName(platMajor.getName());
            platMajorVO.setValue(platMajor.getId());
            platMajorVO.setSelected(false);
            if (platLesson != null && platLesson.getMajors() != null && platLesson.getMajors().contains(IDENTIFICATION + platMajor.getId() + IDENTIFICATION)) {
                platMajorVO.setSelected(true);
            }
            resultList.add(platMajorVO);
        }
        return resultList;
    }
}
