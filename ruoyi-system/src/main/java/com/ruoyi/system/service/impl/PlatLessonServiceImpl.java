package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.LessonFullCalendarVO;
import com.ruoyi.system.domain.vo.LessonHomeVO;
import com.ruoyi.system.domain.vo.LessonScheduleVO;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IPlatLessonService;
import com.ruoyi.system.service.IPlatUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程管理
 */
@Service
public class PlatLessonServiceImpl extends PlatCommonServiceImpl implements IPlatLessonService {

    @Autowired
    private PlatMajorMapper platMajorMapper;
    @Autowired
    private PlatCollegeMapper platCollegeMapper;
    @Autowired
    private IPlatUserService platUserService;
    @Autowired
    private PlatUserMapper platUserMapper;
    @Autowired
    private PlatLessonMapper platLessonMapper;
    @Autowired
    private PlatLessonChapterTypeMapper platLessonChapterTypeMapper;
    @Autowired
    private PlatLessonScheduleMapper platLessonScheduleMapper;
    @Autowired
    private PlatClassMapper platClassMapper;
    @Autowired
    private PlatLessonStudentMapper platLessonStudentMapper;
    @Autowired
    private PlatLessonTypeMapper platLessonTypeMapper;

    @Override
    public void addLesson(PlatLesson platLesson) {
        //校验名称是否重复
        PlatLessonExample example = new PlatLessonExample();
        PlatLessonExample.Criteria criteria = example.createCriteria();
        criteria.andNameEqualTo(platLesson.getName());
        List<PlatLesson> list = platLessonMapper.selectByExample(example);
        if (platLesson.getId() != null && list.size() > 0 && !String.valueOf(list.get(0).getId()).equals(String.valueOf(platLesson.getId()))) {
            throw new BusinessException("课程名称已存在!");
        } else if (platLesson.getId() == null && list.size() > 0) {
            throw new BusinessException("课程名称已存在!");
        }

        if (!StringUtils.isEmpty(platLesson.getTeachers())) {
            String[] arrays = platLesson.getTeachers().split(DOUHAO);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(IDENTIFICATION);
            for (String teacherId : arrays) {
                stringBuffer.append(teacherId);
                stringBuffer.append(IDENTIFICATION);
            }
            platLesson.setTeachers(stringBuffer.toString());
        }

        if (!StringUtils.isEmpty(platLesson.getMajors())) {
            String[] arrays = platLesson.getMajors().split(DOUHAO);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(IDENTIFICATION);
            for (String majorId : arrays) {
                stringBuffer.append(majorId);
                stringBuffer.append(IDENTIFICATION);
            }
            platLesson.setMajors(stringBuffer.toString());
        }


        if (platLesson.getId() != null) {
            platLesson.setUpdateTime(new Date());
            platLessonMapper.updateByPrimaryKeySelective(platLesson);
            return;
        }
        platLesson.setCreateTime(new Date());
        platLessonMapper.insert(platLesson);
    }

    @Override
    public List<PlatLesson> lessonList(PlatLesson platLesson) {
        PlatLessonExample platLessonExample = new PlatLessonExample();
        platLessonExample.setOrderByClause("type_id," + orderByNumber);
        PlatLessonExample.Criteria criteria = platLessonExample.createCriteria();
        if (!StringUtils.isEmpty(platLesson.getName())) {
            criteria.andNameLike(LIKE + platLesson.getName() + LIKE);
        }
        if (platLesson.getTypeId() != null) {
            criteria.andTypeIdEqualTo(platLesson.getTypeId());
        }else{
            //你的意思应该是 点课程列表那里 先不出来全部的课程列表 先选个模块 再出来列表.现在是点课程列表 会出来全部的课程 你再根据条件筛查一下才可以
            criteria.andTypeIdEqualTo(-1L);
        }
        List<PlatLesson> list = platLessonMapper.selectByExample(platLessonExample);

        for (PlatLesson platLessonPO : list) {
            PlatLessonType platLessonType = platLessonTypeMapper.selectByPrimaryKey(platLessonPO.getTypeId());
            platLessonPO.setTypeName(platLessonType == null ? "" : platLessonType.getName());

            PlatCollege platCollege = platCollegeMapper.selectByPrimaryKey(platLessonPO.getCollegeId());
            if (platCollege != null) {
                platLessonPO.setCollegeName(platCollege.getName());
            }

            String teacher = "";
            if (!StringUtils.isEmpty(platLessonPO.getTeachers())) {
                String[] arrays = platLessonPO.getTeachers().split(IDENTIFICATION);
                for (String teacherId : arrays) {
                    try {
                        if (StringUtils.isEmpty(teacherId)) {
                            continue;
                        }
                        PlatUser platUser = platUserMapper.selectByPrimaryKey(Long.parseLong(teacherId));
                        if (platUser != null) {
                            if (StringUtils.isEmpty(teacher)) {
                                teacher = platUser.getNickName();
                            } else {
                                teacher += DOUHAO + platUser.getNickName();
                            }
                        }
                    } catch (Exception e) {
                        error("查询授课教师失败", e);
                    }
                }

            }
            platLessonPO.setTeachers(teacher);

            String majors = "";
            if (!StringUtils.isEmpty(platLessonPO.getMajors())) {
                String[] arrays = platLessonPO.getMajors().split(IDENTIFICATION);
                for (String majorId : arrays) {
                    try {
                        if(StringUtils.isEmpty(majorId)){
                            continue;
                        }

                        PlatMajor platMajor = platMajorMapper.selectByPrimaryKey(Long.parseLong(majorId));
                        if (platMajor != null) {
                            if (StringUtils.isEmpty(majors)) {
                                majors = platMajor.getName();
                            } else {
                                majors += DOUHAO + platMajor.getName();
                            }
                        }
                    } catch (Exception e) {
                        error("查询适用专业失败", e);
                    }
                }

            }
            platLessonPO.setMajors(majors);

            PlatLessonChapterTypeExample platLessonChapterTypeExample = new PlatLessonChapterTypeExample();
            PlatLessonChapterTypeExample.Criteria platLessonChapterTypeCriteria = platLessonChapterTypeExample.createCriteria();
            platLessonChapterTypeCriteria.andLessonIdEqualTo(platLessonPO.getId());
            platLessonPO.setChapterTypeNum(platLessonChapterTypeMapper.selectByExample(platLessonChapterTypeExample).size());
        }
        return list;
    }

    @Override
    public void delLesson(Long id) {
        platLessonMapper.deleteByPrimaryKey(id);

        if(true){
            PlatLessonScheduleExample example = new PlatLessonScheduleExample();
            PlatLessonScheduleExample.Criteria criteria = example.createCriteria();
            criteria.andLessonIdEqualTo(id);
            platLessonScheduleMapper.deleteByExample(example);
        }
        if(true){
            PlatLessonStudentExample example = new PlatLessonStudentExample();
            PlatLessonStudentExample.Criteria criteria = example.createCriteria();
            criteria.andLessonIdEqualTo(id);
            platLessonStudentMapper.deleteByExample(example);
        }
    }

    @Override
    public LessonHomeVO lessonHome(HttpServletRequest request) {
        LessonHomeVO lessonHomeVO = new LessonHomeVO();

        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        if (loginUser.getRole() == ROLE_STUDENT) {
            //未开始的课程
            List<PlatLesson> list = myStudentLessonListData(myStudentLessonList(new PlatLesson(), loginUser));
            for (PlatLesson platLesson : list) {
                if (platLesson.getStudentStateCountans() == 1) {
                    //已结束
                    lessonHomeVO.setEndLessonNum(lessonHomeVO.getEndLessonNum() + 1);
                } else {
                    //未开始
                    lessonHomeVO.setNotStartLessonNum(lessonHomeVO.getNotStartLessonNum() + 1);

                    //本周课程
                    try {
                        Date date = new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).parse(platLesson.getLessonTime());
                        if (date.getTime() >= DateUtils.getWeekTime()[0].getTime() && date.getTime() <= DateUtils.getWeekTime()[1].getTime()) {
                            lessonHomeVO.setLessonWeekIngNum(lessonHomeVO.getLessonWeekIngNum() + 1);
                        }
                    } catch (ParseException e) {
                        error("时间转换失败", e);
                    }

                    //本月课程
                    try {
                        Date date = new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).parse(platLesson.getLessonTime());
                        if (date.getTime() >= DateUtils.getMonthTime()[0].getTime() && date.getTime() <= DateUtils.getMonthTime()[1].getTime()) {
                            lessonHomeVO.setLessonMonthIngNum(lessonHomeVO.getLessonMonthIngNum() + 1);
                        }
                    } catch (ParseException e) {
                        error("时间转换失败", e);
                    }
                }
            }
        }

        //当前教师的课程
        if (loginUser.getRole() == ROLE_TEACHER) {
            PlatLessonExample example = new PlatLessonExample();
            PlatLessonExample.Criteria criteria = example.createCriteria();
            criteria.andTeachersLike(LIKE + loginUser.getId() + LIKE);
            List<PlatLesson> list = platLessonMapper.selectByExample(example);
            //教师课程
            lessonHomeVO.setAllLessonNum(list.size());
        }

        //设置的课程
        if (loginUser.getRole() == ROLE_TEACHER) {
            PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
            PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
            exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
            List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);
            lessonHomeVO.setLessonIngNum(lessonScheduleList.size());
        }

        //设置的课程-本周课程
        if (true) {
            // 本周结束时间
            Date endTime = DateUtils.getWeekTime()[1];

            if (loginUser.getRole() == ROLE_TEACHER) {
                PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
                PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
                exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
                exampleCriteria.andLessonTimeBetween(new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).format(DateUtils.getWeekTime()[0]), new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).format(endTime));
                List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);
                lessonHomeVO.setLessonWeekIngNum(lessonScheduleList.size());
            }
        }

        //设置的课程-本月课程
        if (true) {
            Date endTime = DateUtils.getMonthTime()[1];

            if (loginUser.getRole() == ROLE_TEACHER) {
                PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
                PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
                exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
                exampleCriteria.andLessonTimeBetween(new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).format(DateUtils.getMonthTime()[0]), new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).format(endTime));
                List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);
                lessonHomeVO.setLessonMonthIngNum(lessonScheduleList.size());
            }
        }

        //学院总数
        if (true) {
            PlatCollegeExample example = new PlatCollegeExample();
            lessonHomeVO.setCollegeNum(platCollegeMapper.selectByExample(example).size());
        }

        //专业总数
        if (true) {
            PlatMajorExample example = new PlatMajorExample();
            lessonHomeVO.setMajorNum(platMajorMapper.selectByExample(example).size());
        }

        //班级总数
        if (true) {
            PlatClassExample example = new PlatClassExample();
            lessonHomeVO.setClassNum(platClassMapper.selectByExample(example).size());
        }

        //学院人数
        if (true) {
            PlatUserExample example = new PlatUserExample();
            lessonHomeVO.setUserNum(platUserMapper.selectByExample(example).size());
        }

        //超管人数
        if (loginUser.getRole() == ROLE_SUPER_ADMIN || loginUser.getRole() == ROLE_ADMIN) {
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_SUPER_ADMIN);
            lessonHomeVO.setSuperNum(platUserMapper.selectByExample(example).size());
        }

        //普管人数
        if (loginUser.getRole() == ROLE_SUPER_ADMIN || loginUser.getRole() == ROLE_ADMIN) {
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_ADMIN);
            lessonHomeVO.setAdminNum(platUserMapper.selectByExample(example).size());
        }

        //教师人数
        if (loginUser.getRole() == ROLE_SUPER_ADMIN || loginUser.getRole() == ROLE_ADMIN) {
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_TEACHER);
            lessonHomeVO.setTeacherNum(platUserMapper.selectByExample(example).size());
        }

        //学生人数
        if (loginUser.getRole() == ROLE_SUPER_ADMIN || loginUser.getRole() == ROLE_ADMIN) {
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_STUDENT);
            lessonHomeVO.setStudentNum(platUserMapper.selectByExample(example).size());
        }
        return lessonHomeVO;
    }

    @Override
    public List<PlatLessonSchedule> myLessonScheduleList(PlatUser platUser, PlatUser loginUser) {
        PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
        PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
        exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
        if (platUser.getClassId() != null) {
            exampleCriteria.andClassIdEqualTo(platUser.getClassId());
        }
        List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);
        return lessonScheduleList;
    }

    @Override
    public PlatTableDataInfo myLessonScheduleListData(PlatTableDataInfo platTableDataInfo) {
        List<LessonScheduleVO> result = new ArrayList<>();
        List<PlatLessonSchedule> platLessonScheduleList = (List<PlatLessonSchedule>) platTableDataInfo.getData();
        for (PlatLessonSchedule platLessonSchedule : platLessonScheduleList) {
            PlatLesson platLessonPO = platLessonMapper.selectByPrimaryKey(platLessonSchedule.getLessonId());
            if(platLessonPO==null){
                continue;
            }

            //课程信息
            LessonScheduleVO lessonScheduleVO = new LessonScheduleVO();
            lessonScheduleVO.setLessonId(platLessonSchedule.getLessonId());
            lessonScheduleVO.setLessonName(platLessonPO == null ? "" : platLessonPO.getName());
            lessonScheduleVO.setClassId(platLessonSchedule.getClassId());

            PlatClass platClass = platClassMapper.selectByPrimaryKey(platLessonSchedule.getClassId());
            if (platClass != null) {
                lessonScheduleVO.setClassName(platClass.getName());
            }

            lessonScheduleVO.setLessonTime(platLessonSchedule.getLessonTime());
            lessonScheduleVO.setScheduleId(platLessonSchedule.getId());

            PlatLessonType platLessonType = platLessonTypeMapper.selectByPrimaryKey(platLessonPO.getTypeId());
            platLessonPO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
            lessonScheduleVO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
            result.add(lessonScheduleVO);
        }
        platTableDataInfo.setData(result);
        return platTableDataInfo;
    }

    public List<LessonScheduleVO> myLessonScheduleListOld(PlatLesson platLesson, HttpServletRequest request) {
        List<LessonScheduleVO> result = new ArrayList<>();

        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        //当前教师的课程
        PlatLessonExample example = new PlatLessonExample();
        example.setOrderByClause("type_id," + orderByNumber);
        PlatLessonExample.Criteria criteria = example.createCriteria();
        criteria.andTeachersLike(LIKE + loginUser.getId() + LIKE);
        if (!StringUtils.isEmpty(platLesson.getName())) {
            criteria.andNameLike(LIKE + platLesson.getName() + LIKE);
        }
        List<PlatLesson> list = platLessonMapper.selectByExample(example);
        for (PlatLesson platPO : list) {
            PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
            PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
            exampleCriteria.andLessonIdEqualTo(platPO.getId());
            exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
            List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);

            //课程信息
            LessonScheduleVO scheduleVO = new LessonScheduleVO();
            scheduleVO.setLessonId(platPO.getId());
            scheduleVO.setLessonName(platPO.getName());
            PlatLessonType platLessonType = platLessonTypeMapper.selectByPrimaryKey(platPO.getTypeId());
            scheduleVO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
            result.add(scheduleVO);

            for (PlatLessonSchedule platLessonSchedule : lessonScheduleList) {
                //课程信息
                LessonScheduleVO lessonScheduleVO = new LessonScheduleVO();
                lessonScheduleVO.setLessonId(platPO.getId());
                lessonScheduleVO.setLessonName(platPO.getName());
                lessonScheduleVO.setClassId(platLessonSchedule.getClassId());

                PlatClass platClass = platClassMapper.selectByPrimaryKey(platLessonSchedule.getClassId());
                if (platClass != null) {
                    lessonScheduleVO.setClassName(platClass.getName());
                }

                lessonScheduleVO.setLessonTime(platLessonSchedule.getLessonTime());
                lessonScheduleVO.setScheduleId(platLessonSchedule.getId());

                lessonScheduleVO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
                result.add(lessonScheduleVO);
            }
        }
        return result;
    }

    @Override
    public List<PlatLesson> myLessonList(PlatLesson platLesson, HttpServletRequest request,PlatUser platUser) {
        //当前教师的课程
        PlatLessonExample example = new PlatLessonExample();
        PlatLessonExample.Criteria criteria = example.createCriteria();
        criteria.andTeachersLike(LIKE + platUser.getId() + LIKE);
        if (!StringUtils.isEmpty(platLesson.getName())) {
            criteria.andNameLike(LIKE + platLesson.getName() + LIKE);
        }
        if (platLesson.getTypeId() != null) {
            criteria.andTypeIdEqualTo(platLesson.getTypeId());
        }
        example.setOrderByClause("type_id," + orderByNumber);
        List<PlatLesson> list = platLessonMapper.selectByExample(example);
        for (PlatLesson platLessonPO : list) {
            PlatLessonType platLessonType = platLessonTypeMapper.selectByPrimaryKey(platLessonPO.getTypeId());
            platLessonPO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
        }
        return list;
    }

    @Override
    public List<PlatLessonStudent> myStudentLessonList(PlatLesson platLesson, PlatUser loginUser) {
        //当前学生的课程
        PlatLessonStudentExample example = new PlatLessonStudentExample();
        PlatLessonStudentExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(loginUser.getId());
        /*if(!StringUtils.isEmpty(platLesson.getName())){
            criteria.an(LIKE + loginUser.getId() + LIKE);
        }*/
        if (platLesson.getStudentStateCountans() != null) {
            criteria.andStateEqualTo(platLesson.getStudentStateCountans());
        } else {
            criteria.andStateNotEqualTo(STATE_DELETE);
        }
        List<PlatLessonStudent> list = platLessonStudentMapper.selectByExample(example);

        for (PlatLessonStudent lessonStudent : list) {
            PlatLesson platLessonPO = platLessonMapper.selectByPrimaryKey(lessonStudent.getLessonId());
            lessonStudent.setLessonName(platLessonPO == null ? "" : platLessonPO.getName());
        }
        return list;
    }

    public List<PlatLesson> myStudentLessonListData(List<PlatLessonStudent> lessonStudentList) {
        List<PlatLesson> lessonList = new ArrayList<>();
        for (PlatLessonStudent platLessonStudent : lessonStudentList) {
            PlatLesson platLessonPO = platLessonMapper.selectByPrimaryKey(platLessonStudent.getLessonId());
            if (platLessonPO != null) {
                PlatLessonSchedule platLessonSchedule = platLessonScheduleMapper.selectByPrimaryKey(platLessonStudent.getScheduleId());
                if (platLessonSchedule != null) {
                    platLessonPO.setLessonTime(platLessonSchedule.getLessonTime());
                }
                platLessonPO.setPlatLessonStudentId(platLessonStudent.getId());
                platLessonPO.setStudentState(platLessonStudent.getState() == 1 ? "已结束" : "未开始");
                platLessonPO.setStudentStateCountans(platLessonStudent.getState());
                PlatLessonType platLessonType = platLessonTypeMapper.selectByPrimaryKey(platLessonPO.getTypeId());
                platLessonPO.setTypeName(platLessonType == null ? "" : platLessonType.getName());
                lessonList.add(platLessonPO);
            }
        }
        return lessonList;
    }

    @Override
    public void addLessonSchedule(PlatLessonSchedule platLessonSchedule, HttpServletRequest request) {
        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        platLessonSchedule.setTeacherId(loginUser.getId());

        if (platLessonSchedule.getId() == null) {
            platLessonSchedule.setCreateTime(new Date());
            platLessonScheduleMapper.insert(platLessonSchedule);

            //查询当前班级的学生
            PlatUserExample example = new PlatUserExample();
            PlatUserExample.Criteria exampleCriteria = example.createCriteria();
            exampleCriteria.andRoleEqualTo(ROLE_STUDENT);
            exampleCriteria.andClassIdEqualTo(platLessonSchedule.getClassId());
            List<PlatUser> list = platUserMapper.selectByExample(example);
            for (PlatUser platUser : list) {
                PlatLessonStudent platLessonStudent = new PlatLessonStudent();
                platLessonStudent.setLessonId(platLessonSchedule.getLessonId());
                platLessonStudent.setUserId(platUser.getId());
                platLessonStudent.setState(0);
                platLessonStudent.setCreateTime(new Date());
                platLessonStudent.setScheduleId(platLessonSchedule.getId());
                platLessonStudent.setClassId(platUser.getClassId());
                platLessonStudent.setScore("");
                platLessonStudent.setCompleteTime("");
                platLessonStudentMapper.insert(platLessonStudent);
            }
        } else {
            platLessonSchedule.setUpdateTime(new Date());
            platLessonScheduleMapper.updateByPrimaryKeySelective(platLessonSchedule);
        }
    }

    @Override
    public void delLessonSchedule(Long id) {
        platLessonScheduleMapper.deleteByPrimaryKey(id);

        PlatLessonStudentExample example = new PlatLessonStudentExample();
        PlatLessonStudentExample.Criteria exampleCriteria = example.createCriteria();
        exampleCriteria.andScheduleIdEqualTo(id);
        platLessonStudentMapper.deleteByExample(example);
    }

    @Override
    public List<LessonFullCalendarVO> myLessonFullCalendarList(HttpServletRequest request) {
        List<LessonFullCalendarVO> result = new ArrayList<>();

        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        if (loginUser.getRole() == ROLE_STUDENT) {
            return myLessonFullCalendarListByStudent(request);
        }

        //当前教师的课程
        PlatLessonExample example = new PlatLessonExample();
        PlatLessonExample.Criteria criteria = example.createCriteria();
        criteria.andTeachersLike(LIKE + loginUser.getId() + LIKE);
        List<PlatLesson> list = platLessonMapper.selectByExample(example);
        for (PlatLesson platPO : list) {
            PlatLessonScheduleExample platLessonScheduleExample = new PlatLessonScheduleExample();
            PlatLessonScheduleExample.Criteria exampleCriteria = platLessonScheduleExample.createCriteria();
            exampleCriteria.andLessonIdEqualTo(platPO.getId());
            exampleCriteria.andTeacherIdEqualTo(loginUser.getId());
            List<PlatLessonSchedule> lessonScheduleList = platLessonScheduleMapper.selectByExample(platLessonScheduleExample);

            for (PlatLessonSchedule platLessonSchedule : lessonScheduleList) {
                PlatClass platClass = platClassMapper.selectByPrimaryKey(platLessonSchedule.getClassId());
                if (platClass == null) {
                    continue;
                }
                LessonFullCalendarVO lessonFullCalendarVO = new LessonFullCalendarVO();
                String className = platClass.getName();
                String lessonName = platPO.getName();
                lessonFullCalendarVO.setTitle(className + "(" + lessonName + ")");
                lessonFullCalendarVO.setStart(platLessonSchedule.getLessonTime());
                lessonFullCalendarVO.setUrl("");
                result.add(lessonFullCalendarVO);
            }
        }
        return result;
    }

    public List<LessonFullCalendarVO> myLessonFullCalendarListByStudent(HttpServletRequest request) {
        List<LessonFullCalendarVO> result = new ArrayList<>();

        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);

        PlatLesson platLessonVO = new PlatLesson();
        platLessonVO.setStudentStateCountans(0);

        List<PlatLessonStudent> lessonStudentList = myStudentLessonList(platLessonVO, loginUser);
        List<PlatLesson> list = myStudentLessonListData(lessonStudentList);

        for (PlatLesson platLesson : list) {
            LessonFullCalendarVO lessonFullCalendarVO = new LessonFullCalendarVO();
            String lessonName = platLesson.getName();
            lessonFullCalendarVO.setTitle(lessonName);
            lessonFullCalendarVO.setStart(platLesson.getLessonTime());
            lessonFullCalendarVO.setUrl("");
            result.add(lessonFullCalendarVO);
        }
        return result;
    }

    @Override
    public void endLesson(Long id) {
        PlatLessonStudent platLessonStudent = platLessonStudentMapper.selectByPrimaryKey(id);
        if (platLessonStudent != null) {
            platLessonStudent.setState(platLessonStudent.getState() == null || platLessonStudent.getState() == 0 ? 1 : 0);
            platLessonStudentMapper.updateByPrimaryKey(platLessonStudent);
        }
    }

    @Override
    public List<PlatLessonType> lessonTypeList(PlatLessonType platLessonType) {
        //当前教师的课程
        PlatLessonTypeExample example = new PlatLessonTypeExample();
        PlatLessonTypeExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(platLessonType.getName())) {
            criteria.andNameLike(LIKE + platLessonType.getName() + LIKE);
        }
        example.setOrderByClause("id asc");
        List<PlatLessonType> list = platLessonTypeMapper.selectByExample(example);
        return list;
    }

    @Override
    public void lessonInScore(PlatLessonStudent platLessonStudent, HttpServletRequest request) {
        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        if (loginUser.getRole() != ROLE_STUDENT) {
            return;
        }

        if (platLessonStudent.getLessonId() == null) {
            throw new BusinessException("班级名称不能为空");
        }
        if (StringUtils.isEmpty(platLessonStudent.getScore())) {
            throw new BusinessException("分数不能为空");
        }

        PlatLessonStudentExample example = new PlatLessonStudentExample();
        PlatLessonStudentExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(loginUser.getId());
        criteria.andLessonIdEqualTo(platLessonStudent.getLessonId());
        criteria.andStateNotEqualTo(STATE_DELETE);
        List<PlatLessonStudent> list = platLessonStudentMapper.selectByExample(example);
        for (PlatLessonStudent lessonStudent : list) {
            lessonStudent.setScore(platLessonStudent.getScore());
            lessonStudent.setCompleteTime(new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS).format(new Date()));
            lessonStudent.setState(1);
            platLessonStudentMapper.updateByPrimaryKey(lessonStudent);
        }
    }
}
