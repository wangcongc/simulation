package com.ruoyi.system.service;

import com.ruoyi.system.domain.PlatCollege;

import java.util.List;

/**
 * 学院管理
 */
public interface IPlatCollegeService {
    //添加学院
    void addCollege(PlatCollege platCollege);

    //学院列表
    List<PlatCollege> collegeList(PlatCollege platCollege);

    //删除学院
    void delCollege(Long id);
}
