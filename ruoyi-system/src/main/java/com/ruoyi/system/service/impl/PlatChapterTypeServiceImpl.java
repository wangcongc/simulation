package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.PlatLessonChapterType;
import com.ruoyi.system.domain.PlatLessonChapterTypeExample;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IPlatChapterTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 章节类型管理
 */
@Service
public class PlatChapterTypeServiceImpl extends PlatCommonServiceImpl implements IPlatChapterTypeService {

    @Autowired
    private PlatMajorMapper platMajorMapper;
    @Autowired
    private PlatCollegeMapper platCollegeMapper;
    @Autowired
    private PlatLessonChapterTypeMapper platLessonChapterTypeMapper;
    @Autowired
    private PlatUserMapper platUserMapper;
    @Autowired
    private PlatLessonMapper platLessonMapper;

    @Override
    public void addChapterType(PlatLessonChapterType platLessonChapterType) {
        if (platLessonChapterType.getId() != null) {
            platLessonChapterType.setUpdateTime(new Date());
            platLessonChapterTypeMapper.updateByPrimaryKeySelective(platLessonChapterType);
            return;
        }

        platLessonChapterType.setCreateTime(new Date());
        platLessonChapterTypeMapper.insert(platLessonChapterType);
    }

    @Override
    public List<PlatLessonChapterType> chapterTypeList(PlatLessonChapterType platLessonChapterType) {
        PlatLessonChapterTypeExample platLessonExample = new PlatLessonChapterTypeExample();
        platLessonExample.setOrderByClause(orderByClause);
        if (platLessonChapterType.getLessonId() != null) {
            PlatLessonChapterTypeExample.Criteria criteria = platLessonExample.createCriteria();
            criteria.andLessonIdEqualTo(platLessonChapterType.getLessonId());
        }
        List<PlatLessonChapterType> list = platLessonChapterTypeMapper.selectByExample(platLessonExample);
        return list;
    }

    @Override
    public void delChapterType(Long id) {
        platLessonChapterTypeMapper.deleteByPrimaryKey(id);
    }
}
