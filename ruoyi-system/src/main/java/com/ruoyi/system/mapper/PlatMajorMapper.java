package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatMajor;
import com.ruoyi.system.domain.PlatMajorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatMajorMapper {
    long countByExample(PlatMajorExample example);

    int deleteByExample(PlatMajorExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatMajor row);

    int insertSelective(PlatMajor row);

    List<PlatMajor> selectByExample(PlatMajorExample example);

    PlatMajor selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatMajor row, @Param("example") PlatMajorExample example);

    int updateByExample(@Param("row") PlatMajor row, @Param("example") PlatMajorExample example);

    int updateByPrimaryKeySelective(PlatMajor row);

    int updateByPrimaryKey(PlatMajor row);

    int selectCollegeNum(Long collegeId);
}