package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatCollege;
import com.ruoyi.system.domain.PlatCollegeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatCollegeMapper {
    long countByExample(PlatCollegeExample example);

    int deleteByExample(PlatCollegeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatCollege row);

    int insertSelective(PlatCollege row);

    List<PlatCollege> selectByExample(PlatCollegeExample example);

    PlatCollege selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatCollege row, @Param("example") PlatCollegeExample example);

    int updateByExample(@Param("row") PlatCollege row, @Param("example") PlatCollegeExample example);

    int updateByPrimaryKeySelective(PlatCollege row);

    int updateByPrimaryKey(PlatCollege row);
}