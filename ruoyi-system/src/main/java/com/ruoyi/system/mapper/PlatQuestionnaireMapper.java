package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatQuestionnaire;
import com.ruoyi.system.domain.PlatQuestionnaireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatQuestionnaireMapper {
    long countByExample(PlatQuestionnaireExample example);

    int deleteByExample(PlatQuestionnaireExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatQuestionnaire row);

    int insertSelective(PlatQuestionnaire row);

    List<PlatQuestionnaire> selectByExample(PlatQuestionnaireExample example);

    PlatQuestionnaire selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatQuestionnaire row, @Param("example") PlatQuestionnaireExample example);

    int updateByExample(@Param("row") PlatQuestionnaire row, @Param("example") PlatQuestionnaireExample example);

    int updateByPrimaryKeySelective(PlatQuestionnaire row);

    int updateByPrimaryKey(PlatQuestionnaire row);
}