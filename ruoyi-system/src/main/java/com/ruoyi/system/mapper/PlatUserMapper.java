package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.PlatUser;
import com.ruoyi.system.domain.PlatUserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PlatUserMapper {
    long countByExample(PlatUserExample example);

    int deleteByExample(PlatUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatUser row);

    int insertSelective(PlatUser row);

    List<PlatUser> selectByExample(PlatUserExample example);

    PlatUser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatUser row, @Param("example") PlatUserExample example);

    int updateByExample(@Param("row") PlatUser row, @Param("example") PlatUserExample example);

    int updateByPrimaryKeySelective(PlatUser row);

    int updateByPrimaryKey(PlatUser row);

    PlatUser selectByNumber(String username);
}