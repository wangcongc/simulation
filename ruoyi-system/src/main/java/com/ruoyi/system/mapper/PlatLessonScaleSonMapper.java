package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonScaleSon;
import com.ruoyi.system.domain.PlatLessonScaleSonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonScaleSonMapper {
    long countByExample(PlatLessonScaleSonExample example);

    int deleteByExample(PlatLessonScaleSonExample example);

    int deleteByPrimaryKey(String name);

    int insert(PlatLessonScaleSon row);

    int insertSelective(PlatLessonScaleSon row);

    List<PlatLessonScaleSon> selectByExample(PlatLessonScaleSonExample example);

    PlatLessonScaleSon selectByPrimaryKey(String name);

    int updateByExampleSelective(@Param("row") PlatLessonScaleSon row, @Param("example") PlatLessonScaleSonExample example);

    int updateByExample(@Param("row") PlatLessonScaleSon row, @Param("example") PlatLessonScaleSonExample example);

    int updateByPrimaryKeySelective(PlatLessonScaleSon row);

    int updateByPrimaryKey(PlatLessonScaleSon row);
}