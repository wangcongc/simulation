package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonChapterType;
import com.ruoyi.system.domain.PlatLessonChapterTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonChapterTypeMapper {
    long countByExample(PlatLessonChapterTypeExample example);

    int deleteByExample(PlatLessonChapterTypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonChapterType row);

    int insertSelective(PlatLessonChapterType row);

    List<PlatLessonChapterType> selectByExample(PlatLessonChapterTypeExample example);

    PlatLessonChapterType selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonChapterType row, @Param("example") PlatLessonChapterTypeExample example);

    int updateByExample(@Param("row") PlatLessonChapterType row, @Param("example") PlatLessonChapterTypeExample example);

    int updateByPrimaryKeySelective(PlatLessonChapterType row);

    int updateByPrimaryKey(PlatLessonChapterType row);
}