package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonStudent;
import com.ruoyi.system.domain.PlatLessonStudentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonStudentMapper {
    long countByExample(PlatLessonStudentExample example);

    int deleteByExample(PlatLessonStudentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonStudent row);

    int insertSelective(PlatLessonStudent row);

    List<PlatLessonStudent> selectByExample(PlatLessonStudentExample example);

    PlatLessonStudent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonStudent row, @Param("example") PlatLessonStudentExample example);

    int updateByExample(@Param("row") PlatLessonStudent row, @Param("example") PlatLessonStudentExample example);

    int updateByPrimaryKeySelective(PlatLessonStudent row);

    int updateByPrimaryKey(PlatLessonStudent row);
}