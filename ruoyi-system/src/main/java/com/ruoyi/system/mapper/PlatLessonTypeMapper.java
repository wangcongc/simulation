package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonType;
import com.ruoyi.system.domain.PlatLessonTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonTypeMapper {
    long countByExample(PlatLessonTypeExample example);

    int deleteByExample(PlatLessonTypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonType row);

    int insertSelective(PlatLessonType row);

    List<PlatLessonType> selectByExample(PlatLessonTypeExample example);

    PlatLessonType selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonType row, @Param("example") PlatLessonTypeExample example);

    int updateByExample(@Param("row") PlatLessonType row, @Param("example") PlatLessonTypeExample example);

    int updateByPrimaryKeySelective(PlatLessonType row);

    int updateByPrimaryKey(PlatLessonType row);
}