package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonScaleQuestion;
import com.ruoyi.system.domain.PlatLessonScaleQuestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonScaleQuestionMapper {
    long countByExample(PlatLessonScaleQuestionExample example);

    int deleteByExample(PlatLessonScaleQuestionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonScaleQuestion row);

    int insertSelective(PlatLessonScaleQuestion row);

    List<PlatLessonScaleQuestion> selectByExample(PlatLessonScaleQuestionExample example);

    PlatLessonScaleQuestion selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonScaleQuestion row, @Param("example") PlatLessonScaleQuestionExample example);

    int updateByExample(@Param("row") PlatLessonScaleQuestion row, @Param("example") PlatLessonScaleQuestionExample example);

    int updateByPrimaryKeySelective(PlatLessonScaleQuestion row);

    int updateByPrimaryKey(PlatLessonScaleQuestion row);
}