package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonChapter;
import com.ruoyi.system.domain.PlatLessonChapterExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonChapterMapper {
    long countByExample(PlatLessonChapterExample example);

    int deleteByExample(PlatLessonChapterExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonChapter row);

    int insertSelective(PlatLessonChapter row);

    List<PlatLessonChapter> selectByExample(PlatLessonChapterExample example);

    PlatLessonChapter selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonChapter row, @Param("example") PlatLessonChapterExample example);

    int updateByExample(@Param("row") PlatLessonChapter row, @Param("example") PlatLessonChapterExample example);

    int updateByPrimaryKeySelective(PlatLessonChapter row);

    int updateByPrimaryKey(PlatLessonChapter row);
}