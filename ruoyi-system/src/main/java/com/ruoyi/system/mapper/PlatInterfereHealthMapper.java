package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatInterfereHealth;
import com.ruoyi.system.domain.PlatInterfereHealthExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatInterfereHealthMapper {
    long countByExample(PlatInterfereHealthExample example);

    int deleteByExample(PlatInterfereHealthExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatInterfereHealth row);

    int insertSelective(PlatInterfereHealth row);

    List<PlatInterfereHealth> selectByExample(PlatInterfereHealthExample example);

    PlatInterfereHealth selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatInterfereHealth row, @Param("example") PlatInterfereHealthExample example);

    int updateByExample(@Param("row") PlatInterfereHealth row, @Param("example") PlatInterfereHealthExample example);

    int updateByPrimaryKeySelective(PlatInterfereHealth row);

    int updateByPrimaryKey(PlatInterfereHealth row);
}