package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatQuesSportStudy;
import com.ruoyi.system.domain.PlatQuesSportStudyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatQuesSportStudyMapper {
    long countByExample(PlatQuesSportStudyExample example);

    int deleteByExample(PlatQuesSportStudyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatQuesSportStudy row);

    int insertSelective(PlatQuesSportStudy row);

    List<PlatQuesSportStudy> selectByExample(PlatQuesSportStudyExample example);

    PlatQuesSportStudy selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatQuesSportStudy row, @Param("example") PlatQuesSportStudyExample example);

    int updateByExample(@Param("row") PlatQuesSportStudy row, @Param("example") PlatQuesSportStudyExample example);

    int updateByPrimaryKeySelective(PlatQuesSportStudy row);

    int updateByPrimaryKey(PlatQuesSportStudy row);
}