package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLesson;
import com.ruoyi.system.domain.PlatLessonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonMapper {
    long countByExample(PlatLessonExample example);

    int deleteByExample(PlatLessonExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLesson row);

    int insertSelective(PlatLesson row);

    List<PlatLesson> selectByExample(PlatLessonExample example);

    PlatLesson selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLesson row, @Param("example") PlatLessonExample example);

    int updateByExample(@Param("row") PlatLesson row, @Param("example") PlatLessonExample example);

    int updateByPrimaryKeySelective(PlatLesson row);

    int updateByPrimaryKey(PlatLesson row);
}