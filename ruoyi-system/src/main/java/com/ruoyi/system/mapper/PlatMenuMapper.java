package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatMenu;
import com.ruoyi.system.domain.PlatMenuExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatMenuMapper {
    long countByExample(PlatMenuExample example);

    int deleteByExample(PlatMenuExample example);

    int deleteByPrimaryKey(Long menuId);

    int insert(PlatMenu row);

    int insertSelective(PlatMenu row);

    List<PlatMenu> selectByExample(PlatMenuExample example);

    PlatMenu selectByPrimaryKey(Long menuId);

    int updateByExampleSelective(@Param("row") PlatMenu row, @Param("example") PlatMenuExample example);

    int updateByExample(@Param("row") PlatMenu row, @Param("example") PlatMenuExample example);

    int updateByPrimaryKeySelective(PlatMenu row);

    int updateByPrimaryKey(PlatMenu row);

    List<PlatMenu> selectMenu(@Param("parentId") Long parentId , @Param("roles") String roles);

}