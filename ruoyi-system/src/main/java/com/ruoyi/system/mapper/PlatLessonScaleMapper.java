package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonScale;
import com.ruoyi.system.domain.PlatLessonScaleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonScaleMapper {
    long countByExample(PlatLessonScaleExample example);

    int deleteByExample(PlatLessonScaleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonScale row);

    int insertSelective(PlatLessonScale row);

    List<PlatLessonScale> selectByExample(PlatLessonScaleExample example);

    PlatLessonScale selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonScale row, @Param("example") PlatLessonScaleExample example);

    int updateByExample(@Param("row") PlatLessonScale row, @Param("example") PlatLessonScaleExample example);

    int updateByPrimaryKeySelective(PlatLessonScale row);

    int updateByPrimaryKey(PlatLessonScale row);
}