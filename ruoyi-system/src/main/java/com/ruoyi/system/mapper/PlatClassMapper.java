package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatClass;
import com.ruoyi.system.domain.PlatClassExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatClassMapper {
    long countByExample(PlatClassExample example);

    int deleteByExample(PlatClassExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatClass row);

    int insertSelective(PlatClass row);

    List<PlatClass> selectByExample(PlatClassExample example);

    PlatClass selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatClass row, @Param("example") PlatClassExample example);

    int updateByExample(@Param("row") PlatClass row, @Param("example") PlatClassExample example);

    int updateByPrimaryKeySelective(PlatClass row);

    int updateByPrimaryKey(PlatClass row);
}