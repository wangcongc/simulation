package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonSchedule;
import com.ruoyi.system.domain.PlatLessonScheduleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonScheduleMapper {
    long countByExample(PlatLessonScheduleExample example);

    int deleteByExample(PlatLessonScheduleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonSchedule row);

    int insertSelective(PlatLessonSchedule row);

    List<PlatLessonSchedule> selectByExample(PlatLessonScheduleExample example);

    PlatLessonSchedule selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonSchedule row, @Param("example") PlatLessonScheduleExample example);

    int updateByExample(@Param("row") PlatLessonSchedule row, @Param("example") PlatLessonScheduleExample example);

    int updateByPrimaryKeySelective(PlatLessonSchedule row);

    int updateByPrimaryKey(PlatLessonSchedule row);
}