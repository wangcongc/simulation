package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PlatLessonSon;
import com.ruoyi.system.domain.PlatLessonSonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatLessonSonMapper {
    long countByExample(PlatLessonSonExample example);

    int deleteByExample(PlatLessonSonExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlatLessonSon row);

    int insertSelective(PlatLessonSon row);

    List<PlatLessonSon> selectByExample(PlatLessonSonExample example);

    PlatLessonSon selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") PlatLessonSon row, @Param("example") PlatLessonSonExample example);

    int updateByExample(@Param("row") PlatLessonSon row, @Param("example") PlatLessonSonExample example);

    int updateByPrimaryKeySelective(PlatLessonSon row);

    int updateByPrimaryKey(PlatLessonSon row);
}