package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.PlatMajor;
import com.ruoyi.system.service.IPlatMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 专业管理
 */
@RestController
@RequestMapping("/major")
public class MajorController extends BaseController {

    @Autowired
    private IPlatMajorService platMajorService;

    /**
     * 添加专业
     */
    @PostMapping("/addMajor")
    public AjaxResult addMajor(PlatMajor major) {
        platMajorService.addMajor(major);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 专业列表
     */
    @GetMapping("/majorList")
    public PlatTableDataInfo majorList(PlatMajor platMajor) {
        startPage();
        List<PlatMajor> list = platMajorService.majorList(platMajor);
        return getPlatDataTable(list);
    }

    /**
     * 专业分页列表
     */
    @GetMapping("/majorPageList")
    public PlatTableDataInfo majorPageList(PlatMajor platMajor) {
        startPage();
        List<PlatMajor> list = platMajorService.majorList(platMajor);
        return getPlatDataTable(list);
    }

    /**
     * 删除专业
     */
    @GetMapping("/delMajor/{id}")
    public AjaxResult delMajor(@PathVariable Long id) {
        platMajorService.delMajor(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 适用专业(课程)
     *
     * @return
     */
    @GetMapping("/lecturerMajorList/{lessonId}/{collegeId}")
    public AjaxResult lecturerLessonList(@PathVariable Long lessonId,@PathVariable Long collegeId) {
        AjaxResult ajax = AjaxResult.success(platMajorService.lecturerMajorList(lessonId,collegeId));
        return ajax;
    }
}
