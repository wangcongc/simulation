package com.ruoyi.web.controller.ehms;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.PlatLessonSon;
import com.ruoyi.system.domain.dto.EhmsAnswerDto;
import com.ruoyi.system.domain.dto.EhmsQuestionDto;
import com.ruoyi.system.domain.vo.EhmsQuestionVO;
import com.ruoyi.system.domain.vo.EhmsScaleVO;
import com.ruoyi.system.service.IEhmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 进入课程
 */
@Api("进入课程")
@RestController
@RequestMapping("/ehms")
public class EhmsController extends BaseController {

    @Autowired
    private IEhmsService ehmsService;

    /**
     * 获取课程信息
     */
    @ApiOperation("获取课程信息")
    @GetMapping("/lessonInfo/{lessonId}")
    public AjaxResult lessonInfo(@PathVariable Long lessonId) {
        AjaxResult ajax = AjaxResult.success(ehmsService.lessonInfo(lessonId));
        return ajax;
    }

    /**
     * 获取子课程信息
     */
    @ApiOperation("获取子课程信息")
    @GetMapping("/lessonSonInfo/{lessonSonId}")
    public AjaxResult lessonSonInfo(@PathVariable Long lessonSonId) {
        AjaxResult ajax = AjaxResult.success(ehmsService.lessonSonInfo(lessonSonId));
        return ajax;
    }

    /**
     * 获取子课程
     */
    @ApiOperation("获取子课程")
    @GetMapping("/lessonSonList/{lessonId}")
    public AjaxResult lessonSonList(@PathVariable Long lessonId) {
        List<PlatLessonSon> list = ehmsService.lessonSonList(lessonId);
        AjaxResult ajax = AjaxResult.success(list);
        return ajax;
    }

    /**
     * 获取量表
     */
    @ApiOperation("获取量表")
    @GetMapping("/lessonScaleList/{lessonSonId}")
    public AjaxResult lessonScaleList(@PathVariable Long lessonSonId) {
        List<EhmsScaleVO> list = ehmsService.lessonScaleList(lessonSonId);
        AjaxResult ajax = AjaxResult.success(list);
        return ajax;
    }

    /**
     * 获取量表问卷
     */
    @ApiOperation("获取量表问卷")
    @PostMapping("/lessonQuestionList")
    public AjaxResult lessonQuestionList(@RequestBody EhmsQuestionDto ehmsQuestionDto) {
        EhmsQuestionVO ehmsQuestionVO = ehmsService.lessonQuestionList(ehmsQuestionDto);
        AjaxResult ajax = AjaxResult.success(ehmsQuestionVO);
        return ajax;
    }

    /**
     * 提交问卷
     */
    @ApiOperation("提交问卷")
    @PostMapping("/subQuestion")
    public AjaxResult subQuestion(@RequestBody Map map) {
        AjaxResult ajax = AjaxResult.success(ehmsService.subQuestions(map));
        return ajax;
    }

    /**
     * 提交问卷
     */
    @ApiOperation("提交护理问卷")
    @PostMapping("/subCareQuestion")
    public AjaxResult subCareQuestion(@RequestBody Map map) {
        AjaxResult ajax = AjaxResult.success(ehmsService.subQuestions(map));
        return ajax;
    }

    /**
     * 食材分类游戏 理论学习
     */
    @ApiOperation("食材分类游戏接口 理论学习")
    @GetMapping("/foodClassificationGameTheoreticalLearning/{lessonId}")
    public AjaxResult foodClassificationGameTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.foodClassificationGameTheoreticalLearning(lessonId));
    }


    /**
     * 食材分类游戏 实训流程
     */
    @ApiOperation("食材分类游戏接口 实训流程")
    @GetMapping("/foodClassificationGame/{lessonId}")
    public AjaxResult foodClassificationGame(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.foodClassificationGame(lessonId));
    }

    /**
     * 慢性病饮食干预-高血压  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-高血压  理论学习接口")
    @GetMapping("/interveneHypertensionTheoreticalLearning/{lessonId}")
    public AjaxResult interveneHypertension(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneHypertensionTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-高血压  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-高血压  健康信息收集接口")
    @GetMapping("/healthInformationCollection/{lessonId}")
    public AjaxResult healthInformationCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-高血压  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-高血压  健康干预接口")
    @GetMapping("/healthIntervention/{lessonId}")
    public AjaxResult healthIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthIntervention(lessonId));
    }

    /**
     * 营养配餐课程  理论知识
     */
    @ApiOperation("营养配餐课程  理论知识接口")
    @GetMapping("/nutritionalCateringCourse/{lessonId}")
    public AjaxResult nutritionalCateringCourse(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.nutritionalCateringCourse(lessonId));
    }


    /**
     * 营养配餐课程  健康干预
     */
    @ApiOperation("营养配餐课程  健康干预接口")
    @GetMapping("/nutritionalCateringIntervention/{lessonId}")
    public AjaxResult nutritionalCateringIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.nutritionalCateringIntervention(lessonId));
    }



    /**
     * 慢性病饮食干预-冠心病  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-冠心病  理论学习接口")
    @GetMapping("/interveneCoronaryTheoreticalLearning/{lessonId}")
    public AjaxResult interveneCoronaryTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneCoronaryTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-冠心病  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-冠心病  健康信息收集接口")
    @GetMapping("/healthInformationCoronaryCollection/{lessonId}")
    public AjaxResult healthInformationCoronaryCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationCoronaryCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-冠心病  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-冠心病  健康干预接口")
    @GetMapping("/healthCoronaryIntervention/{lessonId}")
    public AjaxResult healthCoronaryIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthCoronaryIntervention(lessonId));
    }

    /**
     * 饮食评价报告模块  学习及咨询模块接口
     */
    @ApiOperation("饮食评价报告模块  学习及咨询模块接口")
    @GetMapping("/dietEvaluationReportTheoreticalLearning/{lessonId}")
    public AjaxResult dietEvaluationReportTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.dietEvaluationReportTheoreticalLearning(lessonId));
    }


    /**
     * 饮食评价报告模块  饮食评价报告接口
     */
    @ApiOperation("饮食评价报告模块  饮食评价报告接口")
    @GetMapping("/dietEvaluationReport/{lessonId}")
    public AjaxResult dietEvaluationReport(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.dietEvaluationReport(lessonId));
    }


    /**
     * 慢性病饮食干预-脑卒中  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-脑卒中  理论学习接口")
    @GetMapping("/interveneStrokeTheoreticalLearning/{lessonId}")
    public AjaxResult interveneStrokeTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneStrokeTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-脑卒中  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-脑卒中  健康信息收集接口")
    @GetMapping("/healthInformationStrokeCollection/{lessonId}")
    public AjaxResult healthInformationStrokeCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationStrokeCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-脑卒中  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-脑卒中  健康干预接口")
    @GetMapping("/healthStrokeIntervention/{lessonId}")
    public AjaxResult healthStrokeIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthStrokeIntervention(lessonId));
    }


    /**
     * 慢性病饮食干预-糖尿病  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-糖尿病  理论学习接口")
    @GetMapping("/interveneDiabetesTheoreticalLearning/{lessonId}")
    public AjaxResult interveneDiabetesTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneDiabetesTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-糖尿病  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-糖尿病  健康信息收集接口")
    @GetMapping("/healthInformationDiabetesCollection/{lessonId}")
    public AjaxResult healthInformationDiabetesCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationDiabetesCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-糖尿病  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-糖尿病  健康干预接口")
    @GetMapping("/healthDiabetesIntervention/{lessonId}")
    public AjaxResult healthDiabetesIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthDiabetesIntervention(lessonId));
    }


    /**
     * 慢性病饮食干预-肥胖症  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-肥胖症  理论学习接口")
    @GetMapping("/interveneObesityTheoreticalLearning/{lessonId}")
    public AjaxResult interveneObesityTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneObesityTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-肥胖症  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-肥胖症  健康信息收集接口")
    @GetMapping("/healthInformationObesityCollection/{lessonId}")
    public AjaxResult healthInformationObesityCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationObesityCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-肥胖症  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-肥胖症  健康干预接口")
    @GetMapping("/healthObesityIntervention/{lessonId}")
    public AjaxResult healthObesityIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthObesityIntervention(lessonId));
    }


    /**
     * 慢性病饮食干预-慢性阻塞性肺病  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-慢性阻塞性肺病  理论学习接口")
    @GetMapping("/interveneCopdTheoreticalLearning/{lessonId}")
    public AjaxResult interveneCopdTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.interveneCopdTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-慢性阻塞性肺病  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-慢性阻塞性肺病  健康信息收集接口")
    @GetMapping("/healthInformationCopdCollection/{lessonId}")
    public AjaxResult healthInformationCopdCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationCopdCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-慢性阻塞性肺病  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-慢性阻塞性肺病  健康干预接口")
    @GetMapping("/healthCopdIntervention/{lessonId}")
    public AjaxResult healthCopdIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthCopdIntervention(lessonId));
    }



    /**
     * 慢性病饮食干预-骨质疏松  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-骨质疏松  健康信息收集接口")
    @GetMapping("/healthInformationOsteoporosisCollection/{lessonId}")
    public AjaxResult healthInformationOsteoporosisCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationOsteoporosisCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-骨质疏松  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-骨质疏松  健康干预接口")
    @GetMapping("/healthOsteoporosisIntervention/{lessonId}")
    public AjaxResult healthOsteoporosisIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthOsteoporosisIntervention(lessonId));
    }


    /**
     * 慢性病饮食干预-盆底功能障碍  理论学习接口
     */
    @ApiOperation("慢性病饮食干预-盆底功能障碍  理论学习接口")
    @GetMapping("/intervenePelvicFloorTheoreticalLearning/{lessonId}")
    public AjaxResult intervenePelvicFloorTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.intervenePelvicFloorTheoreticalLearning(lessonId));
    }

    /**
     * 慢性病饮食干预-盆底功能障碍  健康信息收集接口
     */
    @ApiOperation("慢性病饮食干预-盆底功能障碍  健康信息收集接口")
    @GetMapping("/healthInformationPelvicFloorCollection/{lessonId}")
    public AjaxResult healthInformationPelvicFloorCollection(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthInformationPelvicFloorCollection(lessonId));
    }

    /**
     * 慢性病饮食干预-盆底功能障碍  健康干预接口
     */
    @ApiOperation("慢性病饮食干预-盆底功能障碍  健康干预接口")
    @GetMapping("/healthPelvicFloorIntervention/{lessonId}")
    public AjaxResult healthPelvicFloorIntervention(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.healthPelvicFloorIntervention(lessonId));
    }

    /**
     * 正常人群食品标签实验 理论学习
     */
    @ApiOperation("正常人群食品标签实验 理论学习")
    @GetMapping("/foodLabelsTheoreticalLearning/{lessonId}")
    public AjaxResult foodLabelsTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.foodLabelsTheoreticalLearning(lessonId));
    }


    /**
     * 正常人群食品标签实验 实训流程
     */
    @ApiOperation("正常人群食品标签实验 实训流程")
    @GetMapping("/foodLabels/{lessonId}")
    public AjaxResult foodLabels(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.foodLabels(lessonId));
    }

    /**
     * 高血压患者食品标签实验 理论学习
     */
    @ApiOperation("高血压患者食品标签实验 理论学习")
    @GetMapping("/hypertensionFoodLabelsTheoreticalLearning/{lessonId}")
    public AjaxResult hypertensionFoodLabelsTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.hypertensionFoodLabelsTheoreticalLearning(lessonId));
    }


    /**
     * 高血压患者食品标签实验 实训流程
     */
    @ApiOperation("高血压患者食品标签实验 实训流程")
    @GetMapping("/hypertensionFoodLabels/{lessonId}")
    public AjaxResult hypertensionFoodLabels(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.hypertensionFoodLabels(lessonId));
    }

    /**
     * 糖尿病患者食品标签实验 理论学习
     */
    @ApiOperation("糖尿病患者食品标签实验 理论学习")
    @GetMapping("/diabetesFoodLabelsTheoreticalLearning/{lessonId}")
    public AjaxResult diabetesFoodLabelsTheoreticalLearning(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.diabetesFoodLabelsTheoreticalLearning(lessonId));
    }


    /**
     * 糖尿病患者食品标签实验 实训流程
     */
    @ApiOperation("糖尿病患者食品标签实验 实训流程")
    @GetMapping("/diabetesFoodLabels/{lessonId}")
    public AjaxResult diabetesFoodLabels(@PathVariable Long lessonId) {
        return AjaxResult.success(ehmsService.diabetesFoodLabels(lessonId));
    }

    /**
     * 慢性病健康干预-获取患者信息
     */
    @ApiOperation("慢性病健康干预-获取患者信息")
    @GetMapping("/interfereHealth/{lessonId}")
    public AjaxResult interfereHealth(@PathVariable Long lessonId) {
        AjaxResult ajax = AjaxResult.success(ehmsService.interfereHealth(lessonId));
        return ajax;
    }

    /**
     * BMI训练-获取学习列表
     */
    @ApiOperation("BMI训练-获取学习列表")
    @GetMapping("/getSportStudy/{lessonId}")
    public AjaxResult getSportStudy(@PathVariable Long lessonId) {
        AjaxResult ajax = AjaxResult.success(ehmsService.getSportStudy(lessonId));
        return ajax;
    }
}
