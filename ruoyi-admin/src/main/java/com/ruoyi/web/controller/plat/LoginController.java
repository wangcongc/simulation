package com.ruoyi.web.controller.plat;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.system.service.IPlatTokenService;
import com.ruoyi.system.service.IPlatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录验证
 */
@RestController
public class LoginController {

    @Autowired
    private IPlatUserService platUserService;
    @Autowired
    private IPlatTokenService platTokenService;

    /**
     * 登录
     */
    @PostMapping("/login")
    public AjaxResult login(LoginBody loginBody) {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = platUserService.login(loginBody.getUsername(), loginBody.getPassword());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }
}
