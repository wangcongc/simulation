package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.LessonFullCalendarVO;
import com.ruoyi.system.domain.vo.LessonHomeVO;
import com.ruoyi.system.domain.vo.LessonScheduleVO;
import com.ruoyi.system.service.IPlatLessonService;
import com.ruoyi.system.service.IPlatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 课程管理
 */
@RestController
@RequestMapping("/lesson")
public class LessonController extends BaseController {

    @Autowired
    private IPlatLessonService platLessonService;
    @Autowired
    private IPlatUserService platUserService;

    /**
     * 添加课程
     */
    @PostMapping("/addLesson")
    public AjaxResult addLesson(PlatLesson platLesson) {
        platLessonService.addLesson(platLesson);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 课程列表
     */
    @GetMapping("/lessonList")
    public PlatTableDataInfo lessonList(PlatLesson platLesson) {
        List<PlatLesson> list = platLessonService.lessonList(platLesson);
        return getPlatDataTable(list);
    }

    /**
     * 课程分页列表
     */
    @GetMapping("/lessonPageList")
    public PlatTableDataInfo lessonPageList(PlatLesson platLesson) {
        startPage();
        List<PlatLesson> list = platLessonService.lessonList(platLesson);
        return getPlatDataTable(list);
    }

    /**
     * 删除课程
     */
    @GetMapping("/delLesson/{id}")
    public AjaxResult delLesson(@PathVariable Long id) {
        platLessonService.delLesson(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 课程数量（首页）
     * 已安排课程数量（首页）
     */
    @GetMapping("/lessonHome")
    public AjaxResult lessonHome(HttpServletRequest request) {
        LessonHomeVO lessonHomeVO = platLessonService.lessonHome(request);
        AjaxResult ajax = AjaxResult.success(lessonHomeVO);
        return ajax;
    }

    /**
     * 我的课程列表(教师)
     */
    @GetMapping("/myLessonList")
    public PlatTableDataInfo myLessonList(PlatLesson platLesson, HttpServletRequest request) {
        PlatUser platUser = platUserService.getLoginUser(request);
        startPage();
        List<PlatLesson> list = platLessonService.myLessonList(platLesson, request,platUser);
        return getPlatDataTable(list);
    }

    /**
     * 我的课程列表(学生)
     */
    @GetMapping("/myStudentLessonList")
    public PlatTableDataInfo myStudentLessonList(PlatLesson platLesson, HttpServletRequest request) {
        platLesson.setStudentStateCountans(0);
        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        startPage();
        List<PlatLessonStudent> list = platLessonService.myStudentLessonList(platLesson, loginUser);
        PlatTableDataInfo platTableDataInfo = getPlatDataTable(list);
        platTableDataInfo.setData(platLessonService.myStudentLessonListData((List<PlatLessonStudent>) platTableDataInfo.getData()));
        return platTableDataInfo;
    }

    /**
     * 我的成绩(学生)
     */
    @GetMapping("/achievementList")
    public PlatTableDataInfo achievementList(PlatLesson platLesson, HttpServletRequest request) {
        platLesson.setStudentStateCountans(1);
        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);
        startPage();
        List<PlatLessonStudent> list = platLessonService.myStudentLessonList(platLesson, loginUser);
        return getPlatDataTable(list);
    }


    /**
     * 我的排课列表
     */
    @GetMapping("/myLessonScheduleList")
    public PlatTableDataInfo myLessonScheduleList(PlatUser platUser, HttpServletRequest request) {
        //当前用户
        PlatUser loginUser = platUserService.getLoginUser(request);

        startPage();
        List<PlatLessonSchedule> list = platLessonService.myLessonScheduleList(platUser, loginUser);

        PlatTableDataInfo platTableDataInfo = getPlatDataTable(list);
        platTableDataInfo = platLessonService.myLessonScheduleListData(platTableDataInfo);
        return platTableDataInfo;
    }

    /**
     * 我的排课列表(首页日程)
     */
    @PostMapping("/myLessonFullCalendarList")
    public AjaxResult addLessonSchedule(HttpServletRequest request) {
        List<LessonFullCalendarVO> list = platLessonService.myLessonFullCalendarList(request);
        AjaxResult ajax = AjaxResult.success(list);
        return ajax;
    }

    /**
     * 排课
     */
    @PostMapping("/addLessonSchedule")
    public AjaxResult addLessonSchedule(PlatLessonSchedule platLessonSchedule, HttpServletRequest request) {
        platLessonService.addLessonSchedule(platLessonSchedule, request);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 删除排课
     */
    @GetMapping("/delLessonSchedule/{id}")
    public AjaxResult delClass(@PathVariable Long id) {
        platLessonService.delLessonSchedule(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 结束排课（学生）
     */
    @GetMapping("/endLesson/{id}")
    public AjaxResult endLesson(@PathVariable Long id) {
        platLessonService.endLesson(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 课程类别
     */
    @GetMapping("/lessonTypeList")
    public PlatTableDataInfo lessonTypeList(PlatLessonType platLessonType) {
        List<PlatLessonType> list = platLessonService.lessonTypeList(platLessonType);
        return getPlatDataTable(list);
    }

    /**
     * 课程类别
     */
    @GetMapping("/lessonTypePageList")
    public PlatTableDataInfo lessonTypePageList(PlatLessonType platLessonType) {
        startPage();
        List<PlatLessonType> list = platLessonService.lessonTypeList(platLessonType);
        return getPlatDataTable(list);
    }

    /**
     * 设置分数
     */
    @PostMapping("/lessonInScore")
    public AjaxResult lessonInScore(PlatLessonStudent platLessonStudent,HttpServletRequest request) {
        platLessonService.lessonInScore(platLessonStudent,request);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }
}
