package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.domain.PlatUser;
import com.ruoyi.system.domain.vo.PlatMenuVO;
import com.ruoyi.system.domain.vo.UserVO;
import com.ruoyi.system.service.IPlatTokenService;
import com.ruoyi.system.service.IPlatUserService;
import com.ruoyi.system.service.impl.PlatCommonServiceImpl;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 登录验证
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private IPlatUserService platUserService;
    @Autowired
    private IPlatTokenService platTokenService;

    /**
     * 获取菜单
     */
    @GetMapping("/menu")
    public AjaxResult menu(HttpServletRequest request) {
        AjaxResult ajax = AjaxResult.success();
        PlatUser loginUser = platTokenService.getLoginUser(request);
        List<PlatMenuVO> list = platUserService.getMenu(loginUser);
        ajax.put(AjaxResult.DATA_TAG, list);
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @param request
     * @return
     */
    @GetMapping("/userInfo")
    public AjaxResult userInfo(HttpServletRequest request) {
        PlatUser loginUser = platUserService.getLoginUser(request);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(loginUser, userVO);
        AjaxResult ajax = AjaxResult.success(userVO);
        return ajax;
    }

    /**
     * 修改个人信息
     *
     * @param request
     * @return
     */
    @PostMapping("/updateUserInfo")
    public AjaxResult updateUserInfo(HttpServletRequest request, PlatUser platUser) {
        platUserService.updateUserInfo(request, platUser);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 修改密码
     *
     * @param request
     * @return
     */
    @PostMapping("/updatePassword")
    public AjaxResult updatePassword(HttpServletRequest request, PlatUser platUser) {
        platUserService.updatePassword(request, platUser);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 教职工列表
     *
     * @param request
     * @return
     */
    @GetMapping("/teacherList")
    public PlatTableDataInfo teacherList(HttpServletRequest request, PlatUser platUser) {
        startPage();
        List<PlatUser> list = platUserService.teacherList(request, platUser);
        return getPlatDataTable(list);
    }

    /**
     * 授课教师列表(班级)
     *
     * @return
     */
    @GetMapping("/lecturerList/{classId}")
    public AjaxResult lecturerList(@PathVariable Long classId) {
        AjaxResult ajax = AjaxResult.success(platUserService.lecturerList(classId));
        return ajax;
    }

    /**
     * 授课教师列表(课程)
     *
     * @return
     */
    @GetMapping("/lecturerLessonList/{lessonId}")
    public AjaxResult lecturerLessonList(@PathVariable Long lessonId) {
        AjaxResult ajax = AjaxResult.success(platUserService.lecturerLessonList(lessonId));
        return ajax;
    }

    /**
     * 添加教师
     */
    @PostMapping("/addTeacher")
    public AjaxResult addTeacher(HttpServletRequest request, PlatUser platUser) {
        platUserService.addTeacher(request, platUser);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 添加学生
     */
    @PostMapping("/addStudent")
    public AjaxResult addStudent(HttpServletRequest request, PlatUser platUser) {
        platUser.setRole(PlatCommonServiceImpl.ROLE_STUDENT);
        platUserService.addTeacher(request, platUser);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 学生列表
     *
     * @param request
     * @return
     */
    @GetMapping("/studentList")
    public PlatTableDataInfo studentList(HttpServletRequest request, PlatUser platUser) {
        startPage();
        List<PlatUser> list = platUserService.studentList(request, platUser);
        return getPlatDataTable(list);
    }

    /**
     * 删除用户
     */
    @GetMapping("/delUser/{id}")
    public AjaxResult delUser(@PathVariable Long id) {
        platUserService.delUser(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 导入学生excel（多个）
     */
    @PostMapping("/importStudent")
    public AjaxResult importStudent(@RequestParam("excelFile") MultipartFile excelFile, HttpServletRequest request) {
        try {
            //转换成输入流
            InputStream is = excelFile.getInputStream();
            //得到excel
            Workbook workbook = Workbook.getWorkbook(is);
            //单元格
            jxl.Cell cell = null;

            String result = platUserService.uploadFileStudent(workbook, cell, request);
            AjaxResult ajax = AjaxResult.success(result);
            return ajax;
        } catch (IOException e) {
            PlatCommonServiceImpl.error("导入失败，请使用下载的模板，重新导入", e);
            return AjaxResult.error(e.getMessage());
        } catch (BiffException e) {
            PlatCommonServiceImpl.error("导入失败，请使用下载的模板，重新导入", e);
            return AjaxResult.error(e.getMessage());
        }
    }
}
