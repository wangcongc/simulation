package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.PlatLessonChapterType;
import com.ruoyi.system.service.IPlatChapterTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 章节类型管理
 */
@RestController
@RequestMapping("/chapterType")
public class ChapterTypeController extends BaseController {

    @Autowired
    private IPlatChapterTypeService platChapterTypeService;

    /**
     * 添加章节类型
     */
    @PostMapping("/addChapterType")
    public AjaxResult addChapterType(PlatLessonChapterType platLessonChapterType) {
        platChapterTypeService.addChapterType(platLessonChapterType);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 章节类型列表
     */
    @GetMapping("/chapterTypeList")
    public PlatTableDataInfo chapterTypeList(PlatLessonChapterType platLessonChapterType) {
        List<PlatLessonChapterType> list = platChapterTypeService.chapterTypeList(platLessonChapterType);
        return getPlatDataTable(list);
    }

    /**
     * 章节类型分页列表
     */
    @GetMapping("/chapterTypePageList/{lessonId}")
    public PlatTableDataInfo chapterTypePageList(@PathVariable Long lessonId, PlatLessonChapterType platLessonChapterType) {
        if (platLessonChapterType.getLessonId() == null || platLessonChapterType.getLessonId() == -1) {
            platLessonChapterType.setLessonId(lessonId == -1 ? null : lessonId);
        }

        startPage();
        List<PlatLessonChapterType> list = platChapterTypeService.chapterTypeList(platLessonChapterType);
        return getPlatDataTable(list);
    }

    /**
     * 删除章节类型
     */
    @GetMapping("/delChapterType/{id}")
    public AjaxResult delChapterType(@PathVariable Long id) {
        platChapterTypeService.delChapterType(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }
}
