package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.PlatCollege;
import com.ruoyi.system.service.IPlatCollegeService;
import com.ruoyi.system.service.IPlatTokenService;
import com.ruoyi.system.service.IPlatUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 学院管理
 */
@RestController
@RequestMapping("/college")
public class CollegeController extends BaseController {

    @Autowired
    private IPlatCollegeService platCollegeService;
    @Autowired
    private IPlatUserService platUserService;
    @Autowired
    private IPlatTokenService platTokenService;

    /**
     * 添加学院
     */
    @PostMapping("/addCollege")
    public AjaxResult addCollege(PlatCollege platCollege) {
        platCollegeService.addCollege(platCollege);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 学院分页列表
     */
    @GetMapping("/collegePageList")
    public PlatTableDataInfo collegePageList(PlatCollege platCollege) {
        startPage();
        List<PlatCollege> list = platCollegeService.collegeList(platCollege);
        return getPlatDataTable(list);
    }

    /**
     * 学院列表
     */
    @GetMapping("/collegeList")
    public PlatTableDataInfo collegeList() {
        List<PlatCollege> list = platCollegeService.collegeList(new PlatCollege());
        return getPlatDataTable(list);
    }

    /**
     * 删除学院
     */
    @GetMapping("/delCollege/{id}")
    public AjaxResult delCollege(@PathVariable Long id) {
        platCollegeService.delCollege(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }
}
