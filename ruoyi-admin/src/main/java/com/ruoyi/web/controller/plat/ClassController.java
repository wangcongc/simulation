package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.PlatClass;
import com.ruoyi.system.service.IPlatClassService;
import com.ruoyi.system.service.IPlatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 班级管理
 */
@RestController
@RequestMapping("/class")
public class ClassController extends BaseController {

    @Autowired
    private IPlatClassService platClassService;
    @Autowired
    private IPlatUserService platUserService;

    /**
     * 添加班级
     */
    @PostMapping("/addClass")
    public AjaxResult addClass(PlatClass platClass) {
        platClassService.addClass(platClass);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 班级列表
     */
    @GetMapping("/classList")
    public PlatTableDataInfo classList(PlatClass platClass) {
        List<PlatClass> list = platClassService.classList(platClass);
        return getPlatDataTable(list);
    }

    /**
     * 我的班级列表（当前教师）
     */
    @GetMapping("/myClassList")
    public PlatTableDataInfo myClassList(PlatClass platClass, HttpServletRequest request) {
        platClass.setTeacher(platUserService.getLoginUser(request).getId() + "");
        List<PlatClass> list = platClassService.classList(platClass);
        return getPlatDataTable(list);
    }

    /**
     * 班级分页列表
     */
    @GetMapping("/classPageList")
    public PlatTableDataInfo classPageList(PlatClass platClass) {
        startPage();
        List<PlatClass> list = platClassService.classList(platClass);
        return getPlatDataTable(list);
    }

    /**
     * 删除班级
     */
    @GetMapping("/delClass/{id}")
    public AjaxResult delClass(@PathVariable Long id) {
        platClassService.delClass(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }
}
