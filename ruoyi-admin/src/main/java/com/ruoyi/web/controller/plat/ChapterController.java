package com.ruoyi.web.controller.plat;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PlatTableDataInfo;
import com.ruoyi.system.domain.PlatLessonChapter;
import com.ruoyi.system.service.IPlatChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 章节管理
 */
@RestController
@RequestMapping("/chapter")
public class ChapterController extends BaseController {

    @Autowired
    private IPlatChapterService platChapterService;

    /**
     * 添加章节
     */
    @PostMapping("/addChapter")
    public AjaxResult addChapter(PlatLessonChapter platLessonChapter) {
        platChapterService.addChapter(platLessonChapter);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }

    /**
     * 章节列表
     */
    @GetMapping("/chapterList")
    public PlatTableDataInfo chapterList(PlatLessonChapter platLessonChapter) {
        List<PlatLessonChapter> list = platChapterService.chapterList(platLessonChapter);
        return getPlatDataTable(list);
    }

    /**
     * 章节分页列表
     */
    @GetMapping("/chapterPageList/{chapterTypeId}")
    public PlatTableDataInfo chapterPageList(@PathVariable Long chapterTypeId, PlatLessonChapter platLessonChapter) {
        if (platLessonChapter.getChapterTypeId() == null || platLessonChapter.getChapterTypeId() == -1) {
            platLessonChapter.setChapterTypeId(chapterTypeId == -1 ? null : chapterTypeId);
        }

        startPage();
        List<PlatLessonChapter> list = platChapterService.chapterList(platLessonChapter);
        return getPlatDataTable(list);
    }

    /**
     * 删除章节
     */
    @GetMapping("/delChapter/{id}")
    public AjaxResult delChapter(@PathVariable Long id) {
        platChapterService.delChapter(id);
        AjaxResult ajax = AjaxResult.success();
        return ajax;
    }
}
